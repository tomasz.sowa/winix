/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "tdb.h"
#include "reply.h"
#include "funthread.h"
#include "createthread.h"
#include "showthreads.h"
#include "core/log.h"
#include "pluginmsg.h"
#include "templates.h"
#include "functions/functions.h"



namespace Winix
{



extern "C" void Init(PluginInfo &);




namespace Thread
{
const wchar_t plugin_name[] = L"thread";

TDb          tdb;
ThreadInfo   thread_info;

Reply        fun_reply;
FunThread    fun_thread;
CreateThread fun_create_thread;
ShowThreads  fun_show_threads;



void AddFunctions(PluginInfo & info)
{
	info.functions->Add(fun_reply);
	info.functions->Add(fun_thread);
	info.functions->Add(fun_create_thread);
	info.functions->Add(fun_show_threads);
}


void SelectDefaultFunction(PluginInfo & info)
{
	if( info.system->mounts.pmount->type == thread_info.mount_type_thread )
	{
		if( info.cur->request->is_item )
			info.cur->request->function = &fun_thread;
		else
			info.cur->request->function = &fun_show_threads;
	}
}


void AddMounts(PluginInfo & info)
{
	Mounts & m = info.system->mounts;
	
	thread_info.mount_type_thread	 = m.AddMountType(L"thread");
	thread_info.mount_par_thread	 = m.AddMountPar(L"thread");
	thread_info.mount_par_thread_dir = m.AddMountPar(L"thread_dir");
}





void RemoveThread(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);
	
	if( item )
	{
		thread_info.RemoveThread(item->id);
		thread_info.RemoveThreadAnswer(item->id);
	}
}




void AddNotifyTemplate(PluginInfo & info)
{
	thread_info.template_index = info.system->notify.AddTemplate(L"notify_email_thread.txt");
}


void SetSortTab(PluginInfo & info)
{
	if( !info.p1 )
	{
		info.log << log1 << "Thread: sort_tab pointer is not set" << logend;
		return;
	}

	typedef std::vector<Item*> SortTab;
	SortTab * psort_tab = reinterpret_cast<SortTab*>(info.p1);

	// !! IMPROVE ME
	// !! dodac jakies czyszczenie tej tablicy wskaznikow na koncu przetwarzania requestu
	// aby kiedys przypadkowo nie odwolac sie do nie istniejacego obiektu
	// poprzez bezposrednie uzycie jakiejs funkcji z ezc
	thread_info.item_sort_tab = *psort_tab;
}


void ReadThreads(PluginInfo & info)
{
	fun_show_threads.ReadThreads();
}


void SetSortTabIndex(PluginInfo & info)
{
	thread_sort_tab(info.l1);
}


void PrepareThread(PluginInfo & info)
{
	fun_thread.PrepareThread(info.l1);
}


void EndRequest(PluginInfo & info)
{
	thread_info.Clear();
}


// !! temporarily
void Rescan(PluginInfo & info)
{
	if( info.config->Bool(L"thread_rescan", false) == true )
	{
		info.log << log1 << "----------  rescanning thread db ------------- " << logend;
		thread_info.Repair();
		info.log << log1 << "----------  thread db rescanning is finished ------------- " << logend;
	}
}

void PluginInit(PluginInfo & info)
{
	Rescan(info);
	info.res = true;
}


} // namespace





void Init(PluginInfo & info)
{
using namespace Thread;

	info.plugin->Assign(WINIX_CREATE_FUNCTIONS,				AddFunctions);
	info.plugin->Assign(WINIX_SELECT_DEFAULT_FUNCTION,		SelectDefaultFunction);
	info.plugin->Assign(WINIX_ADD_MOUNTS,					AddMounts);
	info.plugin->Assign(WINIX_FILE_REMOVED,					RemoveThread);
	info.plugin->Assign(WINIX_NOTIFY_ADD_TEMPLATE,			AddNotifyTemplate);
	info.plugin->Assign(WINIX_TEMPLATES_CREATEFUNCTIONS,	AddEzcFunctions);
	info.plugin->Assign(WINIX_END_REQUEST,					EndRequest);

	// for other plugins
	info.plugin->Assign(WINIX_PL_THREAD_SET_SORTTAB,		SetSortTab);
	info.plugin->Assign(WINIX_PL_THREAD_READ_THREADS,		ReadThreads);
	info.plugin->Assign(WINIX_PL_THREAD_SET_SORTTAB_INDEX,	SetSortTabIndex);
	info.plugin->Assign(WINIX_PL_THREAD_PREPARE_THREAD,		PrepareThread);

	// temporarily
	info.plugin->Assign(WINIX_PLUGIN_INIT, PluginInit);

	tdb.set_dependency(info.functions);

	// thread_info and fun_show_threads are used in 'ticket' plugins too
	thread_info.set_dependency((WinixModelDeprecated*)info.functions);

	thread_info.SetTDb(&tdb);
	thread_info.SetSystem(info.system);
	thread_info.plugin_id = info.plugin_id;

	fun_reply.SetTDb(&tdb);
	fun_reply.SetThreadInfo(&thread_info);

	fun_thread.SetTDb(&tdb);
	fun_thread.SetThreadInfo(&thread_info);

	fun_create_thread.SetTDb(&tdb);
	fun_create_thread.SetThreadInfo(&thread_info);

	fun_show_threads.SetTDb(&tdb);
	fun_show_threads.SetThreadInfo(&thread_info);

	info.p1 = (void*)(plugin_name);
}



} // namespace Winix

