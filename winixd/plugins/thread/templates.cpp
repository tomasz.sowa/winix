/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "threadinfo.h"
#include "reply.h"
#include "funthread.h"
#include "createthread.h"
#include "showthreads.h"
#include "core/request.h"
#include "core/misc.h"
#include "core/plugin.h"
#include "templates/templates.h"
#include "functions/functions.h"

namespace Winix
{



namespace Thread
{
using namespace TemplatesFunctions;
using TemplatesFunctions::system;


extern ThreadInfo   thread_info;
extern Reply        fun_reply;
extern FunThread    fun_thread;
extern CreateThread fun_create_thread;
extern ShowThreads  fun_show_threads;


static size_t item_sort_index;
static HtmlTextStream item_run_content;
static EzcGen ezc_generator;
static bool has_thread;
static size_t thread_index;



// binary search for file_id in thread_tab (thread_tab must be sorted by id)
bool FindThread(const std::vector<Thread> & thread_tab, long file_id, size_t & thread_index)
{
	if( thread_tab.empty() )
		return false;

	size_t o1 = 0;
	size_t o2 = thread_tab.size() - 1;

	if( thread_tab[o1].file_id == file_id )
	{
		thread_index = o1;
		return true;
	}

	if( file_id < thread_tab[o1].file_id )
		return false;

	if( thread_tab[o2].file_id == file_id )
	{
		thread_index = o2;
		return true;
	}

	if( file_id > thread_tab[o2].file_id )
		return false;


	while( o1 + 1 < o2 )
	{
		thread_index = (o1 + o2) / 2;

		if( thread_tab[thread_index].file_id == file_id )
			return true;

		if( thread_tab[thread_index].file_id < file_id )
			o1 = thread_index;
		else
			o2 = thread_index;
	}


return false;
}



bool thread_sort_tab(size_t sort_index)
{
bool res;

	item_sort_index = sort_index;
	res = item_sort_index < thread_info.item_sort_tab.size();
	has_thread = false;

	if( res )
	{
		long file_id = thread_info.item_sort_tab[item_sort_index]->id;
		has_thread = FindThread(thread_info.thread_tab, file_id, thread_index);
	}

return res;
}



void thread_sort_tab(Info & i)
{
	i.res = thread_sort_tab(i.iter);
}


void thread_sort_tab_id(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.out << thread_info.item_sort_tab[item_sort_index]->id;
}


void thread_sort_tab_subject(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.out << thread_info.item_sort_tab[item_sort_index]->subject;
}

void thread_sort_tab_subject_noescape(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.out << R(thread_info.item_sort_tab[item_sort_index]->subject);
}


void thread_sort_tab_content(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	    i.out << thread_info.item_sort_tab[item_sort_index]->item_content.content_raw;
}


void thread_sort_tab_content_noescape(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	    i.out << R(thread_info.item_sort_tab[item_sort_index]->item_content.content_raw);
}


void thread_sort_tab_print_content(Info & i)
{
	if( item_sort_index >= thread_info.item_sort_tab.size() )
		return;
	
	std::wstring & content = thread_info.item_sort_tab[item_sort_index]->item_content.content_raw;
	ItemContent::ContentType type = thread_info.item_sort_tab[item_sort_index]->item_content.content_raw_type;
	
	ItemContent::print_content(i.out, content, type, config->html_filter);
}


void thread_sort_tab_privileges(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.out << "0" << Toa(thread_info.item_sort_tab[item_sort_index]->item_content.privileges, 8);
}



void thread_sort_tab_dir(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		std::wstring path;

		if( system->dirs.MakePath(thread_info.item_sort_tab[item_sort_index]->parent_id, path) )
			i.out << path;
		else
			i.out << "/the path does not exist/";	// !! do konfiga
	}
}


void thread_sort_tab_url(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.out << thread_info.item_sort_tab[item_sort_index]->url;
}


void thread_sort_tab_link(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		doc_proto(i);
		i.out << config->base_url;
		thread_sort_tab_dir(i);
		thread_sort_tab_url(i);
	}
}




void thread_sort_tab_can_read(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		if( system->HasReadAccess(*thread_info.item_sort_tab[item_sort_index]) )
			i.res = true;
	}
}


void thread_sort_tab_can_write(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.res = system->HasWriteAccess(*thread_info.item_sort_tab[item_sort_index]);
}


void thread_sort_tab_user(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		User * puser = system->users.GetUser(thread_info.item_sort_tab[item_sort_index]->item_content.user_id);
		print_user_name(i, puser, thread_info.item_sort_tab[item_sort_index]->item_content.guest_name);
	}
}



void thread_sort_tab_modification_user(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		User * puser = system->users.GetUser(thread_info.item_sort_tab[item_sort_index]->item_content.modification_user_id);
		print_user_name(i, puser, thread_info.item_sort_tab[item_sort_index]->item_content.guest_name);
	}
}



void thread_sort_tab_users_different(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		i.res = (thread_info.item_sort_tab[item_sort_index]->item_content.user_id != thread_info.item_sort_tab[item_sort_index]->item_content.modification_user_id);
	}
}



void thread_sort_tab_group(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		long group_id = thread_info.item_sort_tab[item_sort_index]->item_content.group_id;
		Group * pgroup = system->groups.GetGroup(group_id);
		
		if( pgroup )
			i.out << pgroup->name;
		else
			i.out << group_id;
	}
}


void thread_sort_tab_date_creation(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		pt::Date date = system->ToLocal(thread_info.item_sort_tab[item_sort_index]->item_content.date_creation);
		i.out << DateToStr(date.year, date.month, date.day, date.hour, date.min, date.sec);
	}
}


void thread_sort_tab_date_modification(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		pt::Date date = system->ToLocal(thread_info.item_sort_tab[item_sort_index]->item_content.date_modification);
		i.out << DateToStr(date.year, date.month, date.day, date.hour, date.min, date.sec);
	}
}


void thread_sort_tab_date_creation_nice(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		print_date_nice(i, thread_info.item_sort_tab[item_sort_index]->item_content.date_creation);
	}
}



void thread_sort_tab_date_modification_nice(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		print_date_nice(i, thread_info.item_sort_tab[item_sort_index]->item_content.date_modification);
	}
}


void thread_sort_tab_dates_equal(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		pt::Date & date1 = thread_info.item_sort_tab[item_sort_index]->item_content.date_creation;
		pt::Date & date2 = thread_info.item_sort_tab[item_sort_index]->item_content.date_modification;
		
		i.res = date1 == date2;
	}
}


void thread_sort_tab_run(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		if( system->HasReadExecAccess(*thread_info.item_sort_tab[item_sort_index]) )
		{
			Ezc::Pattern * p = pattern_cacher.GetPattern(*thread_info.item_sort_tab[item_sort_index]);

			item_run_content.clear();
			InitGenerator(ezc_generator, *cur->request);
			ezc_generator.SetPattern(*p);
			ezc_generator.Generate(item_run_content);

			ItemContent::print_content(i.out, item_run_content.get_buffer(), thread_info.item_sort_tab[item_sort_index]->item_content.content_raw_type, config->html_filter);
		}
		else
		{
			i.out << "<!-- run: permission denied -->";
		}
	}
}



void thread_sort_tab_has_static_file(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		i.res = thread_info.item_sort_tab[item_sort_index]->item_content.file_type != WINIX_ITEM_FILETYPE_NONE &&
				!thread_info.item_sort_tab[item_sort_index]->item_content.file_path.empty();
	}
}


void thread_sort_tab_has_thumb(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.res = thread_info.item_sort_tab[item_sort_index]->item_content.file_has_thumb;
}


void thread_sort_tab_type_is_dir(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.res = thread_info.item_sort_tab[item_sort_index]->type == Item::dir;
}


void thread_sort_tab_type_is_file(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.res = thread_info.item_sort_tab[item_sort_index]->type == Item::file;
}


void thread_sort_tab_type_is_symlink(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.res = thread_info.item_sort_tab[item_sort_index]->type == Item::symlink;
}


void thread_sort_tab_is_link_to(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.res = !thread_info.item_sort_tab[item_sort_index]->item_content.link_to.empty();
}


void thread_sort_tab_link_to(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.out << thread_info.item_sort_tab[item_sort_index]->item_content.link_to;
}


void thread_sort_tab_is_link_redirect(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
		i.res = thread_info.item_sort_tab[item_sort_index]->item_content.link_redirect == 1;
}




void thread_sort_tab_has_thread(Info & i)
{
	i.res = has_thread;
}



void thread_sort_tab_replies(Info & i)
{
	if( has_thread && thread_index < thread_info.thread_tab.size() )
		i.out << thread_info.thread_tab[thread_index].replies;
}


void thread_sort_tab_last_item_date_modification_nice(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		if( has_thread && thread_index < thread_info.thread_tab.size() )
		{
			Thread & thread = thread_info.thread_tab[thread_index];

			if( thread.last_item.id != -1 )
				print_date_nice(i, thread.last_item.item_content.date_modification);
			else
				print_date_nice(i, thread_info.item_sort_tab[item_sort_index]->item_content.date_modification);
		}
	}
}


void thread_sort_tab_last_item_user(Info & i)
{
	if( item_sort_index < thread_info.item_sort_tab.size() )
	{
		if( has_thread && thread_index < thread_info.thread_tab.size() )
		{
			Thread & thread = thread_info.thread_tab[thread_index];

			if( thread.last_item.id != -1 )
			{
				User * puser = system->users.GetUser(thread.last_item.item_content.user_id);
				print_user_name(i, puser, thread.last_item.item_content.guest_name);
			}
			else
			{
				User * puser = system->users.GetUser(thread_info.item_sort_tab[item_sort_index]->item_content.user_id);
				print_user_name(i, puser, thread_info.item_sort_tab[item_sort_index]->item_content.guest_name);
			}
		}
	}
}


void thread_can_reply(Info & i)
{
	i.res = fun_reply.has_access();
}



void thread_can_create(Info & i)
{
	i.res = fun_create_thread.has_access();
}



void thread_mount_arg_is(Info & i)
{
	if( system->mounts.pmount )
		i.res = system->mounts.pmount->IsArg(thread_info.mount_par_thread, i.par);
}





void AddEzcFunctions(PluginInfo & info)
{
	using TemplatesFunctions::EzcFun;
	EzcFun * fun = reinterpret_cast<EzcFun*>(info.p1);

	fun->Insert("thread_sort_tab",                     thread_sort_tab);
	fun->Insert("thread_sort_tab_id",                  thread_sort_tab_id);
	fun->Insert("thread_sort_tab_subject",             thread_sort_tab_subject);
	fun->Insert("thread_sort_tab_subject_noescape",    thread_sort_tab_subject_noescape);
	fun->Insert("thread_sort_tab_content",             thread_sort_tab_content);
	fun->Insert("thread_sort_tab_content_noescape",    thread_sort_tab_content_noescape);
	fun->Insert("thread_sort_tab_print_content",       thread_sort_tab_print_content);
	fun->Insert("thread_sort_tab_privileges",          thread_sort_tab_privileges);
	fun->Insert("thread_sort_tab_dir",                 thread_sort_tab_dir);
	fun->Insert("thread_sort_tab_url",                 thread_sort_tab_url);
	fun->Insert("thread_sort_tab_link",                thread_sort_tab_link);
	fun->Insert("thread_sort_tab_can_read",            thread_sort_tab_can_read);
	fun->Insert("thread_sort_tab_can_write",           thread_sort_tab_can_write);
	fun->Insert("thread_sort_tab_user",                thread_sort_tab_user);
	fun->Insert("thread_sort_tab_modification_user",   thread_sort_tab_modification_user);
	fun->Insert("thread_sort_tab_users_different",     thread_sort_tab_users_different);
	fun->Insert("thread_sort_tab_group",               thread_sort_tab_group);
	fun->Insert("thread_sort_tab_date_creation",       thread_sort_tab_date_creation);
	fun->Insert("thread_sort_tab_date_modification",   thread_sort_tab_date_modification);
	fun->Insert("thread_sort_tab_date_creation_nice",  thread_sort_tab_date_creation_nice);
	fun->Insert("thread_sort_tab_date_modification_nice", thread_sort_tab_date_modification_nice);
	fun->Insert("thread_sort_tab_dates_equal",         thread_sort_tab_dates_equal);
	fun->Insert("thread_sort_tab_run",                 thread_sort_tab_run);
	fun->Insert("thread_sort_tab_has_static_file",     thread_sort_tab_has_static_file);
	fun->Insert("thread_sort_tab_has_thumb",           thread_sort_tab_has_thumb);
	fun->Insert("thread_sort_tab_type_is_dir",         thread_sort_tab_type_is_dir);
	fun->Insert("thread_sort_tab_type_is_file",        thread_sort_tab_type_is_file);
	fun->Insert("thread_sort_tab_type_is_symlink",     thread_sort_tab_type_is_symlink);
	fun->Insert("thread_sort_tab_is_link_to",          thread_sort_tab_is_link_to);
	fun->Insert("thread_sort_tab_link_to",             thread_sort_tab_link_to);
	fun->Insert("thread_sort_tab_is_link_redirect",    thread_sort_tab_is_link_redirect);

	fun->Insert("thread_sort_tab_has_thread",          thread_sort_tab_has_thread);
	fun->Insert("thread_sort_tab_replies",             thread_sort_tab_replies);
	fun->Insert("thread_sort_tab_last_item_date_modification_nice", thread_sort_tab_last_item_date_modification_nice);
	fun->Insert("thread_sort_tab_last_item_user",      thread_sort_tab_last_item_user);

	fun->Insert("thread_can_reply",                    thread_can_reply);
	fun->Insert("thread_can_create",                   thread_can_create);
	fun->Insert("thread_mount_arg_is",                 thread_mount_arg_is);

}


} // namespace



} // namespace Winix



