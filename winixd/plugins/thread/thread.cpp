/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "thread.h"
#include "finder.h"


namespace Winix
{

namespace Thread
{


Thread Thread::get_thread(morm::ModelConnector * model_connector, long file_id)
{
	morm::Finder<Thread> finder(model_connector);

	Thread thread = finder.
		select().
		where().
		eq(L"file_id", file_id).
		get();

	return thread;
}


std::vector<Thread> Thread::get_threads(morm::ModelConnector * model_connector, const std::vector<long> & file_id_tab)
{
	morm::Finder<Thread> finder(model_connector);
	std::vector<Thread> threads;

	if( !file_id_tab.empty() )
	{
		threads = finder.
			select().
			where().
			in(L"file_id", file_id_tab).
			raw("order by file_id asc").
			get_vector();
	}

	return threads;
}




bool Thread::do_migration(int & current_table_version)
{
	bool ok = true;

	ok = ok && morm::Model::do_migration(current_table_version, 1, this, &Thread::do_migration_to_1);

	return ok;
}


bool Thread::do_migration_to_1()
{
	const char * str = R"sql(
		CREATE TABLE plugins.thread (
			file_id bigint NOT NULL,
			replies integer,
			last_item bigint,
			closed smallint
		);
	)sql";

	db_query(str);
	return true;
}


//bool Thread::do_migration_to_2()
//{
//	const char * str = R"sql(
//
//	)sql";
//
//	db_query(str);
//	return true;
//}



} // namespace

} // namespace Winix

