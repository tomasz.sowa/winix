/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_thread_tdb
#define headerfile_winix_plugins_thread_tdb

#include <vector>
#include "core/winixmodeldeprecated.h"
#include "thread.h"
#include "threadfiles.h"
#include "core/error.h"


namespace Winix::Thread
{


class TDb : public WinixModelDeprecated
{
public:

	void GetAnswers(long file_id, std::vector<ThreadFiles> & answer_id_tab);
	bool AddAnswer(long file_id, long answer_id);

	bool RemoveAnswer(long answer_id);
	bool RemoveThread(long file_id);

	// looking for the last answer in a thread (in 'thread_files' table)
	// this is not the value from 'thread' table
	long FindLastAnswer(long file_id);

	// returning all threads
	// IMPROVEME need to be refactored
	void GetAllThreads(std::vector<Thread> & threads);

	// removing only the answer in 'thread_files' table without updating 'thread' table
	bool RemoveAnswerOnly(long answer_id);

	// returning how many answers there are
	// calculating from 'thread_files' table
	// this is not the value from 'thread' table
	long CalcAnswers(long file_id);

	// recalculating last_item, replies in 'thread' table on a given thread
	Error RecalcThread(long file_id);


private:

	std::wstring list_id;
	Thread thread_temp;
	std::vector<long> file_id_tab;

	bool do_query(pt::TextStream & query);

	bool RemoveAnswerRecalcLast(long file_id);
	bool RemoveAnswerRecalcLast(const std::vector<long> & file_id_tab);

};



}

#endif

