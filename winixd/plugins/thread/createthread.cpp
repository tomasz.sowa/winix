/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "createthread.h"
#include "functions.h"
#include "functions/functions.h"

namespace Winix
{



namespace Thread
{


CreateThread::CreateThread()
{
	fun.url = L"createthread";
}



void CreateThread::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}

void CreateThread::SetThreadInfo(ThreadInfo * pthread_info)
{
	thread_info = pthread_info;
}




// returning true if we can create a thread in the current directory
bool CreateThread::has_access()
{
	if( cur->request->is_item || !system->HasWriteAccess(*cur->request->dir_tab.back()) )
		return false;

	if( system->mounts.pmount->type != thread_info->mount_type_thread )
		return false;
	
return true;
}


void CreateThread::SendNotify(const Item & item)
{
	// sending notification
	notify_msg.code           = WINIX_NOTIFY_CODE_ADD;
	notify_msg.template_index = thread_info->template_index;
	notify_msg.dir_link       = config->url_proto;
	notify_msg.dir_link      += config->base_url;// !! IMPROVE ME what about subdomains?
	system->dirs.MakePath(item.parent_id, notify_msg.dir_link, false);
	notify_msg.item_link      = notify_msg.dir_link;
	notify_msg.item_link     += item.url;

	system->notify.ItemChanged(notify_msg);
}


void CreateThread::make_post()
{
	// is it a correct bahavior to set spam_score in session?
	// after the first mistake the person will not be allowed to post anything
	cur->session->spam_score = 0;

	cur->request->item.Clear();
	functions->ReadItem(cur->request->item, Item::file);
	functions->SetUser(cur->request->item);
	functions->PrepareUrl(cur->request->item);
	cur->request->item.parent_id  = cur->request->dir_tab.back()->id;
	cur->request->item.item_content.privileges = system->NewFilePrivileges();

	/*
	 * temporary
	 * html templates are using last_item, and if CheckAbuse() returned true
	 * then the form would be empty
	 */
	cur->request->last_item = &cur->request->item;

	if( functions->CheckAbuse() )
		return;

	thread.set_connector(model_connector);
	thread.clear();

	cur->request->status = system->AddFile(cur->request->item) ? WINIX_ERR_OK : WINIX_ERR_PERMISSION_DENIED;
	
	if( cur->request->status == WINIX_ERR_OK )
	{
		thread.file_id = cur->request->item.id;
		thread.last_item.id = cur->request->item.id;
		thread.set_has_primary_key_set(true);

		if( !thread.insert() )
			cur->request->status = WINIX_ERR_PERMISSION_DENIED;
	}

	if( cur->request->status == WINIX_ERR_OK )
	{
		log << log2 << "CreateThread: added a new thread" << logend;
		thread_info->MakeRedirectIfPossible(cur->request->item);
		SendNotify(cur->request->item);
	}
	else
	{
		log << log1 << "CreateThread: problem with adding a new thread, error code: "
			<< cur->request->status << logend;
	}
}


void CreateThread::MakeGet()
{
	/*
	 * workaround:
	 * html form showed the title from the last directory
	 * this will be removed when new ezc object templates will be implemented
	 */
	empty_item.Clear();
	cur->request->last_item = &empty_item;

}






} // namespace

} // namespace Winix

