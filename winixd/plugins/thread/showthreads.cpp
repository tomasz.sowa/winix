/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <algorithm>
#include "showthreads.h"
#include "functions.h"

namespace Winix
{



namespace Thread
{

ShowThreads::ShowThreads()
{
	fun.url = L"showthreads";
}


void ShowThreads::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}



void ShowThreads::SetThreadInfo(ThreadInfo * pthread_info)
{
	thread_info = pthread_info;
}




bool ShowThreads::has_access()
{
	if( cur->request->is_item )
		return false;

	if( system->mounts.pmount->type != thread_info->mount_type_thread )
		return false;

return true;
}




bool ShowThreads::Sort::operator()(const Item * item1, const Item * item2)
{
	if( sort_type == 0 )
	{
		// sorting by url
		return item1->url < item2->url;
	}
	else
	{
		// sorting by date
		return item1->item_content.date_creation > item2->item_content.date_creation;
	}
}




void ShowThreads::ReadFiles()
{
	morm::Finder<Item> finder(model_connector);

	thread_info->item_tab = finder.
			select().
			where().
			eq(L"parent_id", cur->request->dir_tab.back()->id).
			eq(L"type", static_cast<int>(Item::file)).
			eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_NONE).
			get_vector();

	system->CheckAccessToItems(thread_info->item_tab);
}



void ShowThreads::CreatePointers()
{
	// creating a pointers table
	thread_info->item_sort_tab.resize(thread_info->item_tab.size());

	for(size_t i=0 ; i<thread_info->item_tab.size() ; ++i)
		thread_info->item_sort_tab[i] = &thread_info->item_tab[i];
}


void ShowThreads::SortPointers()
{
	int sort_type = 1;

	if( cur->request->ParamValue(L"sort") == L"url" )
		sort_type = 0;

	std::vector<Item*> & table = thread_info->item_sort_tab;
	std::sort(table.begin(), table.end(), Sort(this, sort_type));
}



void ShowThreads::ReadThreads()
{
	// reading threads for the files
	file_id_tab.resize(thread_info->item_sort_tab.size());

	for(size_t i=0 ; i<thread_info->item_sort_tab.size() ; ++i)
		file_id_tab[i] = thread_info->item_sort_tab[i]->id;

	thread_info->thread_tab = Thread::get_threads(model_connector, file_id_tab);
}



void ShowThreads::MakeGet()
{
	thread_info->Clear();

	ReadFiles();
	CreatePointers();
	SortPointers();
	ReadThreads();
}





} // namespace

} // namespace Winix

