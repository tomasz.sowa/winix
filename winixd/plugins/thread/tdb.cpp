/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "tdb.h"


namespace Winix::Thread
{


void TDb::GetAnswers(long file_id, std::vector<ThreadFiles> & answer_id_tab)
{
	morm::Finder<ThreadFiles> finder(model_connector);

	finder.
		select().
		where().
		eq(L"file_id", file_id).
		get_vector(answer_id_tab);
}


bool TDb::AddAnswer(long file_id, long answer_id)
{
	bool status = false;

	ThreadFiles tf;
	tf.set_connector(model_connector);
	tf.clear();
	tf.file_id = file_id;
	tf.answer_id = answer_id;

	if( tf.insert(false) )
	{
		pt::TextStream query;
		query << "UPDATE plugins.thread SET (last_item, replies) = ("
			  << answer_id
			  << ", replies+1) WHERE file_id="
			  << file_id
			  << ";";

		status = do_query(query);
	}

	return status;
}



long TDb::FindLastAnswer(long file_id)
{
	morm::Finder<ThreadFiles> finder(model_connector);
	long last_item_id = -1;

	ThreadFiles tf = finder.
		select().
		raw("LEFT JOIN core.item ON item.id=thread_files.answer_id").
		raw("LEFT JOIN core.content ON item.content_id=content.id").
		where().
		eq(L"file_id", file_id).
		raw("ORDER BY content.date_creation DESC").
		raw("LIMIT 1;").
		get();

	if( tf.found() )
		last_item_id = tf.answer_id;

	return last_item_id;
}


bool TDb::RemoveAnswer(long answer_id)
{
	bool status = false;
	morm::Finder<ThreadFiles> finder(model_connector);

	std::vector<ThreadFiles> thread_files;
	finder.select().where().eq(L"answer_id", answer_id).get_vector(thread_files);

	file_id_tab.resize(thread_files.size());

	for(size_t i=0 ; i<thread_files.size() ; ++i)
	{
		file_id_tab[i] = thread_files[i].file_id;
	}

	pt::TextStream query;
	query << "DELETE FROM plugins.thread_files WHERE answer_id = " << answer_id << ";";

	if( do_query(query) )
	{
		status = RemoveAnswerRecalcLast(file_id_tab);
	}

	return status;
}


bool TDb::RemoveAnswerRecalcLast(long file_id)
{
	long last_item_id = FindLastAnswer(file_id);

	pt::TextStream query;
	query << "UPDATE plugins.thread SET (replies, last_item) = (replies-1,"
		  << last_item_id
		  << ") WHERE file_id="
		  << file_id
		  << ";";
	
	return do_query(query);
}


bool TDb::RemoveAnswerRecalcLast(const std::vector<long> & file_id_tab)
{
	bool status = true;

	for(size_t i=0 ; status && i < file_id_tab.size() ; ++i)
	{
		long file_id = file_id_tab[i];
		status = RemoveAnswerRecalcLast(file_id);
	}

	return status;
}


Error TDb::RecalcThread(long file_id)
{
	long replies   = CalcAnswers(file_id);
	long last_item = FindLastAnswer(file_id);

	pt::TextStream query;
	query << "UPDATE plugins.thread SET (replies, last_item) = ("
		  << replies
		  << last_item
		  << ") WHERE file_id="
		  << file_id
		  << ";";
	
	return do_query(query);
}


long TDb::CalcAnswers(long file_id)
{
	morm::Finder<ThreadFiles> finder(model_connector);
	long answers = 0;

	ThreadFiles tf = finder.
		select(morm::Select::no_auto_generated_columns).
		raw("SELECT COUNT(file_id) as file_id FROM plugins.thread_files").
		where().
		eq(L"file_id", file_id).
		get();

	if( tf.found() )
		answers = tf.file_id;

	return answers;
}


bool TDb::RemoveThread(long file_id)
{
	bool status = false;

	pt::TextStream query;
	query << "DELETE FROM plugins.thread WHERE file_id=" << file_id;

	if( do_query(query) )
	{
		query.clear();
		query << "DELETE FROM plugins.thread_files WHERE file_id=" << file_id;

		status = do_query(query);
	}

	return status;
}


bool TDb::RemoveAnswerOnly(long answer_id)
{
	pt::TextStream query;
	query << "DELETE FROM plugins.thread_files WHERE answer_id=" << answer_id;

	return do_query(query);
}


// this method reads all threads, need to be refactored
void TDb::GetAllThreads(std::vector<Thread> & threads)
{
	morm::Finder<Thread> finder(model_connector);
	finder.select().get_vector(threads);
}


bool TDb::do_query(pt::TextStream & query)
{
	if( model_connector)
	{
		morm::DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			return db_connector->query(query);
		}
	}

	return false;
}



}

