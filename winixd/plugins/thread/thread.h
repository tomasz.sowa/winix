/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2009-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_thread_thread
#define headerfile_winix_plugins_thread_thread

#include <string>
#include <string.h>
#include "model.h"
#include "date/date.h"
#include "models/item.h"


namespace Winix
{

namespace Thread
{


class Thread : public morm::Model
{
public:

	long file_id;
	long replies;
	short int closed;
	Winix::Item last_item;



	Thread()
	{
	}


	void fields()
	{
		field(L"file_id",	file_id, morm::FT::primary_key);
		field(L"replies", 	replies);
		field(L"closed", 	closed);
		field(L"last_item",	last_item, morm::FT::no_insertable | morm::FT::no_updatable | morm::FT::no_removable | morm::FT::foreign_key);
	}


	void table()
	{
		table_name(L"plugins", L"thread");
	}


	void after_insert()
	{
	}


	bool do_migration(int & current_table_version);

	static Thread get_thread(morm::ModelConnector * model_connector, long file_id);
	static std::vector<Thread> get_threads(morm::ModelConnector * model_connector, const std::vector<long> & file_id_tab);



protected:

	bool do_migration_to_1();
	//bool do_migration_to_2();


};



} // namespace

} // namespace Winix

#endif


