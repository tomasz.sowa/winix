/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_thread_threadinfo
#define headerfile_winix_plugins_thread_threadinfo

#include <vector>
#include "core/system.h"
#include "thread.h"
#include "tdb.h"
#include "core/winixmodeldeprecated.h"
#include "models/item.h"



namespace Winix::Thread
{


class ThreadInfo : public WinixModelDeprecated
{
public:


	void SetTDb(TDb * ptdb);
	void SetSystem(System * psystem);

	void Clear();
	Item * FindThreadDir();


	// id of a mount type
	int mount_type_thread;

	// enable a thread in a specific directory
	// and set sort type (asc, desc)
	int mount_par_thread;

	// a special directory in which we store the answers
	int mount_par_thread_dir;

	// id of the current plugin
	int plugin_id;

	// files in the current directory
	// used for 'show_threads' or 'thread' function
	std::vector<Item> item_tab;

	// additional info to the above files if 'show_threads' is used
	// sorted by file_id
	std::vector<Thread> thread_tab;

	// for sorting
	std::vector<Item*> item_sort_tab;

	// template index for notifications
	size_t template_index;


	void RemoveThread(long file_id);
	void RemoveThreadAnswer(long answer_id);

	// repairing the database
	void Repair();

	void MakeRedirectIfPossible(const Item & item, const wchar_t * postfix = nullptr);

private:

	TDb * tdb;
	System * system;

	std::vector<Item*> out_dir_tab;
	std::vector<ThreadFiles> remove_answers_tab;
	std::vector<ThreadFiles> repair_answer_tab;
	std::vector<Thread> repair_threads;
	Item out_item, repair_item;
	//DbItemQuery iq;

	void RepairAnswer(long answer_id);
	void RepairAnswers(long file_id);
	void Repair(long file_id);

	Item GetItemById(long id);

};


}

#endif


