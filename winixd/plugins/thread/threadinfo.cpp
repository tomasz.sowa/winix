/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "threadinfo.h"
#include "pluginmsg.h"


namespace Winix
{



namespace Thread
{


void ThreadInfo::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}


void ThreadInfo::SetSystem(System * psystem)
{
	system = psystem;
}


void ThreadInfo::Clear()
{
	item_tab.clear();
	thread_tab.clear();
	item_sort_tab.clear();
}



Item * ThreadInfo::FindThreadDir()
{
	Item * plugin_dir = 0;
	plugin->Call(WINIX_PL_THREAD_SEARCH_THREAD_DIR, &plugin_dir);

	if( plugin_dir )
		return plugin_dir;

	const std::wstring & dir_str = system->mounts.pmount->FirstArg(mount_par_thread_dir);

	if( dir_str.empty() )
		return 0;

	int res = system->FollowAllLinks(dir_str, out_dir_tab, out_item);

	if( res != 0 )
		return 0;

return out_dir_tab.back();
}


Item ThreadInfo::GetItemById(long id)
{
	morm::Finder<Item> finder(model_connector);
	return finder.select().where().eq(L"id", id).get();
}


void ThreadInfo::RemoveThread(long file_id)
{
	tdb->GetAnswers(file_id, remove_answers_tab);

	for(size_t i=0 ; i < remove_answers_tab.size() ; ++i)
	{
		Item item = GetItemById(remove_answers_tab[i].answer_id);

		if( item.found() )
		{
			item.remove();
		}
	}

	tdb->RemoveThread(file_id);
}


void ThreadInfo::RemoveThreadAnswer(long answer_id)
{
	tdb->RemoveAnswer(answer_id);
}



void ThreadInfo::RepairAnswer(long answer_id)
{
//	iq.Clear();
//	iq.SetAll(false, false);
//	iq.WhereId(answer_id);
//	iq.WhereType(Item::file); // !! moze w przyszlosci pozwolic takze na symlinki?

	morm::Finder<Item> finder(model_connector);

	repair_item = finder.
			select().
			where().
			eq(L"id", answer_id).
			eq(L"type", static_cast<int>(Item::file)).
			get();

	//if( db->GetItem(repair_item, iq) == WINIX_ERR_NO_ITEM )
	if( !repair_item.found() )
	{
		log << log3 << "ThreadInfo: there is no a file for thread answer_id: " << answer_id << logend;
		tdb->RemoveAnswerOnly(answer_id);
	}
}



void ThreadInfo::RepairAnswers(long file_id)
{
	tdb->GetAnswers(file_id, repair_answer_tab);

	for(size_t i=0 ; i<repair_answer_tab.size() ; ++i)
		RepairAnswer(repair_answer_tab[i].answer_id);

	repair_answer_tab.clear();
}


void ThreadInfo::Repair(long file_id)
{
//	iq.Clear();
//	iq.SetAll(false, false);
//	iq.WhereId(file_id);
//	iq.WhereType(Item::file); // !! moze w przyszlosci pozwolic takze na symlinki?

	morm::Finder<Item> finder(model_connector);

	repair_item = finder.
			select().
			where().
			eq(L"id", file_id).
			eq(L"type", static_cast<int>(Item::file)).
			get();

	if( !repair_item.found() )
	{
		log << log3 << "ThreadInfo: there is no a file for thread file_id: " << file_id << logend;
		tdb->RemoveThread(file_id);
	}
	else
	{
		RepairAnswers(file_id);
		tdb->RecalcThread(file_id);
	}
}


void ThreadInfo::Repair()
{
	tdb->GetAllThreads(repair_threads);

	for(size_t i=0 ; i<repair_threads.size() ; ++i)
		Repair(repair_threads[i].file_id);

	repair_threads.clear();
}



void ThreadInfo::MakeRedirectIfPossible(const Item & item, const wchar_t * postfix)
{
	PluginRes res = plugin->Call(WINIX_PL_THREAD_CAN_MAKE_REDIRECT);

	if( res.res_false > 0 )
	{
		log << log4 << "Thread: redirect prevented" << logend;
		return;
	}

	system->RedirectTo(item, postfix);
}



} // namespace



} // namespace Winix




