/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "reply.h"
#include "functions/functions.h"
#include "core/misc.h"
#include "pluginmsg.h"

namespace Winix
{



namespace Thread
{

Reply::Reply()
{
	fun.url = L"reply";
	files_dir = 0;
}



void Reply::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}


void Reply::SetThreadInfo(ThreadInfo * pthread_info)
{
	thread_info = pthread_info;
}


/*
	we can use 'reply' function everywhere where 'thread_dir' mount option is defined
	if there is no such a thread in 'thread' table it will be added automatically
*/
bool Reply::has_access()
{
	if( !cur->request->is_item )
		return false;

	files_dir = thread_info->FindThreadDir();

	if( !files_dir || !system->HasWriteAccess(*files_dir) )
		return false;


	pt::Space * thread_space = cur->request->item.item_content.admin_meta.get_space(L"thread");

	if( thread_space )
	{
		if( thread_space->to_bool(L"closed", false) )
			return false;
	}

return true;
}


void Reply::SendNotify(const Item & item)
{
	// sending notification
	notify_msg.code           = WINIX_NOTIFY_CODE_REPLY;
	notify_msg.template_index = thread_info->template_index;
	notify_msg.dir_link       = config->url_proto;
	notify_msg.dir_link      += config->base_url;// !! IMPROVE ME what about subdomains?
	system->dirs.MakePath(item.parent_id, notify_msg.dir_link, false);
	notify_msg.item_link      = notify_msg.dir_link;
	notify_msg.item_link     += item.url;

	system->notify.ItemChanged(notify_msg);
}


void Reply::make_post()
{
	// !! jak bedzie dostepne assert
	// ASSERT(files_dir)

	// is it a correct bahavior to set spam_score in session?
	// after the first mistake the person will not be allowed to post anything
	cur->session->spam_score = 0;

	thread = Thread::get_thread(model_connector, cur->request->item.id);

	if( !thread.found() )
	{
		thread.clear();
		thread.file_id = cur->request->item.id;
		thread.insert();
	}

	answer.Clear();
	answer.url = cur->request->item.url;
	answer.url += L"_msg_";
	answer.url += Toa(thread.replies + 1);
	answer.subject = answer.url;
	functions->ReadItem(answer, Item::file);
	functions->SetUser(answer);
	functions->PrepareUrl(answer);
	answer.parent_id  = files_dir->id;
	answer.item_content.privileges = system->NewFilePrivileges();

	/*
	 * temporary
	 * html templates are using last_item, and if CheckAbuse() returned true
	 * then the form would be empty
	 *
	 * here would be better to have something like last_item pointing to answer
	 * so when CheckAbuse() returned true thew we would show provieded values in the html form
	 */
	cur->request->last_item = &cur->request->item;
	cur->request->item.item_content.guest_name.clear();

	if( functions->CheckAbuse() )
	{
		// temporary: to show values in the html form
		// this will be changed when a new ezc objects will be implemented
		// there'll be two objects: one registered as [item..] and the other registered as [answer..]
		// (one for url and the other for content)
		cur->request->item.item_content.content_raw = answer.item_content.content_raw;
		cur->request->item.item_content.content_raw_type = answer.item_content.content_raw_type;
		cur->request->item.item_content.guest_name = answer.item_content.guest_name;

		return;
	}

	plugin->Call(WINIX_PL_THREAD_PREPARE_TO_REPLY_IN_THREAD, &answer);

	cur->request->status = system->AddFile(answer) ? WINIX_ERR_OK : WINIX_ERR_PERMISSION_DENIED;

	if( cur->request->status == WINIX_ERR_OK )
		cur->request->status = tdb->AddAnswer(cur->request->item.id, answer.id);


	if( cur->request->status == WINIX_ERR_OK )
	{
		log << log2 << "Reply: added an answer in a thread" << logend;

		const wchar_t * postfix = nullptr;

		if( cur->request->IsParam(L"scrollup") )
			postfix = L"/-/scrollup";

		if( cur->request->IsParam(L"scrolldown") )
			postfix = L"/-/scrolldown";

		thread_info->MakeRedirectIfPossible(cur->request->item, postfix);

		SendNotify(cur->request->item);
	}
	else
	{
		log << log1 << "Reply: problem with adding an answer, error code: "
			<< cur->request->status << logend;
	}
}


void Reply::MakeGet()
{
	/*
	 * workaround:
	 * html form showed the nick name from item (and content)
	 * this will be removed when new ezc object templates will be implemented
	 * we can provide two item templates: one for [item_url] and the rest for the html form
	 * [item_url] is used in <form...>
	 */
	cur->request->item.item_content.guest_name.clear();
	cur->request->item.item_content.content_raw.clear();

}



} // namespace

} // namespace Winix

