/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <algorithm>
#include "core/misc.h"
#include "funthread.h"

namespace Winix
{


namespace Thread
{

FunThread::FunThread()
{
	fun.url = L"thread";
}


void FunThread::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}

void FunThread::SetThreadInfo(ThreadInfo * pthread_info)
{
	thread_info = pthread_info;
}




bool FunThread::has_access()
{
	if( !cur->request->is_item )
		return false;

	if( system->mounts.pmount->type != thread_info->mount_type_thread )
		return false;

return true;
}




void FunThread::PrepareThread(long file_id)
{
	thread_info->Clear();
	//cur->request->status = tdb->GetAnswers(file_id, id_tab);

	morm::Finder<ThreadFiles> finder_answers(model_connector);

	std::vector<ThreadFiles> answers = finder_answers.
		select().
		where().
		eq(L"file_id", file_id).
		get_vector();

	id_tab.resize(answers.size());

	for(size_t i = 0 ; i < answers.size() ; ++i)
	{
		id_tab[i] = answers[i].answer_id;
	}

	if( !id_tab.empty() )
	{
		morm::Finder<Item> finder(model_connector);

		finder.
			select().
			where().
			in(L"id", id_tab).
			eq(L"type", static_cast<int>(Item::file)).
			eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_NONE);

		if( system->mounts.pmount->IsArg(thread_info->mount_par_thread, L"sort_desc") )
			finder.raw("order by content.date_creation desc");
		else
			finder.raw("order by content.date_creation asc");

		thread_info->item_tab = finder.get_vector();

		//db->GetItems(thread_info->item_tab, iq);
		system->CheckAccessToItems(thread_info->item_tab);

		thread_info->item_sort_tab.resize(thread_info->item_tab.size());

		// thread_info.item_tab is already sorted (by the database)
		for(size_t i=0 ; i<thread_info->item_tab.size() ; ++i)
			thread_info->item_sort_tab[i] = &thread_info->item_tab[i];
	}
}


void FunThread::MakeGet()
{
	PrepareThread(cur->request->item.id);
}



} // namespace


} // namespace Winix

