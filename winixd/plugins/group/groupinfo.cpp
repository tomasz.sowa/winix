/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "groupinfo.h"
#include "core/log.h"
#include "core/system.h"

namespace Winix
{


namespace GroupItem
{


GroupInfo::GroupInfo()
{
	mount_par_group_conf		= -1;
}


void GroupInfo::SetSystem(System * psystem)
{
	system = psystem;
}


void GroupInfo::SetConfig(Config * pconfig)
{
	config = pconfig;
}


Groups * GroupInfo::FindGroups(long dir_id)
{
	GroupsWrap::iterator i = groups_wrap.find(dir_id);

	if( i == groups_wrap.end() )
		return 0;

	return &(i->second.groups);
}



bool GroupInfo::ParseGroups(const std::wstring & str, Groups & groups)
{
	groups.Clear();
	pt::Space & space = *groups.GetSpace();

	if( conf_parser.parse_space(str, space) == pt::SpaceParser::ok )
	{
		groups.Reindex();
	}
	else
	{
		//slog << logerror << "Syntax error in line: " << conf_parser.get_last_parsed_line() << logend;
		log << log1 << "Syntax error in line: " << conf_parser.get_last_parsed_line() << logend;
		groups.Clear();
	}

return conf_parser.status == pt::SpaceParser::ok;
}


void GroupInfo::MarkAllGroupsToDelete()
{
	GroupsWrap::iterator i = groups_wrap.begin();

	for( ; i != groups_wrap.end() ; ++i)
		i->second.to_delete = true;
}




void GroupInfo::DeleteAllMarkedGroups()
{
	GroupsWrap::iterator inext;
	GroupsWrap::iterator i = groups_wrap.begin();

	while( i != groups_wrap.end() )
	{
		inext = i;
		++inext;

		if( i->second.to_delete )
		{
			log << log3 << "GroupItem: deleting group for dir id: " << i->first << logend;
			groups_wrap.erase(i);
		}

		i = inext;
	}
}



bool GroupInfo::GetConfContent(const std::wstring & path)
{
	int status = system->FollowAllLinks(path, config_dir_tab, config_file, false, false, false);

	if( status != 1 )
	{
		log << log1 << "GroupItem: problem with reading a config file: " << path << ", status: " << status << logend;
		return false;
	}

return true;
}




// if skip_existing_configs is true then only new config files will be parsed
void GroupInfo::ReadGroupsConf(Mounts & mounts, bool skip_existing_configs)
{
	Mounts::MountTab::const_iterator i;
	const Mounts::MountTab * mtab = mounts.GetMountTab();

	// loop through all mount points
	for(i=mtab->begin() ; i!=mtab->end() ; ++i)
	{
		const Mount & mount = i->second;

		if( mount.param[mount_par_group_conf].defined &&
			mount.param[mount_par_group_conf].arg.size() == 1 )
		{	
			const std::wstring & file_name = mount.param[mount_par_group_conf].arg[0];
			GroupsWrap::iterator c = groups_wrap.find(mount.dir_id);
			bool exists = (c != groups_wrap.end() && c->second.file_name == file_name);

			if( exists )
				c->second.to_delete = false;

			if( !(skip_existing_configs && exists) )
			{
				if( GetConfContent(file_name) )
				{
					log << log3 << "GroupItem: parsing conf file: " << config_file.url << logend;
					groups_wrap[mount.dir_id].file_name = file_name;
					groups_wrap[mount.dir_id].groups.set_dependency(this);

					if( !ParseGroups(config_file.item_content.content_raw, groups_wrap[mount.dir_id].groups) )
						groups_wrap[mount.dir_id].to_delete = true;
				}
				else
				{
					if( exists )
						c->second.to_delete = true;
				}
			}
		}
	}
}



// if skip_existing_configs is true then only new config files will be parsed
void GroupInfo::ReadGroupsConfigs(bool skip_existing_configs)
{
	MarkAllGroupsToDelete();
	ReadGroupsConf(system->mounts, skip_existing_configs);
	DeleteAllMarkedGroups();
}






}


} // namespace Winix

