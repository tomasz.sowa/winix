/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_plugins_groupitem_groups
#define headerfile_winix_plugins_groupitem_groups

#include <map>
#include <vector>
#include <string>
#include "space/spaceparser.h"
#include "core/winixrequest.h"


namespace Winix
{

namespace GroupItem
{


class Groups : public WinixRequest
{
public:

	/*
		reading all space
		(sets and groups)
	*/
	pt::Space * GetSpace();


	/*
		building all indexes
	*/
	void Reindex();


	bool Find(const std::wstring & set, const std::wstring & value, size_t & seti, size_t & groupi);



	/*	
	*/
	size_t Size(size_t seti, size_t groupi);

	const std::wstring & GetOption(size_t seti, size_t groupi, size_t valuei, const wchar_t * option);
	const std::wstring & GetOption(size_t seti, size_t groupi, size_t valuei, const std::wstring & option);


	/*
		removing all groups
	*/
	void Clear();


private:

	/*
		our space:
		set1 = (

		# group1 table
		(
			# first object
			{ value = "value1"
			  other = "foo" }

			# second object
			{ value = "something"
			  other = "x" }
		)

		# group2
		(

		)

		) # end of set1 table

		# we can have more sets
		# sets should have a unique name
		# groups don't have a name
	*/
	pt::Space space;


	/*
		indexes to a set of groups
	*/
	typedef std::map<std::wstring, size_t> SetIndex;
	SetIndex set_index;

	/*
		indexes to a groups in a set
		mapping: value->group index
	*/
	typedef std::map<std::wstring, size_t> GroupIndex;

	/*
		group indexes in a specified set
		std::vector<how many sets there are>
	*/
	std::vector<GroupIndex> group_index_tab;

	const std::wstring empty_str;


	std::wstring sort_by;
	bool sort_asc;
	//std::vector<std::wstring> sort_value;

	struct SortFunHelper
	{
		Groups * groups;
		SortFunHelper(Groups * pgroups) : groups(pgroups) {};
		//size_t SortValue(const std::wstring & val);
		bool operator()(pt::Space * sp1, pt::Space * sp2);
	};



	void ReindexGroups(GroupIndex & group_index, const std::wstring & set_name, pt::Space::TableType & groups, const std::wstring & key);
	void SortValues(pt::Space::TableType & group);
};


}

} // namespace Winix

#endif

