/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates/templates.h"
#include "core/plugin.h"
#include "groupinfo.h"
#include "core/misc.h"

namespace Winix
{



namespace GroupItem
{
using namespace TemplatesFunctions;
using TemplatesFunctions::system;


extern GroupInfo group_info;


static bool group_tab_found = false;
static Groups * pgroups;
static size_t req_id = 0;
static size_t set_index;
static size_t group_index;
static size_t value_index;
static std::wstring last_set, last_value;
static std::wstring path;




void group_tab_init(const std::wstring & set, const std::wstring & value)
{
	if( req_id != cur->request->id || last_set != set || last_value != value )
	{
		req_id     = cur->request->id;
		pgroups    = group_info.FindGroups(cur->mount->dir_id);
		last_set   = set;
		last_value = value;
		group_tab_found = false;

		if( pgroups && pgroups->Find(set, value, set_index, group_index) )
			group_tab_found = true;
	}
}




void group_tab(Info & i)
{
	if( i.params.size() == 2 )
	{
		group_tab_init(i.params[0].str, i.params[1].str);
		value_index = i.iter;

		if( group_tab_found )
			i.res = value_index < pgroups->Size(set_index, group_index);
	}
	else
	{
		group_tab_found = false;
	}
}



void build_current_path()
{
	system->dirs.MakePath(cur->request->dir_tab, path);

	if( cur->request->is_item )
	{
		path += cur->request->item.url;
	}
	else
	{
		// we assume that directories are without the last trailing slash
		NoLastSlash(path);
	}
}



void group_link_tab(Info & i)
{
	if( i.params.size() == 1 )
	{
		build_current_path();
		group_tab_init(i.params[0].str, path);
		value_index = i.iter;

		if( group_tab_found )
			i.res = value_index < pgroups->Size(set_index, group_index);
	}
	else
	{
		group_tab_found = false;
	}
}



void group_tab_value(Info & i)
{
	if( group_tab_found )
	{
		if( !i.par.empty() )
			i.out << pgroups->GetOption(set_index, group_index, value_index, i.par);
		else
			i.out << pgroups->GetOption(set_index, group_index, value_index, L"value");
	}
}


void group_tab_is_current_link(Info & i)
{
	if( group_tab_found )
	{
		build_current_path();

		if( !i.par.empty() )
			i.res = (pgroups->GetOption(set_index, group_index, value_index, i.par) == path);
		else
			i.res = (pgroups->GetOption(set_index, group_index, value_index, L"value") == path);
	}
}




void AddEzcFunctions(PluginInfo & info)
{
	using TemplatesFunctions::EzcFun;
	EzcFun * fun = reinterpret_cast<EzcFun*>(info.p1);

	/*
	*/
	fun->Insert("group_tab",                  group_tab);
	fun->Insert("group_link_tab",             group_link_tab);
	fun->Insert("group_tab_value",            group_tab_value);
	fun->Insert("group_tab_is_current_link",  group_tab_is_current_link);
}


} // namespace



} // namespace Winix



