/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "core/log.h"
#include "core/plugin.h"
#include "groupinfo.h"
#include "functions/functions.h"


namespace Winix
{



extern "C" void Init(PluginInfo &);




namespace GroupItem
{
const wchar_t plugin_name[] = L"group";
GroupInfo group_info;




void AddMountPar(PluginInfo & info)
{
	group_info.mount_par_group_conf = info.system->mounts.AddMountPar(L"group_conf");
}


void FstabChanged(PluginInfo & info)
{
	group_info.ReadGroupsConfigs(true);
}


void ProcessRequest(PluginInfo & info)
{
	if( info.cur->request->function == &info.functions->fun_reload )
	{
		if( info.cur->request->IsParam(L"group") )
			group_info.ReadGroupsConfigs(false);
	}
}




void AddEzcFunctions(PluginInfo & info);

} // namespace





void Init(PluginInfo & info)
{
using namespace GroupItem;

	info.plugin->Assign(WINIX_TEMPLATES_CREATEFUNCTIONS,		AddEzcFunctions);
	info.plugin->Assign(WINIX_ADD_MOUNTS,						AddMountPar);
	info.plugin->Assign(WINIX_FSTAB_CHANGED,					FstabChanged);
	info.plugin->Assign(WINIX_PROCESS_REQUEST,				ProcessRequest);

	group_info.set_dependency((WinixModelDeprecated*)info.functions);
	group_info.SetSystem(info.system);
	group_info.SetConfig(info.config);

	info.p1 = (void*)(plugin_name);
}



} // namespace Winix

