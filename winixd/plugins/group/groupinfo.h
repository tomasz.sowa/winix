/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_plugins_groupitem_groupinfo
#define headerfile_winix_plugins_groupitem_groupinfo

#include <string>
#include <vector>
#include "groups.h"
#include "space/spaceparser.h"
#include "core/system.h"
#include "core/config.h"
#include "core/winixrequest.h"
#include "models/item.h"


namespace Winix
{



namespace GroupItem
{

struct GroupsWrapItem
{
	bool to_delete;
	std::wstring file_name;
	Groups groups;

	GroupsWrapItem()
	{
		to_delete = false;
	}
};




class GroupInfo : public WinixRequest
{

public:


	GroupInfo();

	void SetSystem(System * psystem);
	void SetConfig(Config * pconfig);

	void ReadGroupsConfigs(bool skip_existing_configs);
	Groups * FindGroups(long dir_id); // can return a null pointer


	int mount_par_group_conf;



private:

	System * system;
	Config * config;

	// map[mount_dir_id] -> GroupsWrapItem
	typedef std::map<long, GroupsWrapItem> GroupsWrap;
	GroupsWrap groups_wrap;

	pt::SpaceParser conf_parser;
	std::vector<Item*> config_dir_tab;
	Item config_file;

	void MarkAllGroupsToDelete();
	void DeleteAllMarkedGroups();
	bool GetConfContent(const std::wstring & path);
	bool ParseGroupsConf(long mount_dir_id, const std::wstring & path);
	bool ParseGroups(const std::wstring & str, Groups & groups);
	void ReadGroupsConf(Mounts & mounts, bool skip_existing_configs);
};


}

} // namespace Winix


#endif
