/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "cache.h"
#include "core/misc.h"

namespace Winix
{



namespace Menu
{


void Cache::SetDirs(Dirs * pdirs)
{
	dirs = pdirs;
}


CacheItem * Cache::Get(long dir_id, int param)
{
Tab::iterator i;

	for(i=tab.begin() ; i!=tab.end() ; ++i)
	{
		if( i->dir_id == dir_id && i->param == param )
		{
			return &(*i);
		}
	}

return 0;
}



CacheItem * Cache::Insert(long dir_id, int param)
{
	empty_cache_item.dir_id = dir_id;
	empty_cache_item.param  = param;
	empty_cache_item.menu_items.clear();

	if( !dirs->MakePath(dir_id, empty_cache_item.dir) )
		empty_cache_item.dir.clear();

	NoLastSlash(empty_cache_item.dir);

	tab.push_back(empty_cache_item);

return &tab.back();
}



void Cache::Remove(long dir_id)
{
Tab::iterator i;

	for(i=tab.begin() ; i!=tab.end() ; )
	{
		if( i->dir_id == dir_id )
		{
			tab.erase(i++);
		}
		else
		{
			++i;
		}
	}
}


void Cache::Clear()
{
	tab.clear();
}












} // namespace



} // namespace Winix

