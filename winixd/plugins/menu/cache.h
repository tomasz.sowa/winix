/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_plugins_menu_cache
#define headerfile_winix_plugins_menu_cache

#include <string>
#include <vector>
#include <list>
#include "core/dirs.h"
#include "models/item.h"


namespace Winix
{


namespace Menu
{


struct MenuItem
{
	long         id; // item id
	std::wstring url;
	std::wstring subject;
	pt::Space    meta;
	Item::Type   type;
	int          file_type;
};


struct CacheItem
{
	long dir_id;
	int param;
	std::vector<MenuItem> menu_items;
	std::wstring dir;	// without the last slash

	CacheItem()
	{
		dir_id = -1;
		param  = -1;
	}
};





class Cache
{
public:

	void SetDirs(Dirs * pdirs);

	CacheItem * Get(long dir_id, int param);
	CacheItem * Insert(long dir_id, int param);
	void Remove(long dir_id);
	void Clear();

private:

	Dirs * dirs;
	typedef std::list<CacheItem> Tab;
	Tab tab;
	CacheItem empty_cache_item;
};


} // namespace

} // namespace Winix

#endif
