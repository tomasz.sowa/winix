/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "core/log.h"
#include "core/plugin.h"
#include "cache.h"
#include "templates/templates.h"
#include "functions/functions.h"


namespace Winix
{


extern "C" void Init(PluginInfo &);




namespace Menu
{


const wchar_t plugin_name[] = L"menu";
int mount_par_menu_skip     = -1;
Cache cache;


void AddMountParams(PluginInfo & info)
{
using TemplatesFunctions::system;

	mount_par_menu_skip = system->mounts.AddMountPar(L"menu_skip");
}



void FstabChanged(PluginInfo & info)
{
	if( mount_par_menu_skip == -1 )
		return;

	/*
	// GetMountTab() returns a const pointer so we cannot sort menu_skip parameters 
	// may change it to non-const in the future?

	const Mounts::MountTab * mt = system->mounts.GetMountTab();
	Mounts::MountTab::const_iterator i;

	for(i = mt->begin() ; i != mt->end() ; ++i)
		SortMenuSkip(i->second);
	*/
}

void InvalidateCache(PluginInfo & info, long dir_id)
{
	info.log << log4 << "Menu: removing cache for dir id: " << dir_id << logend;
	cache.Remove(dir_id);
}


void InvalidateCacheByPointerParent(PluginInfo & info)
{
	Item * pitem = reinterpret_cast<Item*>(info.p1);

	if( pitem )
		InvalidateCache(info, pitem->parent_id);
}


void InvalidateCacheByPointer(PluginInfo & info)
{
	Item * pitem = reinterpret_cast<Item*>(info.p1);

	if( pitem )
		InvalidateCache(info, pitem->id);
}


void InvalidateCacheById(PluginInfo & info)
{
	InvalidateCache(info, info.l1);
}



void ProcessRequest(PluginInfo & info)
{
	if( info.cur->request->function == &info.functions->fun_reload )
	{
		if( info.cur->request->IsParam(L"menu") )
		{
			cache.Clear();
			info.log << log3 << "Menu: cache has been cleared" << logend;
		}
	}
}


void AddEzcFunctions(PluginInfo & info);

} // namespace





void Init(PluginInfo & info)
{
using namespace Menu;

	cache.SetDirs(&info.system->dirs);

	info.plugin->Assign(WINIX_TEMPLATES_CREATEFUNCTIONS,		AddEzcFunctions);
	info.plugin->Assign(WINIX_ADD_MOUNTS,						AddMountParams);
	info.plugin->Assign(WINIX_FSTAB_CHANGED,					FstabChanged);

	// !! IMPROVE ME
	// use modify parameter from a directory
	// (not implemented yet)

	info.plugin->Assign(WINIX_FILE_REMOVED,					InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_DIR_ADDED,					InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_DIR_PREPARE_TO_REMOVE,		InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_DIR_REMOVED,					InvalidateCacheById);
	info.plugin->Assign(WINIX_FILE_ADDED,					InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_FILE_CHANGED,					InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_FILE_COPIED,					InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_FILE_PREPARE_TO_MOVE,			InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_FILE_MOVED,					InvalidateCacheByPointerParent);
	info.plugin->Assign(WINIX_DIR_CONTENT_SORTED,			InvalidateCacheByPointer);
	info.plugin->Assign(WINIX_PROCESS_REQUEST,				ProcessRequest);

	// !! IMPROVE ME
	// we need a WINIX_DIR_CHANGED message
	// for example when changing the subject of a directory

	info.p1 = (void*)(plugin_name);
}



} // namespace Winix

