/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <algorithm>
#include "core/log.h"
#include "exportinfo.h"
#include "convert/text.h"


namespace Winix
{



namespace Export
{


ExportInfo::ExportInfo()
{
	use_rsa = false;
}



void ExportInfo::SetSystem(System * psystem)
{
	system = psystem;
}


void ExportInfo::SetConfig(Config * pconfig)
{
	config = pconfig;
}


void ExportInfo::SetEDb(EDb * pdb)
{
	edb = pdb;
}


void ExportInfo::SetExportThread(ExportThread * pexport_thread)
{
	export_thread = pexport_thread;
}



void ExportInfo::ReadExportDirs()
{
	edb->GetExportDirs(model_connector, export_dirs);
}


void ExportInfo::ReadConfigVars()
{
	use_rsa = config->Bool(L"export_rsa", false);
	rsa_key = config->Text(L"export_rsa_key");
}


ExportDir * ExportInfo::FindDir(long dir_id)
{
	for(size_t i=0 ; i<export_dirs.size() ; ++i)
	{
		if( dir_id == export_dirs[i].dir_id || system->dirs.HasParent(dir_id, export_dirs[i].dir_id) )
			return &export_dirs[i];					
	}

return 0;
}




bool ExportInfo::DecodePass(Export & exp)
{
	if( exp.export_ftp.pass_type == 0 )
	{
		
	}
	else
	{
		/*
		if( system->crypt.RSA(false, rsa_key, exp.ftp_pass_bin, pass_decrypted) )
		{
			pt::utf8_to_wide(pass_decrypted, exp.ftp_pass);
			system->crypt.ClearString(pass_decrypted);
		}
		else
		{
			log << log1 << "Export: I cannot decrypt a password (RSA failed)" << logend;
			return false;
		}
		*/
	}

return true;
}



bool ExportInfo::SkipDir(long dir_id, std::wstring & dir)
{
	if( system->dirs.MakePath(dir_id, tmp_dir) )
	{
		if( !tmp_dir.empty() && pt::is_substr(tmp_dir, dir) )
		{
			// tmp_dir has a slash at the end
			// we want the slash at the beginning
			dir.erase(0, tmp_dir.size()-1);
			return true;
		}
	}

return false;
}


void ExportInfo::ResetRecurrenceCheck()
{
	recurrence_check.clear();
}


bool ExportInfo::HasRecurrenceId(long id)
{
	return (std::find(recurrence_check.begin(), recurrence_check.end(), id) != recurrence_check.end());
}


void ExportInfo::SendFile(const Item & item, bool thumb, bool put_to_recurrence_tab)
{
	ExportDir * exp_dir = FindDir(item.parent_id);

	if( !exp_dir )
		return;

	if( HasRecurrenceId(item.id) )
	{
		log << log1 << "Export: recurrence found on item.id: " << item.id << ", url: " << item.url << " (skipping)" << logend;
		return;
	}

	if( put_to_recurrence_tab )
		recurrence_check.push_back(item.id);

	msg.Clear();
	system->dirs.MakePath(item.parent_id, msg.path);
	msg.path += item.url;

	if( item.item_content.file_type != WINIX_ITEM_FILETYPE_NONE )
	{
		msg.type = WINIX_PL_EXPORT_TYPE_CREATE_FILE_STATIC;

		if( !system->MakeFilePath(item, msg.url, thumb) )
		{
			log << log1 << "Export: I cannot create a path to a static file, item id: " << item.id << logend;
			return;
		}
	}
	else
	{
		msg.type = WINIX_PL_EXPORT_TYPE_CREATE_FILE;
		msg.url  = config->url_proto;
		msg.url += config->base_url;// !! IMPROVE ME what about subdomains?
		system->dirs.MakePath(item.parent_id, msg.url, false);
		msg.url += item.url;
		msg.path += L".php"; // !! do konfiga
	}

	msg.errors = 0;

	if( SkipDir(exp_dir->dir_id, msg.path) )
	{
		edb->GetExport(model_connector, exp_dir->id, exp);

		if( exp.found() && DecodePass(exp) )
		{
			msg.ftp_login   = exp.export_ftp.login;
			msg.ftp_pass    = exp.export_ftp.pass;
			msg.ftp_server  = exp.export_ftp.server;
			msg.http_server = exp.http_server;
			system->dirs.MakePath(exp_dir->dir_id, msg.src_dir);

			if( !item.item_content.file_path.empty() && thumb ) // !! uzyc file_type
				msg.path.insert(0, L"/download"); // !! do konfiga

			if( !exp.ftp_dir.empty() )
				msg.path.insert(0, exp.ftp_dir);

			if( !msg.path.empty() && msg.path[0] != '/' )
				msg.path.insert(0, L"/");

			export_thread->AddMessage(msg);
		}
	}
}



void ExportInfo::SendDir(const Item & item)
{
	ExportDir * exp_dir = FindDir(item.id);

	if( !exp_dir )
		return;

	if( HasRecurrenceId(item.id) )
	{
		log << log1 << "Export: recurrence found on item.id: " << item.id << ", url: " << item.url << " (skipping)" << logend;
		return;
	}

	recurrence_check.push_back(item.id);

	msg.Clear();
	system->dirs.MakePath(item.id, msg.path);

	msg.type = WINIX_PL_EXPORT_TYPE_CREATE_FILE;
	msg.url  = config->url_proto;
	msg.url += config->base_url;// !! IMPROVE ME what about subdomains?
	system->dirs.MakePath(item.id, msg.url, false);
	msg.path += L"index.html"; // !! do konfiga

	msg.errors = 0;

	if( SkipDir(exp_dir->dir_id, msg.path) )
	{
		edb->GetExport(model_connector, exp_dir->id, exp);

		if( exp.found() && DecodePass(exp) )
		{
			msg.ftp_login   = exp.export_ftp.login;
			msg.ftp_pass    = exp.export_ftp.pass;
			msg.ftp_server  = exp.export_ftp.server;
			msg.http_server = exp.http_server;
			system->dirs.MakePath(exp_dir->dir_id, msg.src_dir);

			if( !exp.ftp_dir.empty() )
				msg.path.insert(0, exp.ftp_dir);

			if( !msg.path.empty() && msg.path[0] != '/' )
				msg.path.insert(0, L"/");

			export_thread->AddMessage(msg);
			AdditionalExport(item);
		}
	}
}


void ExportInfo::SendDir(long dir_id)
{
	Item * dir = system->dirs.GetDir(dir_id);

	if( !dir )
		return;

	SendDir(*dir);
}


// send all non-static files from a directory
void ExportInfo::SendAllFilesFromDir(long dir_id)
{
//	iq_dir.SetAll(false, false);
//	iq_dir.sel_parent_id = true;
//	iq_dir.sel_url       = true;
//	iq_dir.sel_file      = true;
//	iq_dir.WhereParentId(dir_id);
//	iq_dir.WhereType(Item::file);
//	iq_dir.WhereFileType(WINIX_ITEM_FILETYPE_NONE);


	morm::Finder<Item> finder(model_connector);

	finder.
		select().
		where().
		eq(L"parent_id", dir_id).
		eq(L"type", static_cast<int>(Item::file)).
		eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_NONE).
		get_vector(dir_items);

	//db->GetItems(dir_items, iq_dir);

	for(size_t i=0 ; i<dir_items.size() ; ++i)
	{
		SendFile(dir_items[i]);
	}
}



void ExportInfo::AdditionalExport(const Item & item)
{
	const pt::Space * export_space = item.item_content.meta.get_space(L"export");// CHECK ME it was find_child_space_table();

	if( export_space )
	{
		AdditionalExport(item, *export_space);
	}

//	if( export_space )
//	{
//		for(const pt::Space * child : *export_space)
//		{
//			if( child->is_child_space_name(L"export") )
//				AdditionalExport(item, *child);
//		}
//	}
}


void ExportInfo::AdditionalExport(const Item & item, const pt::Space & meta)
{
	meta.to_list(L"additional_export", additional_export);

	for(size_t i=0 ; i<additional_export.size() ; ++i)
	{
		log << log1 << "additional export: " << additional_export[i] << logend;
		AdditionalExport(additional_export[i]);
	}
}


void ExportInfo::AdditionalExport(const std::wstring & path)
{
	int res = system->FollowAllLinks(path, additional_export_dir_tab, additional_export_item);

	if( res == 0 )
	{
		// directory
		SendDir(*additional_export_dir_tab.back());
	}
	else
	if( res == 1 )
	{
		// file
		// !! uwaga jesli to bedzie plik statyczny (moze dwa razy uzyc raz false i raz true?)
		SendFile(additional_export_item);
	}
	else
	{
		log << log1 << "Export: incorrect path for additional export: " << path << logend;
	}
}


} // namespace


} // namespace Winix

