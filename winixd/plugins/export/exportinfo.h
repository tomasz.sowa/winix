/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_plugins_export_exportinfo
#define headerfile_winix_plugins_export_exportinfo

#include "core/system.h"
#include "export.h"
#include "edb.h"
#include "message.h"
#include "exportthread.h"
#include "core/winixmodeldeprecated.h"



namespace Winix
{

namespace Export
{


class ExportInfo : public WinixModelDeprecated
{
public:

	ExportInfo();

	void SetSystem(System * psystem);
	void SetConfig(Config * pconfig);
	void SetEDb(EDb * pdb);
	void SetExportThread(ExportThread * pexport_thread);

	void ReadExportDirs();
	void ReadConfigVars();

	ExportDir * FindDir(long dir_id);

	bool DecodePass(Export & exp);

	void SendFile(const Item & item, bool thumb = false, bool put_to_recurrence_tab = true);
	void SendDir(const Item & item);
	void SendDir(long dir_id);
	void SendAllFilesFromDir(long dir_id);

	void ResetRecurrenceCheck();

private:

	System * system;
	Config * config;
	EDb * edb;
	ExportThread * export_thread;

	bool use_rsa;
	std::wstring rsa_key;
	std::string pass_decrypted;
	Message msg;
	Export exp;
	std::wstring tmp_dir;
	std::vector<ExportDir> export_dirs;
	//DbItemQuery iq_dir;
	std::vector<Item> dir_items;
	std::vector<std::wstring> additional_export;
	std::vector<Item*> additional_export_dir_tab;
	Item additional_export_item;
	std::vector<long> recurrence_check;

	bool SkipDir(long dir_id, std::wstring & dir);

	void AdditionalExport(const Item & item);
	void AdditionalExport(const Item & item, const pt::Space & meta);
	void AdditionalExport(const std::wstring & path);

	bool HasRecurrenceId(long id);
};


}

} // namespace Winix

#endif

