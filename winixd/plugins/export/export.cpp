/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "export.h"


namespace Winix::Export
{


void Export::fields()
{
	field(L"id",					id, morm::FT::no_insertable | morm::FT::no_updatable | morm::FT::primary_key);
	field(L"user_id",				user_id);
	field(L"dir",					dir);
	field(L"ftp_id",				export_ftp, morm::FT::foreign_key);
	field(L"ftp_dir",				ftp_dir);
	field(L"can_change_ftp_params",	can_change_ftp_params);
	field(L"can_change_dir",		can_change_dir);
	field(L"http_server",			http_server);
}


void Export::after_insert()
{
	get_last_sequence_for_primary_key(L"plugins.export_id_seq", id);
}


void Export::table()
{
	table_name(L"plugins", L"export");
}


bool Export::do_migration(int & current_table_version)
{
	bool ok = true;

	ok = ok && morm::Model::do_migration(current_table_version, 1, this, &Export::do_migration_to_1);
	ok = ok && morm::Model::do_migration(current_table_version, 2, this, &Export::do_migration_to_2);

	return ok;
}


bool Export::do_migration_to_1()
{
	const char * str = R"sql(
		CREATE SEQUENCE plugins.export_id_seq
			INCREMENT BY 1
			MINVALUE 1
			MAXVALUE 9223372036854775807
			START WITH 1
			CACHE 1
			NO CYCLE
			OWNED BY NONE;
	)sql";

	return db_query(str);
}


bool Export::do_migration_to_2()
{
	const char * str = R"sql(
		CREATE TABLE plugins.export (
			id integer NOT NULL DEFAULT nextval('export_id_seq'::regclass),
			user_id bigint,
			dir character varying(1024),
			ftp_id bigint,
			ftp_dir character varying(1024),
			can_change_ftp_params boolean,
			can_change_dir boolean,
			http_server character varying(1024),
			CONSTRAINT export_pkey PRIMARY KEY (id)
		);
	)sql";

	return db_query(str);
}






}



