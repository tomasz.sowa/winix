/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_plugins_export_message
#define headerfile_winix_plugins_export_message

#include <string>

namespace Winix
{


// message types
#define WINIX_PL_EXPORT_TYPE_CREATE_FILE		1
#define WINIX_PL_EXPORT_TYPE_CREATE_FILE_STATIC	2




struct Message
{
	// message type
	int type;

	// original (source) url (if message is WINIX_PL_EXPORT_TYPE_CREATE_FILE)
	// or a file path (WINIX_PL_EXPORT_TYPE_CREATE_FILE_STATIC)
	std::wstring url;

	// source directory
	std::wstring src_dir;

	// output file (directory) name
	// relative path
	// with a slash at the beginning (and at the end if it is a directory)
	std::wstring path;

	std::wstring ftp_server;
	std::wstring ftp_login;
	std::wstring ftp_pass;

	// server on which the site will be visible
	std::wstring http_server;

	// how many errors were with this message
	int errors;

	// shoud be removed
	bool can_remove;


	void Clear()
	{
		type		= 0;
		errors		= 0;
		can_remove	= true;

		url.clear();
		src_dir.clear();
		path.clear();
		ftp_server.clear();
		ftp_login.clear();
		ftp_pass.clear();
		http_server.clear();
	}

};



} // namespace Winix

#endif
