/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "exportftp.h"


namespace Winix::Export
{


void ExportFtp::fields()
{
	field(L"id",		id, morm::FT::no_insertable | morm::FT::no_updatable | morm::FT::primary_key);
	field(L"name",		name);
	field(L"server",	server);
	field(L"login",		login);
	field(L"pass",		pass);
	field(L"pass_type",	pass_type);
}


void ExportFtp::after_insert()
{
	get_last_sequence_for_primary_key(L"plugins.export_ftp_id_seq", id);
}


void ExportFtp::table()
{
	table_name(L"plugins", L"export_ftp");
}


bool ExportFtp::do_migration(int & current_table_version)
{
	bool ok = true;

	ok = ok && morm::Model::do_migration(current_table_version, 1, this, &ExportFtp::do_migration_to_1);
	ok = ok && morm::Model::do_migration(current_table_version, 2, this, &ExportFtp::do_migration_to_2);

	return ok;
}


bool ExportFtp::do_migration_to_1()
{
	const char * str = R"sql(
		CREATE SEQUENCE plugins.export_ftp_id_seq
			INCREMENT BY 1
			MINVALUE 1
			MAXVALUE 9223372036854775807
			START WITH 1
			CACHE 1
			NO CYCLE
			OWNED BY NONE;
	)sql";

	return db_query(str);
}


bool ExportFtp::do_migration_to_2()
{
	const char * str = R"sql(
		CREATE TABLE plugins.export_ftp (
			id integer NOT NULL DEFAULT nextval('export_ftp_id_seq'::regclass),
			name character varying(256),
			server character varying(1024),
			login character varying(1024),
			pass character varying(1024),
			pass_type integer,
			CONSTRAINT export_ftp_pkey PRIMARY KEY (id)
		);
	)sql";

	return db_query(str);
}


}


