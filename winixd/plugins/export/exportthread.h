/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_plugins_export_exportthread
#define headerfile_winix_plugins_export_exportthread

#include <vector>
#include <list>
#include <curl/curl.h>
#include "core/basethread.h"
#include "message.h"

namespace Winix
{


namespace Export
{


class ExportThread : public BaseThread
{
public:

	ExportThread();

	void SetBaseUrl(const std::wstring & url);

	void AddMessage(const Message & message);
	void AddMessage(int type, const std::wstring & url, const std::wstring & path);


private:

	typedef std::list<Message> MessageTab;
	MessageTab message_tab;
	Message message_add_temp;
	Message message_work;			// a message used by the second thread
	static ExportThread * exp_thread;
	std::string url_a;
	std::string buffer;
	size_t buffer_read_index;
	std::wstring base_url;

	std::string browser_name;
	std::string ftp_server;
	std::string ftp_login;
	std::string ftp_pass;
	char error_buf[CURL_ERROR_SIZE];

	int conn_timeout; // timeout in seconds
	int conn_max_errors; // maximum errors (if there are more then a message is treated as undeliverable)

	std::string look_for_url;
	std::string repl_url;
	std::string local_path;

	virtual bool SignalReceived();
	virtual void Do();
	void DoMessage();
	bool Fetch(const char * url);
	bool Put();
	static size_t StaticSaveFunction(char * ptr, size_t size, size_t nmemb, void *userdata);
	size_t SaveFunction(char * ptr, size_t size, size_t nmemb, void *userdata);
	static size_t StaticReadFunction(char * ptr, size_t size, size_t nmemb, void *userdata);
	size_t ReadFunction(char * ptr, size_t size, size_t nmemb, void *userdata);
	void Convert(const std::wstring & in, std::string & out, bool clear = true);
	void ChangeAdresses(std::string & buf);
	bool HasThumbInAdress(std::string & buf, size_t i);
	void ChangeAdressesThumb(std::string & buf, const char * http_prefix);
	void ChangeAdressesThumb(std::string & buf);
	
	void ChangeLogStrings();

	void ChangeBaseAdress(std::string & buf,
						  const char * http_prefix, const char * dir_prefix, const char * dir_postfix,
						  const char * repl_dir_postfix, bool skip_dir_last_slash = false);
	void ChangeBaseAdress(std::string & buf,
						  const char * dir_prefix, const char * dir_postfix,
						  const char * repl_dir_postfix, bool skip_dir_last_slash = false);

	void ChangeSiteNames(std::string & buf);

};



} // namespace

} // namespace Winix

#endif
