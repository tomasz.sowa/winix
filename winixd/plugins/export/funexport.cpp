/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "funexport.h"

namespace Winix
{




namespace Export
{


FunExport::FunExport()
{
	fun.url = L"export";
}


void FunExport::SetExportInfo(ExportInfo * pexport_info)
{
	export_info = pexport_info;
}


bool FunExport::has_access()
{
	// temporarily only a logged user can use this function
	// !! IMPROVEME we have to change to only some users can use this function in a specified directory
	return cur->session->puser;
}


void FunExport::ExportFile(const Item & item)
{
	log << log4 << "Export: exporting file: url: " << item.url << ", id: " << item.id << logend;

	if( item.item_content.file_type == WINIX_ITEM_FILETYPE_NONE )
	{
		export_info->SendFile(item, false, true);
	}
	else
	{
		export_info->SendFile(item, false, false);
		export_info->SendFile(item, true, true);
	}
}


void FunExport::ExportDir(const Item & dir, bool static_files_too)
{
	export_info->SendDir(dir);

	log << log4 << "Export: exporting directory: url: " << dir.url << ", id: " << dir.id << logend;

//	iq_dir.SetAll(false, false);
//	iq_dir.sel_parent_id = true;
//	iq_dir.sel_url       = true;
//	iq_dir.sel_file      = true;
//	iq_dir.WhereParentId(dir.id);
//	iq_dir.WhereType(Item::file);


	morm::Finder<Item> finder(model_connector);

	finder.
		select().
		where().
		eq(L"parent_id", dir.id).
		eq(L"type", static_cast<int>(Item::file));

	if( !static_files_too )
	{
		//iq_dir.WhereFileType(WINIX_ITEM_FILETYPE_NONE);
		finder.eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_NONE);
	}

	finder.get_vector(dir_items);
	//db->GetItems(dir_items, iq_dir);

	for(size_t i=0 ; i<dir_items.size() ; ++i)
	{
		ExportFile(dir_items[i]);
	}

	DirContainer::ParentIterator i = system->dirs.FindFirstChild(dir.id);

	for( ; i != system->dirs.ParentEnd() ; i = system->dirs.NextChild(i) )
	{
		ExportDir(*i->second, static_files_too);
	}
}


void FunExport::Export()
{
	export_info->ResetRecurrenceCheck();
	bool static_files_too = cur->request->IsPostVar(L"staticfilestoo");

	if( cur->request->is_item )
		ExportFile(cur->request->item);
	else
		ExportDir(*cur->request->dir_tab.back(), static_files_too);
}



void FunExport::make_post()
{
	Export();
}


void FunExport::MakeGet()
{
//	if( cur->request->IsParam(L"noconfirm") )
//		Export();
}





} // namespace



} // namespace Winix

