/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "edb.h"
#include "core/log.h"


namespace Winix
{
namespace Export
{


void EDb::SetDirs(Dirs * pdirs)
{
	dirs = pdirs;
}


void EDb::GetExport(morm::ModelConnector * model_connector, long user_id, std::vector<Export> & export_tab)
{
	morm::Finder<Export> finder(model_connector);

	finder.select().where().eq(L"user_id", user_id).get_vector(export_tab);
}


void EDb::GetExport(morm::ModelConnector * model_connector, long id, Export & exp)
{
	morm::Finder<Export> finder(model_connector);

	finder.select().where().eq(L"id", id).get(exp);
}


void EDb::GetExportDirs(morm::ModelConnector * model_connector, std::vector<ExportDir> & export_tab, bool clear_tab)
{
	if( clear_tab )
		export_tab.clear();

	std::vector<Export> exports;
	morm::Finder<Export> finder(model_connector);
	finder.select().get_vector(exports);

	for(size_t i=0 ; i < exports.size() ; ++i)
	{
		Item * pdir = dirs->GetDir(exports[i].dir);

		if( pdir )
		{
			exp_dir.id = exports[i].id;
			exp_dir.dir_id = pdir->id;
			export_tab.push_back(exp_dir);
		}
		else
		{
			//slog << "There is not such a directory: " << dir_temp << " (skipping)" << logend;
		}
	}
}



}

} // namespace Winix

