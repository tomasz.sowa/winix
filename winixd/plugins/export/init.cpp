/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 

#include "core/log.h"
#include "core/plugin.h"
#include "exportthread.h"
#include "exportinfo.h"
#include "edb.h"
#include "funexport.h"
#include "functions/functions.h"
#include "models/migration.h"


namespace Winix
{


extern "C" void Init(PluginInfo &);




namespace Export
{


const wchar_t plugin_name[] = L"export";
int mount_par_export_conf = -1;
ExportThread export_thread;
EDb edb;
ExportInfo export_info;
FunExport fun_export;



void AddWinixFunctions(PluginInfo & info)
{
	info.functions->Add(fun_export);
}


void AddMountParams(PluginInfo & info)
{
	mount_par_export_conf = info.system->mounts.AddMountPar(L"export_conf");
}



void FstabChanged(PluginInfo & info)
{
}



void InitPlugin(PluginInfo & info)
{
	export_thread.SetBaseUrl(info.config->base_url);

	info.res = info.system->thread_manager.Add(&export_thread, L"export");

	export_info.ReadExportDirs();
}


void SendDir(PluginInfo & info)
{
	const Item * dir = reinterpret_cast<Item*>(info.p1);

	if( dir )
	{
		export_info.ResetRecurrenceCheck();
		export_info.SendDir(*dir);
		export_info.SendAllFilesFromDir(dir->id);
	}
}



void SendFileAdded(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
	{
		if( item->item_content.file_type == WINIX_ITEM_FILETYPE_IMAGE && info.config->image_resize )
		{
			// there'll be a next message WINIX_IMAGE_RESIZED
			info.log << log4 << "Export: image will be resized, waiting..." << logend;
		}
		else
		{
			export_info.ResetRecurrenceCheck();
			export_info.SendFile(*item);
			export_info.SendDir(item->parent_id);

			if( item->item_content.file_type == WINIX_ITEM_FILETYPE_NONE )
				export_info.SendAllFilesFromDir(item->parent_id);
		}
	}
}


void SendFileChanged(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
	{
		export_info.ResetRecurrenceCheck();
		export_info.SendFile(*item);
		export_info.SendDir(item->parent_id);

		if( item->item_content.file_type == WINIX_ITEM_FILETYPE_NONE )
			export_info.SendAllFilesFromDir(item->parent_id);
	}
}



void SendFileCopied(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
	{
		export_info.ResetRecurrenceCheck();
		export_info.SendDir(item->parent_id);

		if( item->item_content.file_type == WINIX_ITEM_FILETYPE_NONE )
		{
			export_info.SendAllFilesFromDir(item->parent_id);
		}
		else
		{
			export_info.SendFile(*item);
			export_info.SendFile(*item, true);
		}
	}
}



void SendFileResized(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
	{
		export_info.ResetRecurrenceCheck();
		export_info.SendFile(*item);
		export_info.SendDir(item->parent_id);
	}
}


void SendFileThumb(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
	{
		export_info.ResetRecurrenceCheck();
		export_info.SendFile(*item, true);
		export_info.SendDir(item->parent_id);
	}
}


void SendFilePrepareMove(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
	{
		if( item->item_content.file_type == WINIX_ITEM_FILETYPE_NONE )
		{
			export_info.ResetRecurrenceCheck();
			export_info.SendAllFilesFromDir(item->parent_id);
		}
	}
}


void FileRemoved(PluginInfo & info)
{
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
	{
		export_info.ResetRecurrenceCheck();
		export_info.SendDir(item->parent_id);

		if( item->item_content.file_type == WINIX_ITEM_FILETYPE_NONE )
			export_info.SendAllFilesFromDir(item->parent_id);
	}
}




void ProcessRequest(PluginInfo & info)
{
	if( info.cur->request->function == &info.functions->fun_reload )
	{
		if( info.cur->request->IsParam(L"export") )
			export_info.ReadExportDirs();
	}
}


void DatabaseMigration(PluginInfo & info)
{
	bool ok = true;
	Export export_model;
	ExportFtp export_ftp;

	ok = ok && Migration::do_migration(info.model_connector, export_model);
	ok = ok && Migration::do_migration(info.model_connector, export_ftp);

	info.res = ok;
}



void AddEzcFunctions(PluginInfo & info);

} // namespace





void Init(PluginInfo & info)
{
using namespace Export;

	edb.SetDirs(&info.system->dirs);

	export_info.set_dependency((WinixModelDeprecated*)info.functions);
	export_info.SetSystem(info.system);
	export_info.SetConfig(info.config);
	export_info.SetEDb(&edb);
	export_info.SetExportThread(&export_thread);

	fun_export.SetExportInfo(&export_info);


//	info.plugin->Assign(WINIX_TEMPLATES_CREATEFUNCTIONS,	AddEzcFunctions);
	info.plugin->Assign(WINIX_MAKE_DATABASE_MIGRATION,		DatabaseMigration);
	info.plugin->Assign(WINIX_ADD_MOUNTS,					AddMountParams);
//	info.plugin->Assign(WINIX_FSTAB_CHANGED,				FstabChanged);
	info.plugin->Assign(WINIX_CREATE_FUNCTIONS,				AddWinixFunctions);

	info.plugin->Assign(WINIX_FILE_ADDED,					SendFileAdded);
	info.plugin->Assign(WINIX_FILE_CHANGED,					SendFileChanged);
	info.plugin->Assign(WINIX_CREATED_THUMB,				SendFileThumb);
	info.plugin->Assign(WINIX_IMAGE_RESIZED,				SendFileResized);

	info.plugin->Assign(WINIX_FILE_PREPARE_TO_MOVE,			SendFilePrepareMove);
	info.plugin->Assign(WINIX_FILE_MOVED,					SendFileCopied);
	info.plugin->Assign(WINIX_FILE_COPIED,					SendFileCopied);

	info.plugin->Assign(WINIX_DIR_CONTENT_SORTED,			SendDir);
	info.plugin->Assign(WINIX_FILE_REMOVED,					FileRemoved);

	info.plugin->Assign(WINIX_PROCESS_REQUEST,				ProcessRequest);

	info.plugin->Assign(WINIX_PLUGIN_INIT,					InitPlugin);




	info.p1 = (void*)(plugin_name);
}



} // namespace Winix

