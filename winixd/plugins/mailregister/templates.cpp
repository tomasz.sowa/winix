/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2016-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates/templates.h"
#include "core/plugin.h"
#include "core/log.h"
#include "core/misc.h"
#include "cache.h"
#include "registermail_info.h"
#include "models/item.h"


namespace Winix
{
namespace MailRegister
{
using TemplatesFunctions::Info;
using TemplatesFunctions::EzcFun;
using TemplatesFunctions::R;


extern RegisterMailInfo rm_info;
static size_t mails_tab_index = 0;


void registermail_mails_tab(Info & i)
{
	mails_tab_index = i.iter;
	i.res = mails_tab_index < rm_info.mail_list.size();
}


void registermail_mails_tab_index(Info & i)
{
	i.out << (mails_tab_index + 1);
}


void registermail_mails_tab_mail(Info & i)
{
	if( mails_tab_index < rm_info.mail_list.size() )
		i.out << rm_info.mail_list[mails_tab_index];
}



void AddEzcFunctions(PluginInfo & info)
{
	using TemplatesFunctions::EzcFun;
	EzcFun * fun = reinterpret_cast<EzcFun*>(info.p1);

	fun->Insert("registermail_mails_tab",					registermail_mails_tab);
	fun->Insert("registermail_mails_tab_index",				registermail_mails_tab_index);
	fun->Insert("registermail_mails_tab_mail",				registermail_mails_tab_mail);
}


} // namespace



} // namespace Winix



