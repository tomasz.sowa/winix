/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2016-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "funregistermail.h"
#include "core/misc.h"

namespace Winix
{




namespace MailRegister
{


FunRegisterMail::FunRegisterMail()
{
	fun.url = L"registermail";
	pmdb = 0;
	pinfo = 0;
}


void FunRegisterMail::SetMDb(MDb * pmdb_)
{
	pmdb = pmdb_;
}

void FunRegisterMail::SetInfo(RegisterMailInfo * info)
{
	pinfo = info;
}



bool FunRegisterMail::has_access()
{
	return true;
}



void FunRegisterMail::make_post()
{

}


void FunRegisterMail::MakeGet()
{
	mail = cur->request->ParamValue(L"mail");
	int list_id = Toi(cur->request->ParamValue(L"list_id"));

	if( pinfo->HasListId(list_id) )
	{
		TrimWhite(mail);

		if( ValidateEmail(mail) )
		{
			log << log3 << "RM: registering e-mail address: " << mail << logend;
			//slog << loginfo << "Thank you, your e-mail address has been added to our list" << logend;

			pmdb->AddMail(model_connector, list_id, mail);
		}
		else
		{
			log << log2 << "RM: email: " << mail << " doesn't seem to be correct (skipping)" << logend;
			//slog << logerror << "Sorry but the e-mail address: " << mail << " doesn't seem to be correct" << logend;
		}
	}
	else
	{
		log << log2 << "RM: there is no a list with id: " << list_id << " (skipping request)" << logend;
	}
}






} // namespace

} // namespace Winix

