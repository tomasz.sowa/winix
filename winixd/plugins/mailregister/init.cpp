/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2016-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 

#include "core/log.h"
#include "core/misc.h"
#include "core/plugin.h"
#include "mdb.h"
#include "funregistermail.h"
#include "registermail_info.h"
#include "funregistermail_showusers.h"
#include "functions/functions.h"
#include "models/migration.h"



namespace Winix
{



extern "C" void Init(PluginInfo &);




namespace MailRegister
{


const wchar_t plugin_name[] = L"mailregister";
MDb mdb;
FunRegisterMail fun_rm;
FunRegisterMailShowUsers fun_rm_showusers;
RegisterMailInfo rm_info;


void AddWinixFunctions(PluginInfo & info)
{
	info.functions->Add(fun_rm);
	info.functions->Add(fun_rm_showusers);
}


// create a class from these methods
void ReloadConfigFile(PluginInfo & info, Item & file)
{
	pt::Space space;
	pt::SpaceParser parser;

	pt::SpaceParser::Status status = parser.parse_space(file.item_content.content_raw, space);

	if( status == pt::SpaceParser::syntax_error )
	{
		info.log << log1 << "MR: syntax error in file: " << file.url << ", line: " << parser.get_last_parsed_line() << " (skipping this file)" << logend;
	}
	else
	if( status == pt::SpaceParser::ok )
	{
		std::wstring * list_id_str = space.get_wstr(L"list_id");

		if( list_id_str )
		{
			int list_id = Toi(*list_id_str);
			rm_info.lists.insert( std::make_pair(list_id, file.url) );
			info.log << log3 << "RM: file: " << file.url << " parsed successfully" << logend;
		}
		else
		{
			info.log << log1 << "MR: file: " << file.url << " doesn't have a list_id defined (skipping this file)" << logend;
		}
	}
}



void ConfigReload(PluginInfo & info)
{
	rm_info.lists.clear();
	std::wstring dir_str = info.config->Text(L"rm_directory", L"/etc/registermail.d"); // !! give a better config variable name

	if( dir_str.empty() )
	{
		info.log << log1 << "RM: rm_directory config option is empty" << logend;
		return;
	}

	Item * dir = info.system->dirs.GetDir(dir_str);

	if( dir )
	{
//		DbItemQuery iq;
		std::vector<Item> items;
//
//		iq.SetAll(true, false);
//		iq.WhereParentId(dir->id);

		morm::Finder<Item> finder(info.model_connector);

		finder.
			select().
			where().
			eq(L"parent_id", dir->id).
			get_vector(items);

		//info.db->GetItems(items, iq);

		for(Item & item : items)
		{
			ReloadConfigFile(info, item);
		}
	}
}


void ProcessRequest(PluginInfo & info)
{
	if( info.cur->request->function == &info.functions->fun_reload )
	{
		if( info.cur->request->IsParam(L"mailregister") )
			ConfigReload(info);
	}
}


void InitPlugin(PluginInfo & info)
{
	ConfigReload(info);
	info.res = true;
}


void DatabaseMigration(PluginInfo & info)
{
	bool ok = true;
	RegisterMail register_mail;

	ok = ok && Migration::do_migration(info.model_connector, register_mail);

	info.res = ok;
}




void AddEzcFunctions(PluginInfo & info);

} // namespace





void Init(PluginInfo & info)
{
using namespace MailRegister;

	fun_rm.SetMDb(&mdb);
	fun_rm.SetInfo(&rm_info);

	fun_rm_showusers.SetMDb(&mdb);
	fun_rm_showusers.SetInfo(&rm_info);

	info.plugin->Assign(WINIX_TEMPLATES_CREATEFUNCTIONS,	AddEzcFunctions);
	info.plugin->Assign(WINIX_MAKE_DATABASE_MIGRATION,		DatabaseMigration);
	info.plugin->Assign(WINIX_CREATE_FUNCTIONS,				AddWinixFunctions);

	info.plugin->Assign(WINIX_PLUGIN_INIT,					InitPlugin);
	info.plugin->Assign(WINIX_PROCESS_REQUEST,				ProcessRequest);

	info.p1 = (void*)(plugin_name);
}



} // namespace Winix

