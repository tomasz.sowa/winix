/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "gallery.h"
#include "core/log.h"
#include "core/plugin.h"
#include "functions/functions.h"


namespace Winix
{



extern "C" void Init(PluginInfo &);




namespace Gallery
{
Gallery fun_gallery;
GalleryInfo gallery_info;


const wchar_t plugin_name[] = L"gallery";




void AddFunctions(PluginInfo & info)
{
	info.functions->Add(fun_gallery);
}


void SelectDefaultFunction(PluginInfo & info)
{
	if( !info.cur->request->is_item && info.system->mounts.pmount->type == gallery_info.mount_type_gallery )
		info.cur->request->function = &fun_gallery;
}


void AddMounts(PluginInfo & info)
{
	Mounts & m = info.system->mounts;
	
	gallery_info.mount_type_gallery	     = m.AddMountType(L"gallery");

	gallery_info.mount_par_gallery	     = m.AddMountPar(L"gallery");
	gallery_info.mount_par_gallery_size  = m.AddMountPar(L"gallery_size");
	gallery_info.mount_par_gallery_type  = m.AddMountPar(L"gallery_type");
	gallery_info.mount_par_gallery_theme = m.AddMountPar(L"gallery_theme");
}


void EndRequest(PluginInfo & info)
{
	gallery_info.Clear();
}



void AddEzcFunctions(PluginInfo & info);

} // namespace





void Init(PluginInfo & info)
{
using namespace Gallery;

	info.plugin->Assign(WINIX_CREATE_FUNCTIONS,			 AddFunctions);
	info.plugin->Assign(WINIX_SELECT_DEFAULT_FUNCTION,	 SelectDefaultFunction);
	info.plugin->Assign(WINIX_ADD_MOUNTS,					 AddMounts);
	info.plugin->Assign(WINIX_TEMPLATES_CREATEFUNCTIONS,	 AddEzcFunctions);
	info.plugin->Assign(WINIX_END_REQUEST,                 EndRequest);

	gallery_info.plugin_id = info.plugin_id;

	fun_gallery.SetGalleryInfo(&gallery_info);

	info.p1 = (void*)(plugin_name);
}



} // namespace Winix

