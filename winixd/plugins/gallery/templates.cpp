/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "gallery.h"
#include "core/request.h"
#include "core/misc.h"
#include "core/plugin.h"
#include "templates/templates.h"
#include "functions/functions.h"

namespace Winix
{



namespace Gallery
{
using namespace TemplatesFunctions;
using TemplatesFunctions::system;


extern GalleryInfo   gallery_info;
extern Gallery       fun_gallery;


static size_t gallery_index;
static std::wstring item_dir;


void gallery_tab(Info & i)
{
	gallery_index = i.iter;
	i.res = gallery_index < gallery_info.item_sort_tab.size();
}


void gallery_tab_index(Info & i)
{
	i.out << gallery_index;
}


void gallery_tab_dir(Info & i)
{
	if( gallery_index < gallery_info.item_sort_tab.size() )
	{
		Item & item = *gallery_info.item_sort_tab[gallery_index];
		system->dirs.MakePath(item.parent_id, item_dir);
		i.out << item_dir;
	}
}


void gallery_tab_url(Info & i)
{
	if( gallery_index < gallery_info.item_sort_tab.size() )
	{
		Item & item = *gallery_info.item_sort_tab[gallery_index];
		i.out << item.url;
	}
}


void gallery_tab_subject(Info & i)
{
	if( gallery_index < gallery_info.item_sort_tab.size() )
	{
		Item & item = *gallery_info.item_sort_tab[gallery_index];
		i.out << item.subject;
	}
}



void gallery_tab_link(Info & i)
{
	doc_proto(i);
	i.out << config->base_url;// !! IMPROVE ME what about subdomains?
	gallery_tab_dir(i);
	gallery_tab_url(i);
}





//void gallery_tab_meta_str(Info & i)
//{
//	if( gallery_index < gallery_info.item_sort_tab.size() )
//	{
//		Item & item = *gallery_info.item_sort_tab[gallery_index];
//		item.item_content.meta.serialize_to_space_stream(i.out, true);
//	}
//}
//
//
//void gallery_tab_meta(Info & i)
//{
//	if( gallery_index < gallery_info.item_sort_tab.size() )
//	{
//		Item & item = *gallery_info.item_sort_tab[gallery_index];
//		space_value(i, item.item_content.meta);
//	}
//}
//
//
//void gallery_tab_meta_tab(Info & i)
//{
//	if( gallery_index < gallery_info.item_sort_tab.size() )
//	{
//		Item & item = *gallery_info.item_sort_tab[gallery_index];
//		space_list_tab(i, item.item_content.meta);
//	}
//}
//
//
//void gallery_tab_meta_tab_value(Info & i)
//{
//	if( gallery_index < gallery_info.item_sort_tab.size() )
//	{
//		Item & item = *gallery_info.item_sort_tab[gallery_index];
//		space_list_tab_value(i, item.item_content.meta, L"gallery_tab_meta_tab");
//	}
//}
//
//
//void gallery_tab_meta_tab_has_next(Info & i)
//{
//	if( gallery_index < gallery_info.item_sort_tab.size() )
//	{
//		Item & item = *gallery_info.item_sort_tab[gallery_index];
//		space_list_tab_has_next(i, item.item_content.meta, L"gallery_tab_meta_tab");
//	}
//}








void gallery_width(Info & i)
{
	if( !system->mounts.pmount )
		return;

	Mount & m = *system->mounts.pmount;

	if( !m.param[gallery_info.mount_par_gallery_size].defined || m.param[gallery_info.mount_par_gallery_size].arg.size() != 2 )
	{
		i.out << "800px";
	}
	else
	{
		i.out << m.param[gallery_info.mount_par_gallery_size].arg[0];
	}
}



void gallery_height(Info & i)
{
	if( !system->mounts.pmount )
		return;

	Mount & m = *system->mounts.pmount;

	if( !m.param[gallery_info.mount_par_gallery_size].defined || m.param[gallery_info.mount_par_gallery_size].arg.size() != 2 )
	{
		i.out << "500px";
	}
	else
	{
		i.out << m.param[gallery_info.mount_par_gallery_size].arg[1];
	}
}


void gallery_has_not_mount_type(Info & i)
{
	i.res = !system->mounts.pmount->IsPar(gallery_info.mount_par_gallery_type);
}


void gallery_mount_type_arg_is(Info & i)
{
	i.res = system->mounts.pmount->IsArg(gallery_info.mount_par_gallery_type, i.par);
}


void gallery_mount_theme_arg_is(Info & i)
{
	i.res = system->mounts.pmount->IsArg(gallery_info.mount_par_gallery_theme, i.par);
}




void AddEzcFunctions(PluginInfo & info)
{
	using TemplatesFunctions::EzcFun;
	EzcFun * fun = reinterpret_cast<EzcFun*>(info.p1);

	fun->Insert("gallery_tab",						gallery_tab);
	fun->Insert("gallery_tab_index",				gallery_tab_index);
	fun->Insert("gallery_tab_dir",					gallery_tab_dir);
	fun->Insert("gallery_tab_url",					gallery_tab_url);
	fun->Insert("gallery_tab_subject",				gallery_tab_subject);
	fun->Insert("gallery_tab_link",					gallery_tab_link);
//	fun->Insert("gallery_tab_meta_str",				gallery_tab_meta_str);
//	fun->Insert("gallery_tab_meta",					gallery_tab_meta);
//	fun->Insert("gallery_tab_meta_tab",				gallery_tab_meta_tab);
//	fun->Insert("gallery_tab_meta_tab_value",		gallery_tab_meta_tab_value);
//	fun->Insert("gallery_tab_meta_tab_has_next",	gallery_tab_meta_tab_has_next);
	fun->Insert("gallery_width",					gallery_width);
	fun->Insert("gallery_height",					gallery_height);
	fun->Insert("gallery_has_not_mount_type",		gallery_has_not_mount_type);
	fun->Insert("gallery_mount_type_arg_is",		gallery_mount_type_arg_is);
	fun->Insert("gallery_mount_theme_arg_is",		gallery_mount_theme_arg_is);
}


} // namespace



} // namespace Winix



