/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "gallery.h"

namespace Winix
{



namespace Gallery
{

Gallery::Gallery()
{
	fun.url = L"gallery";
}


void Gallery::SetGalleryInfo(GalleryInfo * pinfo)
{
	gallery_info = pinfo;
}



bool Gallery::has_access()
{

return true;
}



void Gallery::CreatePointers()
{
	gallery_info->item_sort_tab.resize(gallery_info->item_tab.size());

	for(size_t i=0 ; i<gallery_info->item_tab.size() ; ++i)
		gallery_info->item_sort_tab[i] = &gallery_info->item_tab[i];
}


void Gallery::SortPointers()
{

}


void Gallery::MakeGetDir()
{
//	iq.SetAll(false, false);
//	iq.sel_parent_id 	= true;
//	iq.sel_subject 		= true;
//	iq.sel_url 			= true;
//	iq.sel_type 		= true;
//	iq.sel_file 		= true;
//	iq.sel_sort_index 	= true;
//	iq.sel_meta 		= true;
//
//	iq.WhereParentId(cur->request->dir_tab.back()->id);
//	iq.WhereFileType(WINIX_ITEM_FILETYPE_IMAGE);

	morm::Finder<Item> finder(model_connector);

	finder.
		select().
		where().
		eq(L"parent_id", cur->request->dir_tab.back()->id).
		eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_IMAGE).
		get_vector(gallery_info->item_tab);

	//db->GetItems(gallery_info->item_tab, iq);
	CreatePointers();
}


void Gallery::MakeGetFile()
{
	gallery_info->item_sort_tab.resize(1);
	gallery_info->item_sort_tab[0] = &cur->request->item;
}



void Gallery::MakeGet()
{
	if( cur->request->is_item )
		MakeGetFile();
	else
		MakeGetDir();

	SortPointers();
}



} // namespace

} // namespace Winix

