/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "createticket.h"
#include "functions/functions.h"
#include "sessiondata.h"
#include "pluginmsg.h"

namespace Winix
{



namespace Ticket
{



CreateTicket::CreateTicket()
{
	fun.url = L"createticket";
}


void CreateTicket::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}


void CreateTicket::SetTicketInfo(TicketInfo * pinfo)
{
	ticket_info = pinfo;
}




bool CreateTicket::has_access()
{
	if( cur->request->is_item || !system->HasWriteAccess(*cur->request->dir_tab.back()) )
		return false;
	
	if( cur->mount->type != ticket_info->mount_type_ticket )
		return false;

return true;
}



void CreateTicket::AddTicket(Ticket & ticket, Item & item)
{
	ticket.file_id = item.id;
	cur->request->status = tdb->AddTicket(ticket);

	if( cur->request->status == WINIX_ERR_OK )
	{
		// sending notification
		notify_msg.code           = WINIX_NOTIFY_CODE_ADD;
		notify_msg.template_index = ticket_info->template_index;
		notify_msg.dir_link       = config->url_proto;
		notify_msg.dir_link      += config->base_url;// !! IMPROVE ME what about subdomains?
		system->dirs.MakePath(item.parent_id, notify_msg.dir_link, false);
		notify_msg.item_link      = notify_msg.dir_link;
		notify_msg.item_link     += item.url;

		system->notify.ItemChanged(notify_msg);

		// sending a message
		plugin->Call(WINIX_PL_TICKET_ADDED_NEW, &ticket, &item);
	}
}




void CreateTicket::Submit(Ticket & ticket, Item & item)
{
	if( functions->CheckAbuse() )
		return;

	functions->SetUser(item);
	item.item_content.privileges = system->NewFilePrivileges();
	item.parent_id  = cur->request->dir_tab.back()->id;

	plugin->Call(WINIX_PL_TICKET_PREPARE_TO_ADD_TICKET, &item, &ticket);

	// adding without notificating
	cur->request->status = system->AddFile(item, false) ? WINIX_ERR_OK : WINIX_ERR_PERMISSION_DENIED;

	if( cur->request->status == WINIX_ERR_OK )
		AddTicket(ticket, item);
	
	if( cur->request->status == WINIX_ERR_OK )
	{
		log << log2 << "CreateTicket: added a new ticket" << logend;
		RemoveTmpTicket();
		ticket_info->MakeRedirectIfPossible(item);
	}
	else
	{
		log << log1 << "CreateTicket: problem with adding a new ticket, error code: "
			<< cur->request->status << logend;
	}
}



void CreateTicket::RemoveTmpTicket()
{
	SessionData * session_data = reinterpret_cast<SessionData*>(
								 cur->session->plugin_data.Get(ticket_info->plugin_id) );

	long dir_id = cur->request->dir_tab.back()->id;
	session_data->create_ticket_map.erase(dir_id);
	session_data->create_space_map.erase(dir_id);
}



Ticket & CreateTicket::PrepareTicket()
{
	SessionData * session_data = reinterpret_cast<SessionData*>(
								 cur->session->plugin_data.Get(ticket_info->plugin_id) );

	bool is_new;
	long dir_id     = cur->request->dir_tab.back()->id;
	Ticket & ticket = session_data->GetTicket(dir_id, session_data->create_ticket_map, &is_new);
	ticket_info->create_new_ticket = is_new;

return ticket;
}


pt::Space & CreateTicket::PrepareSpace()
{
	SessionData * session_data = reinterpret_cast<SessionData*>(
								 cur->session->plugin_data.Get(ticket_info->plugin_id) );

	long dir_id = cur->request->dir_tab.back()->id;
	pt::Space & new_space = session_data->GetNewSpace(dir_id, session_data->create_space_map);

return new_space;
}


void CreateTicket::make_post()
{
	ticket_info->Clear();
	ticket_info->FindCurrentConf();

	Ticket & ticket     = PrepareTicket();
	pt::Space & meta    = PrepareSpace();
	Item & item         = cur->request->item;
	ticket_info->ticket = &ticket;
	ticket_info->item   = &item;

	bool file_was_deleted;

	ticket_info->ReadTicketParams(ticket, false, meta, file_was_deleted);
	functions->ReadItem(item, Item::file);

	ticket_info->CopyTicketSpace(meta, item);

	if( !cur->request->IsPostVar(L"fileuploadsubmit") && !file_was_deleted )
	{
		Submit(ticket, item);
		ticket_info->ticket = ticket_info->GetEmptyTicket();	// ticket was deleted by Submit() method -- RemoveTmpTicket() was used
	}

	cur->request->is_item = true;
	cur->request->models.Add(L"item", item);
}



void CreateTicket::MakeGet()
{
	ticket_info->Clear();
	ticket_info->FindCurrentConf();

	Ticket & ticket     = PrepareTicket();
	pt::Space & meta    = PrepareSpace();
	ticket_info->ticket = &ticket;
	ticket_info->item   = &cur->request->item;

	// copy meta info to display correctly new files
	ticket_info->CopyTicketSpace(meta, cur->request->item);
}




} // namespace

} // namespace Winix

