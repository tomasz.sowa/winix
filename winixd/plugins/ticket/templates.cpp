/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "ezc.h"
#include "ticketinfo.h"
#include "editticket.h"
#include "createticket.h"
#include "core/request.h"
#include "core/misc.h"
#include "core/plugin.h"
#include "functions/functions.h"
#include "templates/templates.h"
#include "sessiondata.h"
#include "plugins/thread/pluginmsg.h"
#include "space/space.h"
#include "pluginmsg.h"

namespace Winix
{



namespace Ticket
{
using namespace TemplatesFunctions;
using TemplatesFunctions::system;

extern TicketInfo   ticket_info;
extern CreateTicket fun_create_ticket;
extern EditTicket   fun_edit_ticket;


struct TicketValue
{
bool is_param;							// true if there is a paremeter (in config Space)

pt::Space * config_par;					// current space param (from current config) if is_param is true
										// when is_param is true then this pointer is never null

long param_id;							// param id (the same as config_par->Long(L"id"))

bool is_value;							// true if value for such a param was defined
										// (either in ticket_par or in meta)
										// *you have to check this variable first before using ticket_par or value_meta*

bool is_in_ticket_par;					// true if the value is defined in ticket.par_tab
										// else the value can be defined in meta (or not at all)

TicketParam * ticket_par;				// if is_in_ticket_par is true
pt::Space * value_meta;					// if is_in_ticket_par is false and if there is such a value in meta


	TicketValue()
	{
		Clear();
	}

	void Clear()
	{
		is_param	= false;
		config_par	= 0;
		param_id	= 0;
		is_value	= false;
		is_in_ticket_par = false;
		ticket_par  = 0;
		value_meta  = 0;
	}
};


int ticket_calc_progress_image_number(int percent)
{
	percent = int( double(percent) / 10.0 + 0.5 ) * 10;

	if( percent < 0 )
		percent = 0;

	if( percent > 100 )
		percent = 100;

return percent;
}



/*
	binary search for param in par_tab
	(par_tab must be sorted by 'param')
*/
bool find_ticket_param(long param, const Ticket::ParTab & par_tab, size_t & ticket_par_index)
{
	if( par_tab.empty() )
		return false;

	size_t o1 = 0;
	size_t o2 = par_tab.size() - 1;

	if( par_tab[o1].param == param )
	{
		ticket_par_index = o1;
		return true;
	}

	if( param < par_tab[o1].param )
		return false;

	if( par_tab[o2].param == param )
	{
		ticket_par_index = o2;
		return true;
	}

	if( param > par_tab[o2].param )
		return false;


	while( o1 + 1 < o2 )
	{
		ticket_par_index = (o1 + o2) / 2;

		if( par_tab[ticket_par_index].param == param )
			return true;

		if( par_tab[ticket_par_index].param < param )
			o1 = ticket_par_index;
		else
			o2 = ticket_par_index;
	}


return false;
}



/*
	looking for a subspace in meta which has id equal to param_id
	can return a null pointer
*/
pt::Space * find_ticket_param(long param_id, pt::Space & meta)
{
	pt::Space::TableType * params = meta.get_table(L"params");

	if( params )
	{
		for(pt::Space * sp : *params)
		{
			if( sp->to_long(L"id") == param_id )
				return sp;
		}
	}

	return nullptr;
}


/*
	first par_tab is searched for the specified value.param_id
	if it is found then value.is_in_ticket_par is set to true and value.ticket_par is pointing
	to the par_tab item
	if not then meta is searched and if found then value.is_in_ticket_par is false and 
	value.value_meta is set to the specified item in meta

	input:
		value.is_param
		value.config_par
		value.param_id
		par_tab
		meta
	output:
		value.is_value
		value.is_in_ticket_par
		value.ticket_par
		value.value_meta
*/
void find_ticket_value(TicketValue & value, Ticket::ParTab & par_tab, pt::Space & item_meta)
{
size_t par_index;

	if( !value.is_param )
		return;

	value.is_value = false;

	if( find_ticket_param(value.param_id, par_tab, par_index) )
	{
		value.is_value			= true;
		value.is_in_ticket_par	= true;
		value.ticket_par		= &par_tab[par_index];
		value.value_meta		= 0;
	}
	else
	{
		pt::Space * meta = item_meta.get_space(L"ticket");

		if( meta )
		{
			pt::Space * sp = find_ticket_param(value.param_id, *meta);

			if( sp )
			{
				value.is_value			= true;
				value.is_in_ticket_par	= false;
				value.ticket_par		= 0;
				value.value_meta		= sp;
			}
		}
	}
}


void ticket_print_value_select(Info & i, TicketValue & value)
{
	if( value.is_param && value.is_value )
	{
		pt::Space::TableType * options = value.config_par->get_table(L"options");

		if( options )
		{
			for(pt::Space * sp : *options)
			{
				if( sp->to_long(L"id") == value.ticket_par->intv )
				{
					std::wstring * val = sp->get_wstr(L"value");

					if( val )
						i.out << *val;

					break;
				}
			}
		}
	}
}


void ticket_print_value(Info & i, TicketValue & value)
{
	if( !value.is_param || !value.is_value )
		return;

	std::wstring * type = value.config_par->get_wstr(L"type");

	if( value.is_in_ticket_par )
	{
		if( type && *type == L"decimal" )
			i.out << value.ticket_par->decv;
		else
		if( type && *type == L"select" )
			ticket_print_value_select(i, value);
		else
			i.out << value.ticket_par->intv;
	}
	else
	{
		std::wstring * val = value.value_meta->get_wstr(L"value");

		if( val )
			i.out << *val;
	}
}



void ticket_can_create(Info & i)
{
	i.res = fun_create_ticket.has_access();
}


void ticket_can_edit(Info & i)
{
	i.res = fun_edit_ticket.has_access();
}


void ticket_is_creating_new(Info & i)
{
	i.res = ticket_info.create_new_ticket;
}

//void ticket_meta_value(Info & i)
//{
//	if( ticket_info.item )
//		space_value(i, ticket_info.item->item_content.meta);
//}

void ticket_is_closed(Info & i)
{
	pt::Space * ticket_space = ticket_info.item->item_content.admin_meta.get_space(L"ticket");

	if( ticket_space )
		i.res = ticket_space->to_bool(L"closed", false);
}


TicketValue value_for_param_id;


void ticket_param_value_for_param_id(Info & i)
{
	value_for_param_id.Clear();

	pt::Space & space = *ticket_info.cur_conf;
	int id = Toi(i.par);

	pt::Space::TableType * params = space.get_table(L"params");

	if( params )
	{
		for(pt::Space * sp : *params)
		{
			if( sp->to_int(L"id") == id )
			{
				value_for_param_id.Clear();
				value_for_param_id.is_param   = true;
				value_for_param_id.config_par = sp;
				value_for_param_id.param_id   = sp->to_long(L"id");

				if( ticket_info.ticket && ticket_info.item )
				{
					find_ticket_value(value_for_param_id, ticket_info.ticket->par_tab, ticket_info.item->item_content.meta);
					ticket_print_value(i, value_for_param_id);
				}

				break;
			}
		}
	}
}


// it takes two integer parameters (param id and value)
void ticket_does_param_id_have_value(Info & i)
{
	value_for_param_id.Clear();

	pt::Space & space = *ticket_info.cur_conf;
	pt::Space::TableType * params = space.get_table(L"params");

	if( i.params.size() == 2 && params )
	{
		long id  = Tol(i.params[0].str);
		long id2 = Tol(i.params[1].str);

		for(pt::Space * sp : *params)
		{
			if( sp->to_long(L"id") == id )
			{
				value_for_param_id.Clear();
				value_for_param_id.is_param   = true;
				value_for_param_id.config_par = sp;
				value_for_param_id.param_id   = sp->to_long(L"id");

				if( ticket_info.ticket && ticket_info.item )
				{
					find_ticket_value(value_for_param_id, ticket_info.ticket->par_tab, ticket_info.item->item_content.meta);

					if( value_for_param_id.is_value )
					{
						if( value_for_param_id.is_in_ticket_par )
						{
							i.res = value_for_param_id.ticket_par->intv == id2;
						}
						else
						{
							log << log1 << "Ticket: ticket_does_param_id_have_value cannot be used with meta values"
								<< logend;
						}
					}
				}

				break;
			}
		}
	}
}



namespace ns_tickets_tab
{

struct TicketsTabValue
{
	bool is_item;		// true if there is an item (item pointer is valid)
	bool is_ticket;		// true if there is a ticket (ticket pointer is valid)
	Item   * item;		// an item for that ticket (if is_item is true)
	Ticket * ticket;	// pointer to the ticket (if is_ticket is true)

	/*
		there can be three situations:
		 - is_item is false and is_ticket is false
		 - is_item is true and is_ticket is false
		 - is_item is true and is_ticket is true
	    so when is_ticket is true then is_item is true as well so you don't have to check it
	*/

	TicketsTabValue()
	{
		Clear();
	}

	void Clear()
	{
		is_item   = false;
		is_ticket = false;
		ticket = 0;
		item   = 0;
	}
};


	
// binary search for file_id in ticket_tab (ticket_tab must be sorted by 'file_id')
bool find_ticket(long file_id, const std::vector<Ticket> & ticket_tab, size_t & ticket_index)
{
	if( ticket_tab.empty() )
		return false;

	size_t o1 = 0;
	size_t o2 = ticket_tab.size() - 1;

	if( ticket_tab[o1].file_id == file_id )
	{
		ticket_index = o1;
		return true;
	}

	if( file_id < ticket_tab[o1].file_id )
		return false;

	if( ticket_tab[o2].file_id == file_id )
	{
		ticket_index = o2;
		return true;
	}

	if( file_id > ticket_tab[o2].file_id )
		return false;


	while( o1 + 1 < o2 )
	{
		ticket_index = (o1 + o2) / 2;

		if( ticket_tab[ticket_index].file_id == file_id )
			return true;

		if( ticket_tab[ticket_index].file_id < file_id )
			o1 = ticket_index;
		else
			o2 = ticket_index;
	}


return false;
}



static size_t tickets_tab_reqid = 0;
static TicketsTabValue tickets_value;
static TicketValue value;
static size_t item_sort_index;			// index for: ticket_info.item_sort_tab
static size_t conf_index;				// index for: ticket_info.cur_conf->spaces



void tickets_tab_check_reqid()
{
	if( tickets_tab_reqid != cur->request->id )
	{
		tickets_tab_reqid = cur->request->id;
		tickets_value.Clear();
		value.Clear();
	}
}


void tickets_tab(Info & i)
{
	tickets_tab_check_reqid();

	tickets_value.Clear();
	value.Clear();
	item_sort_index = i.iter;
	i.res = (item_sort_index < ticket_info.item_sort_tab.size());

	if( i.res )
	{
		tickets_value.is_item = true;
		tickets_value.item    = ticket_info.item_sort_tab[item_sort_index];
		long file_id          = ticket_info.item_sort_tab[item_sort_index]->id;
		size_t ticket_index;

		// !! IMPROVEME if there is no a ticket (find_ticket returns false)
		// may we add en ampty ticket and tickets_value.ticket will be pointing to it?

		// !! IMPROVEME change the  name of the message
		plugin->Call(WINIX_PL_TICKET_TICKETS_TAB_IS_NEXT, tickets_value.item);

		if( find_ticket(file_id, ticket_info.ticket_tab, ticket_index) )
		{
			tickets_value.is_ticket = true;
			tickets_value.ticket    = &ticket_info.ticket_tab[ticket_index];
		}
		else
		{
			log << log1 << "Ticket: I cannot find a ticket for file_id: " << file_id << logend;
		}

		plugin->Call(WINIX_PL_THREAD_SET_SORTTAB_INDEX, item_sort_index);
	}
}


void tickets_tab_is_closed(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_item )
	{
		pt::Space * ticket_space = tickets_value.item->item_content.admin_meta.get_space(L"ticket");

		if( ticket_space )
			i.res = ticket_space->to_bool(L"closed", false);
	}
}



void tickets_tab_url(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_item )
		i.out << tickets_value.item->url;
}


// !! IMPROVEME change name to tickets_tab_subject_is_empty
void tickets_tab_subject_empty(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_item )
		i.res = tickets_value.item->subject.empty();
}


void tickets_tab_subject(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_item )
		i.out << tickets_value.item->subject;
}


//void tickets_tab_meta_value(Info & i)
//{
//	tickets_tab_check_reqid();
//
//	if( tickets_value.is_item )
//		space_value(i, tickets_value.item->item_content.meta);
//}





void tickets_tab_date_creation(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_item )
		i.out << tickets_value.item->item_content.date_creation;

}


void tickets_tab_date_creation_nice(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_item )
		print_date_nice(i, tickets_value.item->item_content.date_creation);
}


static TicketValue value_for_param_id;


// takes param id (integer) as a first parameter
void tickets_tab_param_value_for_param_id(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_ticket )
	{
		pt::Space & space = *ticket_info.cur_conf;
		long id = Tol(i.par);

		pt::Space::TableType * params = space.get_table(L"params");

		if( params )
		{
			for(pt::Space * sp : *params)
			{
				if( sp->to_long(L"id") == id )
				{
					value_for_param_id.Clear();
					value_for_param_id.is_param   = true;
					value_for_param_id.config_par = sp;
					value_for_param_id.param_id   = sp->to_long(L"id");

					find_ticket_value(value_for_param_id, tickets_value.ticket->par_tab, tickets_value.item->item_content.meta);
					ticket_print_value(i, value_for_param_id);

					break;
				}
			}
		}
	}
}



// it takes two integer parameters (param id and value)
void tickets_tab_does_param_id_have_value(Info & i)
{
	tickets_tab_check_reqid();

	if( tickets_value.is_ticket && i.params.size() == 2 )
	{
		pt::Space & space = *ticket_info.cur_conf;
		long id  = Toi(i.params[0].str);
		long id2 = Toi(i.params[1].str);

		pt::Space::TableType * params = space.get_table(L"params");

		if( params )
		{
			for(pt::Space * sp : *params)
			{
				if( sp->to_long(L"id") == id )
				{
					value_for_param_id.Clear();
					value_for_param_id.is_param   = true;
					value_for_param_id.config_par = sp;
					value_for_param_id.param_id   = sp->to_long(L"id");

					find_ticket_value(value_for_param_id, tickets_value.ticket->par_tab, tickets_value.item->item_content.meta);

					if( value_for_param_id.is_value )
					{
						if( value_for_param_id.is_in_ticket_par )
						{
							i.res = value_for_param_id.ticket_par->intv == id2;
						}
						else
						{
							log << log1 << "Ticket: tickets_tab_does_param_id_have_value cannot be used with meta values"
								<< logend;
						}
					}

					break;
				}
			}
		}
	}
}


void tickets_tab_conf_tab(Info & i)
{
	tickets_tab_check_reqid();
	value.Clear();

	if( tickets_value.is_ticket )
	{
		conf_index = i.iter;
		pt::Space & space = *ticket_info.cur_conf;
		pt::Space::TableType * params = space.get_table(L"params");

		if( params )
		{
			i.res = conf_index < params->size();

			if( i.res )
			{
				value.is_param   = true;
				value.config_par = (*params)[conf_index];
				value.param_id   = value.config_par->to_long(L"id");
				find_ticket_value(value, tickets_value.ticket->par_tab, tickets_value.item->item_content.meta);
			}
		}
	}
}


void tickets_tab_conf_tab_has_value(Info & i)
{
	tickets_tab_check_reqid();

	i.res = value.is_value;
}


void tickets_tab_conf_tab_param_id(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_param )
		i.out << value.param_id;
}


// takes one parameter
void tickets_tab_conf_tab_param_id_is(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_param )
		i.res = (value.param_id == Tol(i.par));
}


void tickets_tab_conf_tab_param_name(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_param )
	{
		std::wstring * name = value.config_par->get_wstr(L"name");

		if( name )
			i.out << *name;
	}
}


void tickets_tab_conf_tab_value(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_value )
		ticket_print_value(i, value);
}


void tickets_tab_conf_tab_type_is(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_param )
		i.res = value.config_par->is_equal(L"type", i.par);
}


void tickets_tab_conf_tab_progress_image_number(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_value && value.is_in_ticket_par )
	{
		int progress = value.ticket_par->intv;
		i.out << ticket_calc_progress_image_number(progress);
	}
}






static size_t tickets_file_index;
static size_t tickets_file_number;

void tickets_tab_conf_tab_file_tab(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		tickets_file_index  = i.iter;

		if( i.iter == 0 )
			tickets_file_number = 0;

		pt::Space & mt = *value.value_meta;
		pt::Space::TableType * files = mt.get_table(L"files");

		if( files )
		{
			i.res = (tickets_file_index < files->size());

			if( i.res && i.iter > 0 )
				tickets_file_number += 1;
		}
	}
}


void tickets_tab_conf_tab_file_tab_index(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		pt::Space & mt = *value.value_meta;
		pt::Space::TableType * files = mt.get_table(L"files");

		if( files && tickets_file_index < files->size() )
		{
			i.out << tickets_file_number;
		}
	}
}



void tickets_tab_conf_tab_file_tab_path(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		pt::Space & mt = *value.value_meta;
		pt::Space::TableType * files = mt.get_table(L"files");

		if( files && tickets_file_index < files->size() )
		{
			(*files)[tickets_file_index]->to_wstr(L"path");
		}
	}
}


void tickets_tab_conf_tab_file_tab_itemid(Info & i)
{
	tickets_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		pt::Space & mt = *value.value_meta;
		pt::Space::TableType * files = mt.get_table(L"files");

		if( files && tickets_file_index < files->size() )
		{
			(*files)[tickets_file_index]->to_long(L"itemid");
		}
	}
}


//void tickets_tab_conf_tab_file_tab_meta(Info & i)
//{
//	tickets_tab_check_reqid();
//
//	if( value.is_value && !value.is_in_ticket_par )
//	{
//		pt::Space & mt = *value.value_meta;
//		pt::Space * sp = mt.find_child_space(tickets_file_index);
//
//		if( sp )
//			space_value(i, *sp);
//	}
//}
//


} // namespace ns_tickets_tab






namespace ns_ticket_tab
{
static size_t conf_index;			// index for: ticket_info.cur_conf->spaces
static TicketValue value;			// current ticket value
static size_t ticket_req_id = 0;
static size_t select_index;
static size_t ticket_file_index;
static size_t ticket_file_number;


void ticket_tab_check_reqid()
{
	if( ticket_req_id != cur->request->id )
	{
		ticket_req_id = cur->request->id;
		value.Clear();
	}
}


void ticket_tab(Info & i)
{
	ticket_tab_check_reqid();
	value.Clear();

	conf_index = i.iter;
	pt::Space & space = *ticket_info.cur_conf; // CHECKME is ticket_info.cur_conf always set?
	pt::Space::TableType * params = space.get_table(L"params");

	if( params )
	{
		i.res = conf_index < params->size();

		if( i.res )
		{
			value.is_param   = true;
			value.config_par = (*params)[conf_index];
			value.param_id   = value.config_par->to_long(L"id");

			if( ticket_info.ticket && ticket_info.item )
				find_ticket_value(value, ticket_info.ticket->par_tab, ticket_info.item->item_content.meta);
		}
	}
}



void ticket_tab_param_id(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
		i.out << value.param_id;
}


// takes one parameter
void ticket_tab_param_id_is(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
		i.res = (value.param_id == Tol(i.par));
}


void ticket_tab_param_name(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
		i.out << value.config_par->to_wstr(L"name");
}


void ticket_tab_has_value(Info & i)
{
	ticket_tab_check_reqid();

	i.res = value.is_value;
}


void ticket_tab_value(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_value )
		ticket_print_value(i, value);
}


void ticket_tab_value_int(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_value && value.is_in_ticket_par )
		i.out << value.ticket_par->intv;
}


void ticket_tab_value_dec(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_value && value.is_in_ticket_par )
		i.out << value.ticket_par->decv;
}


//void ticket_tab_value_meta(Info & i)
//{
//	ticket_tab_check_reqid();
//
//	if( value.is_value && !value.is_in_ticket_par )
//		space_value(i, *value.value_meta);
//}


void ticket_tab_type_is(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
		i.res = value.config_par->is_equal(L"type", i.par);
}


void ticket_tab_select_tab(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
	{
		select_index = i.iter;
		pt::Space::TableType * options = value.config_par->get_table(L"options");

		if( options )
		{
			i.res = (select_index < options->size());
		}
	}
}


void ticket_tab_select_tab_is_selected(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
	{
		pt::Space::TableType * options = value.config_par->get_table(L"options");

		if( options && select_index < options->size() )
		{
			pt::Space & sp = *(*options)[select_index];
			long id = sp.to_long(L"id");

			if( value.is_value )
			{
				if( value.is_in_ticket_par )
					i.res = (id == value.ticket_par->intv);
			}
			else
			{
				i.res = (id == sp.to_long(L"default"));
			}
		}
	}
}


void ticket_tab_select_tab_name(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
	{
		pt::Space::TableType * options = value.config_par->get_table(L"options");

		if( options && select_index < options->size() )
		{
			i.out << (*options)[select_index]->to_wstr(L"value");
		}
	}
}


void ticket_tab_select_tab_id(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_param )
	{
		pt::Space::TableType * options = value.config_par->get_table(L"options");

		if( options && select_index < options->size() )
		{
			i.out << (*options)[select_index]->to_wstr(L"id");
		}
	}
}

//void ticket_tab_select_tab_meta(Info & i)
//{
//	ticket_tab_check_reqid();
//
//	if( value.is_param )
//	{
//		pt::Space * sp = value.config_par->find_child_space(select_index);
//
//		if( sp )
//			space_value(i, *sp);
//	}
//}



void ticket_tab_file_tab(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		ticket_file_index = i.iter;

		if( i.iter == 0 )
			ticket_file_number = 0;
		
		pt::Space::TableType * files = value.value_meta->get_table(L"files");

		if( files )
		{
			i.res = (ticket_file_index < files->size());

			if( i.res && i.iter > 0 )
				ticket_file_number += 1;
		}
	}
}


void ticket_tab_file_tab_index(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		pt::Space::TableType * files = value.value_meta->get_table(L"files");

		if( files && ticket_file_index < files->size() )
		{
			i.out << ticket_file_number;
		}
	}
}


void ticket_tab_file_tab_path(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		pt::Space::TableType * files = value.value_meta->get_table(L"files");

		if( files && ticket_file_index < files->size() )
		{
			i.out << (*files)[ticket_file_index]->to_wstr(L"path");
		}
	}
}

void ticket_tab_file_tab_itemid(Info & i)
{
	ticket_tab_check_reqid();

	if( value.is_value && !value.is_in_ticket_par )
	{
		pt::Space::TableType * files = value.value_meta->get_table(L"files");

		if( files && ticket_file_index < files->size() )
		{
			i.out << (*files)[ticket_file_index]->to_long(L"itemid");
		}
	}
}

//void ticket_tab_file_tab_meta(Info & i)
//{
//	ticket_tab_check_reqid();
//
//	if( value.is_value && !value.is_in_ticket_par )
//	{
//		pt::Space * sp = value.value_meta->find_child_space(ticket_file_index);
//
//		if( sp )
//			space_value(i, *sp);
//	}
//}



} // namespace ns_ticket_tab





void AddEzcFunctions(PluginInfo & info)
{
using namespace ns_tickets_tab;
using namespace ns_ticket_tab;

	using TemplatesFunctions::EzcFun;
	EzcFun * fun = reinterpret_cast<EzcFun*>(info.p1);

	fun->Insert("ticket_can_create",							ticket_can_create);
	fun->Insert("ticket_can_edit",								ticket_can_edit);
	fun->Insert("ticket_is_creating_new",						ticket_is_creating_new);
//	fun->Insert("ticket_meta_value",							ticket_meta_value);
	fun->Insert("ticket_is_closed",								ticket_is_closed);

	fun->Insert("ticket_param_value_for_param_id",				ticket_param_value_for_param_id);
	fun->Insert("ticket_does_param_id_have_value",				ticket_does_param_id_have_value);

	fun->Insert("tickets_tab",									tickets_tab);
	fun->Insert("tickets_tab_is_closed",						tickets_tab_is_closed);
	fun->Insert("tickets_tab_url",								tickets_tab_url);
	fun->Insert("tickets_tab_subject_empty",					tickets_tab_subject_empty);
	fun->Insert("tickets_tab_subject",							tickets_tab_subject);
//	fun->Insert("tickets_tab_meta_value",						tickets_tab_meta_value);
	fun->Insert("tickets_tab_date_creation",					tickets_tab_date_creation);
	fun->Insert("tickets_tab_date_creation_nice",				tickets_tab_date_creation_nice);

	fun->Insert("tickets_tab_param_value_for_param_id",			tickets_tab_param_value_for_param_id);
	fun->Insert("tickets_tab_does_param_id_have_value",			tickets_tab_does_param_id_have_value);

	fun->Insert("tickets_tab_conf_tab",							tickets_tab_conf_tab);
	fun->Insert("tickets_tab_conf_tab_has_value",				tickets_tab_conf_tab_has_value);
	fun->Insert("tickets_tab_conf_tab_param_id",				tickets_tab_conf_tab_param_id);
	fun->Insert("tickets_tab_conf_tab_param_id_is",				tickets_tab_conf_tab_param_id_is);
	fun->Insert("tickets_tab_conf_tab_param_name",				tickets_tab_conf_tab_param_name);
	fun->Insert("tickets_tab_conf_tab_value",					tickets_tab_conf_tab_value);
	fun->Insert("tickets_tab_conf_tab_type_is",					tickets_tab_conf_tab_type_is);
	fun->Insert("tickets_tab_conf_tab_progress_image_number",	tickets_tab_conf_tab_progress_image_number);
	fun->Insert("tickets_tab_conf_tab_file_tab",				tickets_tab_conf_tab_file_tab);
	fun->Insert("tickets_tab_conf_tab_file_tab_index",			tickets_tab_conf_tab_file_tab_index);
	fun->Insert("tickets_tab_conf_tab_file_tab_path",			tickets_tab_conf_tab_file_tab_path);
	fun->Insert("tickets_tab_conf_tab_file_tab_itemid",			tickets_tab_conf_tab_file_tab_itemid);
//	fun->Insert("tickets_tab_conf_tab_file_tab_meta",			tickets_tab_conf_tab_file_tab_meta);


	fun->Insert("ticket_tab",									ticket_tab);
	fun->Insert("ticket_tab_param_id",							ticket_tab_param_id);
	fun->Insert("ticket_tab_param_id_is",						ticket_tab_param_id_is);
	fun->Insert("ticket_tab_param_name",						ticket_tab_param_name);
	fun->Insert("ticket_tab_has_value",							ticket_tab_has_value);
	fun->Insert("ticket_tab_value",								ticket_tab_value);
	fun->Insert("ticket_tab_value_int",							ticket_tab_value_int);
	fun->Insert("ticket_tab_value_dec",							ticket_tab_value_dec);
//	fun->Insert("ticket_tab_value_meta",						ticket_tab_value_meta);
	fun->Insert("ticket_tab_type_is",							ticket_tab_type_is);

	fun->Insert("ticket_tab_select_tab",						ticket_tab_select_tab);
	fun->Insert("ticket_tab_select_tab_is_selected",			ticket_tab_select_tab_is_selected);
	fun->Insert("ticket_tab_select_tab_name",					ticket_tab_select_tab_name);
	fun->Insert("ticket_tab_select_tab_id",						ticket_tab_select_tab_id);
//	fun->Insert("ticket_tab_select_tab_meta",					ticket_tab_select_tab_meta);

	fun->Insert("ticket_tab_file_tab",							ticket_tab_file_tab);
	fun->Insert("ticket_tab_file_tab_index",					ticket_tab_file_tab_index);
	fun->Insert("ticket_tab_file_tab_path",						ticket_tab_file_tab_path);
	fun->Insert("ticket_tab_file_tab_itemid",					ticket_tab_file_tab_itemid);
//	fun->Insert("ticket_tab_file_tab_meta",						ticket_tab_file_tab_meta);

}



} // namespace Ticket


} // namespace Winix



