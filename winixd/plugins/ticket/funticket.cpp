/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "funticket.h"
#include "plugins/thread/pluginmsg.h"

namespace Winix
{



namespace Ticket
{



FunTicket::FunTicket()
{
	fun.url = L"ticket";
}


void FunTicket::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}


void FunTicket::SetTicketInfo(TicketInfo * pinfo)
{
	ticket_info = pinfo;
}


bool FunTicket::has_access()
{
	return cur->request->is_item && system->HasReadAccess(cur->request->item);
}


void FunTicket::MakeGet()
{
	ticket_info->Clear();
	ticket_info->FindCurrentConf();
	ticket_info->ticket = &ticket;
	ticket_info->item   = &cur->request->item;

	tdb->GetTicket(cur->request->item.id, ticket);

	plugin->Call(WINIX_PL_THREAD_PREPARE_THREAD, cur->request->item.id);
}



} // namespace

} // namespace Winix

