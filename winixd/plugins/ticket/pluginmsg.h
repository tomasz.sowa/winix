/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_ticket_pluginmsg
#define headerfile_winix_plugins_ticket_pluginmsg

namespace Winix
{


// values from 4100 - 4199 reserved for 'ticket' plugin

// showtickets winix function is about to sort pointers to tickets
// in p1 you have pointer to std::vector<Item*> struct
// you can sort it in your own (in such a case you have to return true)
#define WINIX_PL_TICKET_SORT_POINTERS		4100


//
#define WINIX_PL_TICKET_CAN_MAKE_REDIRECT	4101


// calling ShowTicketsFromDir(long dir_id) method from ShowTickets object
// when tickets are read and sorted then WINIX_PL_TICKET_TICKETS_LOADED is called
// in l1 you shoud give the directory_id
// from the tickets have to be read
// and you can use only ezc functions to display the tickets
#define WINIX_PL_TICKET_LOAD_TICKETS		4102


// a ticket is going to add
// in p1 you have a pointer to the Item struct
// in p2 you have a pointer to the Ticket struct
#define WINIX_PL_TICKET_PREPARE_TO_ADD_TICKET	4103


// a next ticket will be displayed
// in p1 you have a pointer to the Item struct
// this is call from tickets_tab ezc function
#define WINIX_PL_TICKET_TICKETS_TAB_IS_NEXT		4104


// a ticket has been closed (closed button pressed)
// in p1 you have a pointer to the Item struct
// in p2 you have a pointer to the Ticket struct
#define WINIX_PL_TICKET_CLOSED					4105


// send by showtickets winix function when tickets are loaded (and sorted)
// in p1 you have a pointer to the ticket tab: std::vector<Ticket::Ticket> (include "ticket.h")
// in p2 you have a pointer to the sort tab:   std::vector<Item*>
// those tables don't have to be equal in size (in the case when there is no some tickets in the database)
#define WINIX_PL_TICKET_TICKETS_LOADED			4106


// a new ticket has been added
// in p1 you have a pointer to the Ticket::Ticket structure (include "ticket.h")
// in p2 you have a pointer to the Item structure
#define WINIX_PL_TICKET_ADDED_NEW				4107


// a ticket has been changed
// in p1 you have a pointer to the Ticket::Ticket structure (include "ticket.h")
// in p2 you have a pointer to the Item structure
#define WINIX_PL_TICKET_CHANGED					4108




} // namespace Winix

#endif


