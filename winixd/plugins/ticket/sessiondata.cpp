/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <algorithm>
#include "sessiondata.h"

namespace Winix
{


namespace Ticket
{

SessionData::SessionData()
{
	fun_rm = 0;
}



SessionData::~SessionData()
{
	Clear();
}


void SessionData::Clear()
{
	RemoveFiles(create_space_map, false);
	RemoveFiles(edit_space_map,   true);
}



void SessionData::RemoveFiles(SessionData::SpaceMap & space_map, bool only_stale_files)
{
SpaceMap::iterator i;

	if( fun_rm )
	{
		for(i=space_map.begin() ; i!=space_map.end() ; ++i)
		{
			if( only_stale_files )
				RemoveAllStaleFiles(i->second.new_space, i->second.old_space);
			else
				RemoveAllFiles(i->second.new_space);
		}

		space_map.clear();
	}
}



void SessionData::RemoveAllFiles(pt::Space & new_space)
{
	std::vector<long> new_file_tab;
	BuildFileList(new_file_tab, new_space);

	for(size_t i=0 ; i<new_file_tab.size() ; ++i)
		fun_rm->RemoveItemById(new_file_tab[i], false);
}



void SessionData::RemoveAllStaleFiles(pt::Space & new_space, pt::Space & old_space)
{
	std::vector<long> new_file_tab;
	std::vector<long> old_file_tab;
	size_t n = 0;
	size_t o = 0;

	BuildFileList(new_file_tab, new_space);
	BuildFileList(old_file_tab, old_space);

	std::sort(new_file_tab.begin(), new_file_tab.end());
	std::sort(old_file_tab.begin(), old_file_tab.end());

	// removes only those items present on new_file_tab
	// but not existing on old_space_tab

	while( n < new_file_tab.size() && o < old_file_tab.size() )
	{
		if( new_file_tab[n] < old_file_tab[o] )
		{
			fun_rm->RemoveFileOrSymlink(new_file_tab[n], false);
			n += 1;
		}
		else
		if( new_file_tab[n] == old_file_tab[o] )
		{
			n += 1;
			o += 1;
		}
		else
		{
			o += 1;
		}
	}

	while( n < new_file_tab.size() )
	{
		fun_rm->RemoveFileOrSymlink(new_file_tab[n], false);
		n += 1;
	}
}





void SessionData::BuildFileList(std::vector<long> & file_tab, pt::Space & space)
{
	file_tab.clear();

	pt::Space::TableType * child_table = space.get_table(L"params"); // CHECKME it was space.find_child_space_table();

	if( child_table )
	{
		for(pt::Space * sp : *child_table)
		{
			CheckFile(file_tab, *sp);
		}
	}
}


void SessionData::CheckFile(std::vector<long> & file_tab, pt::Space & space)
{
	pt::Space::TableType * child_table = space.get_table(L"files"); // CHECKME it was space.find_child_space_table();

	if( child_table )
	{
		for(pt::Space * sp : *child_table)
		{
			file_tab.push_back(sp->to_long(L"itemid"));
		}
	}
}




Ticket & SessionData::GetTicket(long id, SessionData::TicketMap & ticket_map, bool * is_new)
{
	std::pair<TicketMap::iterator, bool> res = ticket_map.insert( std::make_pair(id, Ticket()) );

	if( is_new )
		*is_new = res.second;

return res.first->second;
}



pt::Space & SessionData::GetOldSpace(long id, SpaceMap & space_map, bool * is_new)
{
	std::pair<SpaceMap::iterator, bool> res = space_map.insert( std::make_pair(id, SpacePair()) );

	if( is_new )
		*is_new = res.second;

return res.first->second.old_space;
}


pt::Space & SessionData::GetNewSpace(long id, SpaceMap & space_map, bool * is_new)
{
	std::pair<SpaceMap::iterator, bool> res = space_map.insert( std::make_pair(id, SpacePair()) );

	if( is_new )
		*is_new = res.second;

return res.first->second.new_space;
}


} // namespace



} // namespace Winix


