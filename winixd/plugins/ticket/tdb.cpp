/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "tdb.h"


namespace Winix::Ticket
{


bool TDb::IsTicket(long file_id)
{
	morm::Finder<TicketParam> finder(model_connector);

	// it would be better to have a SelectHelper in winix models
	// so we could use a 'select count(*)'
	TicketParam ticket_param = finder.select().where().eq(L"file_id", file_id).raw("LIMIT 1").get();

	return ticket_param.found();
}


void TDb::GetTicket(long file_id, Ticket & ticket)
{
	morm::Finder<TicketParam> finder(model_connector);

	ticket.Clear();
	ticket.file_id = file_id;
	ticket.par_tab = finder.select().where().eq(L"file_id", file_id).get_vector();
}


void TDb::GetTickets(const std::vector<long> & file_id_tab, std::vector<Ticket> & ticket_tab, bool clear_tab)
{
	if( clear_tab )
		ticket_tab.clear();

	if( !file_id_tab.empty() )
	{
		morm::Finder<TicketParam> finder(model_connector);
		std::vector<TicketParam> tmp;
		finder.select().where().in(L"file_id", file_id_tab).raw("ORDER BY file_id, param").get_vector(tmp);

		long last_file_id = -1;
		empty_ticket.Clear();

		for(size_t i=0 ; i < tmp.size() ; ++i)
		{
			if( i==0 || last_file_id != tmp[i].file_id )
			{
				ticket_tab.push_back(empty_ticket);
				ticket_tab.back().file_id = tmp[i].file_id;
				last_file_id = tmp[i].file_id;
			}

			ticket_tab.back().par_tab.push_back(tmp[i]);
		}
	}
}


bool TDb::AddTicket(Ticket & ticket)
{
	bool status = true;

	for(TicketParam & ticket_param : ticket.par_tab)
	{
		if( !ticket_param.insert() )
			status = false;
	}

	return status;
}


bool TDb::RemoveAddTicket(Ticket & ticket)
{
	morm::Transaction transaction(model_connector);
	bool ok = true;

	ok = ok && RemoveTicket(ticket.file_id);
	ok = ok && AddTicket(ticket);

	transaction.set_successful(ok);

	if( !transaction.finish() )
		ok = false;

	return ok;
}


bool TDb::RemoveTicket(long file_id)
{
	bool status = false;

	if( model_connector )
	{
		morm::DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			pt::TextStream sql;
			sql << "DELETE FROM plugins.ticket WHERE file_id=" << file_id;

			status = db_connector->query(sql);
		}
	}

	return status;
}


}

