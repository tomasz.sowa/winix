/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <ctime>
#include <algorithm>
#include "showtickets.h"
#include "pluginmsg.h"
#include "plugins/thread/pluginmsg.h"


namespace Winix
{



namespace Ticket
{



ShowTickets::ShowTickets()
{
	fun.url = L"showtickets";
}


void ShowTickets::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}


void ShowTickets::SetTicketInfo(TicketInfo * pinfo)
{
	ticket_info = pinfo;
}



bool ShowTickets::has_access()
{
	return !cur->request->is_item;
}



bool ShowTickets::Sort::operator()(const Item * item1, const Item * item2)
{
	if( sort_type == 0 )
	{
		// sorting by url
		return item1->url < item2->url;
	}
	else
	{
		// sorting by sort_index and if equal then by date

		if( item1->sort_index != item2->sort_index )
			return item1->sort_index < item2->sort_index;

		return item1->item_content.date_creation > item2->item_content.date_creation;
	}
}




void ShowTickets::ReadFiles(long dir_id)
{
	// reading files
//	DbItemQuery iq;
//	iq.SetAll(false, false);
//	iq.sel_url     = true;
//	iq.sel_subject = true;
//	iq.sel_date    = true;
//	iq.sel_user_id = true;
//	iq.sel_group_id   = true;
//	iq.sel_guest_name = true;
//	iq.sel_privileges = true;
//	iq.sel_date       = true;
//	iq.sel_meta       = true;
//	iq.sel_sort_index = true;
//	iq.WhereParentId(dir_id);
//	iq.WhereType(Item::file);
//	iq.WhereFileType(WINIX_ITEM_FILETYPE_NONE);

	morm::Finder<Item> finder(model_connector);

	finder.
		select().
		where().
		eq(L"parent_id", dir_id).
		eq(L"type", static_cast<int>(Item::file)).
		eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_NONE).
		get_vector(ticket_info->item_tab);

	//db->GetItems(ticket_info->item_tab, iq);
	system->CheckAccessToItems(ticket_info->item_tab);
}



void ShowTickets::CreatePointers()
{
	// creating a pointers table
	ticket_info->item_sort_tab.resize(ticket_info->item_tab.size());

	for(size_t i=0 ; i<ticket_info->item_tab.size() ; ++i)
		ticket_info->item_sort_tab[i] = &ticket_info->item_tab[i];
}



void ShowTickets::SortPointers()
{
	PluginRes res = plugin->Call(WINIX_PL_TICKET_SORT_POINTERS, &ticket_info->item_sort_tab);

	if( res.res_true > 0 )
		return;

	int sort_type = 1;

	if( cur->request->ParamValue(L"sort") == L"url" )
		sort_type = 0;

	std::vector<Item*> & table = ticket_info->item_sort_tab;
	std::sort(table.begin(), table.end(), Sort(this, sort_type));
}



void ShowTickets::ReadTickets()
{
	// reading tickets for the files
	file_id_tab.resize(ticket_info->item_sort_tab.size());

	for(size_t i=0 ; i<ticket_info->item_sort_tab.size() ; ++i)
		file_id_tab[i] = ticket_info->item_sort_tab[i]->id;

	tdb->GetTickets(file_id_tab, ticket_info->ticket_tab);
}


void ShowTickets::ShowTicketsFromDir(long dir_id)
{
	ticket_info->Clear();
	ticket_info->FindCurrentConf(dir_id);

	ReadFiles(dir_id);
	CreatePointers();
	SortPointers();
	ReadTickets();

	plugin->Call(WINIX_PL_TICKET_TICKETS_LOADED, &ticket_info->ticket_tab, &ticket_info->item_sort_tab);

	// !! IMPROVE ME may a better name instead of WINIX_PL_THREAD_SET_SORTTAB?
	plugin->Call(WINIX_PL_THREAD_SET_SORTTAB, &ticket_info->item_sort_tab);
	plugin->Call(WINIX_PL_THREAD_READ_THREADS);
}



void ShowTickets::MakeGet()
{
	ShowTicketsFromDir(cur->request->dir_tab.back()->id);
}



} // namespace

} // namespace Winix

