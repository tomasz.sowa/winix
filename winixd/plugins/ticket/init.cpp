/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "tdb.h"
#include "funticket.h"
#include "editticket.h"
#include "createticket.h"
#include "showtickets.h"
#include "sessiondata.h"
#include "plugins/thread/showthreads.h"
#include "plugins/thread/pluginmsg.h"
#include "pluginmsg.h"

namespace Winix
{



extern "C" void Init(PluginInfo &);


namespace Ticket
{
const wchar_t plugin_name[] = L"ticket";

TDb tdb;
TicketInfo ticket_info;

FunTicket    fun_ticket;
CreateTicket fun_create_ticket;
EditTicket   fun_edit_ticket;
ShowTickets  fun_show_tickets;


void AddFunctions(PluginInfo & info)
{
	info.functions->Add(fun_ticket);
	info.functions->Add(fun_create_ticket);
	info.functions->Add(fun_edit_ticket);
	info.functions->Add(fun_show_tickets);
}


void SelectDefaultFunction(PluginInfo & info)
{
	if( info.system->mounts.pmount->type == ticket_info.mount_type_ticket )
	{
		// !! moze dac fun_ticket tylko dla itemow ktore nie sa plikami statycznymi...

		if( info.cur->request->is_item )
			info.cur->request->function = &fun_ticket;
		else
			info.cur->request->function = &fun_show_tickets;
	}
}


void AddMounts(PluginInfo & info)
{
	Mounts & m = info.system->mounts;
	
	ticket_info.mount_type_ticket		= m.AddMountType(L"ticket");
	ticket_info.mount_par_ticket_conf	= m.AddMountPar(L"ticket_conf");
}




void FstabChanged(PluginInfo & info)
{
	ticket_info.ReadTicketConf(true);
}


void ProcessRequest(PluginInfo & info)
{
	if( info.cur->request->function == &info.functions->fun_reload )
	{
		if( info.cur->request->IsParam(L"tickets") )
			ticket_info.ReadTicketConf();
	}
}




void RemoveTicket(PluginInfo & info)
{
	// !! dodac nowy komunikat PREPERE_TO_REMOVE
	// i tam korzystajac z meta informacji w item
	// usunac odpowiednie pliki
	const Item * item = reinterpret_cast<Item*>(info.p1);

	if( item )
		ticket_info.RemoveTicket(item->id);
}


void InitTicket(PluginInfo & info)
{
	ticket_info.progress_prefix = info.config->Text(L"ticket_form_progress_prefix", L"progress");
	info.res = true;
}


void AddNotifyTemplate(PluginInfo & info)
{
	ticket_info.template_index = info.system->notify.AddTemplate(L"notify_email_ticket.txt");
}


void CreateSession(PluginInfo & info)
{
	if( info.cur->session )
	{
		SessionData * p = new SessionData();

		p->fun_rm = &info.functions->fun_rm;
		info.cur->session->plugin_data.Assign(info.plugin->current_plugin, p);
		info.log << log4 << "Ticket: created ticket plugin data: " << (void*)p << logend;
	}
}


void RemoveSession(PluginInfo & info)
{
	if( info.plugin_data_base )
	{
		info.plugin_data_base->Clear();
		delete info.plugin_data_base;
		info.log << log4 << "Ticket: removed ticket plugin data: " << (void*)info.plugin_data_base << logend;
	}
}


void EndRequest(PluginInfo & info)
{
	ticket_info.Clear();
}


void ShowTicketsFromDir(PluginInfo & info)
{
	fun_show_tickets.ShowTicketsFromDir(info.l1);
}





// defined in templates.cpp
void AddEzcFunctions(PluginInfo & info);

} // namespace Ticket



void Init(PluginInfo & info)
{
using namespace Ticket;

	info.plugin->Assign(WINIX_PLUGIN_INIT,                	InitTicket);
	info.plugin->Assign(WINIX_TEMPLATES_CREATEFUNCTIONS,	AddEzcFunctions);
	info.plugin->Assign(WINIX_CREATE_FUNCTIONS,				AddFunctions);
	info.plugin->Assign(WINIX_SELECT_DEFAULT_FUNCTION,		SelectDefaultFunction);
	info.plugin->Assign(WINIX_ADD_MOUNTS,					AddMounts);
	info.plugin->Assign(WINIX_FSTAB_CHANGED,				FstabChanged);
	info.plugin->Assign(WINIX_PROCESS_REQUEST,				ProcessRequest);
	info.plugin->Assign(WINIX_FILE_REMOVED,					RemoveTicket);
	info.plugin->Assign(WINIX_NOTIFY_ADD_TEMPLATE,			AddNotifyTemplate);
	info.plugin->Assign(WINIX_SESSION_CREATED,				CreateSession);
	info.plugin->Assign(WINIX_PLUGIN_SESSION_DATA_REMOVE,	RemoveSession);
	info.plugin->Assign(WINIX_END_REQUEST,                	EndRequest);
	info.plugin->Assign(WINIX_PL_TICKET_LOAD_TICKETS,		ShowTicketsFromDir);

	tdb.set_dependency((WinixModelDeprecated*)info.functions);

	ticket_info.set_dependency((WinixModelDeprecated*)info.functions);
	ticket_info.SetTDb(&tdb);
	//ticket_info.SetConfig(info.config);
	ticket_info.SetSystem(info.system);
	ticket_info.SetCur(info.cur);
	ticket_info.SetFunctions(info.functions);
	ticket_info.plugin_id = info.plugin_id;

	fun_ticket.SetTDb(&tdb);
	fun_ticket.SetTicketInfo(&ticket_info);

	fun_edit_ticket.SetTDb(&tdb);
	fun_edit_ticket.SetTicketInfo(&ticket_info);

	fun_create_ticket.SetTDb(&tdb);
	fun_create_ticket.SetTicketInfo(&ticket_info);

	fun_show_tickets.SetTDb(&tdb);
	fun_show_tickets.SetTicketInfo(&ticket_info);

	info.p1 = (void*)(plugin_name);
}



} // namespace Winix

