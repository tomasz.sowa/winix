/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_ticket_sessiondata
#define headerfile_winix_plugins_ticket_sessiondata

#include <vector>
#include <string>
#include <map>
#include "core/plugindata.h"
#include "ticket.h"
#include "functions/rm.h"
#include "space/space.h"

namespace Winix
{


namespace Ticket
{


struct SessionData : public PluginDataBase
{
	SessionData();
	~SessionData();

	struct SpacePair
	{
		pt::Space new_space;
		pt::Space old_space;

		// old_space is used when editing an existing ticket
		// current ticket space is copied to old_space
		// and when the session expires only new added files 
		// are deleted
	};

	virtual void Clear();

	typedef std::map<long, Ticket>    TicketMap;
	typedef std::map<long, SpacePair> SpaceMap;


	// temporary tickets for 'createticket' function
	// <parent_dir_id, Ticket>
	TicketMap create_ticket_map;

	// temporary tickets for 'editticket' function
	// <file_id, Ticket>
	TicketMap edit_ticket_map;


	// temporary spaces for 'createticket' function
	// these files should be deleted if a user will not click on the submit button
	SpaceMap create_space_map;

	// temporary spaces for 'editticket' function
	// these files should be deleted if a user will not click on the submit button
	SpaceMap edit_space_map;


	// inserting and returning a new ticket or just returning the ticket if it exists
	Ticket & GetTicket(long id, TicketMap & ticket_map, bool * is_new = 0);

	// inserting and returning a new/old space or just returning the space if it exists
	pt::Space & GetOldSpace(long id, SpaceMap & space_map, bool * is_new = 0);
	pt::Space & GetNewSpace(long id, SpaceMap & space_map, bool * is_new = 0);

	// for deleting files
	Fun::Rm * fun_rm;

private:

	void RemoveFiles(SpaceMap & space_map, bool only_stale_files);
	void RemoveAllFiles(pt::Space & new_space);
	void RemoveAllStaleFiles(pt::Space & new_space, pt::Space & old_space);

	void BuildFileList(std::vector<long> & file_tab, pt::Space & space);
	void CheckFile(std::vector<long> & file_tab, pt::Space & space);

};


} // namespace

} // namespace Winix

#endif
