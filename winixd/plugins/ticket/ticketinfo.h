/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_ticket_ticketinfo
#define headerfile_winix_plugins_ticket_ticketinfo

#include <vector>
#include "ticket.h"
#include "core/system.h"
#include "functions/functions.h"
#include "tdb.h"
#include "space/spaceparser.h"
#include "core/winixmodeldeprecated.h"
#include "models/item.h"


namespace Winix
{


namespace Ticket
{


struct TicketConfWrap
{
	bool to_delete;
	std::wstring file_name;
	pt::Space conf;

	TicketConfWrap()
	{
		to_delete = false;
	}
};


class TicketInfo : public WinixModelDeprecated
{
public:

	TicketInfo();

	void SetTDb(TDb * ptdb);
	//void SetConfig(Config * pconfig);
	void SetSystem(System * psystem);
	void SetCur(Cur * pcur);
	void SetFunctions(Functions * pfunctions);

	void Clear();

	// current ticket for templates
	Ticket * ticket;

	// current item for templates
	// !! zobaczyc czy sie da na conts zamienic
	// teraz zmieniona obsluga templatow wiec moze sie da
	Item * item;

	// true if 'createticket' function is creating a new ticket
	bool create_new_ticket;

	// for displaying all tickets in a directory
	std::vector<Item>   item_tab; // !! in the future we'll use cur->request->item_tab[number] (an array of arrays)
	std::vector<Ticket> ticket_tab;
	std::vector<Item*>  item_sort_tab;


	// <dir_id, TicketConfWrap>
	typedef std::map<long, TicketConfWrap> ConfTab;
	ConfTab conf_tab;

	// current ticket config
	// will be set by FindCurrentConf()
	TicketConfWrap * cur_conf_wrap;
	pt::Space      * cur_conf;

	// default: "progress"
	// can be set in config in option: ticket_form_progress_prefix
	std::wstring progress_prefix;

	// ticket plugin id
	int plugin_id;


	int mount_type_ticket;
	int mount_par_ticket_conf;


	// template index for notifications
	size_t template_index;


	void ReadTicketConf(bool skip_existing_configs = false);
	void FindCurrentConf(long dir_id);
	void FindCurrentConf();
	void ReadTicketParams(Ticket & ticket, bool clear_ticket, pt::Space & meta, bool & file_was_deleted);
	void RemoveTicket(long file_id);
	void CopyTicketSpace(pt::Space & ticket_space, Item & item);

	void MakeRedirectIfPossible(const Item & item);

	// may now it is better to use null pointers instead of this empty objects?
	Ticket * GetEmptyTicket() { return &ticket_empty; };
	//Item *   GetEmptyItem();

private:

	TDb       * tdb;
	//Config    * config;
	System    * system;
	Cur       * cur;
	Functions * functions;

	// for reading parameters
	TicketParam ticket_param;

	// for adding a new image/file to a ticket
	Item file;
	std::wstring file_path;

	// for reading config file
	pt::SpaceParser conf_parser;
	std::vector<Item*> config_dir_tab;
	Item config_file;

	// for Clearing
	// (we don't want to use empty pointers)
	TicketConfWrap cur_conf_wrap_empty;
	pt::Space      cur_conf_empty;
	Ticket         ticket_empty;
	Item           item_empty;

	bool GetConfContent(const std::wstring & path);
	bool ParseTicketConf(long mount_dir_id, const std::wstring & path);
	void ReadTicketConf(Mounts & mounts, bool skip_existing_configs);
	void MarkAllConfToDelete();
	void DeleteAllMarkedConf();
	void ReadTicketConf(const Mount & mount, bool skip_existing_configs);
	pt::Space & FindAddMetaByParam(pt::Space & meta, long param);


	void CheckMinMaxValue(pt::Space & space, TicketParam & par);
	bool ReadTicketValue(pt::Space & space, long param_id, TicketParam & par, const std::wstring & value, pt::Space & meta);
	void ReadTicketValue(pt::Space & space, long param_id, const PostFile & value, pt::Space & meta, Item & upload_dir);
	void ReadTicketValue(pt::Space & space, long param_id, const PostFile & value, pt::Space & meta);
	void ReadTicketParam(pt::Space & space, Ticket & ticket, long param_id, const std::wstring & value, pt::Space & meta);
	void ReadTicketParam(Ticket & ticket, long param_id, const std::wstring & value, pt::Space & meta);
	void ReadTicketParam(long param_id, const PostFile & value, pt::Space & meta);
	bool DeleteTicketFile(Ticket & ticket, long file_id, pt::Space & meta);


};




}

} // namespace Winix

#endif
