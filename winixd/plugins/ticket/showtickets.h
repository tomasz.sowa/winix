/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_ticket_showtickets
#define headerfile_winix_plugins_ticket_showtickets

#include "tdb.h"
#include "ticketinfo.h"
#include "functions/functionbase.h"
#include "models/item.h"


namespace Winix
{


namespace Ticket
{


class ShowTickets : public FunctionBase
{
public:

	ShowTickets();
	void SetTDb(TDb * ptdb);
	void SetTicketInfo(TicketInfo * pinfo);

	bool has_access();
	void MakeGet();

	void ShowTicketsFromDir(long dir_id);

private:

	TDb * tdb;
	TicketInfo * ticket_info;
	std::vector<long> file_id_tab;

	void ReadFiles(long dir_id);
	void CreatePointers();
	void SortPointers();
	void ReadTickets();
	
	struct Sort
	{
		ShowTickets * show_tickets;
		int sort_type;
		Sort(ShowTickets * show, int type) : show_tickets(show), sort_type(type) {}
		bool operator()(const Item * item1, const Item * item2);
	};

};


} // namespace

} // namespace Winix

#endif
