/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "editticket.h"
#include "functions/functions.h"
#include "sessiondata.h"
#include "pluginmsg.h"

namespace Winix
{



namespace Ticket
{



EditTicket::EditTicket()
{
	fun.url = L"editticket";
}


void EditTicket::SetTDb(TDb * ptdb)
{
	tdb = ptdb;
}


void EditTicket::SetTicketInfo(TicketInfo * pinfo)
{
	ticket_info = pinfo;
}



bool EditTicket::has_access()
{
	// not logged users cannot edit tickets
	// !! we are waiting for the sticky bit
	if( !cur->session->puser )
		return false;

	if( !cur->request->is_item || !system->HasWriteAccess(cur->request->item) )
		return false;
	
	if( cur->mount->type != ticket_info->mount_type_ticket )
		return false;

	// !! CHECKME what about closing threads?

return true;
}




void EditTicket::ChangeTicket(Ticket & ticket, Item & item)
{
	cur->request->status = tdb->RemoveAddTicket(ticket);

	if( cur->request->status == WINIX_ERR_OK )
	{
		// sending notification

		notify_msg.code           = WINIX_NOTIFY_CODE_EDIT;
		notify_msg.template_index = ticket_info->template_index;
		notify_msg.dir_link       = config->url_proto;
		notify_msg.dir_link      += config->base_url;// !! IMPROVE ME what about subdomains?
		system->dirs.MakePath(item.parent_id, notify_msg.dir_link, false);
		notify_msg.item_link      = notify_msg.dir_link;
		notify_msg.item_link     += item.url;

		system->notify.ItemChanged(notify_msg);

		// sending a message
		plugin->Call(WINIX_PL_TICKET_CHANGED, &ticket, &item);
	}
}


void EditTicket::Submit(Ticket & ticket, Item & item)
{
	if( functions->CheckAbuse() )
		return;

	// adding without notificating
	if( system->EditFile(item, old_url != item.url) )
		cur->request->status = WINIX_ERR_OK;
	else
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;

	if( cur->request->status == WINIX_ERR_OK )
		ChangeTicket(ticket, item);
	
	if( cur->request->status == WINIX_ERR_OK )
	{
		log << log2 << "EditTicket: ticket modified" << logend;
		RemoveTmpTicket();
		ticket_info->MakeRedirectIfPossible(item);
	}
	else
	{
		log << log1 << "EditTicket: problem with editing a ticket, error code: "
			<< cur->request->status << logend;

		// !! add session log
	}
}




void EditTicket::RemoveTmpTicket()
{
	SessionData * session_data = reinterpret_cast<SessionData*>(
								 cur->session->plugin_data.Get(ticket_info->plugin_id) );

	long file_id = cur->request->item.id;
	session_data->edit_ticket_map.erase(file_id);
	session_data->edit_space_map.erase(file_id);
}



Ticket & EditTicket::PrepareTicket()
{
	SessionData * session_data = reinterpret_cast<SessionData*>(
								 cur->session->plugin_data.Get(ticket_info->plugin_id) );

	bool is_new;
	long file_id = cur->request->item.id;
	Ticket & ticket = session_data->GetTicket(file_id, session_data->edit_ticket_map, &is_new);

	if( is_new )
		tdb->GetTicket(file_id, ticket);
	else
		ticket.file_id = file_id;

return ticket;
}



pt::Space & EditTicket::PrepareSpace()
{
	SessionData * session_data = reinterpret_cast<SessionData*>(
								 cur->session->plugin_data.Get(ticket_info->plugin_id) );

	bool is_new;
	long file_id = cur->request->item.id;
	pt::Space & new_space = session_data->GetNewSpace(file_id, session_data->edit_space_map, &is_new);

	if( is_new )
	{
		pt::Space * ticket_space = cur->request->item.item_content.meta.get_space(L"ticket"); // CHECKME it was find_child_space(L"ticket");

		if( ticket_space )
		{
			new_space = *ticket_space;
			pt::Space & old_space = session_data->GetOldSpace(file_id, session_data->edit_space_map);
			old_space = new_space;
		}
	}

return new_space;
}


bool EditTicket::CloseTicket()
{
	cur->request->item.propagate_connector();

	pt::Space & ticket_space = cur->request->item.item_content.admin_meta.get_add_space(L"ticket"); //CHECKME it was find_add_child_space(L"ticket");
	ticket_space.add(L"closed", true);

	pt::Space & thread_space = cur->request->item.item_content.admin_meta.get_add_space(L"thread"); //CHECKME it was find_add_child_space(L"thread");
	thread_space.add(L"closed", true);

	//if( db->EditAdminMetaById(cur->request->item.ameta, cur->request->item.id) == WINIX_ERR_OK )
	if( cur->request->item.item_content.update() )
	{
		log << log3 << "EditTicket: closing ticket" << logend;
		return true;
	}

return false;
}




void EditTicket::make_post()
{
	ticket_info->Clear();
	ticket_info->FindCurrentConf();

	Ticket & ticket     = PrepareTicket();
	pt::Space & meta    = PrepareSpace();
	Item & item         = cur->request->item;

	old_url             = item.url;
	ticket_info->ticket = &ticket;
	ticket_info->item   = &item;


	if( cur->request->IsParam(L"close") )
	{
		if( CloseTicket() )
			plugin->Call(WINIX_PL_TICKET_CLOSED, ticket_info->item, ticket_info->ticket);

		ticket_info->MakeRedirectIfPossible(*cur->request->last_item);
	}
	else
	{
		bool file_was_deleted;
		ticket_info->ReadTicketParams(ticket, false, meta, file_was_deleted);
		functions->ReadItem(item, Item::file);

		ticket_info->CopyTicketSpace(meta, item);

		if( !cur->request->IsPostVar(L"fileuploadsubmit") && !file_was_deleted )
		{
			Submit(ticket, item);
			ticket_info->ticket = ticket_info->GetEmptyTicket(); // ticket was deleted by Submit() method -- RemoveTmpTicket() was used
		}
	}
}



void EditTicket::MakeGet()
{
	ticket_info->Clear();
	ticket_info->FindCurrentConf();

	Ticket & ticket     = PrepareTicket();
	pt::Space & meta    = PrepareSpace();
	ticket_info->ticket = &ticket;
	ticket_info->item   = &cur->request->item;

	// copy meta info to display correctly new files
	ticket_info->CopyTicketSpace(meta, cur->request->item);
}



} // namespace

} // namespace Winix

