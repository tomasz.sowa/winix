/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "ezc.h"
#include "core/misc.h"
#include "stats.h"
#include "templates/misc.h"

namespace Winix
{



namespace Stats
{
using TemplatesFunctions::Info;

extern long current_item_id;
extern Stats stats;


void stats_from(Info & i)
{
	i.out << DateToStrWithoutHours(stats.stats_start);

}

void stats_all(Info & i)
{
	i.out << stats.global_all;
}


void stats_unique(Info & i)
{
	i.out << stats.global_unique;
}


void stats_google(Info & i)
{
	i.out << stats.global_google;
}


void stats_yahoo(Info & i)
{
	i.out << stats.global_yahoo;
}


void stats_bing(Info & i)
{
	i.out << stats.global_bing;
}



void stats_item_all(Info & i)
{
	i.out << stats.stats_tab[current_item_id].all;
}


void stats_item_google(Info & i)
{
	i.out << stats.stats_tab[current_item_id].google;
}

void stats_item_yahoo(Info & i)
{
	i.out << stats.stats_tab[current_item_id].yahoo;
}

void stats_item_bing(Info & i)
{
	i.out << stats.stats_tab[current_item_id].bing;
}




void CreateFunctions(PluginInfo & info)
{
	using TemplatesFunctions::EzcFun;
	EzcFun * fun = reinterpret_cast<EzcFun*>(info.p1);

	fun->Insert("stats_from",        stats_from);

	fun->Insert("stats_all",         stats_all);
	fun->Insert("stats_unique",      stats_unique);
	fun->Insert("stats_google",      stats_google);
	fun->Insert("stats_yahoo",       stats_yahoo);
	fun->Insert("stats_bing",        stats_bing);

	fun->Insert("stats_item_all",    stats_item_all);
	fun->Insert("stats_item_google", stats_item_google);
	fun->Insert("stats_item_yahoo",  stats_item_yahoo);
	fun->Insert("stats_item_bing",   stats_item_bing);
}


} // namespace


} // namespace Winix

