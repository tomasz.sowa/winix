/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <ctime>
#include "stats.h"
#include "core/log.h"
#include "utf8/utf8.h"

namespace Winix
{



namespace Stats
{



Stats::Stats()
{
	global_all    = 0;
	global_unique = 0;
	global_google = 0;
	global_yahoo  = 0;

	req_save_freq = 1000;
	req_current   = 0;

	stats_start = time(0);
}


void Stats::ReadStats(std::ifstream & file)
{
	file >> stats_start;
	file >> global_all;
	file >> global_unique;
	file >> global_google;
	file >> global_yahoo;
	file >> global_bing;

	size_t len;
	file >> len;

	ItemStats s;
	long item_id;

	for(size_t i = 0 ; i<len && !file.eof() ; ++i)
	{
		file >> item_id;
		file >> s.all;
		file >> s.google;
		file >> s.yahoo;
		file >> s.bing;

		stats_tab.insert( std::make_pair(item_id, s) );
	}
}




void Stats::ReadStats()
{
	if( stats_file.empty() )
		return;
	
	pt::wide_to_utf8(stats_file, astats_file);
	std::ifstream file(astats_file.c_str());

	if( !file )
	{
		log << log1 << "Stats: I cannot open a file: " << astats_file << logend;
		return;
	}

	ReadStats(file);
	file.close();

	log << log3 << "Stats: statistics loaded from: " << astats_file
		<< " (" << stats_tab.size() << " items)" << logend;
}




void Stats::SaveStats(std::ofstream & file)
{
	file << stats_start << std::endl;

	file << global_all		<< ' ';
	file << global_unique	<< ' ';
	file << global_google	<< ' ';
	file << global_yahoo	<< ' ';
	file << global_bing		<< ' ';
	file << std::endl;

	file << stats_tab.size() << std::endl;

	StatsTab::iterator i = stats_tab.begin();

	for( ; i != stats_tab.end() ; ++i)
	{
		file << i->first			<< ' ';
		file << i->second.all		<< ' ';
		file << i->second.google	<< ' ';
		file << i->second.yahoo		<< ' ';
		file << i->second.bing		<< ' ';
		file << std::endl;
	}
}


void Stats::SaveStats()
{
	if( stats_file.empty() )
		return;
	
	pt::wide_to_utf8(stats_file, astats_file);
	std::ofstream file(astats_file.c_str());

	if( !file )
	{
		log << log1 << "Stats: I cannot open a file: " << astats_file << logend;
		return;
	}

	SaveStats(file);
	file.close();

	log << log3 << "Stats: statistics saved to: " << astats_file << logend;
}



void Stats::PeriodicSave()
{
	req_current += 1;

	if( req_save_freq != 0 && req_current >= req_save_freq )
	{
		SaveStats();
		req_current = 0;
	}
}


void Stats::ReadConfig(Config * config)
{
	stats_file    = config->Text(L"stats_file");
	req_save_freq = config->Int(L"stats_req_save_freq", req_save_freq);

	if( stats_file.empty() )
	{
		log << log1 << "you should set stats_file in your config to keep statistics between restarting winix" << logend;
	}
	else
	{
		log << log2 << "Stats: stats_file: " << stats_file << logend;
		log << log2 << "Stats: statistics will be saved after each " << req_save_freq << " requests" << logend;
	}
}


void Stats::RemoveItem(long id)
{
	StatsTab::iterator i = stats_tab.find(id);

	if( i == stats_tab.end() )
		return;

	stats_tab.erase(i);
	log << log3 << "Stats: statistics erased for item: " << id << logend;
}


} // namespace



} // namespace Winix

