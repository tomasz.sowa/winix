/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_plugins_stats_stats
#define headerfile_winix_plugins_stats_stats

#include <string>
#include <map>
#include <fstream>
#include "core/config.h"
#include "core/winixmodeldeprecated.h"


namespace Winix
{

namespace Stats
{



struct Stats : public WinixModelDeprecated
{
	Stats();

	void ReadStats();
	void SaveStats();
	void PeriodicSave();
	void ReadConfig(Config * config);
	void RemoveItem(long id);

	// file name for reading/saving statistics
	std::wstring stats_file;
	std::string astats_file;

	// when the statistics start
	time_t stats_start;

	int global_all;
	int global_unique;

	int global_google;
	int global_yahoo;
	int global_bing;


	// statistics for files/dirs
	struct ItemStats
	{
		int all;
		int google;
		int yahoo;
		int bing;

		ItemStats()
		{
			all    = 0;
			google = 0;
			yahoo  = 0;
			bing   = 0;
		}
	};

	// <item_id, Item_stats>
	typedef std::map<long, ItemStats> StatsTab;
	StatsTab stats_tab;


private:

	// when to save the config (how many requests should have gone)
	// (for safety: power failure etc)
	// default: 1000
	// you can set: stats_req_save_freq in the config file to overwrite it
	// 0 - turn it off
	int req_save_freq;
	int req_current; // helper


	void ReadStats(std::ifstream & file);
	void SaveStats(std::ofstream & file);

};




} // namespace

} // namespace Winix

#endif
