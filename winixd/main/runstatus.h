/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_main_runstatus
#define headerfile_winix_main_runstatus


namespace Winix
{


struct RunStatus
{
	const static int EXIT_CODE_OK = 0;
	const static int EXIT_CODE_CANNOT_INITIALIZE_APPLICATION = 1;
	const static int EXIT_CODE_CANNOT_CORRECTLY_READ_CONFIG = 2;
	const static int EXIT_CODE_CANNOT_DROP_PRIVILEGES = 3;
	const static int EXIT_CODE_CANNOT_DEMONIZE = 4;
	const static int EXIT_CODE_CANNOT_INITIALIZE_FASTCGI = 5;
	const static int EXIT_CODE_PARAMETERS_SYNTAX_ERROR = 6;
	const static int EXIT_CODE_CANNOT_INITIALIZE_CURL = 7;

	int exit_code;
	bool should_continue;

	RunStatus()
	{
		exit_code = EXIT_CODE_OK;
		should_continue = true;
	}
};



}

#endif

