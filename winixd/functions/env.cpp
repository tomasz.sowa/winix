/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "env.h"
#include "core/log.h"
#include "core/misc.h"


namespace Winix::Fun
{


Env::Env()
{
	fun.url = L"env";
}


bool Env::has_access()
{
	if( !cur->session->puser )
		return false;

	if( cur->request->IsParam(L"a") )
	{
		// show/change admin environment variables for a user

		if( !cur->session->puser->is_super_user )
			return false;
	}

return true;
}



bool Env::parse(const std::wstring & env_str)
{
	return (conf_parser.parse_space(env_str, space) == pt::SpaceParser::ok);
}


bool Env::edit_admin_env(const std::wstring & env_str)
{
	if( parse(env_str) )
	{
		user_wrapper.user->admin_env = space;

		if( user_wrapper.user->update() )
		{
			return true;
		}
		else
		{
			log << log1 << "Evn: a database problem with changing admin environment variables for user: "
				<< user_wrapper.user->login << ", id: " << user_wrapper.user->id << logend;
		}
	}
	else
	{
		log << log2 << "Env: Syntax error in line: " << conf_parser.get_last_parsed_line() << logend;

		// if( use_ses_log )
		// 	slog << logerror << T("syntax_error_in_line") << ' ' << conf_parser.get_last_parsed_line() << logend;
	}

return false;
}


bool Env::edit_env(const std::wstring & env_str)
{
	if( parse(env_str) )
	{
		user_wrapper.user->env = space;

		if( user_wrapper.user->update() )
		{
			return true;
		}
		else
		{
			log << log1 << "Evn: a database problem with changing admin environment variables for user: "
				<< user_wrapper.user->login << ", id: " << user_wrapper.user->id << logend;
		}
	}
	else
	{
		log << log2 << "Env: Syntax error in line: " << conf_parser.get_last_parsed_line() << logend;

		// if( use_ses_log )
		// 	slog << logerror << T("syntax_error_in_line") << ' ' << conf_parser.get_last_parsed_line() << logend;
	}

return false;
}



void Env::save_env()
{
	const std::wstring & env_str = cur->request->PostVar(L"envvar");
	bool status  = false;

	if( cur->request->IsParam(L"a") )
	{
		if( cur->session->puser->is_super_user )
		{
			status = edit_admin_env(env_str);

			if( status )
			{
				//slog << loginfo << T(L"env_admin_changed_successfully") << logend;
				redirect_if_needed();
			}
		}
	}
	else
	{
		status = edit_env(env_str);

		if( status )
		{
			//slog << loginfo << T(L"env_changed_successfully") << logend;
			redirect_if_needed();
		}
	}
}


void Env::register_models()
{
	cur->request->models.Add(L"users", system->users);
	cur->request->models.Add(L"env_user", user_wrapper);
}



void Env::make_post()
{
	user_wrapper.user = nullptr;
	user_wrapper.set_connector(model_connector);

	if( cur->session->puser )
	{
		user_wrapper.user = cur->session->puser;

		if( cur->session->puser->is_super_user && cur->request->IsPostVar(L"userid") )
		{
			long id = Tol(cur->request->PostVar(L"userid"));
			user_wrapper.user = system->users.GetUser(id);

			if( user_wrapper.user->id != cur->session->puser->id )
			{
				log << log2 << "Env: changing user to: " << user_wrapper.user->login << ", id: " << user_wrapper.user->id << logend;
			}
		}

		if( !cur->request->IsPostVar(L"changeuser") )
		{
			if( user_wrapper.user )
			{
				save_env();
			}
			else
			{
				cur->request->status = WINIX_ERR_PERMISSION_DENIED;
			}
		}
	}

	register_models();
}


void Env::make_get()
{
	user_wrapper.user = cur->session->puser;
	user_wrapper.set_connector(model_connector);

	register_models();
}


void Env::clear()
{
	user_wrapper.user = nullptr;
}



} // namespace

