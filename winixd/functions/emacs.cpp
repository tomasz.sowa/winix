/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "emacs.h"
#include "templates/templates.h"
#include "functions.h"


namespace Winix::Fun
{


Emacs::Emacs()
{
	fun.url = L"emacs";
}



bool Emacs::has_emacs_access(const Item & item)
{
	if( cur->session->puser && cur->session->puser->is_super_user )
		// super user can use emacs everywhere
		return true;

	if( !system->HasWriteAccess(item) )
		return false;

	if( !system->mounts.pmount->IsPar(system->mounts.MountParEmacsOn()) )
		return true;

	if( system->mounts.pmount->IsArg(system->mounts.MountParEmacsOn(), cur->request->dir_tab.size()) )
		return true;	
	
return false;
}



bool Emacs::has_access()
{
	return has_emacs_access(*cur->request->last_item);
}





// !! IMPROVE ME in functions.cpp there is a similar function
/*
bool Emacs::PostEmacsCheckAbuse(bool adding)
{
	if( !system->rebus.CheckRebus() )
	{
		cur->request->status = WINIX_ERR_INCORRECT_REBUS;
		return false;
	}

	// !! is tested in createthread once
	functions->CheckGetPostTimes();

	if( cur->session->spam_score > 0 )
	{
		cur->request->status = WINIX_ERR_SPAM;
		log << log1 << "Emacs: ignoring due to suspected spamming" << logend;
		return false;
	}

return true;
}
*/


int Emacs::notify_code_edit()
{
// !! not needed
//	if( system->mounts.pmount->type == system->mounts.MountTypeThread() )
//		return WINIX_NOTIFY_CODE_THREAD_POST_CHANGED;

return WINIX_NOTIFY_CODE_EDIT;
}




int Emacs::notify_code_add()
{
// !! not needed
//	if( system->mounts.pmount->type == system->mounts.MountTypeThread() )
//		return WINIX_NOTIFY_CODE_THREAD_REPLAYED;

return WINIX_NOTIFY_CODE_ADD;
}


void Emacs::push_url_to_current_function(const wchar_t * local_url)
{
	if( cur->request->is_htmx_request )
	{
		pt::WTextStream url;

		if( cur->request->IsParam(L"tabs") )
			prepare_current_function(L"/tabs", url);
		else
			prepare_current_function(nullptr, url);

		cur->request->out_headers.add(Header::hx_push_url, url);
	}
}


/*
 * this make_post() is used from Ckeditor, Tinymce and Nicedit as well
 */
void Emacs::make_post()
{
	bool status = false;
	bool adding = !cur->request->is_item;
	std::wstring old_url;

	if( !adding )
		old_url = cur->request->item.url;

	functions->ReadItem(cur->request->item, Item::file);

	if( adding )
		functions->SetUser(cur->request->item); // set user before checking the rebus

	if( functions->CheckAbuse() )
		return;

	if( adding )
	{
		cur->request->item.item_content.privileges = system->NewFilePrivileges();
		status = system->AddFile(cur->request->item, notify_code_edit());

		if( status )
		{
			cur->request->is_item = true;
		}
	}
	else
	{
		status = system->EditFile(cur->request->item, cur->request->item.url != old_url, notify_code_edit());
	}

	cur->request->status = status ? WINIX_ERR_OK : WINIX_ERR_PERMISSION_DENIED;

	if( status )
	{
		functions->CheckSpecialFile(cur->request->item);
		bool was_url_changed = (adding || cur->request->item.url != old_url);
		redirect_if_needed(was_url_changed);
	}

	if( adding )
	{
		// App::AddDefaultModels() is called before make_post() and didn't see the item yet
		cur->request->models.Add(L"item", cur->request->item);
	}
}


void Emacs::clear()
{
}



} // namespace



