/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "tinymce.h"
#include "functions.h"


namespace Winix::Fun
{


Tinymce::Tinymce()
{
	fun.url = L"tinymce";
}


void Tinymce::init()
{
	system->AddCommonFileToVar(L"winix/tinymce.js", L"tinymce.js", Header::text_javascript_utf8);
}


bool Tinymce::has_access()
{
	FunctionBase * emacs = functions->Find(L"emacs");

	if( emacs )
	{
		return emacs->has_access();
	}
	else
	{
		return false;
	}
}


void Tinymce::make_get()
{
	cur->session->last_css.clear();
	int parcss = system->mounts.MountParCss();

	if( cur->mount->param[parcss].defined )
		cur->session->last_css = cur->mount->param[parcss].arg;
}


void Tinymce::make_post()
{
	FunctionBase * emacs = functions->Find(L"emacs");

	if( emacs )
	{
		emacs->make_post();
	}
	else
	{
		log << log2 << "Tinymce: I cannot save your content, there is no emacs function available" << logend;
	}
}




} // namespace

