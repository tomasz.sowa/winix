/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "default.h"
#include "core/misc.h"


namespace Winix
{


namespace Fun
{

Default::Default()
{
	fun.url = L"default";
}


bool Default::has_access()
{
	return !cur->request->is_item && system->HasWriteAccess(*cur->request->dir_tab.back());
}



void Default::make_post()
{
	Item & dir = *cur->request->dir_tab.back();
	dir.propagate_connector();

	dir.item_content.link_to       = cur->request->PostVar(L"linkto");
	dir.item_content.link_redirect = cur->request->IsPostVar(L"makeredirect") ? 1 : 0;
	TrimWhite(dir.item_content.link_to);

	// !! IMPROVEME dodac sprawdzenie czy link_to jest pusty teraz

	//cur->request->status = db->EditLinkItem(dir.id, dir.link_to, dir.link_redirect);
	if( dir.item_content.update() )
	{
		log << log2 << "Default: changed link_to: " << dir.item_content.link_to << ", for dir_id: " << dir.id << logend;
		system->RedirectToLastDir();
	}
}




} // namespace

} // namespace Winix

