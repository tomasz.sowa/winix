/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "reload.h"
#include "templates/templates.h"


namespace Winix
{


namespace Fun
{


Reload::Reload()
{
	fun.url = L"reload";
	fun.item_content.privileges = 07000;
}


bool Reload::has_access()
{
	return cur->session->puser && cur->session->puser->is_super_user;
}



void Reload::FunReloadTemplates()
{
	log << log1 << "Content: reloading html templates" << logend;

	templates->ReadTemplates();
	// reload notify after templates (notify uses locales from templates)
	system->notify.ReadTemplates();
}




void Reload::make_get()
{
	// !! temporarily only an admin has access
	
	if( !cur->session->puser || !cur->session->puser->is_super_user )
	{
		log << log1 << "Content: Only an admin has access to reload function" << logend;
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;
		return;
	}

	if( cur->request->IsParam(L"templates") )
		FunReloadTemplates();
}




} // namespace

} // namespace Winix

