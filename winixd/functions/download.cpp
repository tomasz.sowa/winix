/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "download.h"
#include "core/misc.h"



namespace Winix
{


namespace Fun
{


Download::Download()
{
	fun.url = L"download";
	need_session = false;
}


void Download::make_get()
{
	if( !cur->request->is_item )
	{
		log << log1 << "Download: download function requires an item" << logend;
		cur->request->http_status = Header::status_403_forbidden;
		return;
	}
	
	if( !system->HasReadAccess(cur->request->item)				||
		cur->request->item.item_content.file_type == WINIX_ITEM_FILETYPE_NONE ||
		cur->request->item.item_content.file_path.empty()						)
	{
		cur->request->http_status = Header::status_403_forbidden;
		return;
	}

	cur->request->send_as_attachment = cur->request->IsParam(L"attachment");
	bool is_thumb = (cur->request->item.item_content.file_has_thumb && cur->request->IsParam(L"thumb"));
	bool ok = true;

	if( config->send_file_mode == 0 || config->send_file_mode == 1 )
	{
		system->MakeFilePath(cur->request->item, cur->request->x_sendfile, is_thumb);

		if( config->send_file_mode == 0 )
		{
			log << log3 << "Download: reading the content of a file: " << cur->request->x_sendfile << logend;

			if( GetBinaryFile(cur->request->x_sendfile, cur->request->out_bin_stream) )
			{
				cur->request->send_bin_stream = true;
			}
			else
			{
				ok = false;
				cur->request->http_status = Header::status_404_not_found;
				log << log2 << "Download: I cannot read the content of a file: " << cur->request->x_sendfile << logend;
			}

			cur->request->x_sendfile.clear();
		}
	}
	else
	if( config->send_file_mode == 2 )
	{
		system->MakeRelativeFilePath(cur->request->item, config->send_file_relative_prefix, cur->request->x_sendfile, is_thumb);
	}
	else
	{
		log << log1 << "Download: send_file_mode in the config should be either 0, 1 or 2" << logend;
		cur->request->http_status = Header::status_403_forbidden;
	}

	if( ok )
	{
		cur->request->use_ezc_engine = false;

		if( !cur->request->item.item_content.file_mime_type.empty() )
			cur->request->out_headers.add(Header::content_type, cur->request->item.item_content.file_mime_type);
	}
}




} // namespace

} // namespace Winix

