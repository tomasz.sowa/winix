/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "mkdir.h"
#include "functions.h"


namespace Winix
{


namespace Fun
{

Mkdir::Mkdir()
{
	fun.url = L"mkdir";
}


bool Mkdir::has_mkdir_access(const Item & item)
{
	// you can create a directory only in a directory
	if( item.type != Item::dir )
		return false;
	
	if( cur->session->puser && cur->session->puser->is_super_user )
		// super user can use mkdir everywhere
		return true;

	if( !system->HasWriteAccess(item) )
		return false;

	if( !system->mounts.pmount->IsPar(system->mounts.MountParMkdirOn()) )
		return true;

	if( system->mounts.pmount->IsArg(system->mounts.MountParMkdirOn(), cur->request->dir_tab.size()) )
		return true;	
	
	return false;
}


bool Mkdir::has_access()
{
	if( cur->request->is_item || !has_mkdir_access(*cur->request->dir_tab.back()) )
		return false;

	return true;
}


void Mkdir::post_fun_mkdir(bool add_to_dir_tab, int privileges)
{
	cur->request->item.set_connector(model_connector);
	functions->ReadItem(cur->request->item, Item::dir);
	functions->SetUser(cur->request->item);
	cur->request->item.item_content.privileges = privileges;
	Item * pdir;

	if( system->dirs.AddDirectory(cur->request->item, add_to_dir_tab, &pdir) )
	{
		if( pdir )
			plugin->Call(WINIX_DIR_ADDED, pdir);

		system->RedirectTo(cur->request->item);
	}
	else
	{
		log << log1 << "Mkdir: PostFunMkdir: cannot create directory " << logend;
	}
}


void Mkdir::make_post()
{
	post_fun_mkdir(false, system->NewDirPrivileges());
}



} // namespace
} // namespace Winix

