/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_functions_functionparser
#define headerfile_winix_functions_functionparser

#include <string>

#include "core/winixmodeldeprecated.h"
#include "core/cur.h"
#include "core/dirs.h"


namespace Winix
{


class Functions;


class FunctionParser : public WinixModelDeprecated
{
public:

	FunctionParser();


	/*
	 * if the method returns false then cur->request->http_status will be already set to a specific error
	 */
	bool Parse(Request * request, Dirs * dirs, Functions * functions);
	bool ParseDirs(Request * request, Dirs * dirs);


private:

	Db        * db;
	Request   * request;
	Dirs      * dirs;
	Functions * functions;

	const wchar_t * path;
	std::wstring name, value;
	std::string name_ascii, value_ascii;
	Item * last_dir;
	Param param;
	bool status;

	void SkipSlashes();

	void PrintCurrentDirs();
	void ParseDirs();
	bool ParseDirsItemFunction();
	void ParseParams();
	void ParseAnchor();

	bool IsDir();
	bool CheckAddItem();
	bool CheckAddFunction();
	void LogDir(Item * dir);
	void AddDir();
	void AddParam();
	void ParseOrdinaryParams();
	void ParseWinixParams();

	int  FromHex(int c);
	int  GetChar();
	void ReadName();
	void ReadOrdinaryParName();
	void ReadOrdinaryParValue();
	void ReadWinixParName();
	void ReadWinixParValue();


};



} // namespace Winix

#endif

