/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "ls.h"



namespace Winix
{


namespace Fun
{

Ls::Ls()
{
	fun.url = L"ls";
	need_session = false;
}


void Ls::prepare_files()
{
	morm::Finder<Item> finder(model_connector);

	finder.
		select().
		where().
		eq(L"parent_id", cur->request->dir_tab.back()->id).
		neq(L"type", static_cast<int>(Item::dir));

	if( cur->request->IsParam(L"ckeditor_browse") )
	{
		finder.eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_IMAGE);
	}

	finder.raw("order by item.sort_index asc, item.url asc, item.id asc");
	finder.get_vector(item_tab);
}


void Ls::prepare_dirs()
{
	dir_tab.clear();

	// with parent directory (if exists)
	// if dir_tab.size() is equal to 1 then we at the root directory
	if( cur->request->dir_tab.size() >= 2 )
	{
		Item * dir_up = cur->request->dir_tab[cur->request->dir_tab.size()-2];
		dir_tab.push_back(dir_up);
	}

	system->dirs.GetDirChilds(cur->request->dir_tab.back()->id, dir_tab);
}


void Ls::make_get()
{
	// !! IMPROVE ME
	// this should be moved to ckeditor function (similarly the html content from fun_ls.html)
	if( cur->request->IsParam(L"ckeditor_browse") )
	{
		cur->request->html_template = config->templates_index_generic;
	}

	if( !cur->request->is_item )
	{
		prepare_files();
		prepare_dirs();
	}

	cur->request->models.Add(L"items", item_tab);
	cur->request->models.Add(L"child_dirs", dir_tab);
}



void Ls::clear()
{
	item_tab.clear();
	dir_tab.clear();
}





} // namespace

} // namespace Winix

