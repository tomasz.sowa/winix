/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "ln.h"
#include "core/misc.h"
#include "functions/functions.h"



namespace Winix
{



namespace Fun
{

Ln::Ln()
{
	fun.url = L"ln";
}



bool Ln::has_access()
{
	return system->HasWriteAccess(*cur->request->dir_tab.back());
}



bool Ln::CreateSymbolicLink(long parent_id, const wchar_t * link_to, const wchar_t * url, bool link_redirect)
{
	item.set_connector(model_connector);
	item.Clear(); // setting the date to now
	item.type          = Item::symlink;
	item.parent_id     = parent_id;
	item.url           = url;
	item.item_content.link_to       = link_to;
	item.item_content.link_redirect = static_cast<int>(link_redirect);
	item.item_content.privileges    = system->NewFilePrivileges();
	functions->SetUser(item);
	functions->PrepareUrl(item);

	return item.insert();
//return db->AddItem(item) == WINIX_ERR_OK;
}


bool Ln::CreateSymbolicLink(long parent_id, const std::wstring & link_to, const std::wstring & url, bool link_redirect)
{
	return CreateSymbolicLink(parent_id, link_to.c_str(), url.c_str(), link_redirect);
}


void Ln::CreateSymbolicLink(const std::wstring & link_to)
{
	long parent_id           = cur->request->dir_tab.back()->id;
	const std::wstring & url = cur->request->PostVar(L"url");
	bool link_redirect       = cur->request->IsPostVar(L"makeredirect") ? 1 : 0;

	if( CreateSymbolicLink(parent_id, link_to, url, link_redirect) )
		log << log3 << "Ln: created a symbolic link to: " << link_to << logend;
	else
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;
}


/*
!! IMPROVE ME
we need a better public interface
instead of std::vector<Item*> may std::wstring for path?

	dirs    - directory from we start looking for a destination file
	link_to - destination file (name or path - relative or not)
	url     - suggested url name of the link
*/
bool Ln::CreateHardLink(const std::vector<Item*> & dirs, const std::wstring & link_to, const std::wstring & url)
{
	if( dirs.empty() )
		return false;

	int res = system->FollowAllLinks(dirs, link_to, dir_tab, item, false, false);

	if( res == 0 )
	{
		log << log2 << "Ln: " << link_to << " is a directory (can't create a hard link)" << logend;
		return false;
	}
	else
	if( res == 1 )
	{
		item.type          = Item::file;
		item.parent_id     = dirs.back()->id;
		item.url           = url;
		functions->PrepareUrl(item);

		/*
		 * IMPROVEME
		 *
		 */
		//return db->AddHardLink(item) == WINIX_ERR_OK;
	}

return false;
}




void Ln::CreateHardLink(const std::wstring & link_to)
{
	const std::vector<Item*> & dirs = cur->request->dir_tab;
	const std::wstring & url        = cur->request->PostVar(L"url");

	if( CreateHardLink(dirs, link_to, url) )
		log << log3 << "Ln: created a hard link to: " << link_to << logend;
	else
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;
}



// we do not use notifications for links
void Ln::make_post()
{
	link_to = cur->request->PostVar(L"linkto");
	TrimWhite(link_to);

	if( link_to.empty() )
		return;

	int type = Toi(cur->request->PostVar(L"linktype"));

	if( type == 0 )
		CreateHardLink(link_to);
	else
		CreateSymbolicLink(link_to);

	if( cur->request->status == WINIX_ERR_OK )
		system->RedirectTo(item);
}



} // namespace


} // namespace Winix

