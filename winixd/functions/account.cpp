/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2013-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "account.h"
#include "core/log.h"
#include "core/misc.h"



namespace Winix
{


namespace Fun
{

Account::Account()
{
	fun.url = L"account";
}



bool Account::ActivateAccount(User * puser, long code)
{
	pt::Space * user_code_space = puser->admin_env.get_space(L"activation_code");

	if( user_code_space )
	{
		if( user_code_space->to_long() == code )
		{
			puser->status = WINIX_ACCOUNT_READY;
			puser->admin_env.remove(L"activation_code");

			if( puser->update() )
			{
				log << log2 << "Account: account: " << puser->login << " activated" << logend;

				// if( use_ses_log )
				// 	slog << loginfo << T(L"account_activated") << logend;

				return true;
			}
			else
			{
				log << log1 << "Account: account not activated -- database error" << logend;
			}
		}
		else
		{
			// !! IMPROVE ME if too many errors from the same IP address
			// add this ip to the banip list

			log << log2 << "Account: incorrect activation code" << logend;

			// if( use_ses_log )
			// 	slog << logerror << T(L"incorrect_activation_code") << logend;
		}
	}
	else
	{
		log << log1 << "Account: there is no activation_code value in an admin environment" << logend;

		// if( use_ses_log )
		// 	slog << loginfo << T(L"account_cannot_be_activated") << logend;
	}

return false;
}


bool Account::ActivateAccount(const std::wstring & login, long code)
{
	bool result  = false;
	User * puser = system->users.GetUser(login);

	if( puser )
	{
		if( puser->status == WINIX_ACCOUNT_NOT_ACTIVATED )
		{
			result = ActivateAccount(puser, code);
		}
		else
		{
			log << log2 << "Account: this account is already activated" << logend;

			// if( use_ses_log )
			// 	slog << loginfo << T(L"account_already_activated") << logend;
		}
	}
	else
	{
		log << log1 << "Account: there is no a user: " << login << logend;
	}

return result;
}



void Account::ActivateAccount()
{
	const std::wstring & login = cur->request->ParamValue(L"login");
	long code = Tol(cur->request->ParamValue(L"code"));

	if( !login.empty() )
	{
		ActivateAccount(login, code);
		system->RedirectToLastItem();
	}
}



void Account::make_post()
{
}



void Account::make_get()
{
	if( cur->request->IsParam(L"activate") )
		ActivateAccount();
}




} // namespace


} // namespace Winix

