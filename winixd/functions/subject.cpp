/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "subject.h"



namespace Winix
{


namespace Fun
{

Subject::Subject()
{
	fun.url = L"subject";
}



bool Subject::SubjectCheckAccess()
{
	// super user can always
	if( cur->session->puser && cur->session->puser->is_super_user )
		return true;

	bool access;

	if( cur->request->is_item )
		access = system->HasWriteAccess(cur->request->item);
	else
		access = system->HasWriteAccess(*cur->request->dir_tab.back());

	if( !access )
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;

return access;
}



void Subject::EditDirSubject()
{
	Item & dir = *cur->request->dir_tab.back();

	cur->request->PostVar(L"subject", dir.subject);

	ItemModelData item_model_data;
	item_model_data.prepare_unique_url = false;

	//db->EditSubjectById(dir, dir.id);
	dir.update(item_model_data, false);

	// !! IMPROVE ME
	// we need something like WINIX_DIR_CHANGED message
	//plugin.Call(WINIX______CHANGED, cur->request->dir_tab.back());

	system->RedirectToLastDir();
}



void Subject::EditFileSubject()
{
	cur->request->PostVar(L"subject", cur->request->item.subject);

	ItemModelData item_model_data;
	item_model_data.prepare_unique_url = false;

	cur->request->item.update(item_model_data, false);
	//db->EditSubjectById(cur->request->item, cur->request->item.id);

	plugin->Call(WINIX_FILE_CHANGED, &cur->request->item);

	system->RedirectTo(cur->request->item);
}




void Subject::make_post()
{
	if( !SubjectCheckAccess() )
		return;

	if( cur->request->is_item )
		EditFileSubject();
	else
		EditDirSubject();
}



void Subject::make_get()
{
	SubjectCheckAccess();
}




} // namespace

} // namespace Winix

