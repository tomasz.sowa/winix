/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <algorithm>
#include "sort.h"
#include "core/misc.h"
#include "core/plugin.h"



namespace Winix
{


namespace Fun
{

Sort::Sort()
{
	fun.url = L"sort";
}


bool Sort::has_access()
{
	if( cur->request->is_item )
		return system->HasWriteAccess(cur->request->item);

	// for directories we always return true
	// (permissions are checked later)
	return true;
}



void Sort::GetDirContent()
{
	morm::Finder<Item> finder(model_connector);

	finder.
		select().
		where().
		eq(L"parent_id", cur->request->dir_tab.back()->id).
		raw("order by item.sort_index asc, item.url asc, item.id asc").
		get_vector(items);

	system->CheckWriteAccessToItems(items);
	cur->request->models.Add(L"item_tab", items);
}


Item * Sort::FindItem(long id)
{
	for(Item & item : items)
	{
		if( item.id == id )
			return &item;
	}

	return nullptr;
}


void Sort::UpdateSortIndex(Item & item, int sort_index)
{
	item.sort_index = sort_index;

	ItemModelData item_model_data;
	item_model_data.prepare_unique_url = false;

	if( item.update(item_model_data, false) )
	{
		log << log2
			<< "Sort: updated sort index, item_id=" << item.id
			<< ", url=" << item.url
			<< ", sort_index=" << sort_index << logend;
	}
}


void Sort::UpdateItems()
{
	if( cur->request->post_in.is_object() )
	{
		pt::Space::ObjectType::iterator i2 = cur->request->post_in.value.value_object.begin();

		for( ; i2 != cur->request->post_in.value.value_object.end() ; ++i2 )
		{
			if( i2->second->is_wstr() && pt::is_substr_nc(L"sort", i2->first.c_str()) )
			{
				long id = Tol(i2->first.c_str() + 4);
				int sort_index = Toi(*i2->second->get_wstr());

				Item * pitem = FindItem(id);

				if( pitem )
				{
					if( system->HasWriteAccess(*pitem) )
					{
						UpdateSortIndex(*pitem, sort_index);
					}
				}
			}
		}
	}
}




void Sort::make_post()
{
	if( cur->request->is_item )
	{
		int sort_index = Toi(cur->request->PostVar(L"sortindex"));
		UpdateSortIndex(cur->request->item, sort_index);
	}
	else
	{
		GetDirContent();
		UpdateItems();
	}

	plugin->Call(WINIX_DIR_CONTENT_SORTED, cur->request->dir_tab.back());
	system->RedirectToLastItem();
}


void Sort::make_get()
{
	if( !cur->request->is_item )
		GetDirContent();
}




} // namespace


} // namespace Winix

