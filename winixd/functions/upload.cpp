/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>
#include <errno.h>
#include "upload.h"
#include "core/misc.h"
#include "functions/functions.h"
#include "templates/templates.h"
#include "utf8/utf8.h"



namespace Winix
{


namespace Fun
{

Upload::Upload()
{
	fun.url = L"upload";
}


void Upload::init()
{
}


void Upload::finish()
{
	close_magic_lib();
}


bool Upload::has_upload_access(const Item & item)
{
	// you can use 'upload' only in a directory
	if( item.type != Item::dir )
		return false;

	if( config->upload_dir.empty() )
	{
		log << log1 << "Request: can't use upload function, upload_dir must be set in the config file" << logend;
		return false;
	}

	if( cur->session->puser && cur->session->puser->is_super_user )
		// super user can use upload everywhere
		return true;

	if( !system->HasWriteAccess(item) )
		return false;

return true;
}


bool Upload::has_access()
{
	if( cur->request->is_item || !has_upload_access(*cur->request->dir_tab.back()) )
		return false;

return true;
}




bool Upload::upload_save_static_file(const Item & item, const std::wstring & tmp_filename)
{
	if( !system->MakeFilePath(item, path, false, true, config->upload_dirs_chmod, config->upload_group_int) )
	{
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;
		return false;
	}

	if( RenameFile(tmp_filename, path) )
	{	
		if( !SetPriv(path, config->upload_files_chmod, config->upload_group_int) )
		{
			cur->request->status = WINIX_ERR_PERMISSION_DENIED;
			return false;
		}

		log << log2 << "Upload: uploaded a new file: " << path << logend;
		return true;
	}
	else
	{
		log << log1 << "Upload: can't move the tmp file from: " << tmp_filename << ", to: " << path << logend;
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;
		return false;
	}
}



void Upload::resize_image(Item & item)
{
	Image::Scale scale = system->image.GetImageScale(item.parent_id);
	system->image.Resize(item.id, scale.cx, scale.cy, scale.aspect_mode, scale.quality);
}



void Upload::create_thumb(Item & item)
{
	Image::Scale scale = system->image.GetThumbScale(item.parent_id);
	system->image.CreateThumb(item.id, scale.cx, scale.cy, scale.aspect_mode, scale.quality);
}



void Upload::upload_file(Item & item, const std::wstring & tmp_filename)
{
	// we should add the file beforehand to get the proper item.id
	cur->request->status = WINIX_ERR_PERMISSION_DENIED;
	bool status = system->AddFile(item, 0, false);

	if( status  )
	{
		if( system->CreateNewFile(item) )
		{
			if( upload_save_static_file(item, tmp_filename) )
			{
				ItemModelData item_model_data;
				item_model_data.prepare_unique_url = false;

				//cur->request->status = db->EditFileById(item, item.id);
				if( item.update(item_model_data) )
				{
					cur->request->status = WINIX_ERR_OK;
					plugin->Call(WINIX_FILE_ADDED, &item);

					if( item.item_content.file_type == WINIX_ITEM_FILETYPE_IMAGE )
					{
						if( config->image_resize )
							resize_image(item);

						if( config->create_thumb )
							create_thumb(item);
					}

					if( is_jquery_upload )
						cur->request->item_tab.push_back(item);
				}
			}
			else
			{
				item.remove();
				//db->DelItem(item);
			}
		}
	}
}


bool Upload::fun_upload_check_abuse()
{
	if( config->use_antispam_mechanism_for_not_logged_users )
	{
		if( !system->rebus.CheckRebus() )
		{
			cur->request->status = WINIX_ERR_INCORRECT_REBUS;
			return false;
		}

		functions->CheckGetPostTimes(4);

		if( cur->session->spam_score > 0 )
		{
			cur->request->status = WINIX_ERR_SPAM;
			log << log1 << "Content: ignoring due to suspected spamming" << logend;
			return false;
		}
	}

return true;
}





void Upload::upload_multi()
{
	cur->request->item.Clear(); // clearing and setting date
	cur->request->item.parent_id = cur->request->dir_tab.back()->id;
	cur->request->item.type = Item::file;
	cur->request->item.item_content.privileges = system->NewFilePrivileges();
	functions->SetUser(cur->request->item);

	PostFileTab::iterator i = cur->request->post_file_tab.begin();

	for( ; i != cur->request->post_file_tab.end() ; ++i)
	{
		const wchar_t * file_name = i->second.filename.c_str();

		cur->request->item.subject   = file_name;
		cur->request->item.url       = file_name;
		cur->request->item.item_content.file_type = SelectFileType(file_name);
		cur->request->item.item_content.file_size = i->second.file_size;

		functions->PrepareUrl(cur->request->item);
		analize_file_type(i->second.tmp_filename, cur->request->item.item_content.file_mime_type);
		upload_file(cur->request->item, i->second.tmp_filename);
		i->second.tmp_filename.clear();
	}

	if( is_jquery_upload )
		create_jquery_upload_answer();
	else
		system->RedirectToLastDir();
}


void Upload::upload_single()
{
	cur->request->item.Clear(); // clearing and setting date

	const std::wstring & new_subject = cur->request->PostVar(L"subject");
	const std::wstring & new_url     = cur->request->PostVar(L"url");
	bool has_subject = !new_subject.empty();
	bool has_url     = !new_url.empty();

	functions->ReadItem(cur->request->item, Item::file); // ReadItem() changes the url if it is empty
	functions->SetUser(cur->request->item);
	cur->request->item.item_content.privileges = system->NewFilePrivileges();

	PostFile & post_file = cur->request->post_file_tab.begin()->second;

	const wchar_t * file_name = post_file.filename.c_str();
	cur->request->item.item_content.file_type = SelectFileType(file_name);
	cur->request->item.item_content.file_size = post_file.file_size;

	if( !has_subject )
		cur->request->item.subject = file_name;

	if( !has_url )
	{
		cur->request->item.url = file_name;
		functions->PrepareUrl(cur->request->item);
	}

	analize_file_type(post_file.tmp_filename, cur->request->item.item_content.file_mime_type);
	upload_file(cur->request->item, post_file.tmp_filename);
	post_file.tmp_filename.clear();

	if( is_jquery_upload )
		create_jquery_upload_answer();
	else
	if( is_ckeditor_upload )
		create_ckeditor_upload_answer();
	else
	if( cur->request->status == WINIX_ERR_OK )
		system->RedirectTo(cur->request->item, L"/cat");
}


void Upload::init_magic_lib_if_needed()
{
	if( !magic_cookie )
	{
		log << log3 << "Upload: loading magic library" << logend;
		magic_cookie = magic_open(MAGIC_MIME_TYPE);

		if( magic_cookie )
		{
			if( magic_load(magic_cookie, nullptr) < 0 )
			{
				log << log1 << "Upload: I cannot load the magic file, magic_load failed";
				const char * err = magic_error(magic_cookie);

				if( err )
				{
					log << ", error: " << err;
				}

				log << logend;
				close_magic_lib();
			}
		}
		else
		{
			log << log1 << "Upload: I cannot initialize magic library, magic_open failed" << logend;
		}
	}
}


void Upload::close_magic_lib()
{
	if( magic_cookie )
	{
		log << log3 << "Upload: closing magic library" << logend;
		magic_close(magic_cookie);
		magic_cookie = nullptr;
	}
}


void Upload::analize_file_type(const std::wstring & file_path, std::wstring & file_type)
{
	file_type.clear();

	if( magic_cookie )
	{
		std::string file;
		pt::wide_to_utf8(file_path, file);

		log << log3 << "Upload: analize file: " << file;
		const char * magic_file_type = magic_file(magic_cookie, file.c_str());

		if( magic_file_type )
		{
			log << ", mime type is: " << magic_file_type << logend;
			pt::utf8_to_wide(magic_file_type, file_type);
		}
		else
		{
			log << logend << log1 << "Upload: I cannot analize file: " << file << ", magic_file failed";
			const char * err = magic_error(magic_cookie);

			if( err )
			{
				log << ", error: " << err;
			}

			log << logend;
		}
	}
	else
	{
		log << log1 << "Upload: magic library not initialized" << logend;
	}
}


/*
 * an answer for the 'upload' function
 */
void Upload::create_jquery_upload_answer()
{
	Request & req = *cur->request;

	for(size_t i=0 ; i<req.item_tab.size() ; ++i)
	{
		pt::Space & file = files.add_empty_space();

		file.add(L"name", req.item_tab[i].url);
		file.add(L"size", req.item_tab[i].item_content.file_size);

		std::wstring link;
		system->CreateItemLink(req.item_tab[i], link);
		file.add(L"url", link);

		std::wstring delete_url = link;
		delete_url += L"/rm/jquery_upload/container:json/answer:data";
		file.add(L"deleteUrl", delete_url);

		file.add(L"deleteType", L"POST");

		if( req.item_tab[i].item_content.file_type == WINIX_ITEM_FILETYPE_IMAGE )
		{
			std::wstring thumb = link;

			if( req.item_tab[i].item_content.file_has_thumb )
				thumb += L"/-/thumb";

			file.add(L"thumbnailUrl", thumb);
		}

		/*
		 * if there was an error add "error" item e.g.
		 * "error": "Filetype not allowed"
		 */
	}

	cur->request->models.Add(L"files", files);
}


/*
 * an answer for the ckeditor function
 * https://ckeditor.com/docs/ckeditor4/latest/guide/dev_file_upload.html
 */
void Upload::create_ckeditor_upload_answer()
{
	Request & req = *cur->request;

	files.add(L"uploaded", 1);
	files.add(L"fileName", req.item.url);

	std::wstring link;
	system->CreateItemLink(req.item, link);
	files.add(L"url", link);

	/*
	 * if uploaded directly from the ckeditor (by drag and drop) then the placed image
	 * is very small, add "width" and "height" in such a case
	 */
	//files.add(L"width", "600");
	//files.add(L"height", "500");

	/*
	 * if there was an error add "error" item e.g.
	 * "error": {
	 *   "message": "The file is too big."
	 * }
	 */

	files.serialize_to_json_stream(cur->request->out_bin_stream, false);
	cur->request->send_bin_stream = true;
	cur->request->use_ezc_engine = false;
	cur->request->container_type = Request::container_raw;
	cur->request->out_headers.add(Header::content_type, Header::application_json);
}


void Upload::make_post()
{
	init_magic_lib_if_needed();
	cur->request->item_tab.clear();
	is_jquery_upload = cur->request->IsParam(L"jquery_upload");
	is_ckeditor_upload = cur->request->IsParam(L"ckeditor_upload");

	if( cur->request->post_file_tab.empty() )
	{
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;
		return;
	}

	if( !fun_upload_check_abuse() )
		return;

	if( cur->request->post_file_tab.size() > 1 )
		upload_multi();
	else
		upload_single();

}


void Upload::make_get()
{
	if( cur->request->IsParam(L"jquery_upload") )
	{
		morm::Finder<Item> finder(model_connector);

		finder.
			select().
			where().
			eq(L"parent_id", cur->request->dir_tab.back()->id).
			eq(L"type", static_cast<int>(Item::file)).
			neq(L"content", L"file_type", WINIX_ITEM_FILETYPE_NONE).
			raw("order by item.sort_index asc, item.url asc, item.id asc").
			get_vector(cur->request->item_tab);

		create_jquery_upload_answer();
	}
}


void Upload::clear()
{
	files.clear();
}


} // namespace
} // namespace Winix

