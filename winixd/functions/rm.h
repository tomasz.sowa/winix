/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_functions_rm
#define headerfile_winix_functions_rm

#include "functionbase.h"

namespace Winix
{


namespace Fun
{


class Rm : public FunctionBase
{
public:

	Rm();
	bool has_access();
	void make_post();

	// removing the whole directory structure
	// if remove_this_dir is false then only content of dir is removed
	void RemoveDirTree(Item & dir, bool remove_this_dir, bool check_access = true);

	// removing either a file or a symlink
	bool RemoveFileOrSymlink(long item_id, bool check_access = true);
	bool RemoveFileOrSymlink(Item & item, bool check_access = true);

	// removing either a directory or a symlink or a file
	// if item_id is a directory then the whole subdirectories are removed too
	bool RemoveItemById(long item_id, bool check_access = true);

	// removing either a directory or a symlink or a file
	// if path is a directory then the whole subdirectories are removed too
	// path must begin with a slash (or can be empty then the root directory is removed)
	bool RemoveItemByPath(const std::wstring & path, bool check_access = true);


private:

	// for deleting content in a directory (files and symlinks)
	//DbItemQuery content_dir_iq;
	std::vector<Item> content_item_tab;
	std::wstring path;

	// for logging
	std::wstring old_url;

	// for removing an item by id
	//DbItemQuery rm_by_id_iq;
	Item rm_by_id_item;

	std::vector<Item*> rm_path_dir_tab;
	Item rm_path_item;

	pt::Space files;

	bool HasAccessToDir(const Item & dir, bool only_content);
	void Prepare();
	void clear();

	bool RemoveFile(Item & item);
	bool RemoveDirFiles(long dir_id, bool check_access);
	void RemoveCurrentDir(Item * parent_dir, Item * current_dir, bool check_access);
	void RemoveDirTree(Item * parent_dir, Item * current_dir, bool remove_this_dir, bool check_access);
	void RemoveStaticFile(const std::wstring & path);
	void RemoveStaticFile(const Item & item);
	void RemoveDirContent();
	void RemoveDir();
	void CreateJSON(bool status);

};


} // namespace

} // namespace Winix

#endif
