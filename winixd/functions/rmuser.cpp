/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "rmuser.h"
#include "core/log.h"
#include "core/misc.h"
#include "core/plugin.h"
#include "functions/functions.h"


namespace Winix
{



namespace Fun
{

RmUser::RmUser()
{
	fun.url = L"rmuser";
}


bool RmUser::has_access()
{
	return cur->session->puser && cur->session->puser->is_super_user;
}


bool RmUser::RemoveUser(long user_id)
{
	User * puser = system->users.GetUser(user_id);
	bool result = false;

	if( puser )
	{
		name = puser->login;
		
		if( system->users.Remove(user_id) )
		{
			result = true;
			log << log2 << "RmUser: user id: " << user_id << " name: " << name << " was removed" << logend;

			if( !puser->remove() )
				log << log1 << "RmUser: I cannot remove a user id: " << user_id << " from database" << logend;
		}
	}

return result;
}


void RmUser::make_post()
{
	if(	cur->session->puser )
	{
		long user_id;

		if( cur->session->puser->is_super_user )
			user_id = Tol(cur->request->PostVar(L"userid"));
		else
			user_id = cur->session->puser->id;

		RemoveUser(user_id);
	}

	system->RedirectToLastItem();
}


void RmUser::make_get()
{
}




} // namespace


} // namespace Winix

