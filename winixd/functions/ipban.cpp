/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ipban.h"
#include "functions.h"
#include "core/sessionmanager.h"



namespace Winix
{


namespace Fun
{

IPBanFun::IPBanFun()
{
	fun.url = L"ipban";
}



bool IPBanFun::has_access()
{
	return cur->session->puser && cur->session->puser->is_super_user;
}



void IPBanFun::make_post()
{

}



void IPBanFun::make_get()
{
char   tmp_ip_str[100];
size_t tmp_ip_len = sizeof(tmp_ip_str) / sizeof(char);

	if( cur->request->IsParam(L"removeip") )
	{
		if( cur->request->ParamValue(L"removeip") == L"all" )
		{
			session_manager->ClearIPBanList();
			cur->session->ip_ban = 0;
		}
		else
		{
			int cur_ip = 0;

			if( cur->session->ip_ban )
				cur_ip = cur->session->ip_ban->ip;

			if( pt::wide_to_utf8(cur->request->ParamValue(L"removeip"), tmp_ip_str, tmp_ip_len) )
			{
				int ip = (int)inet_addr(tmp_ip_str);
				session_manager->RemoveIPBan(ip);

				if( cur->session->ip_ban && cur_ip == ip )
					cur->session->ip_ban = 0;
			}
		}

		system->RedirectToLastFunction();
	}
}


} // namespace

} // namespace Winix

