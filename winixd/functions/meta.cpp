/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "meta.h"
#include "core/log.h"


namespace Winix::Fun
{


Meta::Meta()
{
	fun.url = L"meta";
	// !! CHECKME what about follow symlinks?
}


bool Meta::has_access()
{
	if( cur->request->IsParam(L"a") )
		return cur->session->puser && cur->session->puser->is_super_user;
	else
		return system->HasWriteAccess(*cur->request->last_item);
}



bool Meta::parse(const std::wstring & meta_str)
{
	return (conf_parser.parse_space(meta_str, space) == pt::SpaceParser::ok);
}




bool Meta::edit_admin_meta(Item & item, const std::wstring & meta_str)
{
	if( parse(meta_str) )
	{
		item.propagate_connector();
		item.item_content.admin_meta = space;

		//if( db->EditAdminMetaById(space, item_id) == WINIX_ERR_OK )
		if( item.item_content.update() )
		{
			return true;
		}
		else
		{
			log << log1 << "Meta: a database problem with changing admin meta information for item id: "
				<< item.id << logend;
		}
	}
	else
	{
		log << log2 << "Meta: Syntax error in line: " << conf_parser.get_last_parsed_line() << logend;

		// if( use_ses_log )
		// 	slog << logerror << T("syntax_error_in_line") << ' ' << conf_parser.get_last_parsed_line() << logend;
	}

return false;
}



bool Meta::edit_meta(Item & item, const std::wstring & meta_str)
{
	if( parse(meta_str) )
	{
		item.propagate_connector();
		item.item_content.meta = space;

		//if( db->EditMetaById(space, item_id) == WINIX_ERR_OK )
		if( item.item_content.update() )
		{
			return true;
		}
		else
		{
			log << log1 << "Meta: a database problem with changing meta information for item id: "
				<< item.id << logend;
		}
	}
	else
	{
		log << log2 << "Meta: Syntax error in line: " << conf_parser.get_last_parsed_line() << logend;

		// if( use_ses_log )
		// 	slog << logerror << T("syntax_error_in_line") << ' ' << conf_parser.get_last_parsed_line() << logend;
	}

return false;
}



void Meta::change_admin_meta()
{
	// IMPROVE ME we need to show an error msg if the user is not an admin

	if( cur->session->puser && cur->session->puser->is_super_user )
	{
		const std::wstring & meta_str = cur->request->PostVar(L"itemmeta");

		if( edit_admin_meta(*cur->request->last_item, meta_str) )
		{
			redirect_if_needed();
		}
	}
}


void Meta::change_meta()
{
	if( system->HasWriteAccess(*cur->request->last_item) )
	{
		const std::wstring & meta_str = cur->request->PostVar(L"itemmeta");

		if( edit_meta(*cur->request->last_item, meta_str) )
		{
			redirect_if_needed();
		}
	}
}



void Meta::make_post()
{
	if( cur->request->IsParam(L"a") )
		change_admin_meta();
	else
		change_meta();
}



} // namespace


