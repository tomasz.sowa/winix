/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "cat.h"


namespace Winix
{


namespace Fun
{

Cat::Cat()
{
	fun.url = L"cat";
	need_session = false;
}


void Cat::make_get()
{
	// IMPROVE ME this probably should be set for all winix functions
	cur->request->html_template = cur->request->last_item->html_template;

	if( !cur->request->is_item )
	{
		log << log1 << "Content: cat function requires an item" << logend;
		cur->request->status = WINIX_ERR_NO_ITEM;
		return;
	}
	
	if( !system->HasReadAccess(cur->request->item) )
	{
		cur->request->status = WINIX_ERR_PERMISSION_DENIED;
		return;
	}

	if( !cur->request->item.item_content.file_mime_type.empty() )
	{
		cur->request->out_headers.add(Header::content_type, cur->request->item.item_content.file_mime_type);
	}

	cur->request->send_as_attachment = cur->request->IsParam(L"attachment");
}


} // namespace

} // namespace Winix

