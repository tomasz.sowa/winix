/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_functions_functions
#define headerfile_winix_functions_functions

#include <string>
#include "functionbase.h"
#include "functionparser.h" 
#include "account.h"
#include "adduser.h"
#include "cat.h"
#include "chmod.h"
#include "chown.h"
#include "ckeditor.h"
#include "cp.h"
#include "default.h"
#include "download.h"
#include "emacs.h"
#include "env.h"
#include "imgcrop.h"
#include "last.h"
#include "locale.h"
#include "login.h"
#include "logout.h"
#include "ln.h"
#include "ls.h"
#include "ipban.h"
#include "man.h"
#include "meta.h"
#include "mkdir.h"
#include "mount.h"
#include "mv.h"
#include "nicedit.h"
#include "node.h"
#include "passwd.h"
#include "priv.h"
#include "pw.h"
#include "reload.h"
#include "rm.h"
#include "rmuser.h"
#include "run.h"
#include "sort.h"
#include "specialdefault.h"
#include "stat.h"
#include "subject.h"
#include "template.h"
#include "timezone.h"
#include "tinymce.h"
#include "uname.h"
#include "upload.h"
#include "uptime.h"
#include "who.h"
#include "vim.h"
#include "html/htmlparser.h"
#include "core/winixrequest.h"


namespace Winix
{


class Templates;



class Functions : public WinixRequest
{
public:

	Fun::Account	fun_account;
	Fun::AddUser	fun_adduser;
	Fun::Cat		fun_cat;
	Fun::Chmod		fun_chmod;
	Fun::Chown		fun_chown;
	Fun::Ckeditor	fun_ckeditor;
	Fun::Cp			fun_cp;
	Fun::Default	fun_default;
	Fun::Download	fun_download;
	Fun::Emacs		fun_emacs;
	Fun::Env		fun_env;
	Fun::ImgCrop	fun_imgcrop;
	Fun::Last		fun_last;
	Fun::Locale		fun_locale;
	Fun::Login		fun_login;
	Fun::Logout		fun_logout;
	Fun::Ln			fun_ln;
	Fun::Ls			fun_ls;
	Fun::IPBanFun	fun_ipban;
	Fun::Man		fun_man;
	Fun::Meta		fun_meta;
	Fun::Mkdir		fun_mkdir;
	Fun::Mount		fun_mount;
	Fun::Mv			fun_mv;
	Fun::Nicedit	fun_nicedit;
	Fun::Node		fun_node;
	Fun::Passwd		fun_passwd;
	Fun::Priv		fun_priv;
	Fun::Pw			fun_pw;
	Fun::Reload		fun_reload;
	Fun::Rm			fun_rm;
	Fun::RmUser		fun_rmuser;
	Fun::Run		fun_run;
	Fun::Sort		fun_sort;
	Fun::SpecialDefault fun_special_default;
	Fun::Stat		fun_stat;
	Fun::Subject	fun_subject;
	Fun::Template   fun_template;
	Fun::TimeZone	fun_time_zone;
	Fun::Tinymce	fun_tinymce;
	Fun::Uname		fun_uname;
	Fun::Upload		fun_upload;
	Fun::Uptime		fun_uptime;
	Fun::Who		fun_who;
	Fun::Vim		fun_vim;

	typedef std::map<std::wstring, FunctionBase*> Table;
	typedef Table::iterator Iterator;

	void Init();
	void Finish();
	bool Parse();
	bool ParseOnlyDirs();
	size_t FunctionsSize();

	Iterator Begin();
	Iterator End();

	void CheckFunctionAndSymlink();
	void MakeFunction();

	/*
	 * this function is called from the jobs thread
	 * objects are locked
	 */
	void ContinueMakeFunction();


	//void SetConfig(Config * pconfig);
	void SetCur(Cur * pcur);
	void SetSystem(System * psystem);
	void SetTemplates(Templates * ptemplates);
	//void SetSynchro(Synchro * psynchro);
	void SetSessionManager(SessionManager * pmanager);

	void set_dependency(WinixRequest * winix_request);

	FunctionBase * Find(const std::wstring & function_name);
	Error CheckSpecialFile(const Item & item);
	void PrepareUrl(Item & item);
	void SetDefaultFunction();

	void CheckGetPostTimes(time_t difference = 10);

	bool CheckAbuse();

	// !! dac lepsze nazwy
	void ReadItemFilterHtml(const std::wstring & html, std::wstring & html_filtered);
	void ReadItemContent(Item & item);
	void ReadItemContentType(Item & item);

	// if item.url is not empty and there is not a post variable "url"
	// then the item.url will not be changed
	// (the same is for item.subject and "subject" post variable)
	void ReadItem(Item & item, Item::Type item_type);

	void SetUser(Item & item);

	bool Add(FunctionBase * fun);
	bool Add(FunctionBase & fun);


private:

	//Config  * config;
	Cur     * cur;
	System  * system;
	//Synchro * synchro;
	Templates * templates;
	SessionManager * session_manager;

	pt::HTMLParser html_filter;
	std::wstring link_to_temp;

	void CreateFunctions();
	void InitFunctions();
	void FinishFunctions();

	void SetObjects(FunctionBase * fun);
	void SetDefaultFunctionForFile();
	void SetDefaultFunctionForDir();

	void ReadItemUrlSubject(Item & item, Item::Type item_type);

	Table table;

	FunctionParser function_parser;

	void CheckFunctionFollowDir(bool was_default_function);
	void CheckFunctionFollowSymlink(bool was_default_function);

	bool CheckAntispamCounter();

	bool HasAccessToCallMake();
	void CallFunctionMake();
	void CallFunctionContinueMake();

};


} // namespace Winix

#endif
