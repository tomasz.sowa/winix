/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_functions_cp
#define headerfile_winix_functions_cp

#include "functionbase.h"

namespace Winix
{


namespace Fun
{



class Cp : public FunctionBase
{
public:

	Cp();
	bool has_access();
	void make_post();

private:

	Item temp;
	bool remove_defaults;
	bool preserve_attr;
	bool follow_symlinks;
	long new_user;
	long new_group;

	// destination dir (will not be empty)
	std::vector<Item*> dir_tab, symlink_dir_tab;

	// for testing loops in directories (between symlinks)
	std::vector<long> loop_checker;

	// destination file (if exists)
	std::wstring file;

	// for copying static files
	std::wstring new_path, new_path_thumb;
	std::wstring old_path, old_path_thumb;

	std::vector<Item> item_tab;

	bool WasThisDir(const Item & dir);
	bool CheckAccessFrom();
	bool CheckAccessTo();
	bool ParseDir();
	bool CopyStaticFile(const std::wstring & from, const std::wstring & to);
	void CopyStaticFile(Item & item);
	void SetNewAttributes(Item & item);
	void CopyFile(Item & item, long dst_dir_id);
	void CopyFileOrSymlink(Item & item, long dst_dir_id);
	void Prepare();
	void CopyFilesInDir(const Item & dir, long dst_dir_id);
	void CopyDirContentTree(const Item & item, long dst_dir_id);
	long CopyDirTree(const Item & item, long dst_dir_id);
	bool IsTheSameFile(const Item & item);
	void clear();
	void PostCopyFile(Item & item, bool redirect = true);
	void PostCopyDirContent(const Item & dir, bool redirect = true);
	void PostCopyDir(const Item & dir, bool redirect = true);

};


} // namespace

} // namespace Winix

#endif
