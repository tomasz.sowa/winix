/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_functions_upload
#define headerfile_winix_functions_upload

#include "functionbase.h"
#include <magic.h>


namespace Winix
{


namespace Fun
{


class Upload : public FunctionBase
{
public:

	Upload();
	bool has_access();
	void make_post();
	void make_get();
	void upload_file(Item & item, const std::wstring & tmp_filename);

private:

	std::wstring path;
	bool is_jquery_upload;
	bool is_ckeditor_upload;
	magic_t magic_cookie;
	pt::Space files;

	void init();
	void finish();
	void clear();

	bool has_upload_access(const Item & item);
	bool upload_save_static_file(const Item & item, const std::wstring & tmp_filename);
	bool fun_upload_check_abuse();
	void upload_multi();
	void upload_single();
	void resize_image(Item & item);
	void create_thumb(Item & item);
	void create_jquery_upload_answer();
	void create_ckeditor_upload_answer();

	void init_magic_lib_if_needed();
	void close_magic_lib();
	void analize_file_type(const std::wstring & file_path, std::wstring & file_type);


};


} // namespace
} // namespace Winix

#endif
