/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_functions_login
#define headerfile_winix_functions_login

#include "functionbase.h"
#include "models/user.h"

namespace Winix
{


namespace Fun
{

/*
 * WARNING
 * cur will be null if config.use_internal_loggin_mechanism is false
 * in such a case those objects are not registered as winix functions
 * but we have a public api
 * (the public api should be moved somewhere e.g. make a service layer)
 *
 */
class Login : public FunctionBase
{
public:

	Login();

	void make_post();
	void make_get();

	bool ShouldUseCaptchaForCurrentIP();
	bool ShouldUseCaptchaFor(const IPBan & ipban);

	bool CannotLoginFromCurrentIP();
	bool CannotLoginFrom(const IPBan & ipban);

	bool CheckUserPass(const std::wstring & login, const std::wstring & password, long & user_id);
	bool LoginUser(const std::wstring & login, const std::wstring & password, bool remember_me, bool check_abuse = false);


private:
	
	void ClearTmpStruct();
	bool CheckPasswords(User & user, const std::wstring & password);
	void CheckBan();
	bool CheckAbuse();

	std::string pass_decrypted;
	std::wstring salt;

};


} // namespace

} // namespace Winix

#endif
