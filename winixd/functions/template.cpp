/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "template.h"
#include "core/misc.h"



namespace Winix
{


namespace Fun
{

Template::Template()
{
	fun.url = L"template";
}





bool Template::has_access()
{
	if( config->template_only_root_use_template_fun )
	{
		// only root is allowed to change the template
		return (cur->session->puser && cur->session->puser->is_super_user);
	}


	if( cur->request->is_item )
		return system->HasWriteAccess(cur->request->item);
	else
		return system->HasWriteAccess(*cur->request->dir_tab.back());
}


void Template::PutLog(Item & item)
{
	log << log3 << "Template: changed template for item.id: " << item.id << ", new template: ";

	if( item.html_template.empty() )
		log << "(taking from mount point)";
	else
		log << item.html_template;

	log << logend;
}


void Template::CreateTemplateFileName(const std::wstring & index_str)
{
	int index = Toi(index_str);

	if( index < 0 )
	{
		html_template.clear();
	}
	else
	if( index == 0 )
	{
		html_template = config->templates_index;
	}
	else
	if( index == 1 )
	{
		html_template = config->templates_index_generic;
	}
	else
	if( index == 2 )
	{
		html_template = config->templates_index_raw;
	}
	else
	{
		index -= 3;
		Mount::ParamRow & par = system->mounts.pmount->param[system->mounts.MountParHtmlTemplate()];
		
		if( !par.defined || (size_t)index >= par.arg.size() )
			html_template.clear();
		else
			html_template = par.arg[index];
	}
}


void Template::ChangeTemplate(Item & item)
{
	if( html_template != item.html_template )
	{
		item.html_template = html_template;

		ItemModelData item_model_data;
		item_model_data.prepare_unique_url = false;

		//cur->request->status = db->EditTemplateItemById(item.id, html_template);
		if( item.update(item_model_data, false) )
		{
			PutLog(item);
		}
	}
}


void Template::make_post()
{
	CreateTemplateFileName(cur->request->PostVar(L"template"));
	ChangeTemplate(*cur->request->last_item);

	system->RedirectToLastItem();
}



} // namespace

} // namespace Winix

