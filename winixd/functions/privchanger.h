/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_functions_privchanger
#define headerfile_winix_functions_privchanger

#include "core/winixmodeldeprecated.h"
#include "core/system.h"


namespace Winix
{


class PrivChanger : public WinixModelDeprecated
{
public:

	bool CheckAccess();
	void Change(bool change_owner_, bool change_priv_);

	void SetCur(Cur * pcur);
	void SetSystem(System * psystem);

private:

	Cur     * cur;
	System  * system;

	long user_id_file, group_id_file, user_id_dir, group_id_dir;
	int priv_file, priv_dir;
	bool subdirectories;
	bool change_owner, change_priv;

	bool ChangeOwner(Item & item, long user_id, long group_id);
	bool ChangePrivileges(Item & item, int privileges);
	void ChangePriv(Item & item, long user_id, long group_id, int privileges);
	void PrivLogStart(const wchar_t * what, long user, long group, int priv);
	void PrivLog(const wchar_t * what, long id, const std::wstring & url);
	void PrivFilesInDir(long parent_id);
	void PrivDir(long parent_id);
	void ReadPriv(const wchar_t * user_in, const wchar_t * group_in, const wchar_t * priv_in,
				   long & user_id, long & group_id, int & priv);
	void PrivDir();
	void PrivOneItem();
};



} // namespace Winix

#endif
