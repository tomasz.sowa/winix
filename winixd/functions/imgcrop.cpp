/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2013-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "imgcrop.h"
#include "core/misc.h"
#include "functions.h"



namespace Winix
{



namespace Fun
{

ImgCrop::ImgCrop()
{
	fun.url = L"imgcrop";
}



bool ImgCrop::has_access()
{
	if( cur->request->is_item )
		return system->HasWriteAccess(cur->request->item);

	return true;
}



void ImgCrop::GetDirContent()
{
//	iq.sel_type    = Item::file;
//	iq.sel_content = false;
//
//	iq.WhereParentId(cur->request->dir_tab.back()->id);
//	iq.WhereFileType(WINIX_ITEM_FILETYPE_IMAGE);

	morm::Finder<Item> finder(model_connector);

	item_tab = finder.
		select().
		where().
		eq(L"type", static_cast<int>(Item::file)).
		eq(L"parent_id", cur->request->dir_tab.back()->id).
		eq(L"content", L"file_type", WINIX_ITEM_FILETYPE_IMAGE).
		raw("order by item.sort_index asc, item.url asc, item.id asc").
		get_vector();

	//db->GetItems(cur->request->item_tab, iq);
	system->CheckWriteAccessToItems(item_tab);
}



void ImgCrop::make_post()
{
	int xoffset = int(Tod(cur->request->PostVar(L"cropxtop")) + 0.5);
	int yoffset = int(Tod(cur->request->PostVar(L"cropytop")) + 0.5);
	int width   = int(Tod(cur->request->PostVar(L"cropwidth")) + 0.5);
	int height  = int(Tod(cur->request->PostVar(L"cropheight")) + 0.5);

	SetMinMax(xoffset, 	0, 30000);
	SetMinMax(yoffset, 	0, 30000);
	SetMinMax(width, 	1, 30000);
	SetMinMax(height, 	1, 30000);

	Item & item = cur->request->item;

	if( cur->request->is_item && item.type == Item::file && item.item_content.file_type == WINIX_ITEM_FILETYPE_IMAGE )
	{
		if( system->HasWriteAccess(item) )
		{
			// !! IMPROVE ME add info about modification (Item::modify_time)
			if( cur->request->IsParam(L"thumb") )
			{
				Image::Scale scale = system->image.GetThumbScale(item.parent_id);
				system->image.CropThumb(item.id, xoffset, yoffset, width, height, scale.quality);
			}
			else
			if( cur->request->IsParam(L"newthumb") )
			{
				Image::Scale scale = system->image.GetThumbScale(item.parent_id);
				system->image.CropNewThumb(item.id, xoffset, yoffset, width, height, scale.cx, scale.cy,
											scale.aspect_mode, scale.quality);
			}
			else
			{
				Image::Scale scale = system->image.GetImageScale(item.parent_id);
				system->image.Crop(item.id, xoffset, yoffset, width, height, scale.quality);
			}

			// !! IMPROVE ME redirect me somewhere else
			system->RedirectToLastItem();
		}
		else
		{
			cur->request->status = WINIX_ERR_PERMISSION_DENIED;
		}
	}
}


void ImgCrop::make_get()
{
	if( !cur->request->is_item )
	{
		GetDirContent();
		cur->request->models.Add(L"item_tab", item_tab);
	}
}




} // namespace

} // namespace Winix

