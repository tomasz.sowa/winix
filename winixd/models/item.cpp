/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "models/item.h"
#include "core/misc.h"
#include "finder.h"
#include "core/request.h"
#include "templates/templates.h"
#include "core/session.h"
#include "core/log.h"



namespace Winix
{




Item::Item()
{
	Clear();
}



void Item::fields()
{
	//ItemModelData * item_model_data = dynamic_cast<ItemModelData>(get_model_data());

	int type_helper = static_cast<int>(type);

	field(L"id",						id, morm::FT::no_insertable | morm::FT::no_updatable | morm::FT::primary_key);
	field(L"parent_id",					parent_id);
	field(L"type",						type_helper);
	field(L"url",						url);
	field(L"subject",					subject);
	field(L"template",					html_template);
	field(L"sort_index",				sort_index);
	field(L"content_id", L"content", 	item_content, morm::FT::foreign_key);

	field(L"dir_link",					&Item::dir_link);
	field(L"link",						&Item::link);

	field(L"is_parent_for_current_dir",	&Item::is_parent_for_current_dir);
	field(L"is_current_dir",			&Item::is_current_dir);
	field(L"is_root_dir",				&Item::is_root_dir);

	field(L"type_is_symlink",			&Item::type_is_symlink);
	field(L"type_is_file",				&Item::type_is_file);
	field(L"type_is_dir",				&Item::type_is_dir);
	field(L"type_is_none",				&Item::type_is_none);

	field(L"url_is",					&Item::url_is);

	field(L"can_be_removed",			&Item::can_be_removed);
	field(L"has_read_access",			&Item::has_read_access);
	field(L"has_write_access",			&Item::has_write_access);
	field(L"has_read_write_access",		&Item::has_read_write_access);
	field(L"has_read_exec_access",		&Item::has_read_exec_access);

	field(L"execute",					&Item::execute);

	field(L"has_html_template", 		&Item::has_html_template);

	// may we should add a method setTypeFromInt(int t)?
	type = static_cast<Type>(type_helper);
}


void Item::table()
{
	table_name(L"core", L"item");
}


void Item::before_insert()
{
}


void Item::after_insert()
{
	get_last_sequence_for_primary_key(L"core.item_id_seq", id);
}


bool Item::insert(morm::ModelData * model_data, bool update_whole_tree)
{
	ItemModelData * item_model_data = dynamic_cast<ItemModelData*>(model_data);
	bool url_prepared_correctly = true;

	if( !item_model_data || item_model_data->prepare_unique_url )
	{
		url_prepared_correctly = prepare_url();
	}

	bool insert_status = morm::Model::insert(model_data, update_whole_tree);

	if( insert_status )
	{
		if( !url_prepared_correctly )
		{
			pt::Toa(id, url);
			insert_status = morm::Model::update(model_data, false);
		}
	}

	return insert_status;
}


bool Item::update(morm::ModelData * model_data, bool update_whole_tree)
{
	ItemModelData * item_model_data = dynamic_cast<ItemModelData*>(model_data);
	bool url_prepared_correctly = true;

	if( !item_model_data || item_model_data->prepare_unique_url )
	{
		CalcItemsHelper helper = calc_items_by_url(parent_id, url);

		if( helper.size == 1 && helper.item_id == id )
		{
			// the same item
		}
		else
		{
			url_prepared_correctly = prepare_url();
		}
	}

	bool update_status = morm::Model::update(model_data, update_whole_tree);

	if( update_status )
	{
		if( !url_prepared_correctly )
		{
			pt::Toa(id, url);
			update_status = morm::Model::update(model_data, false);
		}
	}

	return update_status;
}


void Item::Clear()
{
	id = -1;
	parent_id = -1;
	type = none;
	url.clear();
	subject.clear();
	html_template.clear();
	sort_index = 0;
	item_content.Clear();
}





bool Item::do_migration(int & current_table_version)
{
	bool ok = true;

	ok = ok && morm::Model::do_migration(current_table_version, 1, this, &Item::do_migration_to_1);
	ok = ok && morm::Model::do_migration(current_table_version, 2, this, &Item::do_migration_to_2);
	ok = ok && morm::Model::do_migration(current_table_version, 3, this, &Item::do_migration_to_3);
	ok = ok && morm::Model::do_migration(current_table_version, 4, this, &Item::do_migration_to_4);
	ok = ok && morm::Model::do_migration(current_table_version, 5, this, &Item::do_migration_to_5);

	return ok;
}


bool Item::do_migration_to_1()
{
	const char * str = R"sql(
		CREATE TABLE core.item (
			id serial,
			user_id integer,
			group_id integer,
			privileges integer,
			date_creation timestamp without time zone,
			date_modification timestamp without time zone,
			parent_id bigint,
			type smallint,
			url character varying(255),
			content_id bigint,
			subject character varying(255),
			guest_name character varying(20),
			modification_user_id integer,
			template character varying(255),
			link_to character varying(2048),
			link_redirect smallint,
			sort_index integer,
			meta text,
			ameta text
		);
	)sql";

	db_query(str);
	return true; // IMPROVEME remove me in the future: this is only for a moment until we do migration on all our sites
}


/*
 * directories didn't have an ItemContent object, so now we create such objects
 */
bool Item::do_migration_to_2()
{
	morm::Finder<Item> finder(model_connector);

	std::list<Item> list = finder.
			select().
			where().
			eq(L"type", static_cast<int>(Item::dir)).
			eq(L"content_id", -1).
			get_list();

	pt::Log * log = model_connector->get_logger();

	for(Item & item : list)
	{
		if( log )
		{
			(*log) << "Item: adding a content row corresponding to item id: " << item.id << ", type: " << (int)item.type << ", url: " << item.url
				   << ", subject: " << item.subject << pt::Log::logend;
		}

		item.item_content.set_save_mode(morm::Model::DO_INSERT_ON_SAVE);

		if( !item.save() )
			return false;
	}

	return true;
}



bool Item::do_migration_to_3()
{
	const char * str[] = {
		"update core.content set user_id = (select user_id from core.item where item.content_id = content.id limit 1);",
		"update core.content set group_id = (select group_id from core.item where item.content_id = content.id limit 1);",
		"update core.content set guest_name = (select guest_name from core.item where item.content_id = content.id limit 1);",
		"update core.content set modification_user_id = (select modification_user_id from core.item where item.content_id = content.id limit 1);",
		"update core.content set privileges = (select privileges from core.item where item.content_id = content.id limit 1);",
		"update core.content set date_creation = (select date_creation from core.item where item.content_id = content.id limit 1);",
		"update core.content set date_modification = (select date_modification from core.item where item.content_id = content.id limit 1);",
		"update core.content set link_to = (select link_to from core.item where item.content_id = content.id limit 1);",
		"update core.content set link_redirect = (select link_redirect from core.item where item.content_id = content.id limit 1);",
		"update core.content set meta = (select meta from core.item where item.content_id = content.id limit 1);",
		"update core.content set admin_meta = (select ameta from core.item where item.content_id = content.id limit 1);",
	};

	size_t len = sizeof(str) / sizeof(const char*);
	return db_query(str, len);
}



bool Item::do_migration_to_4()
{
	const char * str = R"sql(
		alter table core.item
		drop column user_id,
		drop column group_id,
		drop column guest_name,
		drop column modification_user_id,
		drop column privileges,
		drop column date_creation,
		drop column date_modification,
		drop column link_to,
		drop column link_redirect,
		drop column meta,
		drop column ameta;
	)sql";

	return db_query(str);
}


bool Item::do_migration_to_5()
{
	const char * str[] = {
		"ALTER TABLE core.item ADD CONSTRAINT item_pkey PRIMARY KEY (id);",
	};

	size_t len = sizeof(str) / sizeof(const char*);
	return db_query(str, len);
}



bool Item::can_remove_child(const User * current_user, long child_user_id) const
{
	bool res = false;

	if( type == Type::dir )
	{
		if( current_user && current_user->is_super_user )
		{
			res = true;
		}
		else
		{
			res = item_content.has_write_access(current_user);

			if( res && item_content.is_sticky_bit_set() )
			{
				// IMPROVEME what about if child_user_id is -1 (guest)
				res = (item_content.user_id == child_user_id);
			}
		}
	}

	return res;
}


bool Item::can_be_removed(const User * current_user) const
{
	bool res = false;
	const Dirs * dirs = get_dirs();

	if( dirs && parent_id != -1 )
	{
		const Item * parent_dir = dirs->GetDir(parent_id);

		if( parent_dir )
		{
			res = parent_dir->can_remove_child(current_user, id);
		}
	}

	return res;
}


bool Item::can_remove_child(long child_user_id) const
{
	return can_remove_child(get_current_user(), child_user_id);
}


bool Item::can_be_removed() const
{
	return can_be_removed(get_current_user());
}


bool Item::has_read_access() const
{
	return item_content.has_read_access();
}

bool Item::has_write_access() const
{
	return item_content.has_write_access();
}

bool Item::has_read_write_access() const
{
	return item_content.has_read_write_access();
}

bool Item::has_read_exec_access() const
{
	return item_content.has_read_exec_access();
}



bool Item::type_is_symlink() const
{
	return (type == Type::symlink);
}


bool Item::type_is_file() const
{
	return (type == Type::file);
}


bool Item::type_is_dir() const
{
	return (type == Type::dir);
}


bool Item::type_is_none() const
{
	return (type == Type::none);
}


bool Item::is_root_dir() const
{
	return (type == Type::dir && parent_id == -1);
}






CalcItemsHelper Item::calc_items_by_url(long parent_id, const std::wstring & url)
{
	morm::Finder<CalcItemsHelper> finder(model_connector);

	CalcItemsHelper helper = finder.
		select(morm::Select::no_auto_generated_columns).
		raw("select count(id) as size, min(id) as item_id from core.item").
		where().
		eq(L"parent_id", parent_id).
		eq(L"url", url).
		get();

	return helper;
}


/*
 * may we should check for '.' and '..' here too?
 */
bool Item::prepare_url()
{
std::wstring temp_url;
bool is_that_url;
const int max_index = 99;
size_t index = 1;
std::wstring postfix;

	// only root dir may not have the url
	if( parent_id != -1 && url.empty() )
		url = L"_";

	do
	{
		if( index > 1 )
		{
			postfix = L"_(";
			pt::Toa(index, postfix, false);
			postfix += L")";
		}

		PrepareNewFileName(url, postfix, temp_url);

		CalcItemsHelper helper = calc_items_by_url(parent_id, temp_url);

		if( helper.size > 0 )
		{
			is_that_url = true;
		}
		else
		{
			url = temp_url;
			is_that_url = false;
		}

		index += 1;
	}
	while( is_that_url && index <= max_index );

return !is_that_url;
}



void Item::propagate_connector()
{
	item_content.set_connector(model_connector);
}


// IMPROVEME move me to a better place
void Item::print_dir(EzcEnv & env)
{
	Request * req = get_request();

	if( req )
	{
		for(size_t a=0 ; a < req->dir_tab.size() ; ++a)
			env.out << req->dir_tab[a]->url << '/';
	}
}


// IMPROVEME move me to a better place
void Item::print_dir_without_slash(EzcEnv & env)
{
	Request * req = get_request();

	if( req )
	{
		for(size_t a=0 ; a < req->dir_tab.size() ; ++a)
		{
			env.out << req->dir_tab[a]->url;

			if( a < req->dir_tab.size()-1 )
				env.out << '/';
		}
	}
}





void Item::dir_link(EzcEnv & env)
{
	Dirs * dirs = get_dirs();
	std::vector<Item*> dir_tab;

	if( dirs && dirs->CreateDirTab(parent_id, dir_tab) )
	{
		 for(Item * pitem : dir_tab)
		 {
			 env.out << pitem->url << '/';
		 }
	}
}


void Item::link(EzcEnv & env)
{
	Config * config = get_config();
	Request * req = get_request();

	if( config && req )
	{
		TemplatesFunctions::doc_proto(env);

		if( !req->subdomain.empty() )
			env.out << req->subdomain << '.';

		env.out << config->base_url;

		if( parent_id == req->dir_tab.back()->id )
		{
			print_dir(env);
		}
		else
		{
			dir_link(env);
		}

		env.out << url;
	}
}


bool Item::is_parent_for_current_dir() const
{
	bool res = false;
	const Request * req = get_request();

	if( req )
	{
		if( req->dir_tab.size() > 1 )
		{
			res = (id == req->dir_tab[req->dir_tab.size() - 2]->id);
		}
	}

	return res;
}


bool Item::is_current_dir() const
{
	bool res = false;
	const Request * req = get_request();

	if( req )
	{
		res = (id == req->dir_tab.back()->id);
	}

	return res;
}


void Item::url_is(EzcEnv & env)
{
	env.res = (url == env.par);
}


void Item::execute(EzcEnv & env)
{
	if( has_read_access() )
	{
		PatternCacher * pattern_cacher = get_pattern_cacher();
		Request * request = get_request();
		Config * config = get_config();

		if( pattern_cacher && request && config )
		{
			HtmlTextStream item_run_content;
			TemplatesFunctions::EzcGen ezc_generator;

			Ezc::Pattern * p = pattern_cacher->GetPattern(*this);

			TemplatesFunctions::InitGenerator(ezc_generator, *request);
			ezc_generator.SetPattern(*p);

			if( config->allow_ezc_frames_in_executable_items )
				ezc_generator.Generate(item_run_content, request->out_streams);
			else
				ezc_generator.Generate(item_run_content);

			ItemContent::print_content(env.out, item_run_content.get_buffer(), item_content.content_raw_type, config->html_filter);
		}
	}
	else
	{
		Log * log = get_logger();

		if( log )
		{
			(*log) << "Item::execute: permission denied, no read_exec access to item id: " << id << ", url: " << url << ", subject" << subject;
		}
	}

}


bool Item::has_html_template()
{
	return !html_template.empty();
}


} // namespace Winix

