/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "user.h"
#include "core/misc.h"
#include "templates/templates.h"


namespace Winix
{



User::User()
{
	Clear();
}




void User::fields()
{
	field(L"id",					id, morm::FT::no_insertable | morm::FT::no_updatable | morm::FT::primary_key);
	field(L"login",					login);
	field(L"is_super_user",			is_super_user);

	field(L"has_pass",				has_pass);
	field(L"pass_type",				pass_type);
	field(L"password", L"",			password);
	field(L"pass_encrypted", L"", 	pass_encrypted, morm::FT::binary);
	field(L"is_pass_hash_salted",	is_pass_hash_salted);

	field(L"email",		 			email);
	field(L"notify",    			notify);
	field(L"env",   				env);
	field(L"admin_env", 			admin_env);
	field(L"status", 				status);
	field(L"locale_id", 			locale_id);
	field(L"time_zone_id", 			time_zone_id);

	field(L"", L"id_is", 			&User::id_is);
	field(L"", L"is_guest", 		&User::is_guest);
	field(L"", L"is_env_object", 		&User::is_env_object);
	field(L"", L"is_admin_env_object", 	&User::is_admin_env_object);

}


void User::table()
{
	table_name(L"core", L"user");
}


void User::after_select()
{

}

void User::after_insert()
{
	get_last_sequence_for_primary_key(L"core.user_id_seq", id);
}





void User::Clear()
{
	id = -1;
	login.clear();
	is_super_user = false;
	groups.clear();
	email.clear();
	notify = 0;
	env.clear();
	admin_env.clear();
	status = WINIX_ACCOUNT_BLOCKED;
	locale_id = 0;
	time_zone_id = 0;

	has_pass = false;
	pass_type = 0;
	is_pass_hash_salted = false;
	clear_passwords();
}


void User::clear_passwords()
{
	Overwrite(password);
	password.clear();

	Overwrite(pass_encrypted);
	pass_encrypted.clear();
}





bool User::IsMemberOf(long group) const
{
std::vector<long>::const_iterator i;

	for(i=groups.cbegin() ; i!=groups.cend() ; ++i)
		if( *i == group )
			return true;

return false;
}


bool User::do_migration(int & current_table_version)
{
	bool ok = true;

	ok = ok && morm::Model::do_migration(current_table_version, 1, this, &User::do_migration_to_1);
	ok = ok && morm::Model::do_migration(current_table_version, 2, this, &User::do_migration_to_2);
	ok = ok && morm::Model::do_migration(current_table_version, 3, this, &User::do_migration_to_3);
	ok = ok && morm::Model::do_migration(current_table_version, 4, this, &User::do_migration_to_4);

	return ok;
}




bool User::do_migration_to_1()
{
	const char * str = R"sql(
		CREATE TABLE core."user" (
			id serial,
			login character varying(255),
			password character varying(255),
			email character varying(255),
			notify integer,
			pass_type integer,
			pass_hash_salted boolean,
			pass_encrypted bytea,
			super_user boolean,
			env text,
			aenv text,
			status integer,
			locale_id integer,
			time_zone_id integer,
			has_pass boolean
		);
	)sql";

	return db_query(str);
	return true; // IMPROVEME remove me in the future: this is only for a moment until we do migration on all our sites
}


bool User::do_migration_to_2()
{
	const char * str[] = {
		"alter table core.user rename column aenv to admin_env",
		"alter table core.user rename column super_user to is_super_user",
		"alter table core.user rename column pass_hash_salted to is_pass_hash_salted",
	};

	size_t len = sizeof(str) / sizeof(const char*);
	return db_query(str, len);
}


bool User::do_migration_to_3()
{
	const char * str[] = {
		"ALTER TABLE core.\"user\" ADD CONSTRAINT user_pkey PRIMARY KEY (id);",
	};

	size_t len = sizeof(str) / sizeof(const char*);
	return db_query(str, len);
}


bool User::do_migration_to_4()
{
	const char * str[] = {
		"ALTER TABLE core.\"user\" ALTER COLUMN email TYPE character varying(320);",
	};

	size_t len = sizeof(str) / sizeof(const char*);
	return db_query(str, len);
}


void User::display_name(EzcEnv & env)
{
	std::wstring * dname = admin_env.get_wstr(L"display_name");

	if( dname && !IsWhite(*dname, true) )
		env.out << *dname;
	else
		env.out << login;
}


void User::id_is(EzcEnv & env)
{
	if( !env.par.empty() )
	{
		long par_user_id = pt::to_l(env.par);
		env.res = id == par_user_id;
	}
}


bool User::is_guest()
{
	return id == -1;
}


bool User::is_env_object()
{
	return env.is_object();
}


bool User::is_admin_env_object()
{
	return admin_env.is_object();
}



} // namespace Winix


