/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "winixmodel.h"
#include "core/session.h"



namespace Winix
{

Config * WinixModel::get_config()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_config();
	}

	return nullptr;
}


Request * WinixModel::get_request()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_request();
	}

	return nullptr;
}


Log * WinixModel::get_logger()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_logger();
	}

	return nullptr;
}


Dirs * WinixModel::get_dirs()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_dirs();
	}

	return nullptr;
}


Mounts * WinixModel::get_mounts()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_mounts();
	}

	return nullptr;
}


Users * WinixModel::get_users()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_users();
	}

	return nullptr;
}


Groups * WinixModel::get_groups()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_groups();
	}

	return nullptr;
}


Session * WinixModel::get_session()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_session();
	}

	return nullptr;
}


User * WinixModel::get_current_user()
{
	Session * session = get_session();

	if( session )
	{
		return session->puser;
	}

	return nullptr;
}


Locale * WinixModel::get_locale()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_locale();
	}

	return nullptr;
}


SessionManager * WinixModel::get_session_manager()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_session_manager();
	}

	return nullptr;
}


TimeZones * WinixModel::get_time_zones()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_time_zones();
	}

	return nullptr;
}


PatternCacher * WinixModel::get_pattern_cacher()
{
	WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_pattern_cacher();
	}

	return nullptr;
}



const Config * WinixModel::get_config() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_config();
	}

	return nullptr;
}


const Request * WinixModel::get_request() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_request();
	}

	return nullptr;
}


const Log * WinixModel::get_logger() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_logger();
	}

	return nullptr;
}


const Dirs * WinixModel::get_dirs() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_dirs();
	}

	return nullptr;
}


const Mounts * WinixModel::get_mounts() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_mounts();
	}

	return nullptr;
}


const Users * WinixModel::get_users() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_users();
	}

	return nullptr;
}


const Groups * WinixModel::get_groups() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_groups();
	}

	return nullptr;
}


const Session * WinixModel::get_session() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_session();
	}

	return nullptr;
}



const User * WinixModel::get_current_user() const
{
	const Session * session = get_session();

	if( session )
	{
		return session->puser;
	}

	return nullptr;
}



const Locale * WinixModel::get_locale() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_locale();
	}

	return nullptr;
}


const SessionManager * WinixModel::get_session_manager() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_session_manager();
	}

	return nullptr;
}


const TimeZones * WinixModel::get_time_zones() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_time_zones();
	}

	return nullptr;
}


const PatternCacher * WinixModel::get_pattern_cacher() const
{
	const WinixModelConnector * connector = get_winix_model_connector();

	if( connector )
	{
		return connector->get_winix_pattern_cacher();
	}

	return nullptr;
}







WinixModelConnector * WinixModel::get_winix_model_connector()
{
	if( model_connector )
	{
		return dynamic_cast<WinixModelConnector*>(model_connector);
	}

	return nullptr;
}


const WinixModelConnector * WinixModel::get_winix_model_connector() const
{
	if( model_connector )
	{
		return dynamic_cast<const WinixModelConnector*>(model_connector);
	}

	return nullptr;
}






}

