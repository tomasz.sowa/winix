/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "migration.h"
#include "core/log.h"
#include "finder.h"
#include <ctime>


namespace Winix
{


bool Migration::do_migration(morm::ModelConnector * model_connector, morm::Model & model)
{
	model.set_connector(model_connector);

	std::wstring table_name;
	model.get_table_name(table_name);

	morm::Finder<Migration> finder(model_connector);

	Migration migration = finder.
		select().
		where().
		eq(L"table_name", table_name).
		get();

	int current_table_version = 0;
	int old_table_version = 0;

	if( migration.found() )
	{
		old_table_version = current_table_version = migration.table_version;
	}
	else
	{
		migration.set_save_mode(morm::Model::DO_INSERT_ON_SAVE);
		migration.table_name = table_name;
	}

	migration.migration_date.FromTime(std::time(0));
	bool migration_status = model.do_migration(current_table_version);

	if( current_table_version != old_table_version )
	{
		migration.table_version = current_table_version;
		bool migration_save_status = migration.save();

		pt::Log * log = model_connector->get_logger();

		if( log )
		{
			(*log) << pt::Log::log2 << "Migration: table " << table_name << " has been migrated to version " << current_table_version << pt::Log::logend;
		}

		if( !migration_save_status && log )
		{
			(*log) << pt::Log::log1 << "Migration: table " << table_name << " has been migrated to version " << current_table_version;
			(*log) << " but there was a problem with saving this information in the migration table." << pt::Log::logend;
			(*log) << "Make sure the migration table is created and save/update following record there:" << pt::Log::logend;
			(*log) << migration << pt::Log::logend;
		}

		if( !migration_save_status )
		{
			migration_status = false;
		}
	}

	return migration_status;
}


bool Migration::do_migration(int & current_table_version)
{
	bool ok = true;

	ok = ok && morm::Model::do_migration(current_table_version, 1, this, &Migration::do_migration_to_1);

	return ok;
}


bool Migration::create_winix_schema()
{
	const char * str[] = {
		"create schema if not exists core",
	};

	return db_query(str, sizeof(str) / sizeof(const char*));
}


bool Migration::do_migration_to_1()
{
	const char * str = R"sql(
		create table core.migration (
		id serial,
		table_name varchar(255),
		table_version int,
		migration_date timestamp without time zone,
		primary key(id)
		);
	)sql";

	return db_query(str);
}



}

