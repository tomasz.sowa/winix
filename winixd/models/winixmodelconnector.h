/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_models_winixmodelconnector
#define headerfile_winix_models_winixmodelconnector

#include "modelconnector.h"


namespace Winix
{
class Config;
class Request;
class Log;
class Dirs;
class Mounts;
class Users;
class Groups;
class Session;
class Locale;
class SessionManager;
class TimeZones;
class PatternCacher;



class WinixModelConnector : public morm::ModelConnector
{
public:

	WinixModelConnector();

	Config *  get_winix_config();
	Request * get_winix_request();
	Log *     get_winix_logger();
	Dirs *    get_winix_dirs();
	Mounts *  get_winix_mounts();
	Users *   get_winix_users();
	Groups *  get_winix_groups();
	Session * get_winix_session();
	Locale *  get_winix_locale();
	SessionManager * get_winix_session_manager();
	TimeZones * get_winix_time_zones();
	PatternCacher * get_winix_pattern_cacher();


	const Config *  get_winix_config() const;
	const Request * get_winix_request() const;
	const Log *     get_winix_logger() const;
	const Dirs *    get_winix_dirs() const;
	const Mounts *  get_winix_mounts() const;
	const Users *   get_winix_users() const;
	const Groups *  get_winix_groups() const;
	const Session * get_winix_session() const;
	const Locale *  get_winix_locale() const;
	const SessionManager * get_winix_session_manager() const;
	const TimeZones * get_winix_time_zones() const;
	const PatternCacher * get_winix_pattern_cacher() const;

	void set_winix_config(Config * config);
	void set_winix_request(Request * request);
	void set_winix_logger(Log * log);
	void set_winix_dirs(Dirs * dirs);
	void set_winix_mounts(Mounts * mounts);
	void set_winix_users(Users * users);
	void set_winix_groups(Groups * groups);
	void set_winix_session(Session * session);
	void set_winix_locale(Locale * locale);
	void set_winix_session_manager(SessionManager * session_manager);
	void set_winix_time_zones(TimeZones * time_zones);
	void set_winix_pattern_cacher(PatternCacher * pattern_cacher);


protected:

	Config * config; // one global config
	Request * request; // each thread worker has its own request (not implemented yet)
	Log * log; // each thread has its own logger
	Dirs * dirs;
	Mounts * mounts;
	Users * users;
	Groups * groups;
	Session * session;
	Locale * locale;
	SessionManager * session_manager;
	TimeZones * time_zones;
	PatternCacher * pattern_cacher;

};

}

#endif
