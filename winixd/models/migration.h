/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_models_migration
#define headerfile_winix_models_migration

#include <string>
#include <vector>
#include "model.h"


namespace Winix
{



class Migration : public morm::Model
{
public:

	long id;
	std::wstring table_name;
	int table_version;
	pt::Date migration_date;


	void fields()
	{
		field(L"id",				id, morm::FT::no_insertable | morm::FT::no_updatable | morm::FT::primary_key);
		field(L"table_name", 		table_name);
		field(L"table_version", 	table_version);
		field(L"migration_date", 	migration_date);
	}


	void table()
	{
		morm::Model::table_name(L"core", L"migration");
	}


	void after_insert()
	{
		get_last_sequence_for_primary_key(L"core.migration_id_seq", id);
	}

	static bool do_migration(morm::ModelConnector * model_connector, morm::Model & model);


	bool create_winix_schema();
	bool do_migration(int & current_table_version);

private:

	bool do_migration_to_1();

};



} // namespace Winix



#endif
