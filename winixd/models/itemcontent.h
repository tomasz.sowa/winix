/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2021-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_models_itemcontent
#define headerfile_winix_models_itemcontent

#include <string>
#include "winixmodel.h"
#include "space/space.h"
#include "date/date.h"
#include "model.h"
#include "templates/htmltextstream.h"
#include "templates/misc.h"


namespace Winix
{


#define WINIX_ITEM_FILETYPE_NONE		0
#define WINIX_ITEM_FILETYPE_IMAGE		1
#define WINIX_ITEM_FILETYPE_DOCUMENT	2
#define WINIX_ITEM_FILETYPE_VIDEO		3
#define WINIX_ITEM_FILETYPE_UNKNOWN		10




class ItemContent : public WinixModel
{
public:

	/*
	 * IMPROVEME
	 *
	 * the kind of content type would be better to be a string
	 * so instead of 'ContentType content_raw_type' we would have 'std::wstring content_raw_type'
	 * this allows us to use plugin system to define its own types such as markdown
	 *
	 * if we show an editor (such as emacs) then we call plugins with a specified message
	 * and each plugin will put a string if it provides a content type mechanism
	 * (we can have pt::Space as a parameter in plugins - this can be a table of strings)
	 *
	 * after user clicks 'save' button we send another message with content_raw and content_raw_type
	 * and we expect content_raw_parsed to be made by a plugin
	 *
	 * or we have two content_type strings, one for internal use and the other for locale files (to show to a user)
	 * or even three, the last one used as a description
	 *
	 *
	 *
	 * file_type should be a string, we can use libmagick library to get the correct type
	 *
	 */

	/*
	 * may we should add ct_none? and this will be default (set in Clear method)
	 */
	enum ContentType
	{
		ct_text = 0,
		ct_formatted_text,
		ct_html,
		ct_bbcode,
		ct_other,			// no auto-formatting is applied
		ct_markdown,
	};



	/*
	 * record id
	 */
	long id;

	/*
	 * how many Items references to this
	 */
	int references;

	/*
	 * user owner
	 * -1 means the item was created by a not logged in user and in such a case a 'guest_name' field is set
	 */
	long user_id;

	/*
	 * group owner
	 */
	long group_id;

	/*
	 * a guest name in a case when the item was created by a not logged in user (user_id is -1)
	 */
	std::wstring guest_name;

	/*
	 *  who has modified the item last
	 *  this field is not taken into account when checking permissions
	 */
	long modification_user_id;

	/*
	 * privileges, e.g: 0755
	 * IMPROVEME need some info here
	 */
	int privileges;

	/*
	 * when the object was created
	 */
	pt::Date date_creation;

	/*
	 * when the object was last modified
	 */
	pt::Date date_modification;



	/*
	 * name of a link in the case whct_texten type is a symlink or a directory
	 */
	std::wstring link_to;

	/*
	 * whether or not winix should make a redirect to link_to link
	 * IMPROVE ME should it be 'bool'?
	 */
	int link_redirect;



	/*
	 * static file (if exists)
	 * relative file path
	 */
	std::wstring file_path;

	/*
	 * file system type where the file was saved
	 */
	int file_fs;

	/*
	 * file type (none, image, doc, etc)
	 */
	int file_type;

	/*
	 * mime file type e.g. image/jpeg
	 *
	 * CHANGEME
	 * we need to rething what to do with above file_type
	 *
	 * may would be better to have:
	 * remove file_type and add:
	 * bool has_static_file;
	 * std::wstring file_mime_type;
	 *
	 * may it would be good to have a seperate mime_type for the thumnail?
	 *
	 * IMPROVEME
	 * this mime type should be editable (add a 'mimetype' winix function)
	 * and can be return for normal items too
	 * can be renamed just to mime_type
	 *
	 */
	std::wstring file_mime_type;

	/*
	 * whether or not we have a thumbnail
	 */
	bool file_has_thumb;

	/*
	 * file hash (md4, md5, ...)
	 */
	std::wstring file_hash;

	/*
	 * hash type WINIX_CRYPT_HASH_* (see crypt.h)
	 */
	int file_hash_type;

	/*
	 * size of the file
	 */
	size_t file_size;


	
	/*
	 * object content as provided by the user
	 */
	std::wstring content_raw;

	/*
	 * raw content type
	 */
	ContentType content_raw_type;

	/*
	 * parsed object content
	 */
	std::wstring content_parsed;

	/*
	 * parsed content type
	 */
	ContentType content_parsed_type;



	/*
	 * meta information
	 * additional information in the form af a Space struct
	 */
	pt::Space meta;

	/*
	 * admin meta information
	 * additional information available to edit only by an admin
	 */
	pt::Space admin_meta;


	ItemContent();

	void fields();
	void table();
	void after_insert();


	void SetDateToNow();
	void SetDateModifyToNow();


	/*
	 * what about clear() from Model?
	 */
	void Clear();

	static bool CanContentBeHtmlFiltered(ItemContent::ContentType ct);
	static void print_content(HtmlTextStream & out, const pt::WTextStream & content, ItemContent::ContentType content_type, bool is_html_filter_on);
	static void print_content(HtmlTextStream & out, const std::wstring & content, ItemContent::ContentType content_type, bool is_html_filter_on);

	void print_content(HtmlTextStream & str);

	bool CanContentBeHtmlFiltered();

	bool has_read_access(const User * current_user) const;
	bool has_write_access(const User * current_user) const;
	bool has_read_write_access(const User * current_user) const;
	bool has_read_exec_access(const User * current_user) const;

	bool has_read_access() const;
	bool has_write_access() const;
	bool has_read_write_access() const;
	bool has_read_exec_access() const;

	bool is_sticky_bit_set() const;

	bool has_user() const;
	bool has_group() const;
	bool item_users_different();

	bool is_meta_object() const;
	bool is_admin_meta_object() const;

	bool are_dates_equal() const;

	bool is_link_to() const;
	bool is_link_redirect() const;

	static ContentType get_content_type_from_str(const wchar_t * str);
	static ContentType get_content_type_from_str(const std::wstring & str);

protected:

	bool do_migration(int & current_table_version);
	bool do_migration_to_1();
	bool do_migration_to_2();
	bool do_migration_to_3();
	bool do_migration_to_4();
	bool do_migration_to_5();
	bool do_migration_to_6();

	bool has_access(const User * current_user, int mask) const;
	bool content_type_is(const std::wstring & type);

	void print_content(EzcEnv & env);
	void has_static_file(EzcEnv & env);
	void privileges_octal(EzcEnv & env);
	void user(morm::Wrapper & wrapper);
	void group(morm::Wrapper & wrapper);
	void type_is(EzcEnv & env);
	void is_empty(EzcEnv & env);
	void file_type_is_none(EzcEnv & env);
	void file_type_is_image(EzcEnv & env);
	void file_type_is_video(EzcEnv & env);
	void file_type_is_sound(EzcEnv & env);
	void has_thumb(EzcEnv & env);
	void display_user_name(EzcEnv & env);


	MORM_MEMBER_FIELD(ItemContent)

};


} // namespace Winix



#endif
