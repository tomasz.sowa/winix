/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "winixmodelconnector.h"


namespace Winix
{


WinixModelConnector::WinixModelConnector()
{
	config = nullptr;
	request = nullptr;
	log = nullptr;
	dirs = nullptr;
	mounts = nullptr;
	users = nullptr;
	groups = nullptr;
	session = nullptr;
	locale = nullptr;
	session_manager = nullptr;
	time_zones = nullptr;
	pattern_cacher = nullptr;
}


Config * WinixModelConnector::get_winix_config()
{
	return config;
}


Request * WinixModelConnector::get_winix_request()
{
	return request;
}


Log * WinixModelConnector::get_winix_logger()
{
	return log;
}


Dirs * WinixModelConnector::get_winix_dirs()
{
	return dirs;
}


Mounts * WinixModelConnector::get_winix_mounts()
{
	return mounts;
}


Users * WinixModelConnector::get_winix_users()
{
	return users;
}


Groups * WinixModelConnector::get_winix_groups()
{
	return groups;
}


Session * WinixModelConnector::get_winix_session()
{
	return session;
}


Locale * WinixModelConnector::get_winix_locale()
{
	return locale;
}


SessionManager * WinixModelConnector::get_winix_session_manager()
{
	return session_manager;
}


TimeZones * WinixModelConnector::get_winix_time_zones()
{
	return time_zones;
}

PatternCacher * WinixModelConnector::get_winix_pattern_cacher()
{
	return pattern_cacher;
}








const Config * WinixModelConnector::get_winix_config() const
{
	return config;
}


const Request * WinixModelConnector::get_winix_request() const
{
	return request;
}


const Log * WinixModelConnector::get_winix_logger() const
{
	return log;
}


const Dirs * WinixModelConnector::get_winix_dirs() const
{
	return dirs;
}


const Mounts * WinixModelConnector::get_winix_mounts() const
{
	return mounts;
}


const Users * WinixModelConnector::get_winix_users() const
{
	return users;
}


const Groups * WinixModelConnector::get_winix_groups() const
{
	return groups;
}


const Session * WinixModelConnector::get_winix_session() const
{
	return session;
}


const Locale * WinixModelConnector::get_winix_locale() const
{
	return locale;
}


const SessionManager * WinixModelConnector::get_winix_session_manager() const
{
	return session_manager;
}


const TimeZones * WinixModelConnector::get_winix_time_zones() const
{
	return time_zones;
}


const PatternCacher * WinixModelConnector::get_winix_pattern_cacher() const
{
	return pattern_cacher;
}



void WinixModelConnector::set_winix_config(Config * config)
{
	this->config = config;
}


void WinixModelConnector::set_winix_request(Request * request)
{
	this->request = request;
}


void WinixModelConnector::set_winix_logger(Log * log)
{
	this->log = log;
}


void WinixModelConnector::set_winix_dirs(Dirs * dirs)
{
	this->dirs = dirs;
}


void WinixModelConnector::set_winix_mounts(Mounts * mounts)
{
	this->mounts = mounts;
}


void WinixModelConnector::set_winix_users(Users * users)
{
	this->users = users;
}


void WinixModelConnector::set_winix_groups(Groups * groups)
{
	this->groups = groups;
}


void WinixModelConnector::set_winix_session(Session * session)
{
	this->session = session;
}


void WinixModelConnector::set_winix_locale(Locale * locale)
{
	this->locale = locale;
}


void WinixModelConnector::set_winix_session_manager(SessionManager * session_manager)
{
	this->session_manager = session_manager;
}


void WinixModelConnector::set_winix_time_zones(TimeZones * time_zones)
{
	this->time_zones = time_zones;
}


void WinixModelConnector::set_winix_pattern_cacher(PatternCacher * pattern_cacher)
{
	this->pattern_cacher = pattern_cacher;
}




}

