/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "group.h"


namespace Winix
{


Group::Group()
{
	Clear();
}


void Group::fields()
{
	field(L"id",	id, morm::FT::no_insertable | morm::FT::no_updatable | morm::FT::primary_key);
	field(L"name", 	name);
}


void Group::table()
{
	table_name(L"core", L"group");
}


void Group::after_insert()
{
	get_last_sequence_for_primary_key(L"core.group_id_seq", id);
}


void Group::Clear()
{
	id = -1;
	name.clear();
	//members.clear();
}





bool Group::do_migration(int & current_table_version)
{
	bool ok = true;

	ok = ok && morm::Model::do_migration(current_table_version, 1, this, &Group::do_migration_to_1);
	ok = ok && morm::Model::do_migration(current_table_version, 2, this, &Group::do_migration_to_2);
	ok = ok && morm::Model::do_migration(current_table_version, 3, this, &Group::do_migration_to_3);

	return ok;
}


bool Group::do_migration_to_1()
{
	const char * str = R"sql(
		CREATE TABLE core."group" (
			id serial,
			"group" character varying(20)
		);
	)sql";

	db_query(str);
	return true;
}


bool Group::do_migration_to_2()
{
	const char * str = R"sql(
		alter table core.group rename column "group" to "name";
	)sql";

	return db_query(str);
}


bool Group::do_migration_to_3()
{
	const char * str[] = {
		"ALTER TABLE core.\"group\" ADD CONSTRAINT group_pkey PRIMARY KEY (id);",
	};

	size_t len = sizeof(str) / sizeof(const char*);
	return db_query(str, len);
}


}


