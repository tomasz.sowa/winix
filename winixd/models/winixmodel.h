/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_models_winixmodel
#define headerfile_winix_models_winixmodel

#include "model.h"
#include "core/log.h"
#include "winixmodelconnector.h"


namespace Winix
{
class Config;
class Request;
class Log;
class Dirs;
class Mounts;
class Users;
class Groups;
class Session;
class User;
class Locale;
class SessionManager;
class TimeZones;
class PatternCacher;


class WinixModel : public morm::Model
{
public:

	Config * get_config();
	Request * get_request();
	Log * get_logger();
	Dirs * get_dirs();
	Mounts * get_mounts();
	Users * get_users();
	Groups * get_groups();
	Session * get_session();
	User * get_current_user();
	Locale * get_locale();
	SessionManager * get_session_manager();
	TimeZones * get_time_zones();
	PatternCacher * get_pattern_cacher();

	const Config * get_config() const;
	const Request * get_request() const;
	const Log * get_logger() const; // there is no need for logger to be const, we can do nothing with such a logger
	const Dirs * get_dirs() const;
	const Mounts * get_mounts() const;
	const Users * get_users() const;
	const Groups * get_groups() const;
	const Session * get_session() const;
	const User * get_current_user() const;
	const Locale * get_locale() const;
	const SessionManager * get_session_manager() const;
	const TimeZones * get_time_zones() const;
	const PatternCacher * get_pattern_cacher() const;



protected:

	WinixModelConnector * get_winix_model_connector();
	const WinixModelConnector * get_winix_model_connector() const;


};

}

#endif
