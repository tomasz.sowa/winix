/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_models_item
#define headerfile_winix_models_item

#include <string>
#include "winixmodel.h"
#include "itemcontent.h"
#include "funinfo.h"
#include "templates/htmltextstream.h"
#include "templates/misc.h"



namespace Winix
{

// IMPROVEME rename me and move to a better place
class CalcItemsHelper : public morm::Model
{
public:

	long size;
	long item_id;


	void fields()
	{
		field(L"size", size);
		field(L"item_id", item_id);
	}

	void table()
	{
		table_name(L"core", L"item");
	}

};


// IMPROVEME rename me and move to a better place
class ItemModelData : public morm::ModelData
{
public:

	ItemModelData()
	{
		prepare_unique_url = true;
	}

	bool prepare_unique_url;
};


class Item : public WinixModel
{
public:


	enum Type
	{
		dir  = 0,
		file = 1,
		symlink = 2,

		none = 1000
	};



	/*
	 *
	 */
	long id;

	/*
	 *
	 */
	long parent_id;

	/*
	 * may change to int?
	 */
	Type type;

	/*
	 *
	 */
	std::wstring url;

	/*
	 *
	 */
	std::wstring subject;

	/*
	 *
	 */
	std::wstring html_template;

	/*
	 * sort index used when displaying a group of items
	 */
	int sort_index;

	/*
	 *
	 */
	ItemContent item_content;




	Item();

	void fields();
	void table();
	void before_insert();
	void after_insert();

	/*
	 * what about clear() from Model?
	 */
	void Clear();


	bool prepare_url();


	using morm::Model::insert;
	using morm::Model::update;

	bool insert(morm::ModelData * model_data, bool update_whole_tree = true);
	bool update(morm::ModelData * model_data, bool update_whole_tree = true);


	/*
	 * IMPROVEME
	 * when removing we should check whether the item_content.references is zero and then remove it
	 * in other cases only decrement item_content.references
	 *
	 */



	void propagate_connector();

	bool can_remove_child(const User * current_user, long child_user_id) const;
	bool can_be_removed(const User * current_user) const;
	bool can_remove_child(long child_user_id) const;
	bool can_be_removed() const;

	bool has_read_access() const;
	bool has_write_access() const;
	bool has_read_write_access() const;
	bool has_read_exec_access() const;

	bool type_is_symlink() const;
	bool type_is_file() const;
	bool type_is_dir() const;
	bool type_is_none() const;

	bool is_root_dir() const;
	bool is_parent_for_current_dir() const;
	bool is_current_dir() const;

	bool has_html_template();

protected:

	CalcItemsHelper calc_items_by_url(long parent_id, const std::wstring & url);

	bool do_migration(int & current_table_version);
	bool do_migration_to_1();
	bool do_migration_to_2();
	bool do_migration_to_3();
	bool do_migration_to_4();
	bool do_migration_to_5();

	void print_dir(EzcEnv & env);
	void print_dir_without_slash(EzcEnv & env);
	void dir_link(EzcEnv & env);
	void link(EzcEnv & env);
	void url_is(EzcEnv & env);
	void execute(EzcEnv & env);


	MORM_MEMBER_FIELD(Item)

};


} // namespace Winix



#endif
