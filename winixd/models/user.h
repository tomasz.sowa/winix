/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_models_user
#define headerfile_winix_models_user

#include <string>
#include <vector>
#include "model.h"
#include "space/space.h"
#include "date/date.h"
#include "templates/misc.h"
#include "winixmodel.h"


namespace Winix
{



#define WINIX_ACCOUNT_MAX_LOGIN_SIZE		250
#define WINIX_ACCOUNT_MAX_PASSWORD_SIZE		250
#define WINIX_ACCOUNT_MAX_EMAIL_SIZE		250



// account status
// 1 - a user has created its account -- an email was sent back to him
#define WINIX_ACCOUNT_NOT_ACTIVATED		1

// 2 - a user clicked on the link in the mail and now can normally use his account
// (if has a password set too)
#define WINIX_ACCOUNT_READY				2

// 3 - account was suspended
#define WINIX_ACCOUNT_SUSPENDED			3

// 4 - account was banned
#define WINIX_ACCOUNT_BLOCKED			4





/*
	a user can login only to an account which status is equal to WINIX_ACCOUNT_READY

	actually there is no difference between WINIX_ACCOUNT_SUSPENDED and WINIX_ACCOUNT_BANNED
	only a different message will be present on the website

	you can use other values of status in your plugins - this not have any impact on winix
	the default 'login' winix function only allowes to login a user who has WINIX_ACCOUNT_READY value
	but you can provide your own 'login' function which can work in a different way

	winix knows that user is login when cur->session->puser pointer is set
	(when the pointer is not null then winix do not check what the value of 'status' is --
	the status is only tested in 'login' function)
*/
class User  : public WinixModel
{
public:

	long id;
	std::wstring login;
	bool is_super_user;


	bool has_pass;				// true if the user has a password set
								// if false the user cannot login
	int pass_type;				// the kind of hash (WINIX_CRYPT_HASH_* see crypt.h)
	std::wstring password;		// password hashed or plain text if pass_type==0
	std::string pass_encrypted; // password encrypted
	bool is_pass_hash_salted;		// true when the hash was salted (plain text passwords are never salted)


	std::wstring email;
	int notify;

	// environment variables which can be set by this user
	// use 'env' winix function
	pt::Space env;

	// environment variables set only by an administrator
	// an administrator can use 'env' winix function with 'a' parameter
	// IMPROVEME rename me to something better (env_admin?)
	pt::Space admin_env;

	// account status
	// WINIX_ACCOUNT_*
	// a user can normally login only when status is WINIX_ACCOUNT_READY
	int status;

	// locale identifier
	size_t locale_id;

	// time zone identifier
	size_t time_zone_id;


	std::vector<long> groups;


	User();
	
	void fields();
	void table();
	void after_insert();
	void after_select();

	void Clear(); // IMPROVEME what about clear() from Model?
	bool IsMemberOf(long group) const;
	bool ReadMonthDayTime(pt::Date & date, const wchar_t * str);
	bool SetTzFromEnv();

	void clear_passwords();

	void display_name(EzcEnv & env);


	bool is_env_object();
	bool is_admin_env_object();


private:

	bool do_migration(int & current_table_version);
	bool do_migration_to_1();
	bool do_migration_to_2();
	bool do_migration_to_3();
	bool do_migration_to_4();

	void id_is(EzcEnv & env); // takes one argument as a user id
	bool is_guest();



	MORM_MEMBER_FIELD(User)
};


} // namespace Winix


#endif
