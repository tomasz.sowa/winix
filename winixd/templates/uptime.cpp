/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <ctime>
#include "templates.h"

namespace Winix
{


namespace TemplatesFunctions
{



void uptime_more_than_one_day(Info & i)
{
	time_t up   = time(0) - system->system_start;
	time_t days = up / 60 / 60 / 24;

	i.res = ( days > 1 );
}


void uptime_days(Info & i)
{
	time_t up   = time(0) - system->system_start;
	time_t days = up / 60 / 60 / 24;

	i.out << days;
}


void uptime_hours(Info & i)
{
char buf[50];

	time_t sec  = time(0) - system->system_start;
	time_t min  = sec / 60;
	time_t hour = min / 60;

	if( hour == 0 && min == 0 )
		sprintf(buf, "%d:%02d:%02d", (int)hour, (int)(min%60), (int)(sec%60));
	else
		sprintf(buf, "%d:%02d", (int)hour, (int)(min%60));

	i.out << buf;
}





} // namespace

} // namespace Winix

