/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2016, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"

namespace Winix
{



namespace TemplatesFunctions
{



void user_logged(Info & i)
{
	i.res = cur->session->puser != 0;
}


void user_id(Info & i)
{
	if( cur->session->puser )
		i.out << cur->session->puser->id;
}

void user_name(Info & i)
{
	if( cur->session->puser )
		i.out << cur->session->puser->login;
}


// should be at least in one group
// !! moze lepsza nazwa?
void user_is_in_group(Info & i)
{
	if( !cur->session->puser || i.params.empty() )
		return;

	for(size_t a=0 ; a<i.params.size() ; ++a)
	{
		long gid = system->groups.GetGroupId(i.params[a].str);

		if( gid!=-1 && cur->session->puser->IsMemberOf(gid) )
		{
			i.res = true;
			break;
		}
	}
}


// !! IMPROVE ME moze lepsza nazwa?
void user_is_in_all_groups(Info & i)
{
	if( !cur->session->puser || i.params.empty() )
		return;

	for(size_t a=0 ; a<i.params.size() ; ++a)
	{
		long gid = system->groups.GetGroupId(i.params[a].str);

		if( gid==-1 || !cur->session->puser->IsMemberOf(gid) )
			return;
	}

	i.res = true;
}


void user_super_user(Info & i)
{
	if( cur->session->puser && cur->session->puser->is_super_user )
		i.res = true;
}


void user_can_use_html(Info & i)
{
	if( !cur->session->puser )
		i.res = false;
	else
		i.res = system->CanUseHtml(cur->session->puser->id);
}


void user_can_use_bbcode(Info & i)
{
	if( !cur->session->puser )
		i.res = false;
	else
		i.res = system->CanUseBBCode(cur->session->puser->id);
}


void user_can_use_other(Info & i)
{
	if( !cur->session->puser )
		i.res = false;
	else
		i.res = system->CanUseOther(cur->session->puser->id);
}



void user_has_correct_time_zone(Info & i)
{
	if( cur->session->puser )
		i.res = system->time_zones.HasZone(cur->session->puser->time_zone_id);
}


void user_time_zone_name(Info & i)
{
	if( cur->session->puser )
	{
		TimeZone * tz = system->time_zones.GetZone(cur->session->puser->time_zone_id);

		if( tz )
			i.out << locale.Get(tz->name);
	}
}


void user_time_zone_id(Info & i)
{
	if( cur->session->puser )
		i.out << cur->session->puser->time_zone_id;
}


void user_time_zone_offset_hour_min(Info & i)
{
	if( cur->session->puser )
	{
		TimeZone * tz = system->time_zones.GetZoneByIndex(cur->session->puser->time_zone_id);

		if( tz )
		{
			time_t offset = tz->offset;

			if( offset < 0 )
			{
				i.out << '-';
				offset = -offset;
			}
			else
			{
				i.out << '+';
			}

			print_hour_min(i, offset);
		}
	}
}


void user_has_correct_locale(Info & i)
{
	if( cur->session->puser )
		i.res = locale.HasLanguage(cur->session->puser->locale_id);
}


void user_locale_name(Info & i)
{
	if( cur->session->puser )
		i.out << locale.Get(L"locale_name");
}


void user_locale_id(Info & i)
{
	if( cur->session->puser )
		i.out << cur->session->puser->locale_id;
}




static Users::Iterator user_iter;
static size_t user_reqid = 0;
static size_t user_index; // only information


bool user_tab_init()
{
	if( user_reqid != cur->request->id )
	{
		user_reqid = cur->request->id;
		user_iter  = system->users.Begin();
	}

return user_iter != system->users.End();
}


void user_tab(Info & i)
{
	user_tab_init();
	user_index = i.iter;

	if( i.iter == 0 )
		user_iter = system->users.Begin();
	else
	if( user_iter != system->users.End() )
		++user_iter;

	i.res = user_iter != system->users.End();
}


void user_tab_index(Info & i)
{
	i.out << (user_index+1);
}

void user_tab_id(Info & i)
{
	if( user_tab_init() )
		i.out << user_iter->id;
}


void user_tab_name(Info & i)
{
	if( user_tab_init() )
		i.out << user_iter->login;
}


void user_tab_is_super_user(Info & i)
{
	if( user_tab_init() )
		i.res = user_iter->is_super_user;
}


void user_tab_is_active(Info & i)
{
	if( user_tab_init() )
		i.res = user_iter->status == WINIX_ACCOUNT_READY;
}

void user_tab_is_suspended(Info & i)
{
	if( user_tab_init() )
		i.res = user_iter->status == WINIX_ACCOUNT_SUSPENDED;
}

void user_tab_is_blocked(Info & i)
{
	if( user_tab_init() )
		i.res = user_iter->status == WINIX_ACCOUNT_BLOCKED;
}

void user_tab_is_current(Info & i)
{
	if( user_tab_init() && cur->session->puser )
		i.res = (user_iter->id == cur->session->puser->id);
}




} // namespace TemplatesFunctions

} // namespace Winix



