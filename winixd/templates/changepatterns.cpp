/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "changepatterns.h"
#include "core/log.h"

namespace Winix
{



void ChangePatterns::SetPatterns(Patterns * ppatterns)
{
	patterns = ppatterns;
}



void ChangePatterns::Add(long mount_dir_id, const std::wstring & old_pattern_name, const std::wstring & new_pattern_name)
{
	Value & value = pat_tab[mount_dir_id];
	std::pair<Value::iterator, bool> res = value.insert(std::make_pair(old_pattern_name, Template()));

	Template & tmpl = res.first->second;
	tmpl.to_delete = false;

	if( res.second || new_pattern_name != patterns->GetFileName(tmpl.index) )
		tmpl.index = patterns->Add(new_pattern_name);
}





Ezc::Pattern * ChangePatterns::Get(long mount_dir_id, const std::wstring & old_pattern_name, size_t locale_index)
{
	PatTab::iterator i = pat_tab.find(mount_dir_id);

	if( i == pat_tab.end() )
		return 0;

	Value & value = i->second;
	Value::iterator v = value.find(old_pattern_name);

	if( v == value.end() )
		return 0;
	
	Template & tmpl = v->second;

	log << log4 << "ChangePatterns: changing " << old_pattern_name
		<< " to " << patterns->GetFileName(tmpl.index) << logend;

return patterns->Get(tmpl.index, locale_index);
}




void ChangePatterns::MarkAllToDelete()
{
PatTab::iterator i;
Value::iterator v;

	for(i=pat_tab.begin() ; i != pat_tab.end() ; ++i)
	{
		Value & value = i->second;

		for(v=value.begin() ; v != value.end() ; ++v)
			v->second.to_delete = true;
	}
}


void ChangePatterns::DeleteMarked()
{
PatTab::iterator i;
Value::iterator v, next;

	for(i=pat_tab.begin() ; i != pat_tab.end() ; ++i)
	{
		Value & value = i->second;

		for(v=value.begin() ; v != value.end() ; )
		{
			next = v;
			++next;

			if( v->second.to_delete )
			{
				patterns->Erase(v->second.index);
				value.erase(v);
			}

			v = next;
		}
	}
}



void ChangePatterns::Clear()
{
	pat_tab.clear();
}




} // namespace Winix

