/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/misc.h"
#include "core/mounts.h"

namespace Winix
{


namespace TemplatesFunctions
{

void mount_type_is(Info & i)
{
	int mount_type_id = system->mounts.FindMountType(i.par);

	if( mount_type_id == -1 )
		return;

	i.res = (cur->mount->type == mount_type_id);
}



void mount_page_arg_is(Info & i)
{
	i.res = cur->mount->IsArg(system->mounts.MountParPage(), i.par);
}


void mount_lang_arg(Info & i)
{
	i.out << cur->mount->FirstArg(system->mounts.MountParLang());
}


void mount_lang_arg_is(Info & i)
{
	i.res = cur->mount->IsArg(system->mounts.MountParLang(), i.par);
}



void mount_has_html_template(Info & i)
{
	i.res = cur->mount->param[system->mounts.MountParHtmlTemplate()].defined;
}


void mount_first_html_template(Info & i)
{
	Mount::ParamRow & par = cur->mount->param[system->mounts.MountParHtmlTemplate()];

	if( par.defined && !par.arg.empty() )
		i.out << par.arg[0];
}






static size_t mount_css_index = 0;

void mount_css_tab(Info & i)
{
	int parcss = system->mounts.MountParCss();
	

	if( !cur->mount->param[parcss].defined )
		return;

	mount_css_index = i.iter;
	i.res = mount_css_index < cur->mount->param[parcss].arg.size();
}


void mount_css_tab_file(Info & i)
{
	int parcss = system->mounts.MountParCss();
	
	if( mount_css_index < cur->mount->param[parcss].arg.size() )
		i.out << cur->mount->param[parcss].arg[mount_css_index];
}


void mount_css_tab_file_is_global(Info & i)
{
	int parcss = system->mounts.MountParCss();
	
	if( mount_css_index < cur->mount->param[parcss].arg.size() )
		i.res = pt::is_substr(L"http://",  cur->mount->param[parcss].arg[mount_css_index].c_str()) ||
				pt::is_substr(L"https://", cur->mount->param[parcss].arg[mount_css_index].c_str());
}



void mount_css_tab_has_next(Info & i)
{
	int parcss = system->mounts.MountParCss();
	
	if( !cur->mount->param[parcss].defined )
		return;

	i.res = (mount_css_index + 1 < cur->mount->param[parcss].arg.size());
}


size_t mount_css_size()
{
	int parcss = system->mounts.MountParCss();
	
	if( !cur->mount->param[parcss].defined )
		return 0;

return cur->mount->param[parcss].arg.size();
}



void mount_css_is_empty(Info & i)
{
	i.res = mount_css_size() == 0;
}


void mount_css_is_one(Info & i)
{
	i.res = mount_css_size() == 1;
}


void mount_css_more_than_one(Info & i)
{
	i.res = mount_css_size() > 1;
}






static bool mount_tab_inited = false;
static Mounts::MountTab::const_iterator mount_iter;
static std::wstring dir_str;



void mount_cur_type(Info & i)
{
	i.out << system->mounts.GetMountType(cur->mount->type);
}


void mount_cur_dir(Info & i)
{
	Item * pdir = system->dirs.GetDir(cur->mount->dir_id);

	if( pdir && system->dirs.MakePath(pdir->id, dir_str) )
		i.out << dir_str;
}


void mount_cur_fs(Info & i)
{
	i.out << system->mounts.GetMountFs(cur->mount->fs);
}


void mount_print_parlist(Info & i, const Mount::Param & param)
{
	bool was_printed = false;

	for(size_t p=0 ; p < param.size() ; ++p)
	{
		if( param[p].defined )
		{
			if( was_printed )
				i.out << ", ";

			i.out << system->mounts.GetMountPar(p);
			was_printed = true;

			if( !param[p].arg.empty() )
			{
				i.out << "(";

				for(size_t a=0 ; a < param[p].arg.size() ; ++a)
				{
					i.out << param[p].arg[a];

					if( a + 1 < param[p].arg.size() )
						i.out << ", ";
				}

				i.out << ")";
			}
		}
	}
}


void mount_cur_parlist(Info & i)
{
	mount_print_parlist(i, cur->mount->param);
}



void mount_tab(Info & i)
{
	const Mounts::MountTab * pmount_tab = system->mounts.GetMountTab();

	if( i.iter == 0 )
	{
		mount_iter = pmount_tab->begin();
	}
	else
	{
		if( mount_iter != pmount_tab->end() )
			++mount_iter;
	}	

	mount_tab_inited = (mount_iter != pmount_tab->end());
	i.res = mount_tab_inited;
}


void mount_tab_type(Info & i)
{
	if( mount_tab_inited )
	{
		i.out << system->mounts.GetMountType(mount_iter->second.type);
	}
}


void mount_tab_dir(Info & i)
{
	if( mount_tab_inited )
	{
		Item * pdir = system->dirs.GetDir(mount_iter->second.dir_id);

		if( pdir && system->dirs.MakePath(pdir->id, dir_str) )
			i.out << dir_str;
	}
}



void mount_tab_fs(Info & i)
{
	if( mount_tab_inited )
	{
		i.out << system->mounts.GetMountFs(mount_iter->second.fs);
	}
}






void mount_tab_parlist(Info & i)
{
	if( !mount_tab_inited )
		return;
	
	mount_print_parlist(i, mount_iter->second.param);
}





} // namespace TemplatesFunctions

} // namespace Winix



