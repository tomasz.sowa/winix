/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"

namespace Winix
{
namespace TemplatesFunctions
{

static const int ANTISPAM_OPERATOR_PLUS = 0;
static const int ANTISPAM_OPERATOR_MINUS = 1;

static int last_operator = 0;
static size_t last_form_id;


void antispam_create_new_form_id_for_this_session(Info & i)
{
	if( cur->session->id != 0 )
	{
		if( cur->session->antispan.size() > config->antispam_list_max_size )
		{
			log << log2 << "AS: antispam table exceeds size limit, clearing the whole list" << logend;
			cur->session->antispan.clear();
		}

		last_form_id = cur->session->antispan.size();
		i.out << last_form_id;
		cur->session->antispan[last_form_id] = 0;
	}
}


void antispam_loop(Info & i)
{
	if( i.res == 0 )
	{
		last_operator = 0;
	}

	i.res = i.iter < 10;
}


void antispam_loop_operator(Info & i)
{
	last_operator = rand() % 2;

	switch( last_operator )
	{
		case ANTISPAM_OPERATOR_PLUS:
			i.out << "+=";
			break;

		case ANTISPAM_OPERATOR_MINUS:
			i.out << "-=";
			break;
	}
}



void antispam_counter(Info & i)
{
	if( !cur->session->antispan.empty() )
	{
		int val = rand() % 200;

		switch( last_operator )
		{
			case ANTISPAM_OPERATOR_PLUS:
				cur->session->antispan[last_form_id] += val;
				break;

			case ANTISPAM_OPERATOR_MINUS:
				cur->session->antispan[last_form_id] -= val;
				break;
		}

		i.out << val;
	}
}




} // namespace TemplatesFunctions
} // namespace Winix



