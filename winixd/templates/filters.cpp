/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/misc.h"
#include "convert/misc.h"



namespace Winix
{


namespace TemplatesFunctions
{


void fil_urlencode(Info & i)
{
	UrlEncode(i.in.get_buffer(), i.out.get_buffer(), false);
}


void fil_qencode(Info & i)
{
	std::wstring tmp_str;
	std::string qencode_tmp;

	i.in.to_str(tmp_str);
	QEncode(tmp_str, qencode_tmp);

	i.out << R(qencode_tmp);
}


void fil_capitalize(Info & i)
{
	for(size_t a=0 ; a < i.in.size() ; ++a)
		i.out << R(locale.ToCapital(i.in.get_wchar(a)));
}


void fil_tosmall(Info & i)
{
	for(size_t a=0 ; a < i.in.size() ; ++a)
		i.out << R(locale.ToSmall(i.in.get_wchar(a)));
}


// a first letter in a sentence will be capitalized
void fil_firstup(Info & i)
{
	bool was_dot = true;
	
	for(size_t a=0 ; a < i.in.size() ; ++a)
	{
		wchar_t c = i.in.get_wchar(a);

		if( was_dot )
		{
			if( c!=' ' && c!='\t' && c!=13 && c!=10 && c!=160 )
				was_dot = false;

			i.out << R(locale.ToCapital(c));
		}
		else
		{
			i.out << R(c);
		}

		if( c == '.' )
			was_dot = true;
	}
}


// a first letter in each word will be capitalized
void fil_first_wordup(Info & i)
{
	bool was_white = true;
	
	for(size_t a=0 ; a < i.in.size() ; ++a)
	{
		wchar_t c = i.in.get_wchar(a);

		if( was_white )
		{
			i.out << R(locale.ToCapital(c));
		}
		else
		{
			i.out << R(c);
		}

		was_white = (c==' ' || c=='\t' || c==13 || c==10 || c==160);
	}
}


bool fil_csv_has_colon_or_quote(const pt::WTextStream & str)
{
	for(size_t i=0 ; i<str.size() ; ++i)
	{
		wchar_t c = str.get_wchar(i);

		if( c == ',' || c == '"' )
			return true;
	}

return false;
}



void fil_csv_escape(Info & i)
{
	if( fil_csv_has_colon_or_quote(i.in.get_buffer()) )
	{
		i.out << R("\"");

		for(size_t a=0 ; a < i.in.size() ; ++a)
		{
			wchar_t c = i.in.get_wchar(a);

			if( c == '"' )
				i.out << R("\"\"");
			else
				i.out << R(c);
		}

		i.out << R("\"");
	}
	else
	{
		i.out << i.in;
	}
}


void fil_json_escape(Info & i)
{
	pt::WTextStream str;

	for(size_t a=0 ; a < i.in.size() ; ++a)
	{
		wchar_t c = i.in.get_wchar(a);
		pt::esc_to_json(c, str);
		i.out << R(str);
		str.clear();
	}
}


void fil_new_line_to_br(Info & i)
{
	for(size_t a=0 ; a < i.in.size() ; ++a)
	{
		wchar_t c = i.in.get_wchar(a);

		if( c == '\n' )
			i.out << R("<br>\n");
		else
			i.out << R(c);
	}
}



/*
 *  "  -> &#quot;
 *  '  -> &#39; (&apos; but IE8 has a problem with &apos;)
 */
void fil_html_quote(Info & i)
{
	for(size_t a=0 ; a < i.in.size() ; ++a)
	{
		wchar_t c = i.in.get_wchar(a);

		if( c == '\"' )
			i.out << R("&quot;");
		else
		if( c == '\'' )
			i.out << R("&#39;");
		else
			i.out << R(c);
	}
}


/*
 *  10 -> &#10;
 *  13 -> &#13;
 */
void fil_html_newline(Info & i)
{
	for(size_t a=0 ; a < i.in.size() ; ++a)
	{
		wchar_t c = i.in.get_wchar(a);

		if( c == 10 )
			i.out << R("&#10;");
		else
		if( c == 13 )
			i.out << R("&#13;");
		else
			i.out << R(c);
	}
}



} // namespace

} // namespace Winix

