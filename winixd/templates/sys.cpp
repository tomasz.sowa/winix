/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/version.h"
#include "pikotools/version.h"
#include "ezc.h"
#include "tito.h"



namespace Winix
{



namespace TemplatesFunctions
{


void sys_ver_major(Info & i)
{
	i.out << WINIX_VER_MAJOR;
}


void sys_ver_minor(Info & i)
{
	i.out << WINIX_VER_MINOR;
}



void sys_ver_revision(Info & i)
{
	i.out << WINIX_VER_REVISION;
}



void sys_pikotools_ver_major(Info & i)
{
	i.out << PIKOTOOLS_VERSION_MAJOR;
}


void sys_pikotools_ver_minor(Info & i)
{
	i.out << PIKOTOOLS_VERSION_MINOR;
}


void sys_pikotools_ver_patch(Info & i)
{
	i.out << PIKOTOOLS_VERSION_PATCH;
}



void sys_morm_ver_major(Info & i)
{
	i.out << MORM_VERSION_MAJOR;
}


void sys_morm_ver_minor(Info & i)
{
	i.out << MORM_VERSION_MINOR;
}


void sys_morm_ver_patch(Info & i)
{
	i.out << MORM_VERSION_PATCH;
}



void sys_ezc_ver_major(Info & i)
{
	i.out << EZC_VERSION_MAJOR;
}


void sys_ezc_ver_minor(Info & i)
{
	i.out << EZC_VERSION_MINOR;
}


void sys_ezc_ver_patch(Info & i)
{
	i.out << EZC_VERSION_PATCH;
}



void sys_tito_ver_major(Info & i)
{
	i.out << TITO_VERSION_MAJOR;
}


void sys_tito_ver_minor(Info & i)
{
	i.out << TITO_VERSION_MINOR;
}


void sys_tito_ver_patch(Info & i)
{
	i.out << TITO_VERSION_PATCH;
}




static size_t sys_plugin_index = 0;


void sys_plugin_tab(Info & i)
{
	sys_plugin_index = i.iter;
	i.res = sys_plugin_index < plugin->GetPlugins()->size();
}

void sys_plugin_tab_has_name(Info & i)
{
	if( sys_plugin_index < plugin->GetPlugins()->size() )
		i.res = (*plugin->GetPlugins())[sys_plugin_index].plugin_name != 0;
}

void sys_plugin_tab_name(Info & i)
{
	if( sys_plugin_index < plugin->GetPlugins()->size() )
	{
		const wchar_t * name = (*plugin->GetPlugins())[sys_plugin_index].plugin_name;

		if( name )
			i.out << name;
	}
}





} // namespace TemplatesFunctions

} // namespace Winix


