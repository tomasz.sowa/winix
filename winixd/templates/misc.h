/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_templates_misc
#define headerfile_winix_templates_misc

#include <vector>
#include <string>
#include "localefilter.h"
#include "ezc.h"
#include "htmltextstream.h"

namespace Winix
{
class User;
class Request;

// Ezc::FunInfo<> will be renamed to Ezc::Env<> in the future
typedef Ezc::FunInfo<HtmlTextStream> EzcEnv;


namespace TemplatesFunctions
{


typedef Ezc::Functions<HtmlTextStream> EzcFun;
typedef Ezc::Generator<HtmlTextStream, true, true> EzcGen;
typedef Ezc::FunInfo<HtmlTextStream>   Info; // deprecated

template<class RawType>
HtmlTextStream::RawText<RawType> R(const RawType & par)
{
	return HtmlTextStream::RawText<RawType>(par);
}


void InitGenerator(EzcGen & gen, Request & request);

void HtmlEscapeFormTxt(HtmlTextStream & out, const std::wstring & in);

void print_hour_min(Info & i,  time_t time);
void print_date_nice(Info & i, const pt::Date & date);



/*
	puser can be null -- in such a case guest_name is used
	if guest_name is empty then 'display_guest_name' from locales is printed
*/
void print_user_name(Info & i, User * puser, const std::wstring & guest_name);


bool should_escape(Info & env);


} // namespace TemplatesFunctions

} // namespace Winix

#endif

