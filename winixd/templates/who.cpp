/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"

namespace Winix
{


namespace TemplatesFunctions
{

static size_t who_reqid = 0;

SessionContainer::Iterator who_iterator;
size_t who_lp;


bool who_init()
{
	if( who_reqid != cur->request->id )
	{
		who_reqid    = cur->request->id;	
		who_iterator = session_manager->SessionBegin();
		who_lp       = 1;
	}
	
	return who_iterator != session_manager->SessionEnd();
}



void who_tab(Info & i)
{
	who_init();

	if( i.iter != 0 && who_iterator != session_manager->SessionEnd() )
	{
		++who_iterator;
		++who_lp;
	}

	i.res = who_iterator != session_manager->SessionEnd();
}


void who_tab_lp(Info & i)
{
	if( !who_init() )
		return;
	
	i.out << who_lp;
}


void who_tab_user(Info & i)
{
	if( !who_init() )
		return;
	

	if( who_iterator->puser )
		i.out << who_iterator->puser->login;
}



void who_tab_time(Info & i)
{
	if( !who_init() )
		return;
	
	i.out << DateToStr(	0,
						who_iterator->start_date.month,
						who_iterator->start_date.day,
						who_iterator->start_date.hour,
						who_iterator->start_date.min,
						who_iterator->start_date.sec );
}


void who_tab_last_time(Info & i)
{
	if( !who_init() )
		return;
	
	i.out << DateToStr(	0,
						who_iterator->last_date.month,
						who_iterator->last_date.day,
						who_iterator->last_date.hour,
						who_iterator->last_date.min,
						who_iterator->last_date.sec );
}




} // namespace TemplatesFunctions

} // namespace Winix



