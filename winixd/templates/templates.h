/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_templates_templates
#define headerfile_winix_templates_templates

#include <iomanip>
#include <set>
#include "ezc.h"
#include "misc.h"
#include "patterncacher.h"
#include "indexpatterns.h"
#include "locale.h"
#include "patterns.h"
#include "changepatterns.h"
#include "htmltextstream.h"
#include "localefilter.h"
#include "core/config.h"
#include "core/cur.h"
#include "core/system.h"
#include "core/sessionmanager.h"
#include "html/htmlparser.h"
#include "core/winixrequest.h"


namespace Winix
{



class Functions;



namespace TemplatesFunctions
{
	extern size_t pat_index;
	extern size_t pat_index_generic;
	extern size_t pat_index_raw;
	extern size_t pat_request_status;

	extern Patterns          patterns;
	extern IndexPatterns     index_patterns;
	extern ChangePatterns    change_patterns;
	extern PatternCacher     pattern_cacher;
	extern Locale            locale;
	extern EzcFun            ezc_functions;
	extern LocaleFilter      locale_filter;
	extern pt::HTMLParser    html_filter;
	extern Ezc::Stack        empty_stack;

	extern Cur     * cur; // will be removed when TemplateBase class will be implemented
	extern Config  * config; // will be removed when TemplateBase class will be implemented
	extern System  * system; // will be removed when TemplateBase class will be implemented
	extern Functions * functions;
	extern SessionManager * session_manager;

	extern Log log; // will be removed when TemplateBase class will be implemented
	extern Plugin * plugin; // will be removed when TemplateBase class will be implemented

	extern bool gen_trim_white;
	extern bool gen_skip_new_line;
	extern bool gen_use_special_chars;


	/*
		adduser
	*/
	void adduser_last_login(Info & i);
	void adduser_last_email(Info & i);


	/*
		antispam
	*/
	void antispam_create_new_form_id_for_this_session(Info & i);
	void antispam_loop(Info & i);
	void antispam_loop_operator(Info & i);
	void antispam_counter(Info & i);


	/*
		config
	*/
	void config_use_ssl(Info & i);
	void config_use_ssl_static(Info & i);
	void config_use_ssl_common(Info & i);
	void config_url_proto(Info & i);
	void config_url_ssl_proto(Info & i);
	void config_base_url(Info & i);
	void config_base_url_static(Info & i);
	void config_base_url_common(Info & i);
	void config_is_html_filter_allowed(Info & i);


	/*
		current date
	*/
	void current_sec_utc(Info & i);
	void current_min_utc(Info & i);
	void current_hour_utc(Info & i);
	void current_day_utc(Info & i);
	void current_month_utc(Info & i);
	void current_year_utc(Info & i);
	void current_date_utc(Info & i);
	void current_sec(Info & i);
	void current_min(Info & i);
	void current_hour(Info & i);
	void current_day(Info & i);
	void current_month(Info & i);
	void current_year(Info & i);
	void current_date(Info & i);


	/*
		dir
	*/
	void dir(Info & i);
	void dir_without_slash(Info & i);
	void dir_is_root(Info & i);
	void dir_parent(Info & i);
	void dir_parent_without_slash(Info & i);
	void dir_can_read_exec(Info & i);
	void dir_can_write(Info & i);
	void dir_can_remove(Info & i);
	void dir_can_use_emacs(Info & i);
	void dir_can_use_mkdir(Info & i);
	void dir_is(Info & i);
	void dir_is_no(Info & i);
	void dir_has_parents(Info & i);
	void dir_level_is(Info & i);

	/*
	void dir_childs_tab(Info & i);
	void dir_childs_is_parent(Info & i);
	void dir_childs_tab_url(Info & i);
	void dir_childs_tab_privileges(Info & i);
	void dir_childs_tab_user(Info & i);
	void dir_childs_tab_group(Info & i);
	*/

	/*
	void dir_tab(Info & i);
	void dir_tab_url(Info & i);
	void dir_tab_link(Info & i);
	void dir_tab_subject(Info & i);
	void dir_tab_is_root(Info & i);
	*/

	void dir_last_link_to(Info &);
	void dir_last_is_link_redirect(Info & i);
	void dir_last_subject(Info & i);
	void dir_last_user(Info & i);
	void dir_last_url(Info & i);
	void dir_last_url_is(Info & i);
	void dir_last_url_is_no(Info & i);
	void dir_last_date_creation(Info & i);
	void dir_last_date_modification(Info & i);
	void dir_last_date_creation_nice(Info & i);
	void dir_last_date_modification_nice(Info & i);
	void dir_last_dates_equal(Info & i);
	void dir_last_users_different(Info & i);
	void dir_last_modification_user(Info & i);
	void dir_last_html_template(Info & i);
	void dir_last_has_html_template(Info & i);
	void dir_last_meta_str(Info & i);
//	void dir_last_meta(Info & i);
//	void dir_last_meta_tab(Info & i);
//	void dir_last_meta_tab_value(Info & i);
//	void dir_last_meta_tab_has_next(Info & i);
//	void dir_last_admin_meta_str(Info & i);
//	void dir_last_admin_meta(Info & i);
//	void dir_last_admin_meta_tab(Info & i);
//	void dir_last_admin_meta_tab_value(Info & i);
//	void dir_last_admin_meta_tab_has_next(Info & i);


	/*
		doc
	*/
	void doc_title(Info & i);
	void doc_proto(Info & i);
	void doc_proto_static(Info & i);
	void doc_proto_common(Info & i);
	void doc_base_url(Info & i);
	void doc_base_url_static(Info & i);
	void doc_base_url_common(Info & i);
	void doc_current_url(Info & i);
	void doc_css_tab(Info & i);
	void doc_css_tab_file(Info & i);
	void doc_css_tab_file_is_global(Info & i);
	void doc_css_tab_has_next(Info & i);
	void doc_css_is_empty(Info & i);
	void doc_css_is_one(Info & i);
	void doc_css_more_than_one(Info & i);


	/*
		filters
	*/
	void fil_urlencode(Info & i);
	void fil_qencode(Info & i);
	void fil_capitalize(Info & i);
	void fil_tosmall(Info & i);
	void fil_firstup(Info & i);
	void fil_first_wordup(Info & i);
	void fil_csv_escape(Info & i);
	void fil_json_escape(Info & i);
	void fil_new_line_to_br(Info & i);
	void fil_html_quote(Info & i);
	void fil_html_newline(Info & i);


	/*
		generic functions
	*/
	void ezc_false(Info & i);
	void ezc_true(Info & i);
	void ezc_and(Info & i);
	void ezc_any(Info & i);
	void ezc_or(Info & i);
	void ezc_one(Info & i);
	void ezc_and_not(Info & i);
	void ezc_any_not(Info & i);
	void ezc_or_not(Info & i);
	void ezc_one_not(Info & i);
	void ezc_not(Info & i);
	void cmp(Info & i);
	void is(Info & i);
	void is_not(Info & i);
	void is_empty(Info & i);
	void is_not_empty(Info & i);
	void trim(Info & i);
	void to_lower(Info & i);
	void to_upper(Info & i);
	void index(Info & i);
	void str(Info & i);


	/*
		insert
	*/
	void insert_page(Info & i);


	/*
		ipban
	*/
	void ipban_is_current_ip_banned(Info & i);
	void ipban_current_ip_expires_time(Info & i);
	void ipban_is_login_allowed_from_this_ip(Info & i);
	void ipban_tab(Info & i);
	void ipban_tab_id(Info & i);
	void ipban_tab_ip(Info & i);
	void ipban_tab_incorrect_login(Info & i);
	void ipban_tab_broken_encoded_cookie(Info & i);
	void ipban_tab_session_hijacking(Info & i);
	void ipban_tab_no_session_cookie(Info & i);
	void ipban_tab_ban_level(Info & i);
	void ipban_tab_has_active_flag(Info & i);
	void ipban_tab_expires(Info & i);
	void ipban_tab_last_used(Info & i);
	void ipban_tab_is_logging_allowed(Info & i);



	/*
		last
	*/
	void last_tab(Info & i);
	void last_tab_name(Info & i);
	void last_tab_ip(Info & i);
	void last_tab_start(Info & i);
	void last_tab_end(Info & i);


	/*
		login
	*/
	void login_path(Info & i);
	void login_should_use_captcha(Info & i);


	/*
		man
	*/
	void man_winixfun_tab(Info & i);
	void man_winixfun_tab_index(Info & i);
	void man_winixfun_tab_name(Info & i);
	void man_ezcfun_tab(Info & i);
	void man_ezcfun_tab_index(Info & i);
	void man_ezcfun_tab_name(Info & i);


	/*
		mount
	*/
	void mount_type_is(Info & i);
	void mount_page_arg_is(Info & i);
	void mount_lang_arg(Info & i);
	void mount_lang_arg_is(Info & i);
	void mount_has_html_template(Info & i);
	void mount_first_html_template(Info & i);
	void mount_css_tab(Info & i);
	void mount_css_tab_file(Info & i);
	void mount_css_tab_file_is_global(Info & i);
	void mount_css_tab_has_next(Info & i);
	void mount_css_is_empty(Info & i);
	void mount_css_is_one(Info & i);
	void mount_css_more_than_one(Info & i);
	void mount_cur_type(Info & i);
	void mount_cur_dir(Info & i);
	void mount_cur_fs(Info & i);
	void mount_cur_parlist(Info & i);
	void mount_tab(Info & i);
	void mount_tab_type(Info & i);
	void mount_tab_dir(Info & i);
	void mount_tab_fs(Info & i);
	void mount_tab_parlist(Info & i);


	/*
		passwd
	*/
	void passwd_resetpass_login(Info & i);
	void passwd_resetpass_code(Info & i);


	/*

	*/
	void ls_ckeditor_funnum_browse(Info & i);



	/*
		privileges
	*/
	void priv_user_tab(Info & i);
	void priv_user_tab_name(Info & i);
	void priv_user_tab_isdefault(Info & i);
	
	void priv_group_tab(Info & i);
	void priv_group_tab_name(Info & i);
	void priv_group_tab_isdefault(Info & i);
	
	void priv_privileges(Info & i);
	void priv_privileges_for_files(Info & i);
	void priv_privileges_for_dirs(Info & i);
	
	void priv_show_form_chown(Info & i);
	void priv_show_form_chmod(Info & i);


	/*
		pw
	*/



	/*
		rebus
	*/
	void rebus_question(Info & i);


	/*
		server
	*/
	void server_mode(Info & i);
	void server_mode_is(Info & i);


	/*
		stat
	*/
	void stat_item_type_is_file(Info & i);
	void stat_item_type_is_static_file(Info & i);
	void stat_item_type_is_dir(Info & i);
	void stat_item_inode(Info & i);
	void stat_item_user(Info & i);
	void stat_item_group(Info & i);
	void stat_item_privileges(Info & i);
	void stat_item_date_creation(Info & i);
	void stat_item_date_modification(Info & i);
	void stat_item_template(Info & i);
	void stat_item_is_template_from_mount_point(Info & i);


	/*
		sys
	*/
	void sys_ver_major(Info & i);
	void sys_ver_minor(Info & i);
	void sys_ver_revision(Info & i);
	void sys_pikotools_ver_major(Info & i);
	void sys_pikotools_ver_minor(Info & i);
	void sys_pikotools_ver_patch(Info & i);
	void sys_morm_ver_major(Info & i);
	void sys_morm_ver_minor(Info & i);
	void sys_morm_ver_patch(Info & i);
	void sys_ezc_ver_major(Info & i);
	void sys_ezc_ver_minor(Info & i);
	void sys_ezc_ver_patch(Info & i);
	void sys_tito_ver_major(Info & i);
	void sys_tito_ver_minor(Info & i);
	void sys_tito_ver_patch(Info & i);
	void sys_plugin_tab(Info & i);
	void sys_plugin_tab_has_name(Info & i);
	void sys_plugin_tab_name(Info & i);


	/*
		upload
	*/
	void upload_ckeditor_funnum(Info & i);


	/*
		uptime
	*/
	void uptime_more_than_one_day(Info & i);
	void uptime_days(Info & i);
	void uptime_hours(Info & i);


	/*
		user
	*/
	void user_id(Info & i);
	void user_name(Info & i);
	void user_logged(Info & i);
	void user_super_user(Info & i);
	void user_is_in_group(Info & i);
	void user_is_in_all_groups(Info & i);
	void user_can_use_html(Info & i);
	void user_can_use_bbcode(Info & i);
	void user_can_use_other(Info & i);
	void user_has_correct_time_zone(Info & i);
	void user_time_zone_name(Info & i);
	void user_time_zone_id(Info & i);
	void user_time_zone_offset_hour_min(Info & i);
	void user_has_correct_locale(Info & i);
	void user_locale_name(Info & i);
	void user_locale_id(Info & i);
	void user_tab(Info & i);
	void user_tab_index(Info & i);
	void user_tab_id(Info & i);
	void user_tab_name(Info & i);
	void user_tab_is_super_user(Info & i);
	void user_tab_is_active(Info & i);
	void user_tab_is_suspended(Info & i);
	void user_tab_is_blocked(Info & i);
	void user_tab_is_current(Info & i);


	/*
		template (html templates)
	*/
	void template_index(Info & i);
	void template_tab(Info & i);
	void template_tab_index(Info & i);
	void template_tab_isdefault(Info & i);
	void template_tab_file_name(Info & i);


	/*
		winix
	*/
	void winix_account_need_email_verification(Info & i);
	void winix_cur_time(Info & i);
	void winix_how_many_sessions(Info & i);
	void winix_users_logged(Info & i);
	void winix_function(Info & i);
	void winix_function_is(Info & i);
	void winix_function_param_is(Info & i);
	void winix_function_param_is_not(Info & i);
	void winix_function_param_value(Info & i);
	void winix_function_param_value_is(Info & i);
	void winix_function_param_value_is_not(Info & i);
	void winix_has_plugin(Info & i);
	void winix_loadavg_now(Info & i);
	void winix_loadavg_1(Info & i);
	void winix_loadavg_5(Info & i);
	void winix_loadavg_15(Info & i);
	void winix_req_per_sec_now(Info & i);
	void winix_req_per_sec_1(Info & i);
	void winix_req_per_sec_5(Info & i);
	void winix_req_per_sec_15(Info & i);
	void winix_show_content_in_full_window(Info & i);
	void winix_has_postvar(Info & i);
	void winix_postvar(Info & i);
	void winix_postvar_value_is(Info & i);
	void winix_postvar_value_is_not(Info & i);
	void winix_subdomain(Info & i);
	void winix_subdomain_is_empty(Info & i);
	void winix_subdomain_is_not_empty(Info & i);
	void winix_subdomain_is(Info & i);
	void winix_tz_tab(Info & i);
	void winix_tz_tab_id(Info & i);
	void winix_tz_tab_name(Info & i);
	void winix_tz_tab_offset_sec(Info & i);
	void winix_tz_tab_offset_hour_min(Info & i);
	void winix_locale_tab(Info & i);
	void winix_locale_tab_id(Info & i);
	void winix_locale_tab_name(Info & i);
	void winix_is_htmx_request(Info & i);
	void winix_frame_is(Info & i);

	void lang(Info & i); // defined in winix.cpp - MOVE ME to a better place

	/*
		who
	*/
	void who_tab(Info & i);
	void who_tab_lp(Info & i);
	void who_tab_user(Info & i);
	void who_tab_time(Info & i);
	void who_tab_last_time(Info & i);


} // namespace TemplatesFunctions








// will be similar like Functions
// concrete templates classes (based on TemplateBase) will be registered here
class Templates : public WinixRequest
{
public:

	Templates();

	void SetConfig(Config * pconfig);
	void SetCur(Cur * pcur);
	void SetSystem(System * psystem);
	void SetFunctions(Functions * pfunctions);
	void SetSessionManager(SessionManager * psession_manager);

	void set_dependency(WinixRequest * winix_request);

	void Init();
	void ClearAfterRequest();
	void ReadTemplates();
	void ReadNewIndexTemplates();
	void ReadNewChangeTemplates();

	void SetEzcParameters(bool trim_white, bool skip_new_line, bool use_special_chars);

	void Generate();

private:
	
	void ReadTemplatesForFunctions();
	void ReadIndexTemplates();
	void ReadChangeTemplates();
	void ReadLocale();
	void SetHtmlFilter();
	void CreateFunctions(); // should be called before reading patterns (patterns will cache ezc functions)
	void LogProducedFrames();

	Ezc::Pattern * SelectIndexPattern(const std::wstring & template_name);
	Ezc::Pattern * SelectIndexPattern();

	TemplatesFunctions::EzcGen generator;
	std::wstring temp;
	std::wstring fun_file;
};



} // namespace Winix

#endif
