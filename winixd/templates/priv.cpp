/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"
#include "functions/functions.h"

namespace Winix
{



namespace TemplatesFunctions
{


static std::vector<long> priv_user_table;
static size_t priv_user_index;
static size_t priv_user_reqid = 0;



void priv_user_tab_init(Item & item)
{
	priv_user_reqid = cur->request->id;
	priv_user_table.clear();	
	
	if( !cur->session->puser )
	{
		// not logged
		priv_user_table.push_back(item.item_content.user_id);
	}	
	else	
	if( cur->session->puser->is_super_user )
	{
		// super user is allowed to change to any user
		for(Users::Iterator i=system->users.Begin() ; i != system->users.End() ; ++i)
			priv_user_table.push_back(i->id);
		
		// as well to nobody (-1)
		priv_user_table.push_back(-1);
	}
	else	
	{
		// others
		priv_user_table.push_back(item.item_content.user_id);
	}
}


void priv_user_tab_init()
{
	if( cur->request->is_item )
		priv_user_tab_init(cur->request->item);
	else
		priv_user_tab_init(*cur->request->dir_tab.back());
}


void priv_user_tab(Info & i)
{
	if( priv_user_reqid != cur->request->id )
		priv_user_tab_init();
	
	priv_user_index = i.iter;
	i.res = priv_user_index < priv_user_table.size();
}




void priv_user_tab_name(Info & i)
{
	if( priv_user_index < priv_user_table.size() )
	{
		long uid = priv_user_table[priv_user_index];
		
		if( uid != -1 )
		{
			User * puser = system->users.GetUser( uid );
			
			if( puser )
				i.out << puser->login;
			else
				i.out << "user_id: " << uid;
		}
		else
		{
			i.out << config->priv_no_user;
		}
	}
}



void priv_user_tab_isdefault(Info & i)
{
	if( priv_user_index < priv_user_table.size() )
	{
		long uid = priv_user_table[priv_user_index];
		
		if( cur->request->is_item )
		{
			if( uid == cur->request->item.item_content.user_id )
				i.res = true;
		}
		else
		{
			if( uid == cur->request->dir_tab.back()->item_content.user_id )
				i.res = true;
		}
	}
}








static std::vector<long> priv_group_table;
static size_t priv_group_index;
static size_t priv_group_reqid = 0;



void priv_group_tab_init(Item & item)
{
	priv_group_reqid = cur->request->id;
	priv_group_table.clear();	
	
	if( !cur->session->puser )
	{
		// not logged
		priv_group_table.push_back(item.item_content.group_id);
	}	
	else	
	if( cur->session->puser->is_super_user )
	{
		// super user is allowed to change to any group	
		for(Groups::Iterator i=system->groups.Begin() ; i != system->groups.End() ; ++i)
			priv_group_table.push_back(i->id);
		
		// as well to nogroup (-1)
		priv_group_table.push_back(-1);
	}
	else	
	if( cur->session->puser->id == item.item_content.user_id )
	{
		bool was_current_group = false;
		// owner of the item -- is allowed to change only to a group in which he belongs to
		for(size_t i=0 ; i<cur->session->puser->groups.size() ; ++i)
		{
			priv_group_table.push_back(cur->session->puser->groups[i]);
			
			if( item.item_content.group_id == cur->session->puser->groups[i] )
				was_current_group = true;
		}
		
		// we're showing the item.group_id if it's different
		if( !was_current_group )
			priv_group_table.push_back(item.item_content.group_id);
		
		// switching to -1 is allowed too
		priv_group_table.push_back(-1);
	}
	else
	{
		// not the owner and not a super user -- the same as not logged
		priv_group_table.push_back(item.item_content.group_id);
	}
}


void priv_group_tab_init()
{
	if( cur->request->is_item )
		priv_group_tab_init(cur->request->item);
	else
		priv_group_tab_init(*cur->request->dir_tab.back());
}


void priv_group_tab(Info & i)
{
	if( priv_group_reqid != cur->request->id )
		priv_group_tab_init();
	
	priv_group_index = i.iter;
	i.res = priv_group_index < priv_group_table.size();
}




void priv_group_tab_name(Info & i)
{
	if( priv_group_index < priv_group_table.size() )
	{
		long gid = priv_group_table[priv_group_index];
		
		if( gid != -1 )
		{
			Group * pgroup = system->groups.GetGroup( gid );
			
			if( pgroup )
				i.out << pgroup->name;
			else
				i.out << "group_id: " << gid;
		}
		else
		{
			i.out << config->priv_no_group;
		}
	}
}



void priv_group_tab_isdefault(Info & i)
{
	if( priv_group_index < priv_group_table.size() )
	{
		long gid = priv_group_table[priv_group_index];
		
		if( cur->request->is_item )
		{
			if( gid == cur->request->item.item_content.group_id )
				i.res = true;
		}
		else
		{
			if( gid == cur->request->dir_tab.back()->item_content.group_id )
				i.res = true;
		}
	}
}


void priv_privileges(Info & i)
{
	if( cur->request->is_item )
		i.out << Toa(cur->request->item.item_content.privileges, 8);
	else
		i.out << Toa(cur->request->dir_tab.back()->item_content.privileges, 8);
}


void priv_privileges_for_files(Info & i)
{
	i.out << Toa(system->NewFilePrivileges(), 8);
}


void priv_privileges_for_dirs(Info & i)
{
	i.out << Toa(system->NewDirPrivileges(), 8);
}


void priv_show_form_chown(Info & i)
{
	i.res = (cur->request->function == &functions->fun_priv || cur->request->function == &functions->fun_chown);
}


void priv_show_form_chmod(Info & i)
{
	i.res = (cur->request->function == &functions->fun_priv || cur->request->function == &functions->fun_chmod);
}




} // namespace TemplatesFunctions

} // namespace Winix

