/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_templates_htmltextstream
#define headerfile_winix_templates_htmltextstream

#include <ctime>
#include "textstream/textstream.h"
#include "morm.h"


namespace Winix
{


/*
	HtmlTextStream is used as a buffer for creating a html page
	By default all operators<< escape its string arguments. If you don't want
	to escape an argument you should use a helper function R() (raw argument)
	note: you have to define the function yourself, we do not provide it
	because such a short name would make a mess in namespaces

	sample:
	create a helper function R as follows:

		template<class RawType>
		HtmlTextStream::RawText<RawType> R(const RawType & par)
		{
			return HtmlTextStream::RawText<RawType>(par);
		}
	
	now you can use HtmlTextStream in an easy way:
		HtmlTextStream page;
		std::string key = "some <b>string</b>";
		page << key << R("<h2>html goes here</h2>");
	only html tags "<b>" and "</b>" will be correctly escaped

	currently following characters are escaped:
	 <  -> &lt;
	 >  -> &gt;
	 &  -> &nbsp;
	 "  -> &quot;
	 '  -> &#39; (it is "&apos;" but IE8 has a problem with &apos;) (&apos; is valid in HTML5, but not HTML4)
	 10 -> &#10;
	 13 -> &#13;
	 0  -> &#0;
*/
class HtmlTextStream : public pt::Stream
{
public:

	typedef wchar_t char_type;
	typedef pt::WTextStream::iterator iterator;
	typedef pt::WTextStream::const_iterator const_iterator;


	HtmlTextStream();

	bool is_char_stream() const;
	bool is_wchar_stream() const;


	/*
	 * clearing the buffer and setting 'escape' flag to true
	 */
	void clear();
	bool   empty() const;
	size_t size() const;
	void   reserve(size_t len);
	size_t capacity() const;

	iterator begin();
	iterator end();

	const_iterator begin() const;
	const_iterator end() const;

	void to_str(std::string & str,  bool clear_string = true) const;
	void to_str(std::wstring & str, bool clear_string = true) const;

	std::string  to_str() const;
	std::wstring to_wstr() const;

	char get_char(size_t index) const;
	wchar_t get_wchar(size_t index) const;

	const pt::WTextStream & get_buffer() const;
	pt::WTextStream & get_buffer();


	/*
		a helper struct to select a proper operator<<
		(for non-escaping versions of these operators)
	*/
	template<class RawType>
	struct RawText
	{
		const RawType & par;

		RawText(const RawText<RawType> & p) : par(p.par) {}
		RawText(const RawType & p) : par(p) {}
	};



	/*
		without escaping
	*/
	HtmlTextStream & PutText(const char * str);
	HtmlTextStream & PutText(const std::string & str);
	HtmlTextStream & PutText(const wchar_t * str);
	HtmlTextStream & PutText(const std::wstring & str);

	HtmlTextStream & PutText(const char *, size_t len);
	HtmlTextStream & PutText(const wchar_t * str, size_t len);

	HtmlTextStream & PutChar(char c);
	HtmlTextStream & PutChar(unsigned char c);
	HtmlTextStream & PutChar(wchar_t c);
	HtmlTextStream & PutChar(char32_t c);
	HtmlTextStream & PutChar(bool val);


	/*
		we need this template operator for such calling:
		HtmlTextStream_object << R("some string");
		"some string" is actually a table (not a pointer)
	*/
	template<size_t str_size>
	HtmlTextStream & operator<<(const RawText<char [str_size]> & raw)	{ return PutText(raw.par); }

	template<size_t str_size>
	HtmlTextStream & operator<<(const RawText<wchar_t [str_size]> & raw)	{ return PutText(raw.par); }

	HtmlTextStream & operator<<(const RawText<const char*> & raw);
	HtmlTextStream & operator<<(RawText<std::string> raw);

	HtmlTextStream & operator<<(const RawText<const wchar_t*> & raw);
	HtmlTextStream & operator<<(RawText<std::wstring> raw);

	HtmlTextStream & operator<<(RawText<char> raw);
	HtmlTextStream & operator<<(RawText<unsigned char> raw);
	HtmlTextStream & operator<<(RawText<wchar_t> raw);
	HtmlTextStream & operator<<(RawText<char32_t> raw);
	HtmlTextStream & operator<<(RawText<bool> raw);

	HtmlTextStream & operator<<(RawText<short> raw);
	HtmlTextStream & operator<<(RawText<int> raw);
	HtmlTextStream & operator<<(RawText<long> raw);
	HtmlTextStream & operator<<(RawText<long long> raw);

	HtmlTextStream & operator<<(RawText<unsigned short> raw);
	HtmlTextStream & operator<<(RawText<unsigned int> raw);
	HtmlTextStream & operator<<(RawText<unsigned long> raw);
	HtmlTextStream & operator<<(RawText<unsigned long long> raw);

	HtmlTextStream & operator<<(RawText<float> raw);
	HtmlTextStream & operator<<(RawText<double> raw);
	HtmlTextStream & operator<<(RawText<long double> raw);
	HtmlTextStream & operator<<(RawText<void*> raw);

	HtmlTextStream & operator<<(RawText<pt::Stream> raw);
	HtmlTextStream & operator<<(RawText<pt::Space> raw);
	HtmlTextStream & operator<<(RawText<pt::Date> raw);

	template<typename arg_char_type, size_t arg_stack_size, size_t arg_heap_block_size>
	HtmlTextStream & operator<<(RawText<pt::TextStreamBase<arg_char_type, arg_stack_size, arg_heap_block_size> > raw);

	// this method doesn't escape stream as there is no need for such behavior - stream should be already escaped
	HtmlTextStream & operator<<(const HtmlTextStream & stream);

	// 'write' don't escapes too
	// with these methods you can write a zero character too
	HtmlTextStream & write(const char * buf, size_t len);
	HtmlTextStream & write(const wchar_t * buf, size_t len);



	/*
		with escaping
	*/
	HtmlTextStream & EPutText(const char * str);
	HtmlTextStream & EPutText(const std::string & str);
	HtmlTextStream & EPutText(const wchar_t * str);
	HtmlTextStream & EPutText(const std::wstring & str);

	HtmlTextStream & EPutText(const char * str, size_t len);
	HtmlTextStream & EPutText(const wchar_t * str, size_t len);

	HtmlTextStream & ETextPutChar(char c);
	HtmlTextStream & ETextPutChar(unsigned char c);
	HtmlTextStream & ETextPutChar(wchar_t c);
	HtmlTextStream & ETextPutChar(char32_t c);


	/*
	 * by default all operator<< shown below use escaping
	 * but you can turn it off by calling Escape(false)
	 */
	void Escape(bool escape_characters);

	HtmlTextStream & operator<<(const char * str);
	HtmlTextStream & operator<<(const std::string & str);
	HtmlTextStream & operator<<(const wchar_t * str);
	HtmlTextStream & operator<<(const std::wstring & str);
	HtmlTextStream & operator<<(char);
	HtmlTextStream & operator<<(unsigned char);
	HtmlTextStream & operator<<(wchar_t);
	HtmlTextStream & operator<<(char32_t);
	HtmlTextStream & operator<<(bool);
	HtmlTextStream & operator<<(short);
	HtmlTextStream & operator<<(int);
	HtmlTextStream & operator<<(long);
	HtmlTextStream & operator<<(long long);
	HtmlTextStream & operator<<(unsigned short);
	HtmlTextStream & operator<<(unsigned int);
	HtmlTextStream & operator<<(unsigned long);
	HtmlTextStream & operator<<(unsigned long long);
	HtmlTextStream & operator<<(float);
	HtmlTextStream & operator<<(double);
	HtmlTextStream & operator<<(long double);
	HtmlTextStream & operator<<(const void *);

	HtmlTextStream & operator<<(const Stream & stream);
	HtmlTextStream & operator<<(const pt::Space & space);
	HtmlTextStream & operator<<(const pt::Date & Date);

	HtmlTextStream & operator<<(morm::Model & model);

	template<typename arg_char_type, size_t arg_stack_size, size_t arg_heap_block_size>
	HtmlTextStream & operator<<(const pt::TextStreamBase<arg_char_type, arg_stack_size, arg_heap_block_size> & arg);


private:

	pt::WTextStream buffer;
	//std::wstring tmp_string;
	bool escape;

};



template<typename arg_char_type, size_t arg_stack_size, size_t arg_heap_block_size>
HtmlTextStream & HtmlTextStream::operator<<(RawText<pt::TextStreamBase<arg_char_type, arg_stack_size, arg_heap_block_size> > raw)
{
	buffer.operator<<(raw.par);

return *this;
}


/*
 * this method is the same as HtmlTextStream & HtmlTextStream::operator<<(const Stream & stream)
 * but in the future we can use iterators here
 *
 */
template<typename arg_char_type, size_t arg_stack_size, size_t arg_heap_block_size>
HtmlTextStream & HtmlTextStream::operator<<(const pt::TextStreamBase<arg_char_type, arg_stack_size, arg_heap_block_size> & arg)
{
typename pt::TextStreamBase<arg_char_type, arg_stack_size, arg_heap_block_size>::const_iterator i;

	if( escape )
	{
		if(arg.is_char_stream())
		{
			/*
			 * IMPROVEME it would be better to have a better api from pikotools
			 * instead of index we can provide an iterator
			 */
			int res;
			bool correct;
			size_t len;
			size_t index = 0;
			size_t stream_len = arg.size();

			// CHECKME
			while( index < stream_len && (len = pt::utf8_to_int(arg, index, res, correct)) > 0 )
			{
				if( !correct )
					res = 0xFFFD; // U+FFFD "replacement character"

				ETextPutChar(static_cast<wchar_t>(res));
				index += len;
			}
		}
		else
		if(arg.is_wchar_stream())
		{
			for(i=arg.begin() ; i != arg.end() ; ++i)
				ETextPutChar(*i);
		}
	}
	else
	{
		buffer.operator<<(arg);
	}

return *this;
}


} // namespace Winix

#endif

