/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2015, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "templates.h"
#include "core/misc.h"
#include "functions/functions.h"

namespace Winix
{

namespace TemplatesFunctions
{



void current_sec_utc(Info & i)
{
	i.out << cur->request->start_date.sec;
}

void current_min_utc(Info & i)
{
	i.out << cur->request->start_date.min;
}

void current_hour_utc(Info & i)
{
	i.out << cur->request->start_date.hour;
}


void current_day_utc(Info & i)
{
	i.out << cur->request->start_date.day;
}

void current_month_utc(Info & i)
{
	i.out << cur->request->start_date.month;
}

void current_year_utc(Info & i)
{
	i.out << cur->request->start_date.year;
}


void current_date_utc(Info & i)
{
	i.out << cur->request->start_date;
}




void current_sec(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->start_date);
	i.out << date.sec;
}

void current_min(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->start_date);
	i.out << date.min;
}

void current_hour(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->start_date);
	i.out << date.hour;
}


void current_day(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->start_date);
	i.out << date.day;
}

void current_month(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->start_date);
	i.out << date.month;
}

void current_year(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->start_date);
	i.out << date.year;
}


void current_date(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->start_date);
	i.out << date;
}




} // namespace TemplatesFunctions

} // namespace Winix


