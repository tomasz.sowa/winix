/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "templates.h"
#include "misc.h"
#include "core/misc.h"
#include "core/request.h"
#include "models/user.h"

namespace Winix
{
namespace TemplatesFunctions
{

extern EzcFun            ezc_functions;
extern Ezc::Blocks       ezc_blocks;
extern Ezc::Vars         ezc_vars;


void HtmlEscapeFormTxt(HtmlTextStream & out, const std::wstring & in)
{
std::wstring::const_iterator i;
int was_nl = 0; // how many new line characters there were before

	if( in.empty() )
		return;

	out << R("<p>");

	// skipping first new line characters
	for(i = in.begin() ; i != in.end() && (*i==13 || *i==10) ; ++i);

	for( ; i != in.end() ; ++i )
	{
		if( *i == 13 )
			continue;

		if( *i == 10 )
		{
			++was_nl;
		}
		else
		{
			if( was_nl == 1 )
				out << R("<br>\n");
			else
			if( was_nl > 1 )
				out << R("</p>\n<p>");

			was_nl = 0;
		}

		out << *i;
	}
	
	out << R("</p>\n");
}




void InitGenerator(EzcGen & gen, Request & request)
{
	gen.TrimWhite(gen_trim_white);
	gen.SkipNewLine(gen_skip_new_line);
	gen.RecognizeSpecialChars(gen_use_special_chars);
	gen.SetMax(config->ezc_max_elements, config->ezc_max_loop_elements);

	/*
	 * although we have addresses to blocks and functions cached in patters
	 * we have to provide it here because they will be used for variables
	 * if a variable is an alias e.g. [def variable function]
	 */
	gen.SetBlocks(ezc_blocks);
	gen.SetFunctions(ezc_functions);
	gen.SetVariables(ezc_vars);

	gen.SetModels(request.models);
	gen.SetLogger(log);

	if( request.send_all_frames )
	{
		gen.OnlyFrames(nullptr);
	}
	else
	{
		gen.OnlyFrames(request.send_frames);
	}
}





void print_hour_min(Info & i,  time_t time)
{
char buffer[100];

	int hours = time / 60 / 60;
	int mins  = (time / 60 - hours * 60);

	sprintf(buffer, "%02d:%02d", hours, mins);
	i.out << buffer;
}



void print_date_nice(Info & i, const pt::Date & date)
{
	time_t one_day = 60 * 60 * 24;
	pt::Date ltm   = system->ToLocal(date);

	if( date + one_day > cur->request->start_time )
		i.out << DateToStr(ltm.year, ltm.month, ltm.day, ltm.hour, ltm.min, ltm.sec);
	else
		i.out << DateToStr(ltm.year, ltm.month, ltm.day);
}



// DEPRECATED, we have ItemContent::display_user_name and User::display_name
void print_user_name(Info & i, User * puser, const std::wstring & guest_name)
{
	if( puser )
	{
		puser->display_name(i);
	}
	else
	{
		i.out << "~";

		if( !guest_name.empty() && !IsWhite(guest_name) )
			i.out << guest_name;
		else
			i.out << locale.Get(L"display_guest_name");
	}
}


bool should_escape(Info & env)
{
	bool res = true;

	for(size_t i=0 ; i < env.params.size() ; ++i)
	{
		// CHECKME what about env.params[i].is_function ?

		if( env.params[i].str == L"noescape" || env.params[i].str == L"raw" )
		{
			res = false;
			break;
		}
	}

	return res;
}





} // namespace TemplatesFunctions

} // namespace Winix


