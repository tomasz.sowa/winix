/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "htmltextstream.h"
#include "convert/misc.h"

namespace Winix
{



HtmlTextStream::HtmlTextStream()
{
	escape = true;
}


bool HtmlTextStream::is_char_stream() const
{
	return false;
}


bool HtmlTextStream::is_wchar_stream() const
{
	return true;
}



void HtmlTextStream::clear()
{
	escape = true;
	buffer.clear();
}


bool HtmlTextStream::empty() const
{
	return buffer.empty();
}


size_t HtmlTextStream::size() const
{
	return buffer.size();
}


void HtmlTextStream::reserve(size_t len)
{
	buffer.reserve(len);
}


size_t HtmlTextStream::capacity() const
{
	return buffer.capacity();
}


HtmlTextStream::iterator HtmlTextStream::begin()
{
	return buffer.begin();
}


HtmlTextStream::iterator HtmlTextStream::end()
{
	return buffer.end();
}


HtmlTextStream::const_iterator HtmlTextStream::begin() const
{
	return buffer.begin();
}


HtmlTextStream::const_iterator HtmlTextStream::end() const
{
	return buffer.end();
}




void HtmlTextStream::to_str(std::string & str, bool clear_string) const
{
	buffer.to_str(str, clear_string);
}


void HtmlTextStream::to_str(std::wstring & str, bool clear_string) const
{
	buffer.to_str(str, clear_string);
}


std::string HtmlTextStream::to_str() const
{
	return buffer.to_str();
}


std::wstring HtmlTextStream::to_wstr() const
{
	return buffer.to_wstr();
}


char HtmlTextStream::get_char(size_t index) const
{
	return buffer.get_char(index);
}

wchar_t HtmlTextStream::get_wchar(size_t index) const
{
	return buffer.get_wchar(index);
}


const pt::WTextStream & HtmlTextStream::get_buffer() const
{
	return buffer;
}


pt::WTextStream & HtmlTextStream::get_buffer()
{
	return buffer;
}





/*
	without escaping
*/


HtmlTextStream & HtmlTextStream::PutText(const char * str)
{
	buffer.operator<<(str);

return *this;
}


HtmlTextStream & HtmlTextStream::PutText(const std::string & str)
{
	buffer.operator<<(str);

return *this;
}



HtmlTextStream & HtmlTextStream::PutText(const wchar_t * str)
{
	buffer.operator<<(str);

return *this;
}


HtmlTextStream & HtmlTextStream::PutText(const std::wstring & str)
{
	buffer.operator<<(str);

return *this;
}


HtmlTextStream & HtmlTextStream::PutText(const char * str, size_t len)
{
	buffer.write(str, len);

return *this;
}


HtmlTextStream & HtmlTextStream::PutText(const wchar_t * str, size_t len)
{
	buffer.write(str, len);

return *this;
}




HtmlTextStream & HtmlTextStream::PutChar(char c)
{
	buffer.operator<<(c);

return *this;
}


HtmlTextStream & HtmlTextStream::PutChar(unsigned char c)
{
	buffer.operator<<(c);

return *this;
}


HtmlTextStream & HtmlTextStream::PutChar(wchar_t c)
{
	buffer.operator<<(c);

return *this;
}


HtmlTextStream & HtmlTextStream::PutChar(char32_t c)
{
	buffer.operator<<(c);

return *this;
}


HtmlTextStream & HtmlTextStream::PutChar(bool val)
{
	buffer.operator<<(val);

return *this;
}









HtmlTextStream & HtmlTextStream::operator<<(const RawText<const char*> & raw)
{
	return PutText(raw.par);
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<std::string> raw)
{
	return PutText(raw.par);
}


HtmlTextStream & HtmlTextStream::operator<<(const RawText<const wchar_t*> & raw)
{
	return PutText(raw.par);
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<std::wstring> raw)
{
	return PutText(raw.par);
}



HtmlTextStream & HtmlTextStream::operator<<(RawText<char> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<unsigned char> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<wchar_t> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<char32_t> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<bool> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}



HtmlTextStream & HtmlTextStream::operator<<(RawText<short> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<int> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<long> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<long long> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<unsigned short> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<unsigned int> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<unsigned long> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<unsigned long long> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<float> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<double> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<long double> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<void*> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<pt::Stream> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<pt::Space> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(RawText<pt::Date> raw)
{
	buffer.operator<<(raw.par);
	return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(const HtmlTextStream & stream)
{
	buffer.operator<<(stream.buffer);
	return *this;
}



HtmlTextStream & HtmlTextStream::write(const char * buf, size_t len)
{
	buffer.write(buf, len);
	return *this;
}


HtmlTextStream & HtmlTextStream::write(const wchar_t * buf, size_t len)
{
	buffer.write(buf, len);
	return *this;
}





/*
	with escaping
*/
HtmlTextStream & HtmlTextStream::EPutText(const char * str)
{
	int res;
	bool correct;
	size_t utf8_len;

	while( (utf8_len = pt::utf8_to_int(str, res, correct)) > 0 )
	{
		if( !correct )
			res = 0xFFFD; // U+FFFD "replacement character"

		ETextPutChar(static_cast<char32_t>(res));
		str += utf8_len;
	}

return *this;
}


HtmlTextStream & HtmlTextStream::EPutText(const std::string & str)
{
	return EPutText(str.c_str(), str.size());
}


HtmlTextStream & HtmlTextStream::EPutText(const wchar_t * str)
{
	/*
	 * we assume the sizeof(wchar) is 4 here
	 * as at the moment winix works only on unixes
	 * so just copy directly those characters
	 */
	for( ; *str ; ++str )
		ETextPutChar(*str);

return *this;
}


HtmlTextStream & HtmlTextStream::EPutText(const std::wstring & str)
{
	return EPutText(str.c_str(), str.size());
}


HtmlTextStream & HtmlTextStream::EPutText(const char * str, size_t len)
{
	int res;
	bool correct;
	size_t utf8_len;

	// CHECKME
	while( (utf8_len = pt::utf8_to_int(str, len, res, correct)) > 0 )
	{
		if( !correct )
			res = 0xFFFD; // U+FFFD "replacement character"

		ETextPutChar(static_cast<wchar_t>(res));

		len -= utf8_len;
		str += utf8_len;
	}

return *this;
}


HtmlTextStream & HtmlTextStream::EPutText(const wchar_t * str, size_t len)
{
	for(size_t i=0 ; i<len ; ++i)
		ETextPutChar(str[i]);

return *this;
}



HtmlTextStream & HtmlTextStream::ETextPutChar(char c)
{
	return ETextPutChar(static_cast<unsigned char>(c));
}


HtmlTextStream & HtmlTextStream::ETextPutChar(unsigned char c)
{
	return ETextPutChar(static_cast<char32_t>(c));
}


HtmlTextStream & HtmlTextStream::ETextPutChar(wchar_t c)
{
	return ETextPutChar(static_cast<char32_t>(c));
}


HtmlTextStream & HtmlTextStream::ETextPutChar(char32_t c)
{
	if( !pt::try_esc_to_html(c, buffer) )
		buffer << c;

	return *this;
}




void HtmlTextStream::Escape(bool escape_characters)
{
	escape = escape_characters;
}



HtmlTextStream & HtmlTextStream::operator<<(const char * str)
{
	if( escape )
		EPutText(str);
	else
		PutText(str);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(const std::string & str)
{
	if( escape )
		EPutText(str);
	else
		PutText(str);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(const wchar_t * str)
{
	if( escape )
		EPutText(str);
	else
		PutText(str);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(const std::wstring & str)
{
	if( escape )
		EPutText(str);
	else
		PutText(str);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(char v)
{
	if( escape )
		ETextPutChar(v);
	else
		PutChar(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(unsigned char v)
{
	if( escape )
		ETextPutChar(v);
	else
		PutChar(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(wchar_t v)
{
	if( escape )
		ETextPutChar(v);
	else
		PutChar(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(char32_t v)
{
	if( escape )
		ETextPutChar(v);
	else
		PutChar(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(bool v)
{
	/*
	 * bool, short, int, long, long long, float, double and long double don't have to be escaped
	 * they consist of digits only: '0' - '9' (and with an exponent in the case of float/double/long double)
	 * and don't have to be escaped
	 *
	 */
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(short v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(int v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(long v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(long long v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(unsigned short v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(unsigned int v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(unsigned long v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(unsigned long long v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(float v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(double v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(long double v)
{
	buffer.operator<<(v);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(const void * v)
{
	buffer.operator<<(v);

return *this;
}



HtmlTextStream & HtmlTextStream::operator<<(const Stream & stream)
{
	if( escape )
	{
		if(stream.is_char_stream())
		{
			int res;
			bool correct;
			size_t len;
			size_t index = 0;
			size_t stream_len = stream.size();

			// CHECKME
			while( index < stream_len && (len = pt::utf8_to_int(stream, index, res, correct)) > 0 )
			{
				if( !correct )
					res = 0xFFFD; // U+FFFD "replacement character"

				ETextPutChar(static_cast<wchar_t>(res));
				index += len;
			}
		}
		else
		if(stream.is_wchar_stream())
		{
			for(size_t i=0 ; i < stream.size() ; ++i)
				ETextPutChar(stream.get_wchar(i));
		}
	}
	else
	{
		buffer.operator<<(stream);
	}

return *this;
}



HtmlTextStream & HtmlTextStream::operator<<(const pt::Space & space)
{
	if( escape )
	{
		space.serialize_to_json_stream(*this, true);

		pt::WTextStream tmp_stream;
		space.serialize_to_json_stream(tmp_stream, true);

		pt::WTextStream::iterator i = tmp_stream.begin();

		for( ; i != tmp_stream.end() ; ++i)
			ETextPutChar(*i);
	}
	else
	{
		// FIXME this will serialize to Space format !!!!
		buffer.operator<<(space);
	}

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(const pt::Date & date)
{
	// dates don't need to be escaped
	buffer.operator<<(date);

return *this;
}


HtmlTextStream & HtmlTextStream::operator<<(morm::Model & model)
{
	// CHECKME
	pt::TextStream tmp_stream;
	model.to_text(tmp_stream);

	return operator<<(tmp_stream);
}





} // namespace Winix



