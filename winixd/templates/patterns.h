/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_templates_patterns
#define headerfile_winix_templates_patterns

#include <vector>
#include "locale.h"
#include "localefilter.h"
#include "misc.h"
#include "core/winixbase.h"


namespace Winix
{


class Patterns : public WinixBase
{
public:

	Patterns();

	void SetDeleteWhiteItems(bool del_white);
	void SetDirectories(const std::wstring & tmpl_dir, const std::wstring & tmpl_dir_def);

	/*
		setting locale and locale_filter
		this method should always be called on the beginning
	*/
	void SetLocale(Locale * plocale);
	void SetLocaleFilter(LocaleFilter * plocale_filter);

	/*
		remembering a pointer to ezc functions
		this functions (pointers to them) will be cached in patterns

		this caching will be done when a pattern is read from the hard drive
		so you have to call this method before
	*/
	void SetEzcFunctions(TemplatesFunctions::EzcFun * fun);

	/*
		remembering a pointer to ezc blocks
		this blocks (pointers to them) will be cached in patterns

		this caching will be done when a pattern is read from the hard drive
		so you have to call this method before
	*/
	void SetEzcBlocks(Ezc::Blocks * blocks);


	/*
		remembering a pointer to ezc objects
	*/
	void SetEzcObjects(Ezc::Objects<HtmlTextStream> * obj);


	/*
		adding a new pattern and returning its index
		if the pattern already exists the method returns its index only
		and increment internal reference counter for such pattern

		if read_pattern is false then the pattern is not read,
		it will be read when you call Reload() method

		after adding some new patterns you have to call RebuildCache() method
	*/
	size_t Add(const wchar_t * file_name,      bool read_pattern = true);
	size_t Add(const std::wstring & file_name, bool read_pattern = true);


	/*
		returning a pattern (if exists)
		if the pattern does not exist return a null pointer
	*/
	Ezc::Pattern * Get(size_t index, size_t lang_id);


	/*
		returning a file name of a pattern
		or an empty string if the pattern does not exist
	*/
	const std::wstring & GetFileName(size_t index);


	/*
		deleting all patterns
	*/
	void Clear();


	/*
		decrementing internal reference counter and if zero then deletes the pattern
	*/
	void Erase(size_t index);


	/*
		reloading all patterns and rebuilding cache
	*/
	void Reload();


	/*
		returning how many patterns do we have
		remember that we have one pattern for each language
		so the real number of patterns is: locale->Size() * Size()
	*/
	size_t Size();


	/*
	 * rebuilding the cache for functions and blocks
	 * you should call this method when you Add() new patterns
	 */
	void RebuildCache();


private:

	bool del_white_items;
	std::wstring templates_dir, templates_dir_def;
	Locale * locale;
	LocaleFilter * locale_filter;

	// can be null (not set directly)
	TemplatesFunctions::EzcFun * ezc_fun;

	// can be null
	Ezc::Blocks * ezc_blocks;

	// can be null
	Ezc::Objects<HtmlTextStream> * ezc_obj;



	struct Template
	{
		bool to_delete;
		std::wstring file_name;
		size_t references;					// starts from 1 (zero means the pattern was deleted)
											// (we do not delete the Template immediately because
											// indices would be invalidated)
		std::vector<Ezc::Pattern> patterns; // table[through lang index]
	};

	typedef std::vector<Template> PatTab;
	PatTab pat_tab;
	Template template_temp;

	// non-const for default assignment operator to be created
	std::wstring empty_str;

	Ezc::PatternParser pattern_parser;

	void ReadPatterns(Template & templ);

};



} // namespace Winix

#endif
