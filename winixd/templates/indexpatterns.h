/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_templates_indexpatterns
#define headerfile_winix_templates_indexpatterns

#include <string>
#include <vector>
#include <map>
#include "ezc.h"
#include "patterns.h"
#include "core/winixbase.h"



namespace Winix
{


class IndexPatterns : public WinixBase
{
public:

	void SetPatterns(Patterns * ppatterns);


	/*
		adding a new pattern
		if such a pattern exists the method only unmarks the pattern from deleting
	*/
	void Add(const std::wstring & file);


	/*
		returning a pattern
		can return null pointer if there is not such a pattern
	*/
	Ezc::Pattern * Get(const std::wstring & file, size_t lang);

	
	/*
		marking all patterns as ready to delete
	*/
	void MarkAllToDelete();


	/*
		delete marked patterns
		if you have called Add(pattern_name) then such a pattern is not deleted
	*/
	void DeleteMarked();


	/*
		deleting all patterns
	*/
	void Clear();


private:

	Patterns * patterns;

	struct Template
	{
		bool to_delete;
		size_t index;
	};

	typedef std::map<std::wstring, Template> Tab;
	Tab tab;

};


} // namespace Winix

#endif
