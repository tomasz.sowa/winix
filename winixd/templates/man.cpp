/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"
#include "functions/functions.h"

namespace Winix
{


namespace TemplatesFunctions
{

static Functions::Iterator winixfun_iter;
static size_t winixfun_curreq = 0;
static Ezc::Stack * winixstack = &empty_stack;



bool man_winixfun_tabcheck()
{
	if( winixfun_curreq != cur->request->id )
	{
		winixfun_curreq = cur->request->id;
		winixfun_iter   = functions->Begin();
	}

return winixfun_iter != functions->End();
}


void man_winixfun_tab(Info & i)
{
	man_winixfun_tabcheck();
	winixstack = &i.stack;

	if( i.iter == 0 )
		winixfun_iter = functions->Begin();
	else
	if( winixfun_iter != functions->End() )
		++winixfun_iter;
	
	i.res = winixfun_iter != functions->End();
}


void man_winixfun_tab_index(Info & i)
{
	i.out << (winixstack->iter + 1);
}


void man_winixfun_tab_name(Info & i)
{
	if( man_winixfun_tabcheck() )
		i.out << winixfun_iter->first;
}





static EzcFun::Iterator ezcfun_iter;
static size_t ezcfun_curreq = 0;
static Ezc::Stack * ezcstack = &empty_stack;


bool man_ezcfun_tabcheck()
{
	if( ezcfun_curreq != cur->request->id )
	{
		ezcfun_curreq = cur->request->id;
		ezcfun_iter   = ezc_functions.Begin();
	}

return ezcfun_iter != ezc_functions.End();
}


void man_ezcfun_tab(Info & i)
{
	man_ezcfun_tabcheck();
	ezcstack = &i.stack;

	if( i.iter == 0 )
		ezcfun_iter = ezc_functions.Begin();
	else
	if( ezcfun_iter != ezc_functions.End() )
		++ezcfun_iter;

	i.res = ezcfun_iter != ezc_functions.End();
}


void man_ezcfun_tab_index(Info & i)
{
	i.out << (ezcstack->iter + 1);
}


void man_ezcfun_tab_name(Info & i)
{
	if( man_ezcfun_tabcheck() )
		i.out << ezcfun_iter->first;
}







} // namespace TemplatesFunctions

} // namespace Winix



