/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "localefilter.h"
#include "core/misc.h"

namespace Winix
{


LocaleFilter::LocaleFilter()
{
	open_mark    = '{';
	closing_mark = '}';
}



void LocaleFilter::ReadKey()
{
	key.clear();

	for( ; *pchar && *pchar != closing_mark ; ++pchar)
		key += *pchar;

	// skipping last closing mark (if exists)
	if( *pchar == closing_mark )
		++pchar;

	TrimWhite(key);
}



void LocaleFilter::Parse(std::wstring & str)
{
	res.clear();
	pchar = str.c_str();

	while( *pchar )
	{
		if( *pchar == open_mark )
		{
			++pchar;
			ReadKey();
			res += plocale->GetByIndex(key, lang);
		}
		else
		if( *pchar == '\\' && (*(pchar+1) == open_mark || *(pchar+1) == closing_mark || *(pchar+1) == '\\') )
		{
			res   += *(pchar+1);
			pchar += 2;
		}
		else
		{
			res   += *pchar;
			pchar += 1;
		}
	}

	str = res;
}



void LocaleFilter::FilterText(Ezc::Item & item)
{
	if( !item.text.empty() )
		Parse(item.text);

	std::vector<Ezc::Item*>::iterator i = item.item_tab.begin();

	for( ; i != item.item_tab.end() ; ++i )
		FilterText(**i);
}



void LocaleFilter::Filter(Ezc::Pattern & pattern, const Locale & locale, size_t lang_index)
{
	plocale = &locale;
	lang    = lang_index;

	FilterText( pattern.item_root );
}



} // namespace Winix

