/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "templates.h"
#include "core/misc.h"
#include "functions/functions.h"

namespace Winix
{


namespace TemplatesFunctions
{



void dir(Info & i)
{
	for(size_t a=0 ; a<cur->request->dir_tab.size() ; ++a)
		i.out << cur->request->dir_tab[a]->url << '/';
}


void dir_without_slash(Info & i)
{
	for(size_t a=0 ; a<cur->request->dir_tab.size() ; ++a)
	{
		i.out << cur->request->dir_tab[a]->url;

		if( a < cur->request->dir_tab.size()-1 )
			i.out << '/';
	}
}


void dir_is_root(Info & i)
{
	i.res = cur->request->dir_tab.size() == 1;
}


void dir_parent(Info & i)
{
	if( cur->request->dir_tab.empty() )
		return;

	for(size_t a=0 ; a<cur->request->dir_tab.size()-1 ; ++a)
	{
		i.out << cur->request->dir_tab[a]->url << '/';
	}
}


void dir_parent_without_slash(Info & i)
{
	if( cur->request->dir_tab.empty() )
		return;

	for(size_t a=0 ; a<cur->request->dir_tab.size()-1 ; ++a)
	{
		i.out << cur->request->dir_tab[a]->url;

		if( cur->request->dir_tab.size()>=2 && a<cur->request->dir_tab.size()-2 )
			i.out << '/';
	}
}


//!! moze wystarczy sprawdzac tylko ostatni katalog?
// bo inaczej i tak bylo by 'access denied'
void dir_can_read_exec(Info & i)
{
	i.res = system->HasReadExecAccessToPath(cur->request->dir_tab);
}

// !! zamienic na dir_last_can_write ?
void dir_can_write(Info & i)
{
	i.res = system->HasWriteAccess(*cur->request->dir_tab.back());
}


void dir_can_remove(Info & i)
{
bool result = true;
	
	
	if( cur->request->dir_tab.size() == 1 )
	{
		// rm for the root dir
		// only the superuser can do it
		if( !cur->session->puser || !cur->session->puser->is_super_user )
			result = false;
	}
	else
	{
		Item * last_but_one_dir = *(--(--cur->request->dir_tab.end()));

		if( !system->HasWriteAccess(*last_but_one_dir) )
			result = false;
	}

	i.res = result;
}


void dir_can_use_emacs(Info & i)
{
	i.res = functions->fun_emacs.has_access();
}


void dir_can_use_mkdir(Info & i)
{
	i.res = functions->fun_mkdir.has_access();
}


static std::wstring dir_parents_str;



bool dir_is(const std::wstring & par)
{
	dir_parents_str.clear();

	for(size_t a=0 ; a<cur->request->dir_tab.size() ; ++a)
	{
		dir_parents_str += cur->request->dir_tab[a]->url;
		dir_parents_str += '/';
	}

	if( !par.empty() && par[par.size()-1] != '/' )
		if( !dir_parents_str.empty() )
			dir_parents_str.erase(dir_parents_str.size()-1);

	return par == dir_parents_str;
}


void dir_is(Info & i)
{
	i.res = dir_is(i.par);
}


void dir_is_no(Info & i)
{
	i.res = !dir_is(i.par);
}


void dir_has_parents(Info & i)
{
	dir_parents_str.clear();

	for(size_t a=0 ; a<cur->request->dir_tab.size() ; ++a)
	{
		dir_parents_str += cur->request->dir_tab[a]->url;
		dir_parents_str += '/';
	}

	i.res = pt::is_substr(i.par, dir_parents_str);
}


void dir_level_is(Info & i)
{
	i.res = (cur->request->dir_tab.size() == size_t(Toi(i.par)));
}




/*
static std::vector<Item*> dir_childs_table;
static size_t dir_childs_index;
// cur->request->id is never 0 and we can start dir_childs_reqid from 0
static size_t dir_childs_reqid = 0;

// is the first directory the parent ('..')
static bool dir_childs_has_parent;


void dir_childs_tab(Info & i)
{
	if( dir_childs_reqid != cur->request->id )
	{
		dir_childs_reqid = cur->request->id;
		dir_childs_table.clear();
		dir_childs_has_parent = false;

		if( !cur->request->dir_tab.empty() )
		{
			if( cur->request->dir_tab.size() >= 2 && i.par == L"with_parent")
			{
				Item * dir_up = cur->request->dir_tab[cur->request->dir_tab.size()-2];
				dir_childs_table.push_back(dir_up);
				dir_childs_has_parent = true;
			}

			system->dirs.GetDirChilds(cur->request->dir_tab.back()->id, dir_childs_table);
		}
	}
	
	dir_childs_index = i.iter;
	i.res = dir_childs_index < dir_childs_table.size();
}


// is this child a parent ('..')
void dir_childs_is_parent(Info & i)
{
	i.res = (dir_childs_has_parent && dir_childs_index == 0);
}


void dir_childs_tab_url(Info & i)
{
	if( dir_childs_index < dir_childs_table.size() )
		i.out << dir_childs_table[dir_childs_index]->url;
}



void dir_childs_tab_privileges(Info & i)
{
	if( dir_childs_index < dir_childs_table.size() )
		i.out << "0" << Toa(dir_childs_table[dir_childs_index]->item_content.privileges, 8);
}


void dir_childs_tab_user(Info & i)
{
	if( dir_childs_index < dir_childs_table.size() )
	{
		long user_id = dir_childs_table[dir_childs_index]->item_content.user_id;
		User * puser = system->users.GetUser(user_id);

		if( puser )
			i.out << puser->login;
		else
		{
			i.out << "~";
			
			if( !dir_childs_table[dir_childs_index]->item_content.guest_name.empty() )
				i.out << dir_childs_table[dir_childs_index]->item_content.guest_name;
			else
				i.out << "guest"; // !! dodac do konfiga
		}
	}
}



void dir_childs_tab_group(Info & i)
{
	if( dir_childs_index < dir_childs_table.size() )
	{
		long group_id = dir_childs_table[dir_childs_index]->item_content.group_id;
		Group * pgroup = system->groups.GetGroup(group_id);

		if( pgroup )
			i.out << pgroup->login;
		else
			i.out << group_id;
	}
}
*/




/*
static size_t dir_index;


void dir_tab(Info & i)
{
	dir_index = i.iter;	
	i.res  = dir_index < cur->request->dir_tab.size();
}


void dir_tab_url(Info & i)
{
	if( dir_index < cur->request->dir_tab.size() )
		i.out << cur->request->dir_tab[dir_index]->url;
}


void dir_tab_link(Info & i)
{
	for(size_t a = 0 ; a <= dir_index && a < cur->request->dir_tab.size() ; ++a)
	{
		// index zero is a root directory
		if( a != 0 )
			i.out << cur->request->dir_tab[a]->url;

		i.out << '/';
	}
}

void dir_tab_is_root(Info & i)
{
	i.res = (dir_index == 0);
}


void dir_tab_subject(Info & i)
{
	if( dir_index < cur->request->dir_tab.size() )
		i.out << cur->request->dir_tab[dir_index]->subject;
}
*/


void dir_last_link_to(Info & i)
{
	i.out << cur->request->dir_tab.back()->item_content.link_to;
}


void dir_last_is_link_redirect(Info & i)
{
	i.res = cur->request->dir_tab.back()->item_content.link_redirect == 1;
}


void dir_last_subject(Info & i)
{
	i.out << cur->request->dir_tab.back()->subject;
}


void dir_last_user(Info & i)
{
	User * puser = system->users.GetUser(cur->request->dir_tab.back()->item_content.user_id);
	
	if( puser )
		i.out << puser->login;
	else
	{
		i.out << "~";
		
		if( !cur->request->dir_tab.back()->item_content.guest_name.empty() )
			i.out << cur->request->dir_tab.back()->item_content.guest_name;
		else
			i.out << "guest"; // !! dodac do konfiga
	}
}


void dir_last_url(Info & i)
{
	i.out << cur->request->dir_tab.back()->url;
}


void dir_last_url_is(Info & i)
{
	i.res = cur->request->dir_tab.back()->url == i.par;
}

void dir_last_url_is_no(Info & i)
{
	i.res = cur->request->dir_tab.back()->url != i.par;
}


void dir_last_date_creation(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->dir_tab.back()->item_content.date_creation);
	i.out << DateToStr(date.year, date.month, date.day, date.hour, date.min, date.sec);
}


void dir_last_date_modification(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->dir_tab.back()->item_content.date_modification);
	i.out << DateToStr(date.year, date.month, date.day, date.hour, date.min, date.sec);
}


void dir_last_date_creation_nice(Info & i)
{
	print_date_nice(i, cur->request->dir_tab.back()->item_content.date_creation);
}


void dir_last_date_modification_nice(Info & i)
{
	print_date_nice(i, cur->request->dir_tab.back()->item_content.date_modification);
}



void dir_last_dates_equal(Info & i)
{
	pt::Date & date1 = cur->request->dir_tab.back()->item_content.date_creation;
	pt::Date & date2 = cur->request->dir_tab.back()->item_content.date_modification;
	
	i.res =	date1 == date2;
}


void dir_last_users_different(Info & i)
{
	i.res = (cur->request->dir_tab.back()->item_content.user_id != cur->request->dir_tab.back()->item_content.modification_user_id);
}


void dir_last_modification_user(Info & i)
{
	User * puser = system->users.GetUser(cur->request->dir_tab.back()->item_content.modification_user_id);
	print_user_name(i, puser, cur->request->dir_tab.back()->item_content.guest_name);
}


void dir_last_html_template(Info & i)
{
	i.out << cur->request->dir_tab.back()->html_template;
}


void dir_last_has_html_template(Info & i)
{
	i.res = !cur->request->dir_tab.back()->html_template.empty();
}



void dir_last_meta_str(Info & i)
{
	cur->request->dir_tab.back()->item_content.meta.serialize_to_space_stream(i.out, true);
}


//void dir_last_meta(Info & i)
//{
//	space_value(i, cur->request->dir_tab.back()->item_content.meta);
//}
//
//
//void dir_last_meta_tab(Info & i)
//{
//	space_list_tab(i, cur->request->dir_tab.back()->item_content.meta);
//}
//
//
//void dir_last_meta_tab_value(Info & i)
//{
//	space_list_tab_value(i, cur->request->dir_tab.back()->item_content.meta, L"dir_last_meta_tab");
//}
//
//
//
//void dir_last_meta_tab_has_next(Info & i)
//{
//	space_list_tab_has_next(i, cur->request->dir_tab.back()->item_content.meta, L"dir_last_meta_tab");
//}




//void dir_last_admin_meta_str(Info & i)
//{
//	cur->request->dir_tab.back()->item_content.meta_admin.serialize_to_space_stream(i.out, true);
//}
//
//
//void dir_last_admin_meta(Info & i)
//{
//	space_value(i, cur->request->dir_tab.back()->item_content.meta_admin);
//}
//
//
//void dir_last_admin_meta_tab(Info & i)
//{
//	space_list_tab(i, cur->request->dir_tab.back()->item_content.meta_admin);
//}
//
//
//void dir_last_admin_meta_tab_value(Info & i)
//{
//	space_list_tab_value(i, cur->request->dir_tab.back()->item_content.meta_admin, L"dir_last_admin_meta_tab");
//}
//
//
//void dir_last_admin_meta_tab_has_next(Info & i)
//{
//	space_list_tab_has_next(i, cur->request->dir_tab.back()->item_content.meta_admin, L"dir_last_admin_meta_tab");
//}




} // namespace TemplatesFunctions

} // namespace Winix

