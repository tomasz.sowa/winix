/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "misc.h"
#include "core/request.h"
#include "core/misc.h"

namespace Winix
{


namespace TemplatesFunctions
{


void stat_item_type_is_file(Info & i)
{
	i.res = cur->request->is_item && cur->request->item.item_content.file_type == WINIX_ITEM_FILETYPE_NONE;
}



void stat_item_type_is_static_file(Info & i)
{
	i.res = cur->request->is_item && cur->request->item.item_content.file_type != WINIX_ITEM_FILETYPE_NONE;
}



void stat_item_type_is_dir(Info & i)
{
	i.res = !cur->request->is_item;
}



void stat_item_inode(Info & i)
{
	i.out << cur->request->last_item->id;
}


void stat_item_user(Info & i)
{
	User * puser = system->users.GetUser(cur->request->last_item->item_content.user_id);
	print_user_name(i, puser, cur->request->last_item->item_content.guest_name);
}


void stat_item_group(Info & i)
{
	long group_id = cur->request->last_item->item_content.group_id;
	Group * pgroup = system->groups.GetGroup(group_id);
	
	if( pgroup )
		i.out << pgroup->name;
	else
		i.out << group_id;
}


void stat_item_privileges(Info & i)
{
	i.out << Toa(cur->request->last_item->item_content.privileges, 8);
}


void stat_item_date_creation(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->last_item->item_content.date_creation);
	i.out << DateToStr(date.year, date.month, date.day, date.hour, date.min, date.sec);
}


void stat_item_date_modification(Info & i)
{
	pt::Date date = system->ToLocal(cur->request->last_item->item_content.date_modification);
	i.out << DateToStr(date.year, date.month, date.day, date.hour, date.min, date.sec);
}




void stat_item_template(Info & i)
{
	i.out << cur->request->last_item->html_template;
}



void stat_item_is_template_from_mount_point(Info & i)
{
	i.res = cur->request->last_item->html_template.empty();
}




} // namespace TemplatesFunctions

} // namespace Winix



