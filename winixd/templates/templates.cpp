/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/plugin.h"
#include "functions/functions.h"

namespace Winix
{

// !! IMPROVE ME may change to EzcFunctions?
namespace TemplatesFunctions
{

size_t pat_index;					// main index pattern
size_t pat_index_generic;			// generic index pattern, without any css-site styles, only main css (uikit)
size_t pat_index_raw;				// pattern for a raw content (raw content without html, body markup)
size_t pat_request_status;			// error.html

Patterns          patterns;			// all html patterns
IndexPatterns     index_patterns;	// patterns for main index templates (those from mountpoint)
									// index_patterns uses patterns as a storage
ChangePatterns    change_patterns;	// patterns for change_template mount option (storage is in 'patterns' too)
PatternCacher     pattern_cacher;	// patterns for user items (files with an executable bit set)

Locale            locale;			// IMPROVE ME will be moved to a better place

EzcFun            ezc_functions;
Ezc::Blocks       ezc_blocks;
Ezc::Objects<HtmlTextStream> ezc_objects;
Ezc::Vars         ezc_vars;
LocaleFilter      locale_filter;
pt::HTMLParser    html_filter;



// used by GenerateRunRaw()
std::vector<Ezc::Var>     empty_pars;
const std::wstring        empty_string;
const HtmlTextStream      empty_stream;

// en empty stack item for templates functions
Ezc::Stack empty_stack;

Cur     * cur = nullptr;
Config  * config = nullptr;
System  * system = nullptr;
Functions * functions = nullptr;
SessionManager * session_manager = nullptr;


Log log; // temporarily for ezc functions
Plugin * plugin = nullptr; // temporarily for ezc functions


// generator used by content() function
static EzcGen content_gen;

// options used by ezc generators
// set from each Request object
bool gen_trim_white;
bool gen_skip_new_line;
bool gen_use_special_chars;



Ezc::Pattern * GetPatternForFunction()
{
static std::wstring fun_file;
Ezc::Pattern * p;

	if( !cur->request->function )
	{
		log << log1 << "Templates: cannot get a template for a function (there is no a function)" << logend;
		return 0;
	}

	fun_file = config->templates_fun_prefix;
	fun_file += cur->request->function->fun.url;
	fun_file += config->templates_fun_postfix;

	p = change_patterns.Get(cur->mount->dir_id, fun_file, locale.GetCurLang());

	if( p )
	{
		log << log3 << "Templates: function template taken from change_patterns" << logend;
		return p;
	}

	p = patterns.Get(cur->request->function->template_index, locale.GetCurLang());

	if( p )
	{
		log << log3 << "Templates: function template taken from: " << fun_file << logend;
		return p;
	}

	p = patterns.Get(cur->request->function->template_index, locale.GetDefLang());

	if( p )
	{
		log << log3 << "Templates: function template taken from: " << fun_file << " for a default language" << logend;
		return p;
	}

	p = patterns.Get(cur->request->function->template_index, 0);

	if( p )
	{
		log << log3 << "Templates: function template taken from: " << fun_file << " for a first available language" << logend;
		return p;
	}

	return p;
}



void content(EzcEnv & env)
{
Ezc::Pattern * pattern = 0;

	switch( cur->request->http_status )
	{
		case Header::status_200_ok:
			pattern = GetPatternForFunction();
			break;

		default:
			pattern = patterns.Get(pat_request_status, locale.GetCurLang());

			if( pattern )
				log << log3 << "Templates: error pattern taken from: " << patterns.GetFileName(pat_request_status) << logend;

			break;
	}
	
	if( pattern )
	{
		InitGenerator(content_gen, *cur->request);
		content_gen.SetPattern(*pattern);
		content_gen.Generate(env.out, cur->request->out_streams);
	}
	else
	{
		log << log2 << "Templates: content: there is no an available pattern for the function" << logend;
	}
}


} // namespace TemplatesFunctions




void Templates::CreateFunctions()
{
	using namespace TemplatesFunctions;
	
	ezc_functions.Clear();
	
	
	/*
		adduser
	*/
	ezc_functions.Insert("adduser_last_login", adduser_last_login);
	ezc_functions.Insert("adduser_last_email", adduser_last_email);


	/*
		antispam
	*/
	ezc_functions.Insert("antispam_create_new_form_id_for_this_session", antispam_create_new_form_id_for_this_session);
	ezc_functions.Insert("antispam_loop", 					antispam_loop);
	ezc_functions.Insert("antispam_loop_operator", 			antispam_loop_operator);
	ezc_functions.Insert("antispam_counter", 				antispam_counter);

	/*
		config
	*/
	ezc_functions.Insert("config_use_ssl",					config_use_ssl);
	ezc_functions.Insert("config_use_ssl_static",			config_use_ssl_static);
	ezc_functions.Insert("config_use_ssl_common",			config_use_ssl_common);
	ezc_functions.Insert("config_url_proto",				config_url_proto);
	ezc_functions.Insert("config_url_ssl_proto",			config_url_ssl_proto);
	ezc_functions.Insert("config_base_url",					config_base_url);
	ezc_functions.Insert("config_base_url_static",			config_base_url_static);
	ezc_functions.Insert("config_base_url_common",			config_base_url_common);
	ezc_functions.Insert("config_is_html_filter_allowed",	config_is_html_filter_allowed);


	/*
		current date
	*/
	ezc_functions.Insert("current_sec_utc",					current_sec_utc);
	ezc_functions.Insert("current_min_utc",					current_min_utc);
	ezc_functions.Insert("current_hour_utc",				current_hour_utc);
	ezc_functions.Insert("current_day_utc",					current_day_utc);
	ezc_functions.Insert("current_month_utc",				current_month_utc);
	ezc_functions.Insert("current_year_utc",				current_year_utc);
	ezc_functions.Insert("current_date_utc",				current_date_utc);
	ezc_functions.Insert("current_sec",						current_sec);
	ezc_functions.Insert("current_min",						current_min);
	ezc_functions.Insert("current_hour",					current_hour);
	ezc_functions.Insert("current_day",						current_day);
	ezc_functions.Insert("current_month",					current_month);
	ezc_functions.Insert("current_year",					current_year);
	ezc_functions.Insert("current_date",					current_date);


	/*
		dir
	*/	
	ezc_functions.Insert("dir",                        dir);
	ezc_functions.Insert("dir_without_slash",          dir_without_slash);
	ezc_functions.Insert("dir_is_root",                dir_is_root);
	ezc_functions.Insert("dir_parent",                 dir_parent);
	ezc_functions.Insert("dir_parent_without_slash",   dir_parent_without_slash);
	ezc_functions.Insert("dir_can_read_exec",          dir_can_read_exec);
	ezc_functions.Insert("dir_can_write",              dir_can_write);
	ezc_functions.Insert("dir_can_remove",             dir_can_remove);
	ezc_functions.Insert("dir_can_use_emacs",          dir_can_use_emacs);
	ezc_functions.Insert("dir_can_use_mkdir",          dir_can_use_mkdir);
	ezc_functions.Insert("dir_is",                     dir_is);
	ezc_functions.Insert("dir_is_no",                  dir_is_no);
	ezc_functions.Insert("dir_has_parents",            dir_has_parents);
	ezc_functions.Insert("dir_level_is",               dir_level_is);

	/*
	ezc_functions.Insert("dir_childs_tab",             dir_childs_tab);
	ezc_functions.Insert("dir_childs_is_parent",       dir_childs_is_parent);
	ezc_functions.Insert("dir_childs_tab_url",         dir_childs_tab_url);
	ezc_functions.Insert("dir_childs_tab_privileges",  dir_childs_tab_privileges);
	ezc_functions.Insert("dir_childs_tab_user",        dir_childs_tab_user);
	ezc_functions.Insert("dir_childs_tab_group",       dir_childs_tab_group);
	*/

	/*
	ezc_functions.Insert("dir_tab",                    dir_tab);
	ezc_functions.Insert("dir_tab_url",                dir_tab_url);
	ezc_functions.Insert("dir_tab_link",               dir_tab_link);
	ezc_functions.Insert("dir_tab_subject",            dir_tab_subject);
	ezc_functions.Insert("dir_tab_is_root",            dir_tab_is_root);
	*/
	
	ezc_functions.Insert("dir_last_link_to",           dir_last_link_to);
	ezc_functions.Insert("dir_last_is_link_redirect",  dir_last_is_link_redirect);
	ezc_functions.Insert("dir_last_subject",           dir_last_subject);
	ezc_functions.Insert("dir_last_user",              dir_last_user);
	ezc_functions.Insert("dir_last_url",               dir_last_url);
	ezc_functions.Insert("dir_last_url_is",            dir_last_url_is);
	ezc_functions.Insert("dir_last_url_is_no",         dir_last_url_is_no);
	ezc_functions.Insert("dir_last_date_creation",     dir_last_date_creation);
	ezc_functions.Insert("dir_last_date_modification", dir_last_date_modification);
	ezc_functions.Insert("dir_last_date_creation_nice",     dir_last_date_creation_nice);
	ezc_functions.Insert("dir_last_date_modification_nice", dir_last_date_modification_nice);
	ezc_functions.Insert("dir_last_dates_equal",       dir_last_dates_equal);
	ezc_functions.Insert("dir_last_users_different",   dir_last_users_different);
	ezc_functions.Insert("dir_last_modification_user", dir_last_modification_user);
	ezc_functions.Insert("dir_last_html_template",     dir_last_html_template);
	ezc_functions.Insert("dir_last_has_html_template", dir_last_has_html_template);
	ezc_functions.Insert("dir_last_meta_str",          dir_last_meta_str);
//	ezc_functions.Insert("dir_last_meta",              dir_last_meta);
//	ezc_functions.Insert("dir_last_meta_tab",          dir_last_meta_tab);
//	ezc_functions.Insert("dir_last_meta_tab_value",    dir_last_meta_tab_value);
//	ezc_functions.Insert("dir_last_meta_tab_has_next", dir_last_meta_tab_has_next);
//	ezc_functions.Insert("dir_last_admin_meta_str",    dir_last_admin_meta_str);
//	ezc_functions.Insert("dir_last_admin_meta",        dir_last_admin_meta);
//	ezc_functions.Insert("dir_last_admin_meta_tab",    dir_last_admin_meta_tab);
//	ezc_functions.Insert("dir_last_admin_meta_tab_value",    dir_last_admin_meta_tab_value);
//	ezc_functions.Insert("dir_last_admin_meta_tab_has_next", dir_last_admin_meta_tab_has_next);


	/*
		doc
	*/
	ezc_functions.Insert("doc_title",                doc_title);
	ezc_functions.Insert("doc_proto",                doc_proto);
	ezc_functions.Insert("doc_proto_static",         doc_proto_static);
	ezc_functions.Insert("doc_proto_common",         doc_proto_common);
	ezc_functions.Insert("doc_base_url",             doc_base_url);
	ezc_functions.Insert("doc_base_url_static",      doc_base_url_static);
	ezc_functions.Insert("doc_base_url_common",      doc_base_url_common);
	ezc_functions.Insert("doc_current_url",          doc_current_url);
	ezc_functions.Insert("doc_css_tab",              doc_css_tab);
	ezc_functions.Insert("doc_css_tab_file",         doc_css_tab_file);
	ezc_functions.Insert("doc_css_tab_file_is_global",   doc_css_tab_file_is_global);
	ezc_functions.Insert("doc_css_tab_has_next",     doc_css_tab_has_next);
	ezc_functions.Insert("doc_css_is_empty",         doc_css_is_empty);
	ezc_functions.Insert("doc_css_is_one",           doc_css_is_one);
	ezc_functions.Insert("doc_css_more_than_one",    doc_css_more_than_one);


	/*
		filters
	*/
	ezc_functions.Insert("fil_urlencode",            fil_urlencode);
	ezc_functions.Insert("fil_qencode",              fil_qencode);
	ezc_functions.Insert("fil_capitalize",           fil_capitalize);
	ezc_functions.Insert("fil_tosmall",              fil_tosmall);
	ezc_functions.Insert("fil_firstup",              fil_firstup);
	ezc_functions.Insert("fil_first_wordup",         fil_first_wordup);
	ezc_functions.Insert("fil_csv_escape",           fil_csv_escape);
	ezc_functions.Insert("fil_json_escape",          fil_json_escape);
	ezc_functions.Insert("fil_new_line_to_br",       fil_new_line_to_br);
	ezc_functions.Insert("fil_html_quote",           fil_html_quote);
	ezc_functions.Insert("fil_html_newline",         fil_html_newline);


	/*
		generic functions
	*/
	ezc_functions.Insert("false",			ezc_false);
	ezc_functions.Insert("true",			ezc_true);
	ezc_functions.Insert("and",				ezc_and);
	ezc_functions.Insert("any",				ezc_any);
	ezc_functions.Insert("or",				ezc_or);
	ezc_functions.Insert("one",				ezc_one);
	ezc_functions.Insert("and_not",			ezc_and_not);
	ezc_functions.Insert("any_not",			ezc_any_not);
	ezc_functions.Insert("or_not",			ezc_or_not);
	ezc_functions.Insert("one_not",			ezc_one_not);
	ezc_functions.Insert("not",				ezc_not);
	ezc_functions.Insert("cmp",				cmp);
	ezc_functions.Insert("is",				is);
	ezc_functions.Insert("is_not",			is_not);
	ezc_functions.Insert("is_empty",		is_empty);
	ezc_functions.Insert("is_not_empty",	is_not_empty);
	ezc_functions.Insert("trim",			trim);
	ezc_functions.Insert("to_lower",		to_lower);
	ezc_functions.Insert("to_upper",		to_upper);
	ezc_functions.Insert("index",			index);
	ezc_functions.Insert("str",				str);


	/*
		insert
	*/
	ezc_functions.Insert("insert_page",              insert_page);


	/*
		ipban
	*/
	ezc_functions.Insert("ipban_is_current_ip_banned",			ipban_is_current_ip_banned);
	ezc_functions.Insert("ipban_current_ip_expires_time",		ipban_current_ip_expires_time);
	ezc_functions.Insert("ipban_is_login_allowed_from_this_ip",	ipban_is_login_allowed_from_this_ip);
	ezc_functions.Insert("ipban_tab",							ipban_tab);
	ezc_functions.Insert("ipban_tab_id",						ipban_tab_id);
	ezc_functions.Insert("ipban_tab_ip",						ipban_tab_ip);
	ezc_functions.Insert("ipban_tab_incorrect_login",			ipban_tab_incorrect_login);
	ezc_functions.Insert("ipban_tab_broken_encoded_cookie",		ipban_tab_broken_encoded_cookie);
	ezc_functions.Insert("ipban_tab_session_hijacking",			ipban_tab_session_hijacking);
	ezc_functions.Insert("ipban_tab_no_session_cookie",			ipban_tab_no_session_cookie);
	ezc_functions.Insert("ipban_tab_ban_level",					ipban_tab_ban_level);
	ezc_functions.Insert("ipban_tab_has_active_flag",			ipban_tab_has_active_flag);
	ezc_functions.Insert("ipban_tab_expires",					ipban_tab_expires);
	ezc_functions.Insert("ipban_tab_last_used",					ipban_tab_last_used);
	ezc_functions.Insert("ipban_tab_is_logging_allowed",		ipban_tab_is_logging_allowed);


		
	/*
		last
	*/
	ezc_functions.Insert("last_tab",                   last_tab);
	ezc_functions.Insert("last_tab_name",              last_tab_name);
	ezc_functions.Insert("last_tab_ip",                last_tab_ip);
	ezc_functions.Insert("last_tab_start",             last_tab_start);
	ezc_functions.Insert("last_tab_end",               last_tab_end);


	/*
		login
	*/
	ezc_functions.Insert("login_path",					login_path);
	ezc_functions.Insert("login_should_use_captcha",	login_should_use_captcha);


	/*
		man
	*/
	ezc_functions.Insert("man_winixfun_tab",           man_winixfun_tab);
	ezc_functions.Insert("man_winixfun_tab_index",     man_winixfun_tab_index);
	ezc_functions.Insert("man_winixfun_tab_name",      man_winixfun_tab_name);
	ezc_functions.Insert("man_ezcfun_tab",             man_ezcfun_tab);
	ezc_functions.Insert("man_ezcfun_tab_index",       man_ezcfun_tab_index);
	ezc_functions.Insert("man_ezcfun_tab_name",        man_ezcfun_tab_name);


	/*
		mount
	*/
	ezc_functions.Insert("mount_type_is",              mount_type_is);
	ezc_functions.Insert("mount_page_arg_is",          mount_page_arg_is);
	ezc_functions.Insert("mount_lang_arg",             mount_lang_arg);
	ezc_functions.Insert("mount_lang_arg_is",          mount_lang_arg_is);
	ezc_functions.Insert("mount_has_html_template",    mount_has_html_template);
	ezc_functions.Insert("mount_first_html_template",  mount_first_html_template);
	ezc_functions.Insert("mount_css_tab",              mount_css_tab);
	ezc_functions.Insert("mount_css_tab_file",         mount_css_tab_file);
	ezc_functions.Insert("mount_css_tab_file_is_global", mount_css_tab_file_is_global);
	ezc_functions.Insert("mount_css_tab_has_next",     mount_css_tab_has_next);
	ezc_functions.Insert("mount_css_is_empty",         mount_css_is_empty);
	ezc_functions.Insert("mount_css_is_one",           mount_css_is_one);
	ezc_functions.Insert("mount_css_more_than_one",    mount_css_more_than_one);
	ezc_functions.Insert("mount_cur_type",             mount_cur_type);
	ezc_functions.Insert("mount_cur_dir",              mount_cur_dir);
	ezc_functions.Insert("mount_cur_fs",               mount_cur_fs);
	ezc_functions.Insert("mount_cur_parlist",          mount_cur_parlist);
	ezc_functions.Insert("mount_tab",                  mount_tab);
	ezc_functions.Insert("mount_tab_type",             mount_tab_type);
	ezc_functions.Insert("mount_tab_dir",              mount_tab_dir);
	ezc_functions.Insert("mount_tab_fs",               mount_tab_fs);
	ezc_functions.Insert("mount_tab_parlist",          mount_tab_parlist);


	/*
		passwd
	*/
	ezc_functions.Insert("passwd_resetpass_login",		passwd_resetpass_login);
	ezc_functions.Insert("passwd_resetpass_code",		passwd_resetpass_code);


	/*
		
	*/
	ezc_functions.Insert("ls_ckeditor_funnum_browse",	ls_ckeditor_funnum_browse);


	/*
		privileges
	*/
	ezc_functions.Insert("priv_user_tab",             priv_user_tab);
	ezc_functions.Insert("priv_user_tab_name",        priv_user_tab_name);
	ezc_functions.Insert("priv_user_tab_isdefault",   priv_user_tab_isdefault);
	
	ezc_functions.Insert("priv_group_tab",            priv_group_tab);
	ezc_functions.Insert("priv_group_tab_name",       priv_group_tab_name);
	ezc_functions.Insert("priv_group_tab_isdefault",  priv_group_tab_isdefault);
	
	ezc_functions.Insert("priv_privileges",           priv_privileges);
	ezc_functions.Insert("priv_privileges_for_files", priv_privileges_for_files);
	ezc_functions.Insert("priv_privileges_for_dirs",  priv_privileges_for_dirs);
	ezc_functions.Insert("priv_show_form_chown",      priv_show_form_chown);
	ezc_functions.Insert("priv_show_form_chmod",      priv_show_form_chmod);


	/*
		pw
	*/


	/*
		rebus
	*/
	ezc_functions.Insert("rebus_question",            rebus_question);


	/*
		server
	*/
	ezc_functions.Insert("server_mode",					server_mode);
	ezc_functions.Insert("server_mode_is",				server_mode_is);


	/*
		stat
	*/
	ezc_functions.Insert("stat_item_type_is_file",          stat_item_type_is_file);
	ezc_functions.Insert("stat_item_type_is_static_file",   stat_item_type_is_static_file);
	ezc_functions.Insert("stat_item_type_is_dir",           stat_item_type_is_dir);
	ezc_functions.Insert("stat_item_inode",                 stat_item_inode);
	ezc_functions.Insert("stat_item_user",                  stat_item_user);
	ezc_functions.Insert("stat_item_group",                 stat_item_group);
	ezc_functions.Insert("stat_item_privileges",            stat_item_privileges);
	ezc_functions.Insert("stat_item_date_creation",         stat_item_date_creation);
	ezc_functions.Insert("stat_item_date_modification",     stat_item_date_modification);
	ezc_functions.Insert("stat_item_template",              stat_item_template);
	ezc_functions.Insert("stat_item_is_template_from_mount_point", stat_item_is_template_from_mount_point);


	/*
		sys
	*/
	ezc_functions.Insert("sys_ver_major",            sys_ver_major);
	ezc_functions.Insert("sys_ver_minor",            sys_ver_minor);
	ezc_functions.Insert("sys_ver_revision",         sys_ver_revision);
	ezc_functions.Insert("sys_pikotools_ver_major",  sys_pikotools_ver_major);
	ezc_functions.Insert("sys_pikotools_ver_minor",  sys_pikotools_ver_minor);
	ezc_functions.Insert("sys_pikotools_ver_patch",  sys_pikotools_ver_patch);
	ezc_functions.Insert("sys_morm_ver_major",       sys_morm_ver_major);
	ezc_functions.Insert("sys_morm_ver_minor",       sys_morm_ver_minor);
	ezc_functions.Insert("sys_morm_ver_patch",       sys_morm_ver_patch);
	ezc_functions.Insert("sys_ezc_ver_major",       sys_ezc_ver_major);
	ezc_functions.Insert("sys_ezc_ver_minor",       sys_ezc_ver_minor);
	ezc_functions.Insert("sys_ezc_ver_patch",       sys_ezc_ver_patch);
	ezc_functions.Insert("sys_tito_ver_major",       sys_tito_ver_major);
	ezc_functions.Insert("sys_tito_ver_minor",       sys_tito_ver_minor);
	ezc_functions.Insert("sys_tito_ver_patch",       sys_tito_ver_patch);
	ezc_functions.Insert("sys_plugin_tab",           sys_plugin_tab);
	ezc_functions.Insert("sys_plugin_tab_has_name",  sys_plugin_tab_has_name);
	ezc_functions.Insert("sys_plugin_tab_name",      sys_plugin_tab_name);


	/*
		upload
	*/
	ezc_functions.Insert("upload_ckeditor_funnum",    upload_ckeditor_funnum);


	/*
		uptime
	*/
	ezc_functions.Insert("uptime_more_than_one_day",  uptime_more_than_one_day);
	ezc_functions.Insert("uptime_days",               uptime_days);
	ezc_functions.Insert("uptime_hours",              uptime_hours);


	/*
		user
	*/
	ezc_functions.Insert("user_id",                   user_id);
	ezc_functions.Insert("user_name",                 user_name);
	ezc_functions.Insert("user_logged",               user_logged);
	ezc_functions.Insert("user_super_user",           user_super_user);
	ezc_functions.Insert("user_is_in_group",          user_is_in_group);
	ezc_functions.Insert("user_is_in_all_groups",     user_is_in_all_groups);
	ezc_functions.Insert("user_can_use_html",         user_can_use_html);
	ezc_functions.Insert("user_can_use_bbcode",       user_can_use_bbcode);
	ezc_functions.Insert("user_can_use_other",        user_can_use_other);
	ezc_functions.Insert("user_has_correct_time_zone",user_has_correct_time_zone);
	ezc_functions.Insert("user_time_zone_name",       user_time_zone_name);
	ezc_functions.Insert("user_time_zone_id",         user_time_zone_id);
	ezc_functions.Insert("user_time_zone_offset_hour_min", user_time_zone_offset_hour_min);
	ezc_functions.Insert("user_has_correct_locale",   user_has_correct_locale);
	ezc_functions.Insert("user_locale_name",          user_locale_name);
	ezc_functions.Insert("user_locale_id",            user_locale_id);
	ezc_functions.Insert("user_tab",                  user_tab);
	ezc_functions.Insert("user_tab_index",            user_tab_index);
	ezc_functions.Insert("user_tab_id",               user_tab_id);
	ezc_functions.Insert("user_tab_name",             user_tab_name);
	ezc_functions.Insert("user_tab_is_super_user",    user_tab_is_super_user);
	ezc_functions.Insert("user_tab_is_active",        user_tab_is_active);
	ezc_functions.Insert("user_tab_is_suspended",     user_tab_is_suspended);
	ezc_functions.Insert("user_tab_is_blocked",       user_tab_is_blocked);
	ezc_functions.Insert("user_tab_is_current",       user_tab_is_current);


	/*
		template
	*/
	ezc_functions.Insert("template_index",            template_index);
	ezc_functions.Insert("template_tab",              template_tab);
	ezc_functions.Insert("template_tab_index",        template_tab_index);
	ezc_functions.Insert("template_tab_isdefault",    template_tab_isdefault);
	ezc_functions.Insert("template_tab_file_name",    template_tab_file_name);


	/*
		winix
	*/
	ezc_functions.Insert("winix_account_need_email_verification",  winix_account_need_email_verification);
	ezc_functions.Insert("winix_cur_time",                         winix_cur_time);
	ezc_functions.Insert("winix_how_many_sessions",                winix_how_many_sessions);
	ezc_functions.Insert("winix_users_logged",                     winix_users_logged);
	ezc_functions.Insert("winix_function",                         winix_function);
	ezc_functions.Insert("winix_function_is",                      winix_function_is);
	ezc_functions.Insert("winix_function_param_is",                winix_function_param_is);
	ezc_functions.Insert("winix_function_param_is_not",            winix_function_param_is_not);
	ezc_functions.Insert("winix_function_param_value",             winix_function_param_value);
	ezc_functions.Insert("winix_function_param_value_is",          winix_function_param_value_is);
	ezc_functions.Insert("winix_function_param_value_is_not",      winix_function_param_value_is_not);
	ezc_functions.Insert("winix_has_plugin",                       winix_has_plugin);
	ezc_functions.Insert("winix_loadavg_now",                      winix_loadavg_now);
	ezc_functions.Insert("winix_loadavg_1",                        winix_loadavg_1);
	ezc_functions.Insert("winix_loadavg_5",                        winix_loadavg_5);
	ezc_functions.Insert("winix_loadavg_15",                       winix_loadavg_15);
	ezc_functions.Insert("winix_req_per_sec_now",                  winix_req_per_sec_now);
	ezc_functions.Insert("winix_req_per_sec_1",                    winix_req_per_sec_1);
	ezc_functions.Insert("winix_req_per_sec_5",                    winix_req_per_sec_5);
	ezc_functions.Insert("winix_req_per_sec_15",                   winix_req_per_sec_15);
	ezc_functions.Insert("winix_show_content_in_full_window",      winix_show_content_in_full_window);
	ezc_functions.Insert("winix_has_postvar",                      winix_has_postvar);
	ezc_functions.Insert("winix_postvar",                          winix_postvar);
	ezc_functions.Insert("winix_postvar_value_is",                 winix_postvar_value_is);
	ezc_functions.Insert("winix_postvar_value_is_not",             winix_postvar_value_is_not);
	ezc_functions.Insert("winix_subdomain",                        winix_subdomain);
	ezc_functions.Insert("winix_subdomain_is_empty",               winix_subdomain_is_empty);
	ezc_functions.Insert("winix_subdomain_is_not_empty",           winix_subdomain_is_not_empty);
	ezc_functions.Insert("winix_subdomain_is",                     winix_subdomain_is);
	ezc_functions.Insert("winix_tz_tab",                           winix_tz_tab);
	ezc_functions.Insert("winix_tz_tab_id",                        winix_tz_tab_id);
	ezc_functions.Insert("winix_tz_tab_name",                      winix_tz_tab_name);
	ezc_functions.Insert("winix_tz_tab_offset_sec",                winix_tz_tab_offset_sec);
	ezc_functions.Insert("winix_tz_tab_offset_hour_min",           winix_tz_tab_offset_hour_min);
	ezc_functions.Insert("winix_locale_tab",                       winix_locale_tab);
	ezc_functions.Insert("winix_locale_tab_id",                    winix_locale_tab_id);
	ezc_functions.Insert("winix_locale_tab_name",                  winix_locale_tab_name);
	ezc_functions.Insert("winix_is_htmx_request",                  winix_is_htmx_request);
	ezc_functions.Insert("winix_frame_is",                         winix_frame_is);

	ezc_functions.Insert("lang",                                   lang);

	/*
		who
	*/
	ezc_functions.Insert("who_tab",             who_tab);
	ezc_functions.Insert("who_tab_lp",          who_tab_lp);
	ezc_functions.Insert("who_tab_user",        who_tab_user);
	ezc_functions.Insert("who_tab_time",        who_tab_time);
	ezc_functions.Insert("who_tab_last_time",   who_tab_last_time);



	/*
		others
	*/	
	ezc_functions.Insert("content", content);


	plugin->Call(WINIX_TEMPLATES_CREATEFUNCTIONS, &ezc_functions, &ezc_objects);


	Ezc::Objects<HtmlTextStream>::Iterator i = ezc_objects.Begin();
	size_t objects_size = 0, methods_size = 0;

	for( ; i != ezc_objects.End() ; ++i)
	{
		objects_size += 1;

		(*i)->ClearFunctions();
		(*i)->AddFunctions();

		methods_size += (*i)->FunctionsSize();
	}


	log << log3 << "Templates: there are " << ezc_functions.Size() << " global ezc functions" << logend;
	log << log3 << "Templates: there are " << objects_size << " ezc objects with " << methods_size << " ezc methods" << logend;
}






void Templates::ReadLocale()
{
	TemplatesFunctions::locale.SetLocaleFiles(config->locale_files);
	TemplatesFunctions::locale.SetLocaleMaxId(config->locale_max_id);
	TemplatesFunctions::locale.SetDefLang(config->locale_default_id);
	TemplatesFunctions::locale.Read(config->locale_dir, config->locale_dir_default);

	log << log3 << "Templates: there are " << TemplatesFunctions::locale.Size() << " locales" << logend;
}


void Templates::ReadTemplatesForFunctions()
{
using namespace TemplatesFunctions;

	Functions::Iterator i = functions->Begin();

	for(; i != functions->End() ; ++i)
	{
		const std::wstring & fun_name = i->first;

		fun_file = config->templates_fun_prefix;
		fun_file += fun_name;
		fun_file += config->templates_fun_postfix;

		i->second->template_index = patterns.Add(fun_file);
	}
}


void Templates::ReadIndexTemplates()
{
using namespace TemplatesFunctions;

	Mounts::MountTab::const_iterator i;
	const Mounts::MountTab * pmount_tab = TemplatesFunctions::system->mounts.GetMountTab();
	const size_t html_id = (size_t)TemplatesFunctions::system->mounts.MountParHtmlTemplate();

	// loop through all mount points
	for(i=pmount_tab->begin() ; i!=pmount_tab->end() ; ++i)
	{
		const Mount & mount = i->second;

		if( html_id < mount.param.size() && mount.param[html_id].defined )
		{
			size_t len = mount.param[html_id].arg.size();

			// loop through all html_templates() values
			for(size_t a=0 ; a<len ; ++a)
				index_patterns.Add(mount.param[html_id].arg[a]);
		}
	}
}



/*
	reading only new index templates
	those which are exists are not touched
	the rest are deleted
*/
void Templates::ReadNewIndexTemplates()
{
using namespace TemplatesFunctions;

	index_patterns.MarkAllToDelete();
	ReadIndexTemplates();
	index_patterns.DeleteMarked();
}



void Templates::SetHtmlFilter()
{
using namespace TemplatesFunctions;

	//html_filter.TrimWhite(config->html_filter_trim_white);
	//html_filter.BreakWord(config->html_filter_break_word);
	html_filter.white_chars_mode(config->html_filter_white_char_mode);
	html_filter.WrapLine(config->html_filter_wrap_line);
	html_filter.InsertTabs(config->html_filter_tabs);
	html_filter.SetNoFilterTag(config->html_filter_nofilter_tag);
	html_filter.ClearOrphans();

	if( config->html_filter_orphans )
	{
		html_filter.OrphansMode(config->html_filter_orphans_mode_str);

		for(size_t i=0 ; i<TemplatesFunctions::locale.Size() ; ++i)
		{
			if( TemplatesFunctions::locale.IsKeyByIndex(L"winix_html_lang_attr", i, false) &&
				TemplatesFunctions::locale.IsListByIndex(L"language_orphans", i, false) )
			{
				const std::wstring & html_lang_attr = TemplatesFunctions::locale.GetByIndex(L"winix_html_lang_attr", i, false);

				std::vector<std::wstring> orphans;
				TemplatesFunctions::locale.GetListByIndex(L"language_orphans", i, orphans, false);

				html_filter.AssignOrphans(html_lang_attr, orphans);
			}
		}
	}
}



void Templates::ReadChangeTemplates()
{
using namespace TemplatesFunctions;

	Mounts::MountTab::const_iterator i;
	const Mounts::MountTab * pmount_tab = TemplatesFunctions::system->mounts.GetMountTab();
	const size_t change_id = (size_t)TemplatesFunctions::system->mounts.MountParChangeTemplate();

	// loop through all mount points
	for(i=pmount_tab->begin() ; i!=pmount_tab->end() ; ++i)
	{
		const Mount & mount = i->second;

		if( change_id < mount.param.size() && mount.param[change_id].defined )
		{
			const std::vector<std::wstring> & arg = mount.param[change_id].arg;

			for(size_t a=0 ; a+1 < arg.size() ; a += 2)
				change_patterns.Add(mount.dir_id, arg[a], arg[a+1]);
		}
	}
}



/*
	reading only new 'change' templates
	those which are exists are not touched
	the rest are deleted
*/
void Templates::ReadNewChangeTemplates()
{
using namespace TemplatesFunctions;

	change_patterns.MarkAllToDelete();
	ReadChangeTemplates();
	change_patterns.DeleteMarked();
}




void Templates::ReadTemplates()
{
using namespace TemplatesFunctions;

	ReadLocale();
	patterns.Clear();
	index_patterns.Clear();
	change_patterns.Clear();
	pattern_cacher.ClearCache();
	ezc_blocks.Clear();

	pat_index				= patterns.Add(config->templates_index);
	pat_index_generic		= patterns.Add(config->templates_index_generic);
	pat_index_raw			= patterns.Add(config->templates_index_raw);
	pat_request_status		= patterns.Add(config->templates_request_status);

	ReadTemplatesForFunctions();
	ReadIndexTemplates();
	ReadChangeTemplates();

	plugin->Call(WINIX_ADD_TEMPLATE);

	patterns.RebuildCache();
	ezc_blocks.CacheObjects(ezc_objects);
	ezc_blocks.CacheFunctions(ezc_functions);
	ezc_blocks.CacheBlocks(ezc_blocks);
	pattern_cacher.RebuildCache();

	SetHtmlFilter();

	log << log3 << "Templates: there are " << patterns.Size() << " patterns"
		<< " (" << (TemplatesFunctions::locale.Size() * patterns.Size()) << " for all locales)" << logend;
}







void Templates::Init()
{
using namespace TemplatesFunctions;

	patterns.SetDirectories(config->templates_dir, config->templates_dir_default);
	pattern_cacher.SetWhenDelete(config->pattern_cacher_when_delete, config->pattern_cacher_how_many_delete);
	CreateFunctions(); // create functions first (functions will be cached by patterns)
	ReadTemplates();
}




// clearing at the end of a request
void Templates::ClearAfterRequest()
{
using namespace TemplatesFunctions;

	log << log4 << "Templates: patterns cache size: " << pattern_cacher.Size() << logend;
	pattern_cacher.DeleteOldPatterns();
}



// can return a null pointer
Ezc::Pattern * Templates::SelectIndexPattern(const std::wstring & template_name)
{
using namespace TemplatesFunctions;

	Ezc::Pattern * index = 0;

	if( template_name == config->templates_index )
	{
		index = patterns.Get(pat_index, TemplatesFunctions::locale.GetCurLang());
	}
	else
	if( template_name == config->templates_index_generic )
	{
		index = patterns.Get(pat_index_generic, TemplatesFunctions::locale.GetCurLang());
	}
	else
	if( template_name == config->templates_index_raw )
	{
		index = patterns.Get(pat_index_raw, TemplatesFunctions::locale.GetCurLang());
	}
	else
	{
		index = index_patterns.Get(template_name, TemplatesFunctions::locale.GetCurLang());
	}

	if( index )
	{
		log << log3 << "Templates: index template taken from: " << template_name << logend;
	}

return index;
}




// can return a null pointer
Ezc::Pattern * Templates::SelectIndexPattern()
{
using namespace TemplatesFunctions;

	Ezc::Pattern * index = 0;

	if( !cur->request->html_template.empty() )
	{
		index = SelectIndexPattern(cur->request->html_template);
	}
	else
	{
		Mounts & mounts = TemplatesFunctions::system->mounts;
		const std::wstring & mount_file_name = cur->mount->FirstArg(mounts.MountParHtmlTemplate());

		if( !mount_file_name.empty() )
			index = SelectIndexPattern(mount_file_name);
	}

	if( !index )
	{
		index = change_patterns.Get(cur->mount->dir_id, config->templates_index, TemplatesFunctions::locale.GetCurLang());

		if( index )
			log << log3 << "Templates: index template taken from change_patterns" << logend;
	}

	if( !index )
	{
		index = patterns.Get(pat_index, TemplatesFunctions::locale.GetCurLang());

		if( index )
			log << log3 << "Templates: index template taken from: " << config->templates_index << logend;

	}

	if( !index )
	{
		index = patterns.Get(pat_index, TemplatesFunctions::locale.GetDefLang());

		if( index )
			log << log3 << "Templates: index template taken from: " << config->templates_index << " for a default language" << logend;

	}

	if( !index )
	{
		index = patterns.Get(pat_index, 0);

		if( index )
			log << log3 << "Templates: index template taken from: " << config->templates_index << " for a first available language" << logend;

	}

	return index;
}


void Templates::SetEzcParameters(bool trim_white, bool skip_new_line, bool use_special_chars)
{
using namespace TemplatesFunctions;

	gen_trim_white 			= trim_white;
	gen_skip_new_line 		= skip_new_line;
	gen_use_special_chars 	= use_special_chars;
}



void Templates::Generate()
{
using namespace TemplatesFunctions;

	ezc_vars.clear();
	Ezc::Pattern * index = SelectIndexPattern();

	if( index )
	{
		InitGenerator(generator, *cur->request);
		generator.SetPattern(*index);
		generator.UseMainStream(cur->request->send_main_stream);
		generator.Generate(cur->request->out_main_stream, cur->request->out_streams);

		LogProducedFrames();
	}
	else
	{
		log << log1 << "Templates: I cannot find an index template" << logend;
	}
}


void Templates::LogProducedFrames()
{
	if( !cur->request->out_main_stream.empty() )
	{
		log << log3 << "Templates: main stream size: " << cur->request->out_main_stream.size() << logend;
	}

	if( !cur->request->out_streams.streams_map.empty() )
	{
		log << log3 << "Templates: frames: ";
		bool is_first = true;

		for(auto i=cur->request->out_streams.streams_map.begin() ; i != cur->request->out_streams.streams_map.end() ; ++i)
		{
			if( !is_first )
			{
				log << ", ";
			}

			log << i->first << " (size: " << i->second->size() << ")";
			is_first = false;
		}

		log << logend;
	}
}


void Templates::SetConfig(Config * pconfig)
{
	TemplatesFunctions::config = pconfig;
}


void Templates::SetCur(Cur * pcur)
{
	TemplatesFunctions::cur = pcur;
}




void Templates::SetSystem(System * psystem)
{
	TemplatesFunctions::system = psystem;
}


void Templates::SetFunctions(Functions * pfunctions)
{
	TemplatesFunctions::functions = pfunctions;
}


void Templates::SetSessionManager(SessionManager * psession_manager)
{
	TemplatesFunctions::session_manager = psession_manager;
}


void Templates::set_dependency(WinixRequest * winix_request)
{
	WinixRequest::set_dependency(winix_request);
	TemplatesFunctions::locale.set_dependency(winix_request);
	TemplatesFunctions::change_patterns.set_dependency(winix_request);
	TemplatesFunctions::patterns.set_dependency(winix_request);
	TemplatesFunctions::index_patterns.set_dependency(winix_request);
	TemplatesFunctions::pattern_cacher.set_dependency(winix_request);
	TemplatesFunctions::locale_filter.set_dependency(winix_request);

	TemplatesFunctions::log.SetDependency(&this->log);
	TemplatesFunctions::plugin = winix_request->get_plugin();
}


Templates::Templates()
{
	using namespace TemplatesFunctions;

	patterns.SetEzcObjects(&ezc_objects);
	patterns.SetEzcFunctions(&ezc_functions);
	patterns.SetEzcBlocks(&ezc_blocks);

	patterns.SetLocale(&TemplatesFunctions::locale);
	patterns.SetLocaleFilter(&locale_filter);

	index_patterns.SetPatterns(&patterns);
	change_patterns.SetPatterns(&patterns);

	pattern_cacher.SetEzcObjects(&ezc_objects);
	pattern_cacher.SetEzcFunctions(&ezc_functions);
	pattern_cacher.SetEzcBlocks(&ezc_blocks);
}



} // namespace Winix

