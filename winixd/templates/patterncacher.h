/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_templates_patterncacher
#define headerfile_winix_templates_patterncacher

#include <vector>
#include <map>
#include "misc.h"
#include "ezc.h"
#include "core/winixbase.h"
#include "models/item.h"



namespace Winix
{




class PatternCacher : public WinixBase
{
public:

	PatternCacher();

	// setting when we should delete patterns
	// we are deleting when we have more (or equal) patterns than 'when_delete'
	// and then we are deleting 'how_many_del' patterns
	void SetWhenDelete(size_t when_delete, size_t how_many_del);

	// remembering a pointer to ezc objects
	// this ezc objects (functions) will be cached in patterns
	void SetEzcObjects(Ezc::Objects<HtmlTextStream> * obj);

	// remembering a pointer to ezc functions
	// this functions (pointers to them) will be cached in patterns
	void SetEzcFunctions(TemplatesFunctions::EzcFun * fun);

	// remembering a pointer to ezc blocks
	// this blocks (pointers to them) will be cached in patterns
	void SetEzcBlocks(Ezc::Blocks * blocks);

	// returning a pattern corresponding to the 'item'
	Ezc::Pattern * GetPattern(const Item & item);

	// updating the pattern for the item
	// if there is no such a pattern the method does nothing
	void UpdatePattern(const Item & item);
	
	// deleting the pattern for the item
	// if there is no such a pattern the method does nothing
	void DeletePattern(const Item & item);

	// deleting some rarely used patters
	// call it at the end of a request (or at the beginning)
	void DeleteOldPatterns();

	// size of the current cache in use
	size_t Size();

	// rebuild cache in all patterns
	void RebuildCache();

	// clearing all pointers to functions and blocks
	void ClearCache();


private:

	struct PatternUsed
	{
		Ezc::Pattern pattern;
		int used;    // how many times used (incremented by GetPattern and UpdatePattern)
		int item_id;
	};

	typedef std::map<long, PatternUsed> PatternTab;
	PatternTab pattern_tab;

	
	// temporarily struct used during deleting some items from pattern_tab
	struct PatternErase
	{
		PatternTab::iterator iter;
		int used;
		
		bool operator<(const PatternErase & p) const { return used < p.used; }
	};
	
	// temporarily buffer used during deleting some items from pattern_tab
	std::vector<PatternErase> erase_tab;
	
	

	void CreatePattern(const Item & item, Ezc::Pattern & pattern);
	void AddIndexes(const Item & item, size_t pattern_index);
	Ezc::Pattern * AddPattern(const Item & item);


	// the size of pattern_tab when we are deleting some items
	size_t when_delete_patterns;
	
	// how many items to delete
	size_t how_many_delete;

	// can be null
	TemplatesFunctions::EzcFun * ezc_fun;

	// can be null
	Ezc::Blocks * ezc_blocks;

	// can be null
	Ezc::Objects<HtmlTextStream> * ezc_obj;

	// parser for patterns
	Ezc::PatternParser pattern_parser;

	void RebuildCache(Ezc::Pattern & pattern);

};


} // namespace Winix

#endif
