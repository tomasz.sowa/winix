/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2014-2015, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "templates.h"
#include "core/request.h"
#include "core/misc.h"


namespace Winix
{

namespace TemplatesFunctions
{



void ezc_false(Info & i)
{
	i.res = false;
}


void ezc_true(Info & i)
{
	i.res = true;
}


void ezc_and(Info & i)
{
	if( !i.params.empty() )
	{
		i.res = true;

		for(size_t a=0 ; a < i.params.size() ; ++a)
		{
			if( !i.params[a].res )
			{
				i.res = false;
				break;
			}
		}
	}
}


void ezc_any(Info & i)
{
	ezc_and(i);
}


void ezc_or(Info & i)
{
	i.res = false;

	for(size_t a=0 ; a < i.params.size() ; ++a)
	{
		if( i.params[a].res )
		{
			i.res = true;
			break;
		}
	}
}


void ezc_one(Info & i)
{
	ezc_or(i);
}


void ezc_and_not(Info & i)
{
	if( !i.params.empty() )
	{
		i.res = true;

		for(size_t a=0 ; a < i.params.size() ; ++a)
		{
			if( i.params[a].res )
			{
				i.res = false;
				break;
			}
		}
	}
}


void ezc_any_not(Info & i)
{
	ezc_and_not(i);
}



void ezc_or_not(Info & i)
{
	i.res = false;

	for(size_t a=0 ; a < i.params.size() ; ++a)
	{
		if( !i.params[a].res )
		{
			i.res = true;
			break;
		}
	}
}


void ezc_one_not(Info & i)
{
	ezc_or_not(i);
}



void ezc_not(Info & i)
{
	if( !i.params.empty() )
		i.res = !i.params[0].res;
}




/*
 * compare strings
 */
void cmp(Info & i)
{
	if( i.params.size() >= 2 )
	{
		i.res = true;

		for(size_t a=0 ; a < i.params.size() - 1 ; ++a)
		{
			if( i.params[a].str != i.params[a+1].str )
			{
				i.res = false;
				break;
			}
		}
	}
}



void is(Info & i)
{
	cmp(i);
}



void is_not(Info & i)
{
	cmp(i);
	i.res = !i.res;
}



void is_empty(Info & i)
{
	i.res = true;

	for(size_t a=0 ; a < i.params.size() ; ++a)
	{
		if( !i.params[a].str.empty() )
		{
			i.res = false;
			break;
		}
	}
}



void is_not_empty(Info & i)
{
	is_empty(i);
	i.res = !i.res;
}




// IMPROVE ME !! may we need such a filter too?
void trim(Info & i)
{
	for(size_t a=0 ; a < i.params.size() ; ++a)
	{
		// trimming with new lines too
		TrimWhite(i.params[a].str, true);
		i.out << R(i.params[a].str);
	}
}



void to_lower(Info & i)
{
	for(size_t a=0 ; a < i.params.size() ; ++a)
	{
		for(size_t z=0 ; z < i.params[a].str.size() ; ++z)
			i.out << R(locale.ToSmall(i.params[a].str[z]));
	}
}


void to_upper(Info & i)
{
	for(size_t a=0 ; a < i.params.size() ; ++a)
	{
		for(size_t z=0 ; z < i.params[a].str.size() ; ++z)
			i.out << R(locale.ToCapital(i.params[a].str[z]));
	}
}



bool index_find_last_for_statement(Info & i, size_t & last_for_index)
{
	last_for_index = i.stack_index;

	do
	{
		if( i.stack_tab[last_for_index].is_for )
			return true;
	}
	while( last_for_index-- > 0 );

return false;
}



void index(Info & i)
{
size_t for_index;

	if( !index_find_last_for_statement(i, for_index) )
		return;

	if( i.params.size() != 1 )
	{
		i.out << i.stack_tab[for_index].iter;
		return;
	}

	if( i.par == L"odd" )
	{
		i.res = (i.stack_tab[for_index].iter & 1) == 1;
	}
	else
	if( i.par == L"even" )
	{
		i.res = (i.stack_tab[for_index].iter & 1) == 0;
	}
	else
	if( i.par == L"first" )
	{
		i.res = i.stack_tab[for_index].iter == 0;
	}
	else
	if( IsSize(i.par, true) )
	{

		size_t number = static_cast<size_t>(Tol(SkipWhite(i.par.c_str(), true)));
		i.res = (i.stack_tab[for_index].iter == number);
	}
}



void str(Info & i)
{
	for(size_t a=0 ; a < i.params.size() ; ++a)
		i.out << R(i.params[a].str);
}




} // namespace

} // namespace Winix

