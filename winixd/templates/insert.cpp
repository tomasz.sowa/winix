/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "misc.h"
#include "core/request.h"
#include "core/misc.h"

namespace Winix
{


// max 20 nested insert_page ezc functions allowed
#define WINIX_TEMPLATES_INSERT_PAGE_MAX 20



namespace TemplatesFunctions
{

struct InsertPageInfo
{
	std::vector<Item*>	dirs;
	Item				item;
	EzcGen				ezc_gen;
	HtmlTextStream		run_content;
};


static InsertPageInfo insert_page_info[WINIX_TEMPLATES_INSERT_PAGE_MAX];
size_t insert_page_cur   = 0;
size_t insert_page_reqid = 0;



void insert_page_run(Info & i)
{
	InsertPageInfo & info = insert_page_info[insert_page_cur];
	Ezc::Pattern * pat    = pattern_cacher.GetPattern(info.item);

	log << log4 << "Templates: insert_page_run: using generator number: " << insert_page_cur << logend;
	insert_page_cur += 1;

	info.run_content.clear();

	InitGenerator(info.ezc_gen, *cur->request);
	info.ezc_gen.SetPattern(*pat);
	info.ezc_gen.Generate(info.run_content);
	ItemContent::print_content(i.out, info.run_content.get_buffer(), info.item.item_content.content_raw_type, config->html_filter);

	insert_page_cur -= 1;
}



bool insert_page_init(const std::wstring & path)
{
	if( path.empty() )
		return false;

	if( insert_page_reqid != cur->request->id )
	{
		insert_page_reqid = cur->request->id;
		insert_page_cur   = 0;
	}

	if( insert_page_cur >= WINIX_TEMPLATES_INSERT_PAGE_MAX )
	{
		log << log1 << "Templates: insert_page: maximum nested insert_page functions exceeded" << logend;
		return false;
	}

return true;
}



void insert_page(Info & i)
{
	if( !insert_page_init(i.par) )
		return;

	InsertPageInfo & info = insert_page_info[insert_page_cur];

	if( system->FollowAllLinks(cur->request->dir_tab, i.par, info.dirs, info.item) == 1 )
	{
		if( system->HasReadExecAccess(info.item) )
			insert_page_run(i);
		else
		if( system->HasReadAccess(info.item) )
			ItemContent::print_content(i.out, info.item.item_content.content_raw, info.item.item_content.content_raw_type, config->html_filter);
	}
}






} // namespace TemplatesFunctions

} // namespace Winix


