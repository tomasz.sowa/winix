/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <ctime>
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"
#include "functions/functions.h"

namespace Winix
{


namespace TemplatesFunctions
{


void winix_account_need_email_verification(Info & i)
{
	i.res = config->account_need_email_verification;
}


void winix_cur_time(Info & i)
{
static char buffer[100];

	pt::Date & date = cur->request->start_date;

	sprintf(buffer, "%02d:%02d:%02d", date.hour, date.min, date.sec);
	i.out << buffer;
}


void winix_how_many_sessions(Info & i)
{
	i.out << session_manager->Size();
}


void winix_users_logged(Info & i)
{
	i.out << system->users.HowManyLogged();
}


void winix_function(Info & i)
{
	i.out << cur->request->function->fun.url;
}


void winix_function_is(Info & i)
{
	if( !cur->request->function )
		return;

	i.res = (cur->request->function->fun.url == i.par);
}


void winix_function_param_is(Info & i)
{
	i.res = cur->request->IsParam(i.par);
}


void winix_function_param_is_not(Info & i)
{
	i.res = !cur->request->IsParam(i.par);
}


void winix_function_param_value(Info & i)
{
	i.out << cur->request->ParamValue(i.par);
}


// first parameter is param_name
// second parameter is param_value
void winix_function_param_value_is(Info & i)
{
	if( i.params.size() == 2 )
		i.res = (cur->request->ParamValue(i.params[0].str) == i.params[1].str);
}


void winix_function_param_value_is_not(Info & i)
{
	if( i.params.size() == 2 )
		i.res = (cur->request->ParamValue(i.params[0].str) != i.params[1].str);
}


void winix_has_plugin(Info & i)
{
size_t exist = 0;

	if( i.params.empty() )
		return;

	for(size_t a=0 ; a<i.params.size() ; ++a)
	{
		if( plugin->HasPlugin(i.params[a].str) )
			++exist;
	}

	i.res = (exist == i.params.size());
}



void winix_loadavg_now(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.LoadAvgNow());
	i.out << buf;
}


void winix_loadavg_1(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.LoadAvg1());
	i.out << buf;
}


void winix_loadavg_5(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.LoadAvg5());
	i.out << buf;
}


void winix_loadavg_15(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.LoadAvg15());
	i.out << buf;
}


void winix_req_per_sec_now(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.ReqPerSecNow());
	i.out << buf;
}


void winix_req_per_sec_1(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.ReqPerSec1());
	i.out << buf;
}


void winix_req_per_sec_5(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.ReqPerSec5());
	i.out << buf;
}


void winix_req_per_sec_15(Info & i)
{
char buf[20];

	sprintf(buf, "%.2f", (double)system->load_avg.ReqPerSec15());
	i.out << buf;
}





void winix_show_content_in_full_window(Info & i)
{
	if( cur->request->function )
		i.res = (cur->request->function == &functions->fun_ckeditor		||
				 cur->request->function == &functions->fun_tinymce		||
				 cur->request->function == &functions->fun_nicedit		||
				 cur->request->function == &functions->fun_vim			||
				 cur->request->function == &functions->fun_emacs		||
				 cur->request->function == &functions->fun_upload );
}






void winix_has_postvar(Info & i)
{
	i.res = cur->request->IsPostVar(i.par);
}


void winix_postvar(Info & i)
{
	i.out << cur->request->PostVar(i.par);
}


// first parameter is param_name
// second parameter is param_value
void winix_postvar_value_is(Info & i)
{
	if( i.params.size() == 2 )
		i.res = (cur->request->PostVar(i.params[0].str) == i.params[1].str);
}


void winix_postvar_value_is_not(Info & i)
{
	if( i.params.size() == 2 )
		i.res = (cur->request->PostVar(i.params[0].str) != i.params[1].str);
}


void winix_subdomain(Info & i)
{
	i.out << cur->request->subdomain;
}


void winix_subdomain_is_empty(Info & i)
{
	i.res = cur->request->subdomain.empty();
}


void winix_subdomain_is_not_empty(Info & i)
{
	i.res = !cur->request->subdomain.empty();
}

void winix_subdomain_is(Info & i)
{
	i.res = cur->request->subdomain == i.par;
}









static size_t tz_index;

void winix_tz_tab(Info & i)
{
	tz_index = i.iter;

	i.res = tz_index < system->time_zones.Size();
}


void winix_tz_tab_id(Info & i)
{
	TimeZone * tz = system->time_zones.GetZoneByIndex(tz_index);

	if( tz )
		i.out << tz->id;
}


void winix_tz_tab_name(Info & i)
{
	TimeZone * tz = system->time_zones.GetZoneByIndex(tz_index);

	if( tz )
		i.out << locale.Get(tz->name);
}


void winix_tz_tab_offset_sec(Info & i)
{
	TimeZone * tz = system->time_zones.GetZoneByIndex(tz_index);

	if( tz )
		i.out << tz->offset;
}


void winix_tz_tab_offset_hour_min(Info & i)
{
	TimeZone * tz = system->time_zones.GetZoneByIndex(tz_index);

	if( tz )
	{
		time_t offset = tz->offset;

		if( offset < 0 )
		{
			i.out << '-';
			offset = -offset;
		}
		else
		{
			i.out << '+';
		}

		print_hour_min(i, offset);
	}
}



static size_t locale_id;

void winix_locale_tab(Info & i)
{
	locale_id = i.iter;
	i.res = locale_id < locale.Size();
}


void winix_locale_tab_id(Info & i)
{
	if( locale_id < locale.Size() )
		i.out << Toi(locale.GetByIndex(L"winix_locale_id", locale_id, false));
}


void winix_locale_tab_name(Info & i)
{
	if( locale_id < locale.Size() )
		i.out << locale.GetByIndex(L"locale_name", locale_id, false);
}


void winix_is_htmx_request(Info & i)
{
	i.res = cur->request->is_htmx_request;
}


void winix_frame_is(Info & i)
{
	for(std::wstring & f: cur->request->send_frames)
	{
		if( f == i.par )
		{
			i.res = true;
			break;
		}
	}
}


void lang(Info & i)
{
	if( i.params.size() > 1 )
	{
		std::wstring key;

		for(Ezc::Var & var : i.params)
		{
			key += var.str;
		}

		i.res = !key.empty();

		if( i.res )
			i.out << locale.Get(key);
	}
	else
	{
		i.res = !i.par.empty();

		if( i.res )
			i.out << locale.Get(i.par);
	}
}




} // namespace

} // namespace Winix

