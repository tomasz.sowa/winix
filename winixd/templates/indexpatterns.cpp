/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "indexpatterns.h"
#include "core/log.h"

namespace Winix
{



void IndexPatterns::SetPatterns(Patterns * ppatterns)
{
	patterns = ppatterns;
}



Ezc::Pattern * IndexPatterns::Get(const std::wstring & file, size_t lang)
{
Tab::iterator i = tab.find(file);

	if( i == tab.end() )
		return 0;

return patterns->Get(i->second.index, lang);
}



void IndexPatterns::Add(const std::wstring & file)
{
	std::pair<Tab::iterator, bool> res = tab.insert( std::make_pair(file, Template()) );

	Template & tmpl = res.first->second;

	// mark the pattern to not delete
	tmpl.to_delete = false;

	if( res.second )
		tmpl.index = patterns->Add(file);
}



void IndexPatterns::MarkAllToDelete()
{
Tab::iterator i;

	for(i=tab.begin() ; i!=tab.end() ; ++i)
		i->second.to_delete = true;
}



void IndexPatterns::DeleteMarked()
{
Tab::iterator i = tab.begin();
Tab::iterator next;

	while( i != tab.end() )
	{
		next = i;
		++next;

		if( i->second.to_delete )
		{
			patterns->Erase(i->second.index);
			tab.erase(i);
		}

		i = next;
	}
}



void IndexPatterns::Clear()
{
	tab.clear();
}




} // namespace Winix

