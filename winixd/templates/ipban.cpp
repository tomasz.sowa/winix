/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"
#include "functions/functions.h"

namespace Winix
{



namespace TemplatesFunctions
{


void ipban_is_current_ip_banned(Info & i)
{
	if( cur->session->ip_ban )
	{
		i.res = cur->session->ip_ban->IsIPBanned();
	}
}


void ipban_current_ip_expires_time(Info & i)
{
	if( cur->session->ip_ban && cur->session->ip_ban->expires != 0 )
	{
		pt::Date date = cur->session->ip_ban->expires;
		i.out << date << " UTC";
	}
}


void ipban_is_login_allowed_from_this_ip(Info & i)
{
	i.res = !functions->fun_login.CannotLoginFromCurrentIP();
}






static size_t ipban_index;


void ipban_tab(Info & i)
{
	ipban_index = i.iter;
	i.res = ipban_index < session_manager->BanListSize();
}


void ipban_tab_id(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
		i.out << ipban_index;
}


void ipban_tab_ip(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
	{
		pt::WTextStream buf = IPToStr(session_manager->GetIPBan(ipban_index).ip);
		i.out << buf;
	}
}


void ipban_tab_incorrect_login(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
		i.out << session_manager->GetIPBan(ipban_index).incorrect_login_events;
}


void ipban_tab_broken_encoded_cookie(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
		i.out << session_manager->GetIPBan(ipban_index).broken_encoded_cookie_events;
}


void ipban_tab_session_hijacking(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
		i.out << session_manager->GetIPBan(ipban_index).session_hijacking_events;
}


void ipban_tab_no_session_cookie(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
		i.out << session_manager->GetIPBan(ipban_index).no_session_cookie_events;
}


void ipban_tab_ban_level(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
	{
		IPBan & ipban = session_manager->GetIPBan(ipban_index);

		if( ipban.HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL3) )
			i.out << "3";
		else
		if( ipban.HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL2) )
			i.out << "2";
		else
		if( ipban.HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL1) )
			i.out << "1";
	}
}


void ipban_tab_has_active_flag(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
		i.res = session_manager->GetIPBan(ipban_index).HasFlag(WINIX_IPBAN_FLAG_ACTIVE);
}


void ipban_tab_expires(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
	{
		IPBan & ipban = session_manager->GetIPBan(ipban_index);

		if( ipban.expires != 0 )
		{
			time_t expires_local = system->ToLocal(ipban.expires);
			pt::Date date(expires_local);
			i.out << date;
		}
	}
}


void ipban_tab_last_used(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
	{
		IPBan & ipban = session_manager->GetIPBan(ipban_index);

		if( ipban.last_used != 0 )
		{
			time_t last_used_local = system->ToLocal(ipban.last_used);
			pt::Date date(last_used_local);
			i.out << date;
		}
	}
}


void ipban_tab_is_logging_allowed(Info & i)
{
	if( ipban_index < session_manager->BanListSize() )
	{
		IPBan & ipban = session_manager->GetIPBan(ipban_index);
		i.res = !functions->fun_login.CannotLoginFrom(ipban);
	}
}





} // namespace TemplatesFunctions

} // namespace Winix




