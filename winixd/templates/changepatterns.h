/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_templates_changepatterns
#define headerfile_winix_templates_changepatterns

#include <map>
#include "patterns.h"
#include "core/winixbase.h"


namespace Winix
{


class ChangePatterns : public WinixBase
{
public:


	void SetPatterns(Patterns * ppatterns);


	/*
		adding a new pattern
		if such a pattern exists the method only unmarks the pattern from deleting
	*/
	void Add(long mount_dir_id, const std::wstring & old_pattern_name, const std::wstring & new_pattern_name);


	/*
		returning a pattern (if exists)
		can return a null pointer
	*/
	Ezc::Pattern * Get(long mount_dir_id, const std::wstring & old_pattern_name, size_t locale_index);


	/*
		marking all patterns as ready to delete
	*/
	void MarkAllToDelete();


	/*
		delete marked patterns
		if you have called Add() then such a pattern is not deleted
	*/
	void DeleteMarked();


	/*
		deleting all patterns
	*/
	void Clear();


private:

	Patterns * patterns;

	struct Template
	{
		bool to_delete;
		size_t index;		// pattern index in 'patterns' object
	};

	typedef std::map<std::wstring, Template> Value;
	typedef std::map<long, Value> PatTab;
	PatTab pat_tab;

};



} // namespace Winix

#endif
