/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2016, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <vector>
#include <string>
#include "templates.h"
#include "core/misc.h"

namespace Winix
{


namespace TemplatesFunctions
{


void template_index(Info & i)
{
	i.out << config->templates_index;
}


static std::vector<std::wstring> temp_tab;
static size_t temp_req_id = 0;
static size_t temp_def;
static size_t temp_index;



void template_set_def()
{
	temp_def = (size_t)-1; // none
	const std::wstring * html_file;

	if( cur->request->is_item )
		html_file = &cur->request->item.html_template;
	else
		html_file = &cur->request->dir_tab.back()->html_template;


	if( html_file->empty() )
	{
		// the first default item should be selected (that first in html source with value="")
		return;
	}


	for(size_t i=0 ; i<temp_tab.size() ; ++i)
	{
		if( temp_tab[i] == *html_file )
		{
			temp_def = i;
			break;
		}
	}
}

void template_init()
{
	// the first item in the html template is an empty string
	// added in the html template
	temp_tab.clear();
	temp_tab.push_back(config->templates_index);		// index: 0 (indices are used in template.cpp in functions) !! IMPROVE ME it should be done better
	temp_tab.push_back(config->templates_index_generic);// index: 1
	temp_tab.push_back(config->templates_index_raw);	// index: 2

	Mount::ParamRow & par = system->mounts.pmount->param[system->mounts.MountParHtmlTemplate()];

	if( par.defined )
	{
		size_t len = par.arg.size();

		for(size_t i=0 ; i<len ; ++i)
			temp_tab.push_back(par.arg[i]);
	}

	template_set_def();
}


void template_tab(Info & i)
{
	if( cur->request->id != temp_req_id )
	{
		temp_req_id = cur->request->id;
		template_init();
	}

	temp_index = i.iter;
	i.res = temp_index < temp_tab.size();
}


void template_tab_index(Info & i)
{
	i.out << temp_index;
}


void template_tab_isdefault(Info & i)
{
	if( temp_index < temp_tab.size() )
		i.res = (temp_index == temp_def);
}



void template_tab_file_name(Info & i)
{
	if( temp_index < temp_tab.size() )
		i.out << temp_tab[temp_index];
}





} // namespace

} // namespace Winix

