/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templates.h"
#include "core/request.h"
#include "core/misc.h"

namespace Winix
{


namespace TemplatesFunctions
{


static std::wstring doc_proto_str;



void doc_title(Info & i)
{
bool was_dir = false;

	for(size_t a=0 ; a<cur->request->dir_tab.size() ; ++a)
	{
		if( !cur->request->dir_tab[a]->subject.empty() )
		{
			if( was_dir )
				i.out << config->title_separator;

			i.out << cur->request->dir_tab[a]->subject;
			was_dir = true;
		}
	}

	if( cur->request->is_item &&
		!cur->request->item.subject.empty() &&
		cur->request->item.subject != cur->request->dir_tab.back()->subject
		)
	{
		if( was_dir )
			i.out << config->title_separator;

		i.out << cur->request->item.subject;
		was_dir = true;
	}
}




void doc_proto(Info & i)
{
	system->PutUrlProto(config->use_ssl, doc_proto_str);
	i.out << doc_proto_str;
}


void doc_proto_static(Info & i)
{
	system->PutUrlProto(config->use_ssl_static, doc_proto_str);
	i.out << doc_proto_str;
}


void doc_proto_common(Info & i)
{
	system->PutUrlProto(config->use_ssl_common, doc_proto_str);
	i.out << doc_proto_str;
}


void doc_base_url(Info & i)
{
	doc_proto(i);

	if( !cur->request->subdomain.empty() )
		i.out << cur->request->subdomain << '.';

	i.out << config->base_url;
}


void doc_base_url_static(Info & i)
{
	doc_proto_static(i);
	i.out << config->base_url_static;
}


void doc_base_url_common(Info & i)
{
	doc_proto_common(i);
	i.out << config->base_url_common;
}



void doc_current_url(Info & i)
{
	i.out << cur->request->env_request_uri;
}







static size_t doc_css_index = 0;


/*
	cur->session->last_css is set by some javascript wysiwyg editors (ckeditor, tinymce)
*/
void doc_css_tab(Info & i)
{
	doc_css_index = i.iter;
	i.res = doc_css_index < cur->session->last_css.size();
}


void doc_css_tab_file(Info & i)
{
	if( doc_css_index < cur->session->last_css.size() )
		i.out << cur->session->last_css[doc_css_index];
}


void doc_css_tab_file_is_global(Info & i)
{
	// !! z konfiga wziasc przedrostki

	if( doc_css_index < cur->session->last_css.size() )
		i.res = pt::is_substr(L"http://",  cur->session->last_css[doc_css_index].c_str()) ||
				pt::is_substr(L"https://", cur->session->last_css[doc_css_index].c_str());
}


void doc_css_tab_has_next(Info & i)
{
	i.res = (doc_css_index + 1 < cur->session->last_css.size());
}


void doc_css_is_empty(Info & i)
{
	i.res = cur->session->last_css.empty();
}


void doc_css_is_one(Info & i)
{
	i.res = cur->session->last_css.size() == 1;
}


void doc_css_more_than_one(Info & i)
{
	i.res = cur->session->last_css.size() > 1;
}







} // namespace TemplatesFunctions

} // namespace Winix


