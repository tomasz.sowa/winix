/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2023, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "acceptbaseparser.h"
#include "convert/convert.h"
#include <algorithm>


namespace Winix
{


AcceptBaseParser::AcceptBaseParser()
{
}

AcceptBaseParser::~AcceptBaseParser()
{
}



bool AcceptBaseParser::is_delimiter(wchar_t c, wchar_t delimiter)
{
	return delimiter != 0 && delimiter == c;
}


bool AcceptBaseParser::is_delimiter(wchar_t c, wchar_t delimiter1, wchar_t delimiter2, wchar_t delimiter3)
{
	return is_delimiter(c, delimiter1) || is_delimiter(c, delimiter2) || is_delimiter(c, delimiter3);
}


void AcceptBaseParser::read_token(std::wstring & token, size_t max_len, wchar_t delimiter1, wchar_t delimiter2, wchar_t delimiter3)
{
	token.clear();
	text = pt::skip_white(text, false, false);

	while( *text!=0 && !is_delimiter(*text, delimiter1, delimiter2, delimiter3) )
	{
		if( token.size() < max_len )
		{
			token += *text;
		}

		++text;
	}

	pt::trim_white(token);
}


void AcceptBaseParser::read_name()
{
	read_token(name, MAX_NAME_LENGTH, ',', ';');

	if( !name.empty() )
	{
		parsed_name(name);
	}
}


void AcceptBaseParser::read_parameter()
{
	param_value.clear();
	read_token(param, MAX_PARAM_LENGTH, '=', ',', ';');

	if( *text == '=' )
	{
		++text;
		read_token(param_value, MAX_PARAM_VALUE_LENGTH, ';', ',');
	}

	if( !name.empty() && !param.empty() )
	{
		parsed_param(param, param_value);
	}

	if( param == L"q" && !param_value.empty() )
	{
		q = pt::to_double(param_value);
	}
}


void AcceptBaseParser::read_loop(std::vector<HeaderValue> * header_values, size_t max_len)
{
	while( *text != 0 )
	{
		q = 1.0;
		read_name();

		while( *text == ';' )
		{
			++text;
			read_parameter();
		}

		if( !name.empty() && q > 0.0 )
		{
			if( q > 1.0 )
				q = 1.0;

			parsed_name_q(name, q);

			if( header_values && (max_len == 0 || header_values->size() < max_len) )
			{
				header_values->resize(header_values->size() + 1);
				header_values->back().value = name;
				header_values->back().weight = q;
			}
		}

		if( *text != 0 )
			++text;
	}
}


void AcceptBaseParser::read(const wchar_t * str, std::vector<HeaderValue> * header_values, size_t max_len)
{
	text = str;
	init();

	read_loop(header_values, max_len);

	name.clear();
	param.clear();
	param_value.clear();
}


void AcceptBaseParser::parse(const wchar_t * str)
{
	read(str, nullptr, 0);
}


void AcceptBaseParser::parse(const std::wstring & str)
{
	parse(str.c_str());
}


void AcceptBaseParser::parse(const wchar_t * str, std::vector<HeaderValue> & header_values, size_t max_len, bool clear_header_values)
{
	if( clear_header_values )
		header_values.clear();

	AcceptBaseParser::read(str, &header_values, max_len);

	std::sort(header_values.begin(), header_values.end(), [](HeaderValue & h1, HeaderValue & h2) -> bool {
		return h1.weight > h2.weight;
	});
}


void AcceptBaseParser::parse(const std::wstring & str, std::vector<HeaderValue> & header_values, size_t max_len, bool clear_header_values)
{
	return parse(str.c_str(), header_values, max_len, clear_header_values);
}



} // namespace Winix

