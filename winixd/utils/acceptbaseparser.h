/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_utils_acceptbaseparser
#define headerfile_winix_utils_acceptbaseparser

#include <string>
#include "core/header.h"


namespace Winix
{

class AcceptBaseParser
{
public:

	static size_t constexpr MAX_NAME_LENGTH = 64;
	static size_t constexpr MAX_PARAM_LENGTH = 32;
	static size_t constexpr MAX_PARAM_VALUE_LENGTH = 32;

	AcceptBaseParser();
	virtual ~AcceptBaseParser();

	/*
	 *
	 *
	 *
	 *
	 */
	// sample:
	// object.parse(L"   text/html  , text/* ; q = 45, application  /   xhtml+xml  ; q  = 0.4  ; limit = 1 ,  application/xml ;  charset = UTF-8  ;  q  =  0.9 , */* ;  q =  0.8  ");
	void parse(const wchar_t * str);
	void parse(const std::wstring & str);

	void parse(const wchar_t * str, std::vector<HeaderValue> & header_values, size_t max_len, bool clear_header_values = true);
	void parse(const std::wstring & str, std::vector<HeaderValue> & header_values, size_t max_len, bool clear_header_values = true);


private:

	bool is_delimiter(wchar_t c, wchar_t delimiter);
	bool is_delimiter(wchar_t c, wchar_t delimiter1, wchar_t delimiter2, wchar_t delimiter3);
	void read_token(std::wstring & token, size_t max_len, wchar_t delimiter1, wchar_t delimiter2 = 0, wchar_t delimiter3 = 0);
	void read_name();
	void read_parameter();
	void read_loop(std::vector<HeaderValue> * header_values, size_t max_len);
	void read(const wchar_t * str, std::vector<HeaderValue> * header_values, size_t max_len);

	/*
	 * called one at the beginning of parsing
	 */
	virtual void init() {} ;

	/*
	 * called when a name was parsed and was not empty
	 *
	 * sample: for such string: "text/html ; q = 0.5 ; charset = UTF-8"
	 * parsed_name will be called with "text/html" argument
	 */
	virtual void parsed_name(const std::wstring & name) {};

	/*
	 * called when a param was parsed, param_value can be empty
	 * parsed_param() is called after parsed_name()
	 *
	 * sample: for such string: "text/html ; q = 0.5 ; charset = UTF-8"
	 * parsed_name will be called twice, first with ("q", "0.5) arguments
	 * and later with ("charset", "UTF-8")
	 */
	virtual void parsed_param(const std::wstring & param, const std::wstring & param_value) {};

	/*
	 * called always when a name was parsed and was not empty
	 * "q" parameter will be in the range [0.0 - 1.0]
	 *
	 * if there was no "q" parameter then "q" will be equal 1.0
	 */
	virtual void parsed_name_q(const std::wstring & name, double q) {};
	

	const wchar_t * text;
	std::wstring name;
	std::wstring param;
	std::wstring param_value;
	double q;


};



} // namespace Winix



#endif
