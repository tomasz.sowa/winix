/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_acceptencodingparser
#define headerfile_winix_core_acceptencodingparser

#include "acceptbaseparser.h"
#include "core/log.h"


namespace Winix
{



class AcceptEncodingParser : public AcceptBaseParser
{
public:

	bool AcceptDeflate()
	{
		return accept_deflate;
	}


	bool AcceptGzip()
	{
		return accept_gzip;
	}


	void ParseAndLog(const wchar_t * str, Log & log)
	{
		parse(str);

		if( accept_deflate || accept_gzip )
		{
			log << log3 << "AEP: ";

			if( accept_deflate )
			{
				log << "accept deflate";

				if( accept_gzip )
					log << ", ";
			}

			if( accept_gzip )
				log << "accept gzip";

			log << logend;
		}
	}


	void ParseAndLog(const std::wstring & str, Log & log)
	{
		ParseAndLog(str.c_str(), log);
	}


private:

	void init()
	{
		accept_deflate = false;
		accept_gzip    = false;
	}


	void parsed_name_q(const std::wstring & param, double q)
	{
		if( param == L"deflate" && q!=0.0 )
		{
			accept_deflate = true;
		}

		if( param == L"gzip" && q!=0.0 )
		{
			accept_gzip = true;
		}
	}

	bool accept_deflate;
	bool accept_gzip;
};



} // namespace Winix


#endif
