/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_winixd_utils_movehelper
#define headerfile_winix_winixd_utils_movehelper

#include "core/winixmodeldeprecated.h"
#include "core/system.h"
#include "functions/functions.h"



namespace Winix
{


class MoveHelper : public WinixModelDeprecated
{
public:

	MoveHelper(System * system, Functions * functions);

	// moving a directory
	// new_url can be empty (in such a case the old one is preserved)
	// src_dir will be accordingly updated
	bool MoveDir(Item & src_dir, long dst_dir_id, const std::wstring & new_url, bool check_access = true);
	bool MoveDir(Item & src_dir, const std::wstring & dst_path, bool check_access = true);

	// moving only the content of src_dir
	// dst_path should point to an existing directory
	// src_dir will be accordingly updated
	void MoveDirContent(Item & src_dir, long dst_dir_id, bool check_access = true);
	void MoveDirContent(Item & src_dir, const std::wstring & dst_dir, bool check_access = true);

	// moving a file
	// new_url can be empty (the old one is used then)
	// src_file will be accordingly updated
	bool MoveFileOrSymlink(Item & src_file, long dst_dir_id, const std::wstring & new_url, bool check_access = true);
	bool MoveFileOrSymlink(Item & src_file, const std::wstring & dst_path, bool check_access = true);


private:

	System * system;
	Functions * functions;

	// directories parsed by ParseDir()
	std::vector<Item*> out_dir_tab;		// destination directories
	Item out_item;						// destination file/symlink (if out_has_file is true)
	bool out_has_file;					// if true then out_item is set
	std::wstring out_filename;			// the last part in an input path (not an existing directory, file or symlink)
										// can be empty

	// temporaries
	std::wstring out_path;
	std::wstring old_url;

	std::wstring old_static_path;
	std::wstring old_static_thumb_path;
	std::wstring new_static_path;
	std::wstring new_static_thumb_path;

	// for files/symlinks in a directory
	std::vector<Item> files_item_tab;
	//DbItemQuery files_iq;

	// for moving content of a directory (all dirs/files/symlinks)
	//DbItemQuery content_dir_iq;
	std::vector<Item> item_tab;


	bool CanRemoveRenameChild(const Item & child);
	bool ParseDirCheckLastName();
	bool ParseDir(const std::wstring & dst_path, bool check_access);
	bool MoveStaticFile(const std::wstring & from, const std::wstring & to);
	void MoveStaticFile(Item & item);
	void MoveFilesPrepareTreeGo(const Item & src_dir);
	void MoveFilesPrepareTree(const Item & src_dir);
	void MoveFilesTree(const Item & dir);
	bool MoveDir(Item & src_dir, std::vector<Item*> & dst_dir_tab, const std::wstring & dst_name);
	bool MoveFileOrSymlink(Item & src_file, std::vector<Item*> & dst_dir_tab, const std::wstring & new_url);
	void Clear();
	bool MoveDir2(Item & src_dir, long dst_dir_id, const std::wstring & new_url, bool check_access);
	bool MoveDir2(Item & src_dir, const std::wstring & dst_path, bool check_access);
	bool MoveFileOrSymlink2(Item & src_file, long dst_dir_id, const std::wstring & new_url, bool check_access);
	bool MoveFileOrSymlink2(Item & src_file, const std::wstring & dst_path, bool check_access);
	void MoveDirContent2(Item & src_dir, long dst_dir_id, bool check_access);
	void MoveAllFilesFromDir(Item & src_dir, std::vector<Item*> & dst_dir_tab, bool check_access);
	void MoveDirContent2(Item & src_dir, const std::wstring & dst_dir, bool check_access);


};



} // namespace Winix

#endif
