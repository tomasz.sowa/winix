/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_image
#define headerfile_winix_core_image

#include <string>
#include <list>
#include "basethread.h"
#include "core/config.h"
#include "models/item.h"


namespace Winix
{



class System;


// aspect modes:
// Width given, height automagically selected to preserve aspect ratio.
#define WINIX_IMAGE_MODE_1	1

// Height given, width automagically selected to preserve aspect ratio.
#define WINIX_IMAGE_MODE_2	2

// Maximum values of height and width given, aspect ratio preserved.
#define WINIX_IMAGE_MODE_3	3

// Minimum values of width and height given, aspect ratio preserved.
#define WINIX_IMAGE_MODE_4	4

// Width and height emphatically given, original aspect ratio ignored.
#define WINIX_IMAGE_MODE_5	5

// Change as per widthxheight but only if an image dimension exceeds a specified dimension.
#define WINIX_IMAGE_MODE_6	6

// Change dimensions only if both image dimensions are less than specified dimensions.
#define WINIX_IMAGE_MODE_7	7




// resizing
#define WINIX_IMAGE_TYPE_RESIZE			1

// generating a thumbnail
#define WINIX_IMAGE_TYPE_CREATE_THUMB	2

// cropping an image
#define WINIX_IMAGE_TYPE_CROP			3

// cropping an thumbnail
#define WINIX_IMAGE_TYPE_CROP_THUMB		4

// creating by cropping a new thumbnail (from an original image)
#define WINIX_IMAGE_TYPE_CREATE_CROP_NEW_THUMB	5


/*

*/
class Image : public BaseThread
{
public:

	struct Scale
	{
		size_t cx;
		size_t cy;
		int aspect_mode;
		int quality;

		Scale()
		{
			cx = cy = 1;
			aspect_mode = 2;
			quality = 100;
		};
	};


	void SetConfig(Config * pconfig);
	void SetSystem(System * psystem);


	// returning scale info for a directory
	Scale GetImageScale(long dir_id);

	// returning scale info (for thumbnails) for a directory
	Scale GetThumbScale(long dir_id);



	void Resize(long file_id, size_t cx, size_t cy, int aspect_mode, int quality);

	// creating a new thumbnail from an original image
	void CreateThumb(long file_id, size_t thumb_cx, size_t thumb_cy, int aspect_mode, int quality);

	// cropping an image (the thumbnail is not changed)
	void Crop(long file_id, size_t xoffset, size_t yoffset, size_t cx, size_t cy, int quality);

	// cropping an existing thumbnail
	void CropThumb(long file_id, size_t xoffset, size_t yoffset, size_t cx, size_t cy, int quality);

	// creating and cropping a new thumbnail (from an original image)
	void CropNewThumb(long file_id, size_t xoffset, size_t yoffset, size_t cx, size_t cy,
					  size_t thumb_cx, size_t thumb_cy, int aspect_mode, int quality);


private:

	Config * config;
	System * system;

	struct ImageItem
	{
		int type;		// WINIX_IMAGE_TYPE_*
		long file_id;
		size_t cx;
		size_t cy;
		size_t xoffset;	// xoffset and yoffset are used when cropping
		size_t yoffset;
		size_t thumb_cx;
		size_t thumb_cy;
		int aspect_mode;
		int quality;
	};


	template<typename int_type>
	void SetMinMax(int_type & var, int var_min, int var_max)
	{
		if( static_cast<int>(var) < var_min )
			var = var_min;

		if( static_cast<int>(var) > var_max )
			var = var_max;
	}


	// queue of thumbnails to create
	typedef std::list<ImageItem> ImageTab;
	ImageTab image_tab;
	ImageItem item_temp;

	// only for second thread
	ImageItem item_work;
	std::wstring src_path, dst_path;
	pt::TextStream command;
	pt::WTextStream stream_tmp_path;
	std::string input_file_name;
	std::string tmp_file_name;
	Item file_work;

	virtual bool SignalReceived();
	virtual void Do();
	bool CreateCommand();
	bool CreateInputFileName();
	void CreateTmpFileName();
	void SaveImage();
	void CreateImage();
	void SelectAspect(size_t cx, size_t cy);
	void EscapePath(const std::string & path, pt::TextStream & out, bool clear_stream = true);
	void CheckParam(ImageItem & item);
	void ImageSavedCorrectly();

};


} // namespace Winix


#endif
