/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_dircontainer
#define headerfile_winix_core_dircontainer

#include <list>
#include <map>
#include "winixbase.h"
#include "models/item.h"


namespace Winix
{



class DirContainer : public WinixBase
{

public:
	typedef std::list<Item>  Table;
	typedef Table::iterator  Iterator;
	typedef Table::const_iterator  ConstIterator;
	typedef Table::size_type SizeType;

	typedef std::map<long, Iterator>      TableId;
	typedef std::multimap<long, Iterator> TableParent;
	typedef TableParent::iterator         ParentIterator;
	typedef TableParent::size_type        ParentSizeType;


	DirContainer();

	Iterator GetRoot();
	Iterator GetEtc();
	Iterator GetVar();

	ConstIterator Begin() const;
	ConstIterator End() const;
	SizeType Size();
	bool     Empty();
	Iterator PushBack(const Item & item);
	bool     ChangeParent(long dir_id, long new_parent_id);
	void     Clear();

	Iterator FindId(long id);
	ConstIterator FindId(long id) const;

	bool DelById(long id);

	ParentIterator ParentBegin(); // IMPROVE ME: may it should be renamed to ChildBegin() similarly as FindFirstChild() ?
	ParentIterator ParentEnd();
	ParentSizeType ParentSize();
	bool           ParentEmpty();
	ParentIterator FindFirstChild(long parent);
	ParentIterator NextChild(ParentIterator pi);

	bool IsNameOfSpecialFolder(const std::wstring & name);
	void FindSpecialFolders();

private:

	void CheckSpecialFolder(const Item & item, Iterator iter);

	// main table with dirs
	Table table;

	// true if there is a root dir in the table
	bool is_root;

	// root 
	Iterator root_iter;

	// true if there is a etc dir in the table
	bool is_etc;

	// etc
	Iterator etc_iter;

	// true if there is a var dir in the table
	bool is_var;

	// var
	Iterator var_iter;

	// indexes
	TableId table_id;
	TableParent table_parent;

	// names of folders
	static std::wstring dir_etc, dir_var;
};



} // namespace Winix


#endif
