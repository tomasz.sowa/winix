/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_core_timezones
#define headerfile_winix_core_timezones

#include <string>
#include <vector>
#include "timezone.h"
#include "space/spaceparser.h"
#include "winixbase.h"


namespace Winix
{




class TimeZones : public WinixBase
{
public:

	TimeZones();

	// maximum allowed time zone's identifier
	void SetTimeZoneMaxId(size_t max_id);

	// reading time zone from a file
	bool ReadTimeZones(const wchar_t * path);
	bool ReadTimeZones(const std::wstring & path);

	// returning true if there is a time zone with the zone_id identifier
	bool HasZone(size_t zone_id);

	// returning a time zone by time zone's identifier
	// can return a null pointer if there is no such a zone
	TimeZone * GetZone(size_t zone_id);

	// returning the number of all time zones
	size_t Size() const;

	// returning a time zone by an internal index
	// usuful for iterating through all zones
	// this index is in a range of <0, Size()-1>
	// can return a null pointer if the index is out of range
	TimeZone * GetZoneByIndex(size_t zone_index);

	// return true if there are not any time zones
	bool Empty() const;

	// clears all time zones
	// this does not affect SetTimeZoneMaxId()
	// so the size of zone_indices is not changed but all indices are invalidated
	void Clear();


private:

	// indices to 'tab'
	// with this we have O(1) time to find a time zone in 'tab'
	// everywhere we use zone_id we refer to this table
	// SetTimeZoneMaxId(size_t max_id) sets size of this table to max_id+1
	std::vector<size_t> zone_indices;

	// time zones
	// everywhere we use zone_index we refer to this table
	std::vector<TimeZone> zone_tab;

	pt::SpaceParser parser;
	pt::Space temp_space;
	TimeZone temp_zone;

	void ParseZones();

};



} // namespace Winix



#endif
