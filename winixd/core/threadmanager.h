/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_threadmanager
#define headerfile_winix_core_threadmanager

#include <string>
#include <list>
#include "basethread.h"
#include "synchro.h"
#include "textstream/textstream.h"
#include "models/winixmodelconnector.h"


namespace Winix
{


class ThreadManager : public WinixModelDeprecated
{
public:

	ThreadManager();

	// initializing
	void Init();

	// adding a new thread to the queue
	// the thread will be running only if we call StartAll() before
	// otherwise the thread will be waiting for StartAll()
	bool Add(BaseThread * pbase, const wchar_t * thread_name);
	bool Add(BaseThread & pbase, const wchar_t * thread_name);
	bool Add(BaseThread * pbase, const std::wstring & thread_name);
	bool Add(BaseThread & pbase, const std::wstring & thread_name);

	// starting all threads
	void StartAll();

	// sending a stop signal to all threads
	// and waiting until they finish
	void StopAll();

private:

	struct ThreadItemData
	{
		WinixModelConnector model_connector;
		morm::JSONConnector json_connector;
		morm::PostgreSQLConnector postgresql_connector;

		pt::WTextStream log_buffer;
	};

	struct ThreadItem
	{
		BaseThread * object;
		std::wstring name;

		ThreadItemData * thread_item_data;
	};


	typedef std::list<ThreadItem> ThreadTab;
	ThreadTab thread_tab;
	bool were_started;

	void Start(int id, ThreadItem * item);

};


} // namespace Winix


#endif

