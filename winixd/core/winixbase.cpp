/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "winixbase.h"


namespace Winix
{


WinixBase::WinixBase()
{
	config = nullptr;
	synchro = nullptr;
}


WinixBase::~WinixBase()
{
}


void WinixBase::set_config(Config * config)
{
	this->config = config;
}


void WinixBase::set_synchro(Synchro * synchro)
{
	this->synchro = synchro;
}


void WinixBase::set_log_buffer(pt::WTextStream * log_buffer)
{
	log.set_log_buffer(log_buffer);
}


void WinixBase::set_file_log(pt::FileLog * file_log)
{
	log.set_file_log(file_log);
}


Log * WinixBase::get_logger()
{
	return &log;
}


void WinixBase::set_dependency(WinixBase * winix_base)
{
	config = winix_base->config;
	synchro = winix_base->synchro;
	log.SetDependency(&winix_base->log);
}



void WinixBase::save_log()
{
	log << logsave;
}



}


