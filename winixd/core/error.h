/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_error
#define headerfile_winix_core_error


namespace Winix
{



#define WINIX_ERR_OK							0
//#define WINIX_ERR_INCORRECT_PATH				1

//#define WINIX_ERR_NO_POSTVAR					2
#define WINIX_ERR_INCORRECT_DIR					3
#define WINIX_ERR_CANT_CHANGE_USER				4
#define WINIX_ERR_CANT_CHANGE_GROUP				5
#define WINIX_ERR_CANT_CHANGE_PRIVILEGES		6
#define WINIX_ERR_PERMISSION_DENIED				7
#define WINIX_ERR_NO_ROOT_DIR					8
#define WINIX_ERR_NO_FUNCTION					9


#define WINIX_ERR_NO_ITEM						10
#define WINIX_ERR_UNKNOWN_PARAM					11

#define WINIX_ERR_MOUNT_UNKNOWN					12
#define WINIX_ERR_UNKNOWN_FILESYSTEM			13
#define WINIX_ERR_NO_MOUNTPOINT					14
//#define WINIX_ERR_MOUNT_NO_PARAM				15

#define WINIX_ERR_NO_THREAD						16
#define WINIX_ERR_EMPTY							17
#define WINIX_ERR_SPAM							18
#define WINIX_ERR_INCORRECT_REBUS				19



#define WINIX_ERR_NO_BOUNDARY					20
#define WINIX_ERR_BROKEN_INPUT					21
#define WINIX_ERR_INPUT_TOO_LARGE				22
#define WINIX_ERR_CANT_CREATE_FILE				23

#define WINIX_ERR_NO_TICKET						24

//#define WINIX_ERR_PASSWORDS_DIFFERENT			25
//#define WINIX_ERR_PASSWORD_TOO_SHORT			26
//#define WINIX_ERR_USER_EXISTS					27
//#define WINIX_ERR_LOGIN_EMPTY					28
#define WINIX_DIFFERENT_MOUNT_POINTS			29


#define WINIX_ERR_DB_FATAL_ERROR_DURING_CONNECTING	100
#define WINIX_ERR_DB_INCORRECT_QUERY			101
#define WINIX_ERR_DB_INCORRENT_RESULT_STATUS	102
#define WINIX_ERR_DB_NO_COLUMN					103
#define WINIX_ERR_DB_INCORRECT_LOGIN			104
#define WINIX_ERR_DB_MORE_THAN_ONE_LOGIN		105
#define WINIX_ERR_DB_ERR_CURRVAL				106

#define WINIX_ERR_FILE_EXPECTED					107
#define WINIX_ERR_DIR_EXPECTED					108

//#define WINIX_ERR_UNKNOWN						1000
#define WINIX_NOTHING_TO_DO						109
#define WINIX_ERR_INCORRECT_URI					110

#define WINIX_ERR_INTERNAL_ERROR				2000


typedef int Error;



} // namespace Winix


#endif
