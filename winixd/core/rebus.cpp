/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <cstdlib>
#include <limits.h>
#include "log.h"
#include "rebus.h"
#include "misc.h"
#include "cur.h"


namespace Winix
{




void Rebus::SetCur(Cur * pcur)
{
	cur = pcur;
}


bool Rebus::InitPair(int a, int b, Item & item)
{
wchar_t buffer[100];
size_t buffer_len = sizeof(buffer) / sizeof(wchar_t);
bool add = false;

	if( a+b <= 15 )
	{
		swprintf(buffer, buffer_len, L"%d+%d", a, b);
		item.question = buffer;
		item.answer = a+b;
		add = true;
	}

	if( a-b >= 0 )
	{
		swprintf(buffer, buffer_len, L"%d-%d", a, b);
		item.question = buffer;
		item.answer = a-b;
		add = true;
	}

return add;
}



void Rebus::Init()
{
int a, b;
Item item;

	item.key = 0;

	for(a=1 ; a<10 ; ++a)
	{
		for(b=1 ; b<10 ; ++b)
		{
			if( InitPair(a, b, item) )
			{
				table.push_back(item);
				item.key += 1;
			}
		}
	}
}


Rebus::Item * Rebus::Rand()
{
size_t len = table.size();

	if( len == 0 )
		// empty table, call Init() first
		return 0;

	size_t i = size_t( ((double)rand()*(double)len) / (double(RAND_MAX)+1.0) );

	if( i >= table.size() )
	{
		log << log1 << "Rebus: Rand() index too big" << logend;
		// oops, it should not be greater then table.size()
		return &table[0];
	}

return &table[0] + i;
}



bool Rebus::IsAnswerOk(Rebus::Item * item, const std::wstring & answer)
{
	if( item == 0 )
		return false;

	wchar_t * end;
	const wchar_t * a = answer.c_str();
	a = SkipWhite(a);

	int value = (int)wcstol(a, &end, 10);

	if( a == end )
		// nothing has been read
		return false;

	log << log2 << "Rebus: your answer is: " << value << logend;
	end = (wchar_t*)SkipWhite(end);

	if( *end != 0 )
	{
		// something is at the end
		return false;
	}

	bool result = (item->answer == value);

	if( result )
		log << log3 << "Rebus: the answer is correct" << logend;

return result;
}



bool Rebus::CheckRebus()
{
	if( !cur->session )
		return false;

	if( cur->session->puser )
		// logged users don't have to use the rebus
		return true;

	if( cur->session->rebus_checked )
		return true;

	cur->session->rebus_checked = true;

	if( !cur->session->rebus_item )
	{
		log << log1 << "Rebus: rebus not set" << logend;
		return false;
	}

	if( IsAnswerOk(cur->session->rebus_item, cur->request->PostVar(L"rebus")) )
		return true;

	log << log1 << "Rebus: rebus has an incorrect answer, expected: "
		<< cur->session->rebus_item->answer << logend;

	// don't add cur->session->spam_score when the rebus has incorrect answer
	// a user could have made a mistake

return false;
}





} // namespace Winix

