/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_core_ipban
#define headerfile_winix_core_ipban

#include <ctime>


namespace Winix
{




// telling if the IPBan record is active
// we have two records: active and non active
// non active records is something like a history
// it is used to remember the last ban level
// so based on this in the future a next greater ban can be calculated
#define WINIX_IPBAN_FLAG_ACTIVE					1

// current ban level
// (if one of these flag is set and the record is active then it means the IP is banned at the moment)
// level 1: banned for short time
// level 2: can be set after level 1 has expired and the attacker still have not given up
//          banned for some longer time in level 1
// level 3: can be set after level 2
//          banned for much more time
#define WINIX_IPBAN_FLAG_BAN_LEVEL1				2
#define WINIX_IPBAN_FLAG_BAN_LEVEL2				4
#define WINIX_IPBAN_FLAG_BAN_LEVEL3				8





/*
	struct defining some restrictions to an IP address
*/
struct IPBan
{
	// at the moment only IPv4 are supported
	int ip;

	// one or more flags from WINIX_IPBAN_FLAG_*
	int flags;

	// when this record was last used
	time_t last_used;

	// when the restrictions (ban) should be removed
	// valid only if some of WINIX_IPBAN_FLAG_BAN_LEVELX flags are set
	// actually we do not remove the record but unsets WINIX_IPBAN_FLAG_ACTIVE flag
	// so in the future we can check whether we need to change
	// the ban level to a greater value
	time_t expires;


	// how many incorrect login attempts there are
	unsigned short int incorrect_login_events;

	// how many incorrect encoded cookie were sent
	// only used if config.session_cookie_encode is true and session_keys_file is defined
	unsigned short int broken_encoded_cookie_events;

	// how many incorrect session identifiers were sent
	unsigned short int session_hijacking_events;

	// client didn't send a session cookie
	// it can be a bot or just someone wants to DOS the server
	// (a new session will be create)
	unsigned short int no_session_cookie_events;


	bool HasFlag(int flag) const
	{
		return (flags & flag) != 0;
	}


	void SetFlag(int flag)
	{
		flags = flags | flag;
	}


	void ClearFlag(int flag)
	{
		flags = flags & (~flag);
	}


	bool IsIPBanned() const
	{
		if( !HasFlag(WINIX_IPBAN_FLAG_ACTIVE) )
			return false;

		return  HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL1) ||
				HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL2) ||
				HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL3);
	}


	void IncrementBanLevel(time_t level1_expires, time_t level2_expires, time_t level3_expires)
	{
		if( HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL3) )
		{
			expires = level3_expires;
			return;
		}
		else
		if( HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL2) )
		{
			SetFlag(WINIX_IPBAN_FLAG_BAN_LEVEL3);
			expires = level3_expires;
			return;
		}
		else
		if( HasFlag(WINIX_IPBAN_FLAG_BAN_LEVEL1) )
		{
			SetFlag(WINIX_IPBAN_FLAG_BAN_LEVEL2);
			expires = level2_expires;
			return;
		}
		else
		{
			SetFlag(WINIX_IPBAN_FLAG_BAN_LEVEL1);
			expires = level1_expires;
		}
	}


	IPBan()
	{
		Clear();
	}


	void Clear()
	{
		ip 			= 0;
		flags 		= 0;
		last_used	= 0;
		expires		= 0;
		incorrect_login_events       = 0;
		broken_encoded_cookie_events = 0;
		session_hijacking_events     = 0;
		no_session_cookie_events     = 0;
	}


	void ResetEventsCounters()
	{
		ClearFlag(WINIX_IPBAN_FLAG_ACTIVE);
		incorrect_login_events       = 0;
		broken_encoded_cookie_events = 0;
		session_hijacking_events     = 0;
		no_session_cookie_events     = 0;
		expires = 0;
	}

};



} // namespace Winix


#endif
