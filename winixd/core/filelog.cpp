/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "filelog.h"
#include <ctime>
#include <string.h>
#include "utf8/utf8.h"
#include "timezones.h"
#include "plugin.h"


namespace Winix
{



FileLog::FileLog()
{
	time_zones = nullptr;
	synchro = nullptr;
	plugin = nullptr;
	log_time_zone_id = 0;
}


FileLog::~FileLog()
{
}


void FileLog::set_synchro(Synchro * synchro)
{
	this->synchro = synchro;
}


void FileLog::set_plugin(Plugin * plugin)
{
	this->plugin = plugin;
}


void FileLog::init(const std::wstring & log_file, bool log_stdout, int log_level, bool save_each_line, size_t log_time_zone_id)
{
	pt::FileLog::init(log_file, log_stdout, log_level, save_each_line);
	this->log_time_zone_id = log_time_zone_id;
}


void FileLog::set_time_zones(TimeZones * time_zones)
{
	this->time_zones = time_zones;
}


int FileLog::get_log_level()
{
	return log_level;
}


bool FileLog::should_save_each_line()
{
	return save_each_line;
}




pt::Date FileLog::get_local_date(const pt::Date & date)
{
	if( time_zones )
	{
		Lock lock(synchro);
		TimeZone * tz = time_zones->GetZone(log_time_zone_id);

		if( tz )
		{
			pt::Date local_date = tz->ToLocal(date);
			return local_date;
		}
		else
		{
			return date;
		}
	}
	else
	{
		return date;
	}
}



void FileLog::save_log(pt::WTextStream * buffer)
{
	if( !buffer->empty() )
	{
		Lock lock(synchro);
		pt::FileLog::save_log(buffer);

		// at the beginning when winix starts the plugin pointer is null
		// this plugin is set after the plugin object is initialized
		if( plugin )
		{
			plugin->Call(WINIX_SAVE_FILELOG, buffer);
		}
	}
}




} // namespace Winix

