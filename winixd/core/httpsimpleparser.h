/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_httpsimpleparser
#define headerfile_winix_core_httpsimpleparser

#include <string>

#include "winixmodeldeprecated.h"


namespace Winix
{



class HttpSimpleParser : public WinixModelDeprecated
{
protected:

	bool read_name;
	bool value_can_be_quoted;
	bool skip_white_chars;
	bool recognize_special_chars;
	
	// if false then GetChar() returns wide characters (converted to int)
	// if true then GetChar() returns utf8 characters (we have to convert them from utf8 to wide chars)
	bool getchar_returns_utf8_chars;

	int  ParseHalfHex(int c);
	void ReadName();
	void ReadQuotedValue();
	void ReadNormalValue();
	void ReadValue();
	void Clear();

	std::wstring last_name;
	std::wstring last_value;
	std::string utf8_token;

	int last_c;
	int separator;

	// '-1' means end (eof)
	// when there is an eof this method can be called more than once (it should always return -1 in such a case)
	virtual int GetChar() = 0;
	virtual void Parameter(std::wstring & last_name, std::wstring & last_value) = 0;

	void ToLower(std::wstring & s);
	bool IsWhite(int c);
	
	void SkipWhiteChars();
	void CheckSpecialChar();

	void Parse();
	
	
public:


	HttpSimpleParser()
	{
		separator = '&';
		read_name = true;
		value_can_be_quoted        = false;
		skip_white_chars           = false;
		recognize_special_chars    = true;
		getchar_returns_utf8_chars = false;
	}

};



} // namespace Winix


#endif
