/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_compress
#define headerfile_winix_core_compress

#include <cstring>
#include <zlib.h>
#include "requesttypes.h"
#include "winixbase.h"


namespace Winix
{



class Compress : public WinixBase
{

public:

	Compress();
	~Compress();

	int Init(int compress_level_ = 6);

	/*
		encoding:
		0 - raw deflate data with no zlib header or trailer, and will not compute an adler32 check value
		    (for Internet Explorer)
		1 - deflate
		2 - gzip
	*/
	int Compressing(const char * source, size_t source_len, BinaryPage & out_stream, int encoding = 2);
	int Compressing(const BinaryPage & in, BinaryPage & out, int encoding = 2);


private:

	bool AllocateMemory();
	bool InitRawDeflate();
	bool InitDeflate();
	bool InitGzip();

	int MakeCompress(z_stream & strm, const char * source, size_t source_len, BinaryPage & out_stream, int encoding);
	int MakeCompress(z_stream & strm, const BinaryPage & page, BinaryPage & out, int encoding);
	z_stream * SelectStream(int encoding);
	void ResetStream(z_stream * pstrm, int encoding);
	void PutLog(size_t source_len, int encoding);
	void CopyToInputBuffer(BinaryPage::const_iterator & i, size_t len);

	int compress_level;
	size_t buffer_max_len;

	// size of the last compressed page
	size_t last_out_size;

	char * buffer_in;
	char * buffer_out;
	z_stream strm_raw_deflate, strm_deflate, strm_gzip;
	bool raw_deflate_inited, deflate_inited, gzip_inited;
	bool ready_for_compress;

};


} // namespace Winix


#endif
