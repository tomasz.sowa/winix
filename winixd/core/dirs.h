/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_dirs
#define headerfile_winix_core_dirs


#include <vector>
#include <map>
#include <string>
#include "dircontainer.h"
#include "request.h"
#include "models/item.h"
#include "winixmodeldeprecated.h"



namespace Winix
{

class Notify;

// !! IMPROVE ME 
// we do not support '..' in a path (for simplicity and security reasons)
// (we will support '..' in the future)


class Dirs : public WinixModelDeprecated
{
public:

	void Clear();
	void ReadDirs();

	void SetCur(Cur * pcur);
	void SetNotify(Notify * pnotify);

	void set_dependency(WinixModelDeprecated * winix_model);

	// these methods return false if there is no such a dir
	bool IsDir(long dir_id);
	bool GetDirChilds(long parent_id, std::vector<Item*> & childs_tab); // !! zamienic na GetChilds()
	bool MakePath(long dir_id, std::wstring & path, bool clear_path = true);
	void MakePath(const std::vector<Item*> dir_tab, std::wstring & path, bool clear_path = true);
	bool ChangeParent(long dir_id, long new_parent_id);

	bool HasParent(long dir_id, long parent_id);

	bool DelDir(long dir_id);

	// if returns true then out_dir_tab is not empty
	bool CreateDirTab(long dir_id, std::vector<Item*> & out_dir_tab);

	void LogDir(const std::vector<Item*> & dir_tab);

	int AnalyzePath(const std::wstring & path, long & dir_id, std::wstring & dir, std::wstring & file);
	int FollowLink(const std::vector<Item*> & current_dir_tab, const std::wstring & link_to,
				   std::vector<Item*> & out_dir_tab, std::wstring & out_item);
	static void SplitPath(const std::wstring & path, std::wstring & dir, std::wstring & file);

	DirContainer::ParentIterator FindFirstChild(long parent_id);
	DirContainer::ParentIterator NextChild(DirContainer::ParentIterator i);
	DirContainer::ParentIterator ParentEnd(); // !! pozostalo do zamiany na child


	// these methods return null if there is no such a dir 
	// !! zmienic nazwy wskazujace ze operujemy tylko na lokalnej tablicy
	Item * GetRootDir();
	Item * GetEtcDir();
	Item * GetVarDir();

	Item * GetDir(const wchar_t * name, long parent);
	Item * GetDir(const std::wstring & name, long parent);
	Item * GetDir(const wchar_t * path);
	Item * GetDir(const std::wstring & path);
	Item * GetDir(long id);
	Item * AddDir(const Item & item);
	
	const Item * GetDir(long id) const;

	void CheckRootDir();

	Item * CreateVarDir();

	// !! jak juz wczesniejsze nazwy beda zmienione to tutaj damy AddDir() /AddDir() juz istnieje przeciez?/
	bool AddDirectory(Item & item, bool add_to_dir_tab = false, Item ** pdir = 0, int notify_code = 0);
	
	// returns how many levels of directories there are
	// "/"        -> 0  (root dir)
	// "/abc"     -> 1
	// "/abc/def" -> 2
	size_t DirLevel(long id);

	// checking if child_id is really a child of parent_id
	bool IsChild(long parent_id, long child_id);

private:

	Cur    * cur;
	Notify * notify;

	DirContainer dir_tab;
	std::wstring temp_path;
	std::wstring temp_link_to;

	size_t AnalyzeDir(Item * pdir, const std::wstring & path, long & dir_id, std::wstring & dir);
	bool AnalyzeDir(std::vector<Item*> & dir_tab, const std::wstring & link_to, size_t & i);
	std::wstring analyze_temp;
	std::wstring get_dir_temp;

	void CopyDirTab(const std::vector<Item*> & in, std::vector<Item*> & out);
	int FollowLink(std::vector<Item*> & dir_tab, const std::wstring & link_to, std::wstring & out_item);

	bool ExtractName(const wchar_t * & s, std::wstring & name);
	bool HasReadExecAccessForRoot(const Item & item);
	
};


} // namespace Winix


#endif
