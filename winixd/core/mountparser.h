/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_mountparser
#define headerfile_winix_core_mountparser

#include <map>
#include <string>
#include <vector>
#include <stdlib.h>
#include <limits.h>
#include "mount.h"
#include "models/item.h"
#include "winixmodeldeprecated.h"



namespace Winix
{
class Dirs;



class MountParser : public WinixModelDeprecated
{
public:

	MountParser();

	void SkipStaticDirs(bool skip);
	void SetStaticMountId(int id);

	void Parse(const std::wstring & input, std::map<long, Mount> & output);
	void SetDirs(Dirs * pdirs);
	void SetMountTypeTab(const std::vector<std::wstring> & tab);
	void SetMountFsTab(const std::vector<std::wstring> & tab);
	void SetMountParTab(const std::vector<std::wstring> & tab);

private:

	Dirs * dirs;
	bool skip_static;
	int static_mount_id;

	const std::vector<std::wstring> * mount_type_tab;
	const std::vector<std::wstring> * mount_fs_tab;
	const std::vector<std::wstring> * mount_par_tab;

	bool IsWhite(int c);
	void SkipWhite();
	void SkipLine();
	void ReadWordQuote(std::wstring & res);
	void ReadWordWhite(std::wstring & res);
	void ReadWordComma(std::wstring & res);
	void ReadWord(std::wstring & res, bool comma_bracket_separator = false);
	void ReadParamArgsLoop(Mount::ParamRow::ParamArg & args);
	void ReadParamArgs(Mount::ParamRow::ParamArg & args);
	void ReadParamName(std::wstring & res);
	void ReadParam(std::wstring & res, Mount::ParamRow::ParamArg & args);
	int FindIndex(const std::vector<std::wstring> * tab, const std::wstring & value);
	bool ReadMountType();
	bool ReadMountPoint();
	bool ReadFs();
	void LogMountParams();
	void ReadMountParams();
	void ReadRow();
	void AddParams(Mount::Param & src, Mount::Param & dst);
	bool AddParamsBefore(long dir_id);
	void AddParamsBefore();
	void AddParamsAfter();

	const wchar_t * pinput;
	std::wstring temp;
	std::wstring last_dir;
	std::wstring temp_arg;
	Mount::ParamRow::ParamArg param_args;
	Mount mount;
	Item * pdir;
	std::map<long, Mount> * poutput;
	std::pair<std::map<long, Mount>::iterator, bool> mount_inserted;
};



} // namespace Winix


#endif
