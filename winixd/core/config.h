/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_config
#define headerfile_winix_core_config

#include <string>
#include "space/spaceparser.h"
#include "log.h"
#include "filelog.h"



namespace Winix
{





class Config
{
public:

	// server mode
	// you can assign any string to it such as "production" "dev"
	// this value is not used by winix itself
	// you can refer to it from [server_mode] and [server_mode_is] ezc functions
	std::wstring server_mode;


	// start as a demon (in the background)
	// default: true
	bool demonize;

	// system user's name to whom winix should drop privileges
	// used only if winix is started as the root
	std::wstring user;

	// system group's name to which drop privileges
	// used only if winix is started as the root
	std::wstring group;

	// setting additional effective groups from /etc/group
	// by using initgroups()
	// used only if winix is started as the root
	// default: true
	bool additional_groups;

	// log file name
	std::wstring log_file;
	
	// the log level (how much info should be inserted to logs)
	// 1 - minimum
	// 2 - (default)
	// 3 - more logs
	// 4 - maximum - all logs
	int log_level;

	// logging to stdout too
	// this option is valid only if 'demonize' option is set to 'false'
	// default: false
	bool log_stdout;

	// how many requests should be saved in the same time
	// if you have a very busy server you can increase this value
	// default: 1
	int log_request;

	// whether to save each line of the config (used for debugging purposes)
	// default: false
	bool log_save_each_line;

	// time zone identifier used in log messages
	// this affects only the first line of logs (where there is IP address, request method etc)
	// see time_zone_default_id below for more info
	// default: 34 (Coordinated Universal Time UTC+00:00)
	size_t log_time_zone_id;

	// put to log what server is sending back to the client (html/json etc)
	// default: false
	bool log_server_answer;

	// logging db queries
	// warning: use it only on a developer's server as it logs the hashes of passwords too
	// default: false
	bool log_db_query;

	// logging when a plugin function is called
	// default: false
	bool log_plugin_call;


	// how many characters in values should be logged from POST parameters
	// default: 80
	// set to 0 to turn it off
	size_t log_post_value_size;

	// request delimiter in the log file, default "---------"
	std::wstring log_delimiter;

	// log environment variables (fastcgi environment)
	// default: false;
	bool log_env_variables;

	// log environment http variables (only HTTP_* variables from fastcgi environment)
	// default: false;
	bool log_env_http_variables;

	// log the whole http post structure (in json format) after parsing the post input
	// default: false;
	bool log_whole_http_post;

	// log headers (+cookies) which are returned to the client
	// this is what winix has generated -- the web server can change or add other headers
	// default: false
	bool log_http_answer_headers;

	// fast cgi: socket (unix domain)
	std::wstring fcgi_socket;

	// fast cgi: whether to change chmod of the socket
	// default: true
	// if true then you should set fcgi_socket_chmod as well
	bool fcgi_set_socket_chmod;

	// fast cgi: whether to change owner/group of the socket
	// default: true
	// if true then you should set fcgi_socket_user and fcgi_socket_group as well
	bool fcgi_set_socket_owner;

	// fast cgi: socket permissions
	// taken into account if fcgi_set_socket_chmod is true
	// chmod and chown of the socket are set before winix drops privileges
	int fcgi_socket_chmod;

	// fast cgi: owner of the socket
	// chmod and chown of the socket are set before winix drops privileges
	// taken into account if fcgi_set_socket_owner is true
	std::wstring fcgi_socket_user;
	
	// fast cgi: group of the socket
	// chmod and chown of the socket are set before winix drops privileges
	// taken into account if fcgi_set_socket_owner is true
	std::wstring fcgi_socket_group;

	// fcgi_socket_listen is the listen queue depth used in the listen() call
	// when creating a FastCGI socket for the web server
	// default: 100
	int fcgi_socket_listen;

	// in order to read a request we need to properly initialize a FastCGI structure by
	// using FCGX_InitRequest function, if FCGX_InitRequest fails then we wait fcgi_cannot_create_request_delay
	// seconds and trying again
	// default: 3
	size_t fcgi_cannot_create_request_delay;

	std::wstring templates_dir;
	std::wstring templates_dir_default; // html templates from winix

	std::wstring txt_templates_dir;
	std::wstring txt_templates_dir_default; // txt (notifications) templates from winix

	// prefix and postfix for functions templates
	// default: 
	// prefix:  "fun_"
	// postfix: ".html"
	std::wstring templates_fun_prefix;
	std::wstring templates_fun_postfix;

	// main html template
	// default: index.html
	std::wstring templates_index;

	// html template used to send generic content - without site-css styles and markup (only uikit)
	// default: index_generic.html
	std::wstring templates_index_generic;

	// html template used to send raw content
	// default: index_raw.html
	std::wstring templates_index_raw;

	// html template used to show a http status such as Forbidden or Not Found
	// default: request_status.html
	std::wstring templates_request_status;

	// if true then only root can use 'template' winix function
	// default: false
	bool template_only_root_use_template_fun;

	// the database connection string
	// https://www.postgresql.org/docs/14/libpq-connect.html#LIBPQ-CONNSTRING
	// default: empty
	// if empty then winix uses db_host and db_hostaddr
	std::wstring db_conn_string;

	// the database host name used if db_conn_string is empty
	// default: empty
	std::wstring db_host;

	// the database host ip address used if db_conn_string is empty
	// default: empty
	// if db_host is empty and db_hostaddr is empty then winix connects to a Unix-domain socket
	// meaning of db_host and db_hostaddr parameters is the same as described in
	// https://www.postgresql.org/docs/14/libpq-connect.html#LIBPQ-CONNSTRING
	//
	// from above documentation:
	// Using hostaddr allows the application to avoid a host name look-up, which might be important
	// in applications with time constraints. However, a host name is required for GSSAPI or SSPI
	// authentication methods, as well as for verify-full SSL certificate verification.
	// The following rules are used:
	//
	// - If host is specified without hostaddr, a host name lookup occurs.
	//
	// - If hostaddr is specified without host, the value for hostaddr gives the server network address.
	//   The connection attempt will fail if the authentication method requires a host name.
	//
	// - If both host and hostaddr are specified, the value for hostaddr gives the server network address.
	//   The value for host is ignored unless the authentication method requires it, in which case it will
	//   be used as the host name.
	std::wstring db_hostaddr;

	// the database port number
	std::wstring db_port;

	// the database name, user name and a password for the PostgreSQL database
	std::wstring db_database;
	std::wstring db_user;
	std::wstring db_pass;

	// specify how many times we can try to connect to the database at startup
	// default 0 (infinite)
	size_t db_startup_connection_max_attempts;

	// delay between each connection attempt at startup
	// default 5 (seconds)
	size_t db_startup_connection_attempt_delay;

	// make database migration if needed
	bool db_make_migration_if_needed;

	// if a migration fails then stop winix
	bool db_stop_if_migration_fails;

	// use internal session mechanism
	// default: true
	// if false then we do not load/save and create internal sessions
	// but there always be the temporary session object for a request
	// also we do not load 'who' winix function
	bool use_internal_session_mechanism;

	// use internal loggin mechanism
	// default: true
	// if false then we do not load 'login' and 'logout' winix functions
	// and the system will not allow to login through its api
	bool use_internal_loggin_mechanism;

	// the name of the cookie which has the session identifier
	std::wstring session_cookie_name;

	// session cookie path
	// default: /
	std::wstring session_cookie_path;

	// session cookie domain
	// if empty then the domain will be equal base_url
	// default: empty
	std::wstring session_cookie_domain;

	// session cookie samesite attribute
	// 0 - dont set the attribute
	// 1 - Strict
	// 2 - Lax
	// 3 - None
	// (values the same as values from CookieSameSite enum)
	// default: 0
	int session_cookie_same_site;

	// whether or not set HttpOnly attribute on the session cookie
	// default: false
	bool session_cookie_http_only;

	// whether or not set Secure attribute on the session cookie
	// default: false
	bool session_cookie_secure;

	// string used in a place where is a user (or group) selected
	// !! IMPROVE ME should be moved to locales
	std::wstring priv_no_user;
	std::wstring priv_no_group;

	// time in seconds when a user will be automatically logged out (iddle time)
	// default: 10800 = 3 hours
	int session_max_idle;
	
	// time in seconds when a user will be automatically logged out
	// when he has selected the 'remember me' option when logging in
	// this time is usually greater than session_max_idle
	// default: 16070400 = 3 months
	int session_remember_max_idle;

	// a file to which winix stores sessions info
	// it is used when winix starts (boots) and quits
	std::wstring session_file;

	// how many sessions can be (zero turn off this checking)
	// default: 1000000 (one milion)
	size_t session_max;

	// whether or not we should encode the session cookie
	// (we have a special algorithm)
	// default: false
	bool session_cookie_encode;

	// if session_cookie_encode is true then you should provide
	// a file where AES keys will be stored
	std::wstring session_keys_file;

	// each session has an index -- an unsigned int value
	// this value is sent in the cookie string (is encoded)
	// and is incremented when session_index_time_increment time is passed since the last incrementing
	// if a client sent the cookie back the difference between
	// current index and the index in the cookie should be less than or equal to session_allow_index_difference
	// default: 8
	size_t session_allow_index_difference;

	// the time which should pass after the session index is incremented
	// default: 30
	// (session_allow_index_difference + 1) * session_index_time_increment should be less than a time
	// load of a page and all elements on it such as images (of course it depends on client's download too)
	time_t session_index_time_increment;

	// time in seconds after a new AES key pair should be generated
	// we have 256 pairs of keys so this time multiplied by 256 should not be less than
	// the max time of a session (session_remember_max_idle),
	// by default: 256 * 2 days = 512 days = 1.4 year > 3 months (session_remember_max_idle)
	// default: 172800 = 2 days (max: 2678400 = 1 month, min: 10)
	size_t session_key_renew_time;

 	// after how many broken encoded cookie we should ban the current IP
 	// default: 2 (value in the range <0 - 65535>)
 	size_t broken_encoded_cookie_treshold;

 	// after how many incorrect session identifiers (or session indices) we should ban the current IP
 	// do not set this value too low, as people connecting from the same IP address (from behind a NAT)
 	// would be banned if they have an old session cookie remembered in the browser
 	// default: 128 (value in the range <0 - 65535>)
 	size_t session_hijacking_treshold;

 	// after how many times a client will be banned (or given temporary session) if it did not send a session cookie
 	// this can be a bot such as a Google Bot or just people connecting from a NAT and all have the same IP
 	// default: 128 (value in the range <0 - 65535>)
 	size_t no_session_cookie_treshold;

 	// the way we behave when no_session_cookie_treshold limit is exceeded
 	// 0 - if a client doesn't send a session cookie again then use a temporary session
 	//     (other sessions from this IP address are not affected)
 	// 1 - add this IP address to ban list and create a temporary session
 	//     (this will block other sessions from this IP address too)
 	// default: 0
 	int no_session_cookie_ban_mode;

	// allow the winix output to be compressed
	// default: true
	bool compression;

	// compress only if the output is greater or equal to this value
	// default: 512 bytes
	size_t compression_page_min_size;

	// 1  - use deflate if available (or raw deflate for Internet Explorer) or don't compress
	// 2  - use gzip if available or don't compress
	// 10 - prefer deflate -- use deflate (or raw deflate for IE) if both deflate and gzip are available
	// 20 - prefer gzip    -- use gzip if both deflate and gzip are available
	// default: 20
	int compression_encoding;
	
	// plugins directory
	// default: /usr/local/winix/plugins
	std::wstring plugins_dir;

	// plugins
	// you can provide either a relative path (plugins_dir will be used) 
	// or a full path to a plugin
	std::vector<std::wstring> plugin_file;

	// should the html code be cleaned by the html filter
	// default: false
	bool html_filter;

	// how white characters between html tags are treated
	// 0 - WHITE_MODE_ORIGIN - they are copied from input to output
	// 1 - WHITE_MODE_SINGLE_LINE - new line characters are removed
	// 2 - WHITE_MODE_TREE - try to create a tree (some kind of pretty printing)
	// default: 2
	int html_filter_white_char_mode;

	// when long lines should be broken (a new line character will be inserted)
	// default: 110
	// set zero to turn off
	size_t html_filter_wrap_line;

	// how many spaces will be put at one tree level
	// default: 2
	size_t html_filter_tabs;

	// use checking for 'orphans' for a specicic language
	// default: true
	bool html_filter_orphans;

	// orphans mode
	// either: "nbsp" or "160"
	// default: "nbsp"
	std::wstring html_filter_orphans_mode_str;

	// the html nofilter tag
	// content betweeng these tags (opening and closing) will not be filtered
	// and this tag will not be included in the html output
	// default: nofilter
	std::wstring html_filter_nofilter_tag;

	// the url of a new empty item (if there is not the subject too)
	// !! IMPROVE ME should be moved to locale
	std::wstring item_url_empty;

	// maximum length of a file send by post multipart form
	// default: 8388608 - 8MB
	// 0 - not used
	size_t post_file_max;

	// maximum length of the input stream when parsing it as json
	// default: 8388608 - 8MB
	// set zero to disable checking
	size_t post_json_max;

	// maximum key/value pairs in one object when parsing post json structure
	// default: 1024
	// set zero to disable checking
	// each winix function can set its own limits
	size_t post_max_object_items;

	// maximum items in one table when parsing post json structure
	// default: 4096
	// set zero to disable checking
	// each winix function can set its own limits
	size_t post_max_table_items;

	// maximum items (key/values pairs of objects or table items) througout the whole post input json structure
	// default: 8192
	// set zero to disable checking
	// each winix function can set its own limits
	size_t post_max_all_items;

	// maximum nested objects/tables when parsing post json structure
	// default: 32
	// set zero to disable checking
	// each winix function can set its own limits
	size_t post_max_nested_objects;

	// directory for static files
	std::wstring upload_dir;

	// additional static directories *not* managed by winix
	// you can refer to this directories by using 'static' mount point
	// e.g.
	// let we say in the config you have:
	// static_dirs = ("/home/a", "/home/b", "/home/c")
	// and in /etc/fstab (in winix) you have such a line:
	// "static /my/dir simplefs static(2)"
	// above line in /etc/fstab means that when a user enters http://domain.com/my/dir/file.txt
	// then "file.txt" will be taken from "/home/c/file.txt"
	// this is useful for some javascript files which are protected from running from other domains
	std::vector<std::wstring> static_dirs;

	// sometimes you can have trouble when you have set an incorrect static path in /etc/fstab
	// in such a case set dont_use_static_dirs to true
	// and winix will behave as there are not any static directories
	// so you can correct your mistake and then set this value to false
	bool dont_use_static_dirs;

	// static common directory
	// this is a place where there are some common javascripts, images, css files
	// winix is allowed to read some files from there
	// for example ckeditor config, vim config and so on
	// winix read it as a ezc template and put it into database
	// default: empty
	std::wstring common_dir;

	// system group's name for new uploaded files (created directories in the file system)
	// it can be empty (it is not used then)
	std::wstring upload_group;

	// this value will be set based on upload_group
	// will be -1 if upload_group is empty or if it is invalid
	int upload_group_int;

	// chmod of newly created directories (under upload_dir)
	// default: 0750
	int upload_dirs_chmod;

	// chmod of newly created files (under upload_dir)
	// default: 0640
	int upload_files_chmod;

	// create a thumbnail from an image
	// default: true
	bool create_thumb;

	// the mode of creating a thumbnail
	// width means thumb_cx, height means thumb_cy
	// 1: Width given, height automagically selected to preserve aspect ratio.
	// 2: Height given, width automagically selected to preserve aspect ratio.
	// 3: Maximum values of height and width given, aspect ratio preserved.
	// 4: Minimum values of width and height given, aspect ratio preserved.
	// 5: Width and height emphatically given, original aspect ratio ignored.
	// 6: Change as per widthxheight (3) but only if an image dimension exceeds a specified dimension.
	// 7: Change dimensions only if both image dimensions are less than specified dimensions.
	// default: 2
	int thumb_mode;

	// width of thumbnails
	// default: 150
	size_t thumb_cx;

	// height of thumbnails
	// default: 150
	size_t thumb_cy;

	// quality of thumbnails
	// from 0 (the worst) to 100 (the best)
	// more info: http://www.imagemagick.org/script/command-line-options.php?ImageMagick=p4jtel7557hovd34ui3tgb54h6#quality
	// default: 92
	int thumb_quality;

	// resizing images
	// this not affects thumbnails
	// default: true
	bool image_resize;

	// the mode of resizing an image
	// the same as 'thumb_mode' above
	// default: 6
	int image_mode;

	// width of images
	// default: 1000
	size_t image_cx;

	// height of images
	// default: 800
	size_t image_cy;

	// quality of an image (the same as in thumbnails)
	// from 0 (the worst) to 100 (the best)
	// default: 92
	int image_quality;

	// the convert program (ImageMagic) (for images and thumbnails)
	// default: /usr/local/bin/convert
	std::wstring convert_cmd;

	// directory with locale files
	std::wstring locale_dir;

	// directory with default locale files (those from winix)
	std::wstring locale_dir_default;

	// locale files (e.g. "en", "pl")
	// default: only one item: en
	std::vector<std::wstring> locale_files;

	// a maximum value of a locale identifier
	// default: 100 (maximum: 1000)
	// each locale files should have its own identifier (in "winix_locale_id" field)
	// from zero to this value
	size_t locale_max_id;

	// locale for not logged users
	// or for newly created accounts
	// default: 0
	size_t locale_default_id;

	// url protocol
	// default: http://
	std::wstring url_proto;

	// url protocol when using SSL
	// default: https://
	std::wstring url_ssl_proto;

	// enables SSL
	// it means this site should be accessed through SSL encrypted connection
	// default: false
	bool use_ssl;

	// enables SSL for static content
	// used mainly in templates, look at doc_base_url_static ezc function
	// default: false
	bool use_ssl_static;

	// enables SSL for common content
	// used mainly in templates, look at doc_base_url_common ezc function
	// default: false
	bool use_ssl_common;

	// if SSL is enabled then if this is true the SSL will be used
	// only for logged users
	// default: false
	bool use_ssl_only_for_logged_users;

	// if current connection is without SSL and should be made through SSL
	// or if is via SSL and should be done in plain text
	// then we make a redirect
	// default: 303
	int use_ssl_redirect_code;

	// winix is testing an environment variable "HTTPS" for the value "on"
	// to think that the connection is made via SSL
	// you can set assume_connection_is_through_ssl to true for the winix
	// to not testing any environment variables but just think the connection
	// was made by https (useful when using proxy)
	// default: false
	bool assume_connection_is_through_ssl;

	// when the HOST_HTTP environment variable is not equal to 'base_url'
	// (the part 'http://' and the last slash is removed)
	// the server will redirect into base_url + 'REQUEST_URI'
	// it's useful when you want to redirect from 'mydomain.tld' into 'www.mydomain.tld' etc.
	// set this option to false if you have multiple subdomains
	// default: false
	bool base_url_redirect;

	// the HTTP code used during the base redirect
	// default: 301
	int base_url_redirect_code;

	// the main address of the site (e.g. www.someserver.com)
	// (without http:// prefix)
	std::wstring base_url;

	// static content not authorized by winix
	// (e.g. static.someserver.com)
	std::wstring base_url_static;

	// additional static server for common content (not authorized)
	// (e.g. common.someserver.com)
	std::wstring base_url_common;

	// separator used in <title> html tag
	std::wstring title_separator;

	// how to send static files (uploaded by users) to the webserver
	// 0 - winix will read the content of the file and send it back to the webserver
	// 1 - winix will use send_file_header header with a full path to the file
	// 2 - winix will use send_file_header header with a relative path to the file
	// default: 0
	// 0 can be be used with all webservers but it requires to copy the whole file content
	// you can omit copying the content with values 1 or 2:
	// for Apache set: 1
	// for Lighttpd set: 1
	// for Nginx set: 2
	int send_file_mode;

	// http header recognized by www server as a file to send back, used if send_file_mode is 1 or 2
	// default: X-SENDFILE
	// for Apache set: X-SENDFILE (Apache needs an external module: https://tn123.org/mod_xsendfile/)
	// for Lighttpd set: X-LIGHTTPD-send-file (https://redmine.lighttpd.net/projects/1/wiki/X-LIGHTTPD-send-file)
	// for Nginx set: X-Accel-Redirect (https://nginx.org/en/docs/http/ngx_http_core_module.html#internal)
	std::wstring send_file_header;

	// relative prefix used for sending static files if send_file_mode is 2
	// default: "upload-files-internal"
	// this prefix is added at the beginning of a relative file path e.g.
	// /upload-files-internal/simplefs/normal/some_directories/file.jpg
	//
	// in Nginx config file use 'location' with the prefix, e.g:
	//    server {
	//        .....
	//        location /upload-files-internal/ {
	//            alias /path/to/winix/upload/; # trailing slash at the end
	//            internal;
	//        }
	//    }
	std::wstring send_file_relative_prefix;

	// in editors (emacs, ckeditor,...) the html will be filtered and unsafe tags
	// will be dropped (script, frame, etc.)
	// default: true;
	bool editors_html_safe_mode;

	// don't filter the html code for root
	// default: true
	// (if true the html code for root is not filtered)
	bool editors_html_safe_mode_skip_root;

	// how many maximum symlinks can be followed
	// (symlinks on directories as well)
	// default: 20
	size_t symlinks_follow_max;

	// the prefix of a name of html form controls used in the ticket plugin
	// default: ticketparam
	std::wstring ticket_form_prefix;

	// the minimal size of a user's password
	// default: 5
	size_t pass_min_size;

	// how passwords should be stored
	// 0  - plain text
	// 1  - md4 hash
	// 2  - md5 hash
	// 10 - sha1 hash
	// 11 - sha224 hash
	// 12 - sha256 hash (default)
	// 13 - sha384 hash
	// 14 - sha512 hash
	int pass_type;

	// whether the passwords' hashes should be salted or not
	// this affects newly created accounts
	// default: false
	bool pass_hash_use_salt;

	// salt to a password's hash
	// default empty
	// !! once you set this salt don't change it any more (people wouldn't be allowed to login)
	std::wstring pass_hash_salt;

	// whether the passwords' hashes should be encrypted
	// this affects newly created accounts
	// default: false
	bool pass_use_rsa;

	// path to a RSA private key
	// this is actually private + public key in one file
	// generated by "openssl genrsa"
	// default empty which means encryption will not be used
	// !! once you set these keys don't change it any more (people wouldn't be allowed to login)
	std::wstring pass_rsa_private_key;

	// path to 'openssl'
	// default: /usr/bin/openssl
	std::wstring opensll_path;

	// setting when we should delete patterns (EZC patterns)
	// we are deleting when we have more (or equal) patterns than 'when_delete'
	// and then we are deleting 'how_many_del' patterns
	// those patterns comes from items (pages) with executable bit set
	size_t pattern_cacher_when_delete;
	size_t pattern_cacher_how_many_delete;

	// header "Content-Type" send to the client
	// 0 - text/html				- for HTML
	// 1 - application/xhtml+xml	- for XHTML 1.0
	// 2 - application/xml			- for XHTML 1.0 or for XHTML 1.1
	// default: 0
	// if utf8 is true then "; charset=UTF-8" will also be appended
	// may it would be better to set just the string here instead of integers?
	int content_type_header;

	// global umask
	// it is used when an user doesn't have your own umask or for guests (not logged users)
	// default: 0222
	int umask;

	// maximum number for elements through the whole template
	// default: 50000
	size_t ezc_max_elements;

	// maximum number of each [for] loop
	// default: 5000 (from ezc generator)
	size_t ezc_max_loop_elements;

 	// how many output streams do we have in Request class
	// those streams used in ajax responses
	// you can use no more [ezc frames] statements than this limit
 	// default: 128
 	size_t ezc_out_streams_size;

 	// the name of the url parameter for returning a frame, e.g. https://domain.tld/mydir/myfunction/frame:foo
 	// default: frame
 	std::wstring request_frame_parameter;

 	// the name of the url parameter for returning all frames, e.g. https://domain.tld/mydir/myfunction/all_frames
 	// default: all_frames
 	std::wstring request_all_frames_parameter;

 	// the name of the url parameter for returning the main ezc stream, e.g. https://domain.tld/mydir/myfunction/main_stream
 	// default: main_stream
 	std::wstring request_main_stream_parameter;

 	// max lenght of the url frame parameter
 	// default: 128
 	size_t request_frame_parameter_max_length;

 	// max number of frames in the frame url parameter (they are separated by a comma)
 	// default: 16
 	// if you need more frames you can use all_frames url parameter to return all frames
 	size_t request_frame_parameter_max_frames;

 	// the name of the root element when serializing request answer to xml
 	// default: winix
 	std::wstring xml_root;

 	// the name of the field of the binary stream when serializing a request
 	// default: bin_stream
 	std::wstring bin_stream_field;

 	// the name of the field of the main ezc stream when serializing a request
 	// default: main_stream
 	std::wstring main_stream_field;

 	// the name of the field (object) of the ezc frames when serializing a request
 	// default: ezc_frames
 	std::wstring ezc_frames_field;

 	// max Accept header mime types to be parsed in a request
 	// default: 8
 	size_t request_max_accept_fields;

 	// max Accept-Language languages to be parsed in a request
 	// default: 8
 	size_t request_max_accept_language_fields;

 	// when true then when a user want to create a new account
	// he has to provide his email and a message will be sent back to him
	// with a link to activate the account
	// default: true
	bool account_need_email_verification;

	// when a user forgot his password we are able to send an email to him
	// with a link to the page where there is a html form for setting a new password
	// this option tells how long (in seconds) the link is valid
	// default: 86400 (24 hours)
	long reset_password_code_expiration_time;

	// time zone identifier for not logged users
	// or for newly created accounts
	// those identifiers you can see in etc/time_zones.conf file
	// or by using timezone winix function with 'a' parameter (timezone/a) (!!IMPROVE ME NOT IMPLEMENTED YET)
	// default: 34 (Coordinated Universal Time UTC+00:00)
 	size_t time_zone_default_id;

 	// a maximum value of a time zone identifier
 	// time zones with an id greater than this will be skipped
 	// default: 130 (maximum: 1000)
 	size_t time_zone_max_id;

 	// a directory in which there are some config files
 	// used mainly when winix starts
 	// default: empty (means not for using)
 	std::wstring etc_dir;

 	// a file in etc_dir with time zones info
 	// default: time_zones.conf
 	// this is a Space structure with all time zones
 	// with following format:
 	// "tz_-12:00" (				# the name of a space is also a key to the locale files
 	// tz_offset_str = "-12:00" 	# a string representing the offset from UTC in a format: [+|-]HH:MM
 	// tz_has_dst    = "false"		# whether the time zone has daylight saving time (bool)
 	// tz_dst_offset = ""			# offset of the daylight saving time
 	// tz_dst_start  = ""			# when the daylight saving time starts, format: MM:DD HH:MM:SS
 	// tz_dst_end    = "")			# when the daylight saving time ends, format: MM:DD HH:MM:SS
 	// each time zone is in a seperate space
 	std::wstring time_zones_file;

 	// turn on the IP ban mechanizm
 	// we have got three levels of bans (level 1, level 2, and the highest level 3)
 	// default: true
 	bool use_ban_list;

 	// the so called 'soft' max size
 	// read below description for explanation
 	// this is introduced to avoid deleting only one record from the ban list
 	// default: 50000
 	size_t ban_list_soft_max_size;

 	// this is the 'hard' max size of an IP's ban list
 	// if there are more records than this value
 	// then some of them will be removed (until the size will be ban_list_soft_max_size equal)
 	// this value should be a little larger from ban_list_soft_max_size
 	// default: 51000
 	size_t ban_list_max_size;

 	// delay in seconds of the first level ban
 	// default: 1800 (30 minutes)
 	// it means withing the next 30 minutes you see only 'you are banned...' message on your webbrowser
 	size_t ban_level_1_delay;

 	// delay in seconds of the second level ban
 	// default: 86400 (24 hours)
 	size_t ban_level_2_delay;

 	// delay in seconds of the third level ban
 	// default: 604800 (7 days)
 	size_t ban_level_3_delay;

 	// the minimum time in seconds which has to pass between the first GET request
 	// (showing your the login form) and the second POST request (which sends the
 	// login and password to the server)
 	// if the time is shorter then the login attempt is treated as incorrect
 	// (the same as if you provide incorrect user/password)
 	// default: 2
 	size_t incorrect_login_min_time_between_get_post;

 	// how many incorrect logins there must have been passed to display a captcha
 	// next to the login form
 	// default: 3 (value in the range <0 - 65535>)
 	size_t incorrect_login_captcha_treshold;

 	// the way how we prevent to login if there are too many incorrect login attempts
 	// 0 - 'block logging' - do not show the login form in 'login' winix function
 	//     (instead a warning message will be printed)
 	// 1 - add to ban list (warning: people from this IP will not be able to see your site and do anything)
 	// default: 0
 	int incorrect_login_cannot_login_mode;

 	// after how many incorrect login attempts we do the incorrect_login_cannot_login_mode action
 	// default: 20 (value in the range <0 - 65535>)
 	size_t incorrect_login_cannot_login_treshold;

 	// used when incorrect_login_cannot_login_mode is zero
 	// it is the time which should be passed to allow logging
 	// default: 1800 (30 minutes)
 	// if incorrect_login_cannot_login_mode is one then ban_level_X_delay times
 	// will be taken accordingly
 	size_t incorrect_login_cannot_login_delay;

 	// pid file (a full path to a pid file)
 	// default: empty which means there is not a pid file used
 	// pid file is saved after winix has dropped privileges
 	std::wstring pid_file;

 	// allow to use [ezc frame] statements in executable items (used in 'run' winix function)
 	// default false
 	// warning: if you enable this options a user can override a different output stream when using ajax
	bool allow_ezc_frames_in_executable_items;


	// check whether there is a 'log_proxy_ip_header' header
	// and if so then log the IP address from it
	// default: false
	bool check_proxy_ip_header;

	// proxy header representing the real IP address of a client
	// default: X_Real_IP
	std::wstring proxy_ip_header;

	// use an antispam mechanism for not logged users
	// when they try to add a new item
	// default: true
	bool use_antispam_mechanism_for_not_logged_users;

	// antispam mechanism
	// size of an list for map: form_id to counter_id for anonymous users (each session has such an map)
	// this value allowes you to open the same or different html form in the browser more than once
	// and each form has its own form_id and counter_id
	// default: 10 (max ten forms can be open in different tabs)
	size_t antispam_list_max_size;


	// send "Cache-Control: no-store, max-age=0" http header if a request is made by htmx library (ajax)
	// if a webbrowser get a page from the cache then it will render just the last request without the whole html page (css, js, etc)
	// https://github.com/bigskysoftware/htmx/issues/497
	bool add_header_cache_no_store_in_htmx_request;

	// a limit for the queue for requests
	// when a request is scheduled to a job
	// default: 1024
	// if the limit is reached then the http status 503 Service Unavailable is returned
	size_t request_queue_job_limit;

	// list of allowed origins in cors requests
	// can be set per controller in a method: virtual bool FunctionBase::IsOriginAvailable(const std::wstring & origin_url)
	// default: empty
	std::vector<std::wstring> allowed_origins;

	// whether or not all origins are allowed if allowed_origins is empty
	// default: true
	// this is true by default because Origin header is sent not only in cors requests
	// (you can still allow it in your function/controller by overriding IsOriginAvailable(...) method)
	bool allow_all_origins;

	// whether or not cors preflight requests are available
	// https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request
	// default: false
	bool are_cors_preflight_requests_available;

	// list of additional headers sent in Access-Control-Expose-Headers header
	// can be set per controller in a method: virtual void FunctionBase::AddAccessControlExposeHeadersHeader()
	// default: empty
	std::vector<std::wstring> access_control_expose_headers;

	// if true return Access-Control-Allow-Credentials header equal "true"
	// whethert credentials (e.g. cookies, authorization headers) are available in cors requests
	// can be set per controller in a method: virtual bool FunctionBase::AreCorsCredentialsAvailable()
	// default: false
	bool access_control_allow_credentials;



	Config();
	bool ReadConfig(const std::wstring & config_file);
	void AssignValuesFromSpace();


	/*
	 *
	 * CHECK ME
	 * there was a change in Pikotools
	 * before Text() returns a reference to the std::wstring
	 *
	 */
	std::wstring Text(const wchar_t * name);
	std::wstring Text(const wchar_t * name,	     const wchar_t * def);
	std::wstring Text(const std::wstring & name, const wchar_t * def);

	int  Int(const wchar_t *);
	int  Int(const wchar_t * name, int def);
	int  Int(const std::wstring & name, int def);
	long Long(const wchar_t *);
	long Long(const wchar_t * name, long def);
	long Long(const std::wstring & name, long def);
	size_t Size(const wchar_t *);
	size_t Size(const wchar_t * name, size_t def);
	size_t Size(const std::wstring & name, size_t def);
	bool Bool(const wchar_t *);
	bool Bool(const wchar_t * name, bool def);
	bool Bool(const std::wstring & name, bool def);
	bool ListText(const wchar_t * name, std::vector<std::wstring> & list);
	bool ListText(const std::wstring & name, std::vector<std::wstring> & list);

	bool HasValue(const wchar_t * name,      const wchar_t * value);
	bool HasValue(const std::wstring & name, const std::wstring & value);

	// for debug
	//void Print(std::wostream & out);

	// raw access to the config
	pt::Space space;


	void SetFileLog(FileLog * file_log);
	void SetLogBuffer(pt::WTextStream * log_buffer);


private:

	pt::SpaceParser parser;
	Log log;

	void ShowError(const std::wstring & config_file);
	void AssignValues();
	void SetAdditionalVariables();
	void CheckPasswd();

};




} // namespace Winix




#endif
