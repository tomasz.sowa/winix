/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "session.h"
#include "misc.h"


namespace Winix
{



Session::Session()
{
	Clear();
}



Session::Session(const Session & ses)
{
	operator=(ses);
}

Session & Session::operator=(const Session & ses)
{
	/*
		we can only copy ses.id because it is needen in SessionContainer
		it have indexes to id
	*/

	Clear();
	id = ses.id;

return *this;
}





void Session::SetTimesTo(time_t time)
{
	start_time = time;
	start_date = time;

	last_time = start_time;
	last_date = start_date;
	
	// the first request can be a POST (it doesn't matter)
	last_time_get = start_time;
}



// clear_plugin_data is used when clearing the temporary session
void Session::Clear(bool clear_plugin_data)
{
	id               = 0;
	id_index         = 0;
	id_index_changed = 0;
	puser            = 0;
	rebus_item       = 0;
	rebus_checked    = false;
	remember_me      = false;
	new_session      = true;
	spam_score       = 0;
	remove_me        = false;
	allow_to_delete  = true;

	start_time       = 0;
	last_time        = 0;
	last_time_get    = 0;
	start_date.Clear();
	last_date.Clear();

	last_css.clear();
	ip_ban = 0;

	if( clear_plugin_data )
		plugin_data.Resize(0);
}


// clearing some variables when a request is ended (just for safety)
void Session::ClearAfterRequest()
{
	// ip_ban list can be sorted by SessionManager (in the special thread)
	ip_ban = 0;
}


bool Session::is_temporary_session() const
{
	return id == 0;
}


} // namespace Winix

