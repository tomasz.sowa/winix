/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2009-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_lastcontainer
#define headerfile_winix_core_lastcontainer

#include <string>
#include <list>
#include <cstring>
#include <ctime>
#include "date/date.h"
#include "winixbase.h"



namespace Winix
{



// how many items we store in the 'last' function
#define WINIX_LASTCONTAINER_TABLE_SIZE 100



struct LastItem
{
	long user_id;
	
	// additional we store the whole string-name 
	// (you can delete a user from the database but we can still print the name)
	std::wstring name;
	
	// ip address
	unsigned int ip;
	
	// session id (used when logging out)
	long session_id;

	// start logging and end logging
	pt::Date start;
	pt::Date end;


	LastItem();
	bool IsLoggedOut();

};





class LastContainer : public WinixBase
{
public:

	typedef std::list<LastItem> LastTab;
	typedef LastTab::iterator Iterator;


public:
	
	Iterator Begin();
	Iterator End();
	void UserLogin(long user_id, const std::wstring & name, unsigned int ip, long session_id);
	void UserLogout(long user_id, long session_id);
         
	
private:

	LastTab last_tab;
	Iterator FindNotLoggedOut(long user_id, long session_id);

};



} // namespace Winix


#endif
