/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "winixrequest.h"

namespace Winix
{



WinixRequest::WinixRequest()
{
	cur = nullptr;
	locale = nullptr;
	session_manager = nullptr;
}


WinixRequest::~WinixRequest()
{
}


void WinixRequest::set_cur(Cur * cur)
{
	this->cur = cur;
}


void WinixRequest::set_session_manager(SessionManager * session_manager)
{
	this->session_manager = session_manager;
}


void WinixRequest::set_locale(Locale * locale)
{
	this->locale = locale;
}



void WinixRequest::set_dependency(WinixBase * winix_base)
{
	WinixBase::set_dependency(winix_base);
}


void WinixRequest::set_dependency(WinixModelDeprecated * winix_model)
{
	WinixModelDeprecated::set_dependency(winix_model);
}


void WinixRequest::set_dependency(WinixSystem * winix_system)
{
	WinixSystem::set_dependency(winix_system);
}


void WinixRequest::set_dependency(WinixRequest * winix_request)
{
	WinixSystem::set_dependency(winix_request);
	set_cur(winix_request->cur);
	set_locale(winix_request->locale);
	set_session_manager(winix_request->session_manager);
}



}


