/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2008-2023, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 

#ifndef headerfile_winix_core_pluginmsg
#define headerfile_winix_core_pluginmsg


namespace Winix
{

// IMPROVEME give some description
#define WINIX_MAKE_DATABASE_MIGRATION		997



// here you can add your own EZC functions ([function])
// PluginInfo.p1 is a pointer to Ezc::Functions object
// PluginInfo.p2 is a pointer to Ezc::Objects object
// session pointer is null
#define WINIX_TEMPLATES_CREATEFUNCTIONS		999

// here you can add your own EZC functions to notify system
// warning: this functions will be called from an other thread
// so you should use synchro->Lock() and synchro->Unlock()
// when attempting to winix objects
// PluginInfo.p1 is a pointer to Ezc::Functions object
// which is defined as:
//    Ezc::Functions<NotifyStream> ezc_functions;
// and Notify Stream is:
//    typedef TextStream<std::wstring> NotifyStream;
// session pointer is null
#define WINIX_NOTIFY_TEMPLATES_CREATEFUNCTIONS			998

// winix function and parameters have been parsed
// the request.status is OK
// (the winix function was not called yet)
#define WINIX_PREPARE_REQUEST				20000

// post and get functions have done their jobs
// now you can act
// this is called only if the request.status is OK
#define WINIX_PROCESS_REQUEST				20010

// prepere your content for displaying
// this is called after WINIX_PROCESS_REQUEST
// and when there is not a redirect
// request.status is not checked here
#define WINIX_CONTENT_MAKE					20020

// here you can attach your own session data (based on PluginDataBase class)
// call cur->session->plugin_data.Assign(pointer)
#define WINIX_SESSION_CREATED				30000

// here you should remove your session data
// this message can be sent even if you don't assing your plugin data
#define WINIX_PLUGIN_SESSION_DATA_REMOVE	30010

// when a session is changed (you can save a pointer to your data here)
// 'session changed' means that there is a new request
// !! IMPROVE ME it has to be changed to a better name
#define WINIX_SESSION_CHANGED				30020

// a session has changed its id
// it is typically when you are logging in
// 'login' winix function will change the session id (for security reasons)
// in l1 you have the old id, in l2 you have the new id
#define WINIX_SESSION_CHANGED_ID			30025

// a session is going to be removed
// it is called from session manager's thread (with lock/unlock)
#define	WINIX_PREPARE_SESSION_TO_REMOVE		30027

// a session has been removed
// it is called from session manager's thread (with lock/unlock)
// in l1 you have the old session id
#define WINIX_SESSION_REMOVED				30029

// the winix is being closing
// there is not any sessions available (cur->session is null)
// session pointer is null
#define WINIX_PREPARE_TO_CLOSE				30040

// winix has been closes, all other threads are stopped now, this is a last message for the main thread
// there is not any sessions available (cur->session is null)
#define WINIX_QUIT							30041

// preparing to remove a file (rm function)
// in p1 you have a pointer to the Item struct (file)
// valid members are:
//  id, parent_id, type, url, file_path, file_fs, file_type, has_thumb, hash, hash_type, file_size
//  user_id, group_id, privileges
//  (sometimes rest members can be valid as well -- when you call directly fun_rm->RemoveFileOrSymlink() method)
#define WINIX_FILE_PREPARE_TO_REMOVE		30045

// a file or symlink was removed (rm function)
// in p1 you have a pointer to the Item struct (old file)
// valid members are the same as in WINIX_FILE_PREPARE_TO_REMOVE
#define WINIX_FILE_REMOVED					30050

// preparing to remove a directory (rm function)
// in p1 you have a pointer to the Item struct (directory)
// this message is sent after checking the directory permissions
// so consequently if there is no any database error then the
// directory will be removed
// and after removed WINIX_DIR_REMOVED message is sent
#define WINIX_DIR_PREPARE_TO_REMOVE			30070

// directory was removed (rm function)
// PluginInfo::l1 is the directory id
#define WINIX_DIR_REMOVED					30060

// winix is initialized,
// now you can initialize your plugin
// session pointer is null
// you have to set the result status to true (env.res) if your
// plugin was initialized correctly, otherwise winix will not start
#define WINIX_PLUGIN_INIT					30080	

// winix is ready to remove a plugin
// you can clear some plugin data here e.g. controllers
// all other threads are stopped now
#define WINIX_PLUGIN_QUIT					30081

// here you can add your own mount point, file systems, mount parameters
// for adding a new mount type call: system->mounts.AddMountType("new_mount_name")
// session pointer is null
#define WINIX_ADD_MOUNTS					30090

// add plugin functions (winix functions) here
// call info.functions->Add() to add a function
// session pointer is null
#define WINIX_CREATE_FUNCTIONS				30100

// choose a default function
// if you do not select it then it will be choosen by winix
#define WINIX_SELECT_DEFAULT_FUNCTION		30110

// /etc/fstab has been changed
// now we have new mount points
// session pointer is null
#define WINIX_FSTAB_CHANGED					30120

// here you add your own template to notification system
// call system->notify.AddTemplate() method
// with a template file name
// session pointer is null
#define WINIX_NOTIFY_ADD_TEMPLATE			30130

// the request is being ended
// you can clear some of your objects here
// in p1 you have a pointer to the Request object
#define WINIX_END_REQUEST					30140

// a new file (page) has been added
// in p1 you have a pointer to the Item struct
#define WINIX_FILE_ADDED					30150

// a file (page) has been changed (edited)
// in p1 you have a pointer to the Item struct
#define WINIX_FILE_CHANGED					30160

// a new directory has been added
// in p1 you have a pointer to the Item struct
#define WINIX_DIR_ADDED						30155

// a file (page) has been copied
// in p1 you have a pointer to the Item struct
// not every fields of Item struct are filled
#define WINIX_FILE_COPIED					30170

// a file will be moved
// in p1 you have a pointer to the Item struct
// valid members are:
//  id, parent_id, type, url, file_path, file_fs, file_type, has_thumb, hash, hash_type, file_size
//  user_id, group_id, privileges, meta
//  (sometimes rest members can be valid as well -- when you call directly fun_rm->RemoveFileOrSymlink() method)
#define WINIX_FILE_PREPARE_TO_MOVE			30180

// a file has been moved
// in p1 you have a pointer to the Item struct (new file)
// valid members are the same as in WINIX_FILE_PREPARE_TO_MOVE
#define WINIX_FILE_MOVED					30190

// a thumbnail was created
// this message is called from another thread
// the thread called Lock() before sending this message
// in p1 you have a pointer to the Item struct
// session pointer is null
#define WINIX_CREATED_THUMB					30500

// an image has been resized
// this message is called from another thread
// the thread called Lock() before sending this message
// in p1 you have a pointer to the Item struct
// session pointer is null
#define WINIX_IMAGE_RESIZED					30520

// content of a directory was sorted
// (winix 'sort' function was used)
// in p1 you have a pointer to the Item struct (of the directory)
// this is from system->dirs so you should not change the item
#define WINIX_DIR_CONTENT_SORTED			30500

// a user will be logged in
// set PluginInfo::res to false (it is by default) to prevent logging the user
// directly after this message (if you do not return false) 
// a WINIX_USER_LOGGED is sent
// in p1 you have a pointer to User struct
// (if at least one plugin returns false then the user will not be logged)
// this message is not sent when winix starts and sessions are read from a sessions file
// in such a case only WINIX_USER_LOGGED will be sent
#define WINIX_PREPARE_USER_TO_LOGIN			30550

// a user has been logged
// send from 'login' winix function
// this message is also called when winix starts and reads sessions 
// from the session file
#define WINIX_USER_LOGGED					30600

// a user is going to logout
// in p1 you have a pointer to User struct
// the message is also sent from the session GC (special thread for deleting sessions)
// in such a case is sent with Lock() and Unlock()
// so *don't* use Lock() in your procedure 
#define WINIX_PREPARE_USER_TO_LOGOUT		30610

// here you add your own html templates
// call TemplatesFunctions::patterns.Add(L"file_name.html")
// the method returns an index which you have to remember
// and you can get the template with patterns.Get(index, lang_index)
// it returns a pointer to Ezc::Pattern (or null pointer if the index is wrong)
// the message will be sent too whenever 'reload/templates' winix function is called
// templates you should add only in this message 
// in other cases after 'reload' function the indexes would be wrong 
// session pointer is null
#define WINIX_ADD_TEMPLATE					31000

// a user will be removed
// in p1 you have a pointer to the User struct
// directly after this message a WINIX_USER_REMOVED will be sent
#define WINIX_PREPARE_TO_REMOVE_USER		31010

// a user has been removed
// in l1 you have the old user_id
#define WINIX_USER_REMOVED					31020

// here you can check if the domain part of the URL address is correct
// if no then set cur-request->redirect_to
// and winix will do a 'base url redirect' without making any winix functions
// the session pointer in info is null
#define WINIX_BASE_URL_REDIRECT				31030

// this message is sent before calling MakePost() or MakeGet()
// if you return false (which is default) you can prevent the access
// to the resource
#define WINIX_CHECK_PLUGIN_ACCESS			31060

// http headers (without cookies) were created and are ready to send
// here you can make some changes to them
// in p1 you have a pointer to the pt::Space (Request::out_headers)
#define WINIX_PREPARE_TO_SEND_HTTP_HEADERS			31070

// http cookies were created and are ready to send
// here you can make some changes to them
// in p1 you have a pointer to the pt::Space (Request::out_cookies)
#define WINIX_PREPARE_TO_SEND_HTTP_COOKIES			31080

// a file log was saved
// in p1 you have a pointer to the pt::WTextStream buffer
// this can be called from a different thread but with locking
#define WINIX_SAVE_FILELOG					31090

// this message is sent when winix has been started
// it is called from the main thread (with locking)
#define WINIX_STARTED						31100


/*
	messages sent from other threads
	!! IMPROVE ME check if some above messages should be moved here
*/


// a job has to be done
// this is sent from the other thread (without locking)
// in p1 you have a pointer to PT::Space
// session is null
// if you process the job then return 'true' from the processing method (from plugin call)
// so this prevent to make a standard (system) job
#define WINIX_JOB							31200






// values from 4000 - 4099 reserved for 'thread' plugin
// see plugins/thread/pluginmsg.h

// values from 4100 - 4199 reserved for 'ticket' plugin
// see plugins/ticket/pluginmsg.h


} // namespace Winix




#endif
