/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2014, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_core_winix_const
#define headerfile_winix_core_winix_const



/*
 * some constants used throughout winix
 */



/*
 * a size of an UTF-8 buffer when converting a file system path from a wide string
 * to UTF-8 string
 *
 * don't set it too long as it is used in local arrays: char[WINIX_OS_PATH_SIZE]
 */
#define WINIX_OS_PATH_SIZE		4096



/*
 * a size of an UTF-8 buffer when converting a user's name from a wide string
 * to UTF-8 string
 * it can be applied to a user, group etc.
 *
 * don't set it too long as it is used in local arrays: char[WINIX_OS_USERNAME_SIZE]
 *
 */
#define WINIX_OS_USERNAME_SIZE	128



/*
 * maximum size of a URL
 *
 * URLs longer than this value will be rejected (permission denied)
 * don't set it too long as it is used in local arrays: char[WINIX_URL_MAX_SIZE]
 */
#define WINIX_URL_MAX_SIZE		4096





#endif

