/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_cookieparser
#define headerfile_winix_core_cookieparser

#include "httpsimpleparser.h"
#include "requesttypes.h"
#include "log.h"


namespace Winix
{



class CookieParser : public HttpSimpleParser
{

	const wchar_t * cookie_string;
	CookieTab * cookie_tab;
	

protected:


	virtual int GetChar()
	{
		if( !cookie_string || *cookie_string == 0 )
			return -1;
		
		return (int)*(cookie_string++);
	}

	

	virtual void Parameter(std::wstring & name, std::wstring & value)
	{
		// Cookie names are case insensitive according to section 3.1 of RFC 2965
		// (we don't use locale here)
		ToLower(name);
		
		std::pair<CookieTab::iterator, bool> res = cookie_tab->insert( std::make_pair(name, value) );
		log << log2 << "Cookie, name: \"" << name << "\", value: \"" << value << "\"";
		
		if( res.second == false )
		{
			res.first->second = value;
			log << " (overwritten)";
		}
			
		log << logend;
	}


public:


	CookieParser()
	{
		HttpSimpleParser::separator                  = ';';
		HttpSimpleParser::value_can_be_quoted        = true;
		HttpSimpleParser::skip_white_chars           = true;
		HttpSimpleParser::recognize_special_chars    = false;
		HttpSimpleParser::getchar_returns_utf8_chars = false;
	}


	// cookie_string can be null
	void Parse(const wchar_t * cookie_string_, CookieTab & cookie_tab_)
	{
		cookie_string = cookie_string_;
		cookie_tab    = &cookie_tab_;

		HttpSimpleParser::Parse();
	}

	void Parse(const std::wstring & cookie_string_, CookieTab & cookie_tab_)
	{
		Parse(cookie_string_.c_str(), cookie_tab_);
	}

};



} // namespace Winix


#endif
