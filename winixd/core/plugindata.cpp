/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "plugindata.h"
#include "plugin.h"
#include "log.h"



namespace Winix
{




PluginData::PluginData()
{
}



PluginData::PluginData(const PluginData & p)
{
	operator=(p);
}



PluginData & PluginData::operator=(const PluginData & p)
{
	// we don't copy all pointers - only resize the table
	// pointers will be set to zero
	Resize(p.Size());

return *this;
}



PluginData::~PluginData()
{
	//DeleteAll();
}




void PluginData::Assign(size_t index, PluginDataBase * data)
{
	if( index >= table.size() )
		Resize(index+1);

	table[index] = data;
}


//void PluginData::Assign(PluginDataBase * data)
//{
//	if( plugin.current_plugin == -1 )
//	{
//		log << log1 << "PD: Assign(PluginDataBase*) should be called only from plugins" << logend;
//		return;
//	}
//
//	Assign(plugin.current_plugin, data);
//}



PluginDataBase * PluginData::Get(size_t index)
{
	if( index >= table.size() )
		Resize(index+1);

return table[index];
}


//PluginDataBase * PluginData::Get()
//{
//	if( plugin.current_plugin == -1 )
//	{
//		log << log1 << "PD: Get() should be called only from plugins" << logend;
//		return 0;
//	}
//
//return Get(plugin.current_plugin);
//}



bool PluginData::HasAllocatedData()
{
	bool all_null = true;

	for(size_t i=0 ; i<table.size() ; ++i)
	{
		if( table[i] != 0 )
		{
			all_null = false;
			break;
		}
	}

	return !all_null;
}



//void PluginData::DeleteAll()
//{
//	bool all_null = true;
//
//		when we copy a session's object (and this object then)
//		we resize the table and there are only null pointers there
//		consequently if all pointers are null there is no sens
//		to send WINIX_PLUGIN_SESSION_DATA_REMOVE
//
//	for(size_t i=0 ; i<table.size() ; ++i)
//	{
//		if( table[i] != 0 )
//		{
//			all_null = false;
//			break;
//		}
//	}
//
//		in the future this message may be removed
//		and we directly 'delete' the pointers
//
//	if( !all_null )
//		plugin.Call(session, WINIX_PLUGIN_SESSION_DATA_REMOVE);
//
//	table.clear();
//}





size_t PluginData::Size() const
{
	return table.size();
}



void PluginData::Resize(size_t new_size)
{
size_t old_size = table.size();

	if( old_size == new_size )
		return;

	table.resize(new_size);

	for(size_t i = old_size ; i<new_size ; ++i)
		table[i] = 0;
}



} // namespace Winix

