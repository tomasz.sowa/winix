/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "sessioncontainer.h"
#include "log.h"
#include "misc.h"


namespace Winix
{




SessionContainer::SessionContainer()
{
	table_size = 0;
}


void SessionContainer::SetCur(Cur * pcur)
{
	cur = pcur;
}


void SessionContainer::SetConfig(Config * pconfig)
{
	config = pconfig;
}


void SessionContainer::SetTmpSession(Session * psession)
{
	tmp_session = psession;
}



void SessionContainer::Clear()
{
Table::iterator i = table.begin();

	log << log3 << "SC: deleting all sessions" << logend;

	cur->session = tmp_session;

	// don't use table.clear();
	// because plugins session data would not be erased
	while( i != table.end() )
	{
		if( i->plugin_data.HasAllocatedData() )
		{
			plugin->Call(model_connector, &log, &*i, nullptr, nullptr, WINIX_PLUGIN_SESSION_DATA_REMOVE, nullptr, nullptr, 0, 0); // the session passed here is ok?
		}

		//i->plugin_data.DeleteAll(); // it's better to call it here instead in the destructor
		table.erase(i++);
	}

	// erasing indexes
	index_id.clear();
	table_size = 0;
}



void SessionContainer::EraseById(long id)
{
IndexId::iterator i = index_id.find(id);

	if( i != index_id.end() )
	{
		Session * old_session = tmp_session;

		if( cur->session != &(*i->second) )
			old_session = cur->session;

		cur->session = &(*i->second);

		log << log4 << "SC: deleting session, id: " << i->second->id << logend;

		// call first DeleteAll() because if not then it would be called from the destructor
		// and there'll be a problem if it throws an exception there

		if( i->second->plugin_data.HasAllocatedData() )
		{
			plugin->Call(model_connector, &log, &*(i->second), nullptr, nullptr, WINIX_PLUGIN_SESSION_DATA_REMOVE, nullptr, nullptr, 0, 0); // the session passed here is ok?
		}

		//i->second->plugin_data.DeleteAll();

		table.erase(i->second);
		index_id.erase(i);
		table_size -= 1;

		cur->session = old_session;
	}
	else
	{
		log << log1 << "SC: I cannot delete a session with id: " << id
			<< " (there is no such a session)" << logend;
	}
}



size_t SessionContainer::Size()
{
	// don't use table.size() as it has O(n) complexity on FreeBSD
	return table_size;
}


SessionContainer::Iterator SessionContainer::Begin()
{
	return table.begin();
}


SessionContainer::Iterator SessionContainer::End()
{
	return table.end();
}


Session & SessionContainer::Back()
{
	return table.back();	
}


SessionContainer::IdIterator SessionContainer::IdBegin()
{
	return index_id.begin();
}


SessionContainer::IdIterator SessionContainer::IdEnd()
{
	return index_id.end();
}



SessionContainer::Iterator SessionContainer::AddSession(long id)
{
	std::pair<IndexId::iterator, bool> index_id_res = index_id.insert( std::make_pair(id, table.end()) );

	if( !index_id_res.second )
	{
		// that element already exists (was not inserted now)
		return End();
	}

	Iterator last = table.insert(table.end(), empty_session);
	last->id = id;
	index_id_res.first->second = last;
	table_size += 1;

	log << log3 << "SC: added session, id: " << id << logend;

return last;
}




SessionContainer::Iterator SessionContainer::FindById(long id)
{
IndexId::iterator i;

	i = index_id.find(id);
	
	if( i == index_id.end() )
		return table.end();
		
return i->second;
}


bool SessionContainer::ChangeSessionId(SessionContainer::Iterator ses, long new_id)
{
	std::pair<IndexId::iterator, bool> index_id_res = index_id.insert( std::make_pair(new_id, ses) );

	if( !index_id_res.second )
	{
		log << log1 << "SC: session with id: " << new_id << " already exists" << logend;
		return false;
	}

	long old_id = ses->id;
	index_id.erase(old_id);	// remove the old index
	ses->id = new_id;
	log << log3 << "SC: changed session id from: " << old_id << " to " << new_id << logend;

return true;
}


bool SessionContainer::ChangeSessionId(long old_id, long new_id)
{
IndexId::iterator i = index_id.find(old_id);
	
	if( i != index_id.end() )
	{
		return ChangeSessionId(i->second, new_id);
	}
	else
	{
		log << log2 << "SC: there is no a session with id: " << old_id << logend;
	}

return false;	
}





} // namespace Winix




