/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "winixmodeldeprecated.h"


namespace Winix
{



WinixModelDeprecated::WinixModelDeprecated()
{
	plugin = nullptr;
	model_connector = nullptr;
}


WinixModelDeprecated::~WinixModelDeprecated()
{
}


void WinixModelDeprecated::set_plugin(Plugin * plugin)
{
	this->plugin = plugin;
}



void WinixModelDeprecated::set_model_connector(morm::ModelConnector * model_connector)
{
	this->model_connector = model_connector;
}


morm::ModelConnector * WinixModelDeprecated::get_model_connector()
{
	return model_connector;
}


void WinixModelDeprecated::set_dependency(WinixBase * winix_base)
{
	WinixBase::set_dependency(winix_base);
}


void WinixModelDeprecated::set_dependency(WinixModelDeprecated * winix_model)
{
	WinixBase::set_dependency(winix_model);
	model_connector = winix_model->model_connector;
	plugin = winix_model->plugin;
}


Plugin * WinixModelDeprecated::get_plugin()
{
	return plugin;
}


}

