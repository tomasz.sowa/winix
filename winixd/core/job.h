/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2023, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_job
#define headerfile_winix_core_job

#include <vector>
#include <queue>
#include "basethread.h"
#include "space/space.h"
#include "jobtask.h"
#include "cur.h"
#include "loadavg.h"
#include "mounts.h"
#include "requestjobs/requestjobbase.h"


namespace Winix
{
class Functions;






class Job : public BaseThread
{
public:

	static const size_t PRIORITY_LOWEST = 0;
	static const size_t PRIORITY_HIGHEST = 31;

	static const size_t PRIORITY_DEFAULT = 16;
	static const size_t PRIORITY_REQUEST_CONTINUATION = 17;


	Job();

	void SetCur(Cur * cur);
	void SetFunctions(Functions * functions);
	void SetLoadAvg(LoadAvg * load_avg);
	void SetMounts(Mounts * mounts);
	void SetReqTab(std::list<Request> * req_tab);

	/*
		add a new job to the queue
		priority: 0-31 (0 - the lowest priority, 31 - the highest priority)
	*/
	void Add(pt::Space & job, size_t priority = PRIORITY_DEFAULT);
	void Add(Request * request, pt::Space & job, size_t priority = PRIORITY_DEFAULT);

	void Add(long job_id, pt::Space & job, size_t priority = PRIORITY_DEFAULT);
	void Add(long job_id, Request * request, pt::Space & job, size_t priority = PRIORITY_DEFAULT);

	/*
	 * register a new request job worker
	 */
	void RegisterRequestJob(long job_id, RequestJobBase & request_job);

	/*
		queue size, and size of all jobs in any priority
	*/
	size_t Size(size_t priority) const;
	size_t Size() const;


	/*
		true if specified queue is empty
		or if all queues are empty
	*/
	bool Empty(size_t priority) const;
	bool Empty() const;


private:

	Cur * cur;
	Functions * functions;
	LoadAvg * load_avg;
	Mounts * mounts;
	std::list<Request> * req_tab;

	typedef std::queue<JobTask> JobsQueue;
	typedef std::vector<JobsQueue> JobsQueueTab;
	JobsQueueTab jobs_queue_tab;
	std::multimap<long, RequestJobBase*> request_jobs;

	void CheckPriority(size_t & priority) const;
	void SaveToFile();
	void ReadFromFile();


	/*
		second thread
	*/

	// standard winix jobs
	// Image image;
	// sending emails
	// etc.

	bool SignalReceived();
	void Do();
	void DoQueue(JobsQueue & jobs_queue, size_t priority);
	void DoJob(JobTask & task, size_t priority);
	void DoRequestContinuationJob(JobTask & job_task, size_t priority);
	void DoWinixJob(pt::Space & job);
	void RemoveOldRequest(Request * request);
	void RemoveAllJobsRequests();
	PluginRes DoRequestJobs(JobTask & task, long job_id, size_t priority);

};


} // namespace Winix


#endif

