/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_core_ipbancontainer
#define headerfile_winix_core_ipbancontainer

#include <vector>
#include "ipban.h"
#include "winixmodeldeprecated.h"



namespace Winix
{



class IPBanContainer : public WinixModelDeprecated
{
public:

	IPBanContainer();

	IPBan & AddIP(int ip);
	IPBan * FindIP(int ip);
	void    Sort();
	size_t  Size();
	IPBan & GetIPBan(size_t index);
	void    SetMaxSize(size_t soft_size, size_t size);
	void    RemoveIP(int ip);
	void    Clear();
	bool    IsSorted();

private:

	std::vector<IPBan> ipban_tab;
	bool is_ipban_tab_sorted;
	size_t soft_max_size, max_size;
	std::vector<size_t> sort_helper_tab;

	static bool SortIPBansFunction(const IPBan & ip1, const IPBan & ip2);
	void RemoveOldRecords();
	void PrintTab();
	void PrintTab2();

	struct SortByLastUsedHelper
	{
		IPBanContainer * container;

		SortByLastUsedHelper(IPBanContainer * c) : container(c) {}
		bool operator()(size_t index1, size_t index2);
	};

};


} // namespace Winix


#endif

