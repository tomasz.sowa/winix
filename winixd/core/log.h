/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_log
#define headerfile_winix_core_log


#include <sstream>
#include <fstream>
#include <iostream>
#include <string>

#include "logmanipulators.h"
#include "filelog.h"
#include "log/log.h"
#include "morm.h"



namespace Winix
{



class Log : public pt::Log
{
public:

	Log();
	virtual ~Log();

	virtual void SetDependency(Log * log);

	virtual void SetMaxRequests(int max_requests);

	virtual Log & operator<<(const void * s);
	virtual Log & operator<<(const char * s);
	virtual Log & operator<<(const std::string * s);
	virtual Log & operator<<(const std::string & s);
	virtual Log & operator<<(const wchar_t * s);
	virtual Log & operator<<(const std::wstring * s);
	virtual Log & operator<<(const std::wstring & s);
	virtual Log & operator<<(char s);
	virtual Log & operator<<(unsigned char);
	virtual Log & operator<<(wchar_t s);
	virtual Log & operator<<(char32_t s);
	virtual Log & operator<<(bool);
	virtual Log & operator<<(short);
	virtual Log & operator<<(int s);
	virtual Log & operator<<(long s);
	virtual Log & operator<<(long long);
	virtual Log & operator<<(unsigned short);
	virtual Log & operator<<(unsigned int);
	virtual Log & operator<<(unsigned long);
	virtual Log & operator<<(unsigned long long);
	virtual Log & operator<<(float);
	virtual Log & operator<<(double s);
	virtual Log & operator<<(long double);

	virtual Log & operator<<(const pt::Stream & stream);
	virtual Log & operator<<(const pt::Space & space);
	virtual Log & operator<<(const pt::Date & date);

	virtual Log & operator<<(LogManipulators m);
	virtual Log & operator<<(pt::Log::Manipulators m);
	virtual Log & operator<<(morm::Model & model);


	virtual void PrintDate(const pt::Date & date);

	template<typename char_type, size_t stack_size, size_t heap_block_size>
	Log & operator<<(const pt::TextStreamBase<char_type, stack_size, heap_block_size> & buf);


	/*
	virtual Log & put_string(const std::string & value, size_t max_size);
	virtual Log & put_string(const std::wstring & value, size_t max_size);

	virtual Log & put_binary_blob(const char * blob, size_t blob_len);
	virtual Log & put_binary_blob(const std::string & blob);
	*/

	//virtual Log & SystemErr(int err);


private:

	// current request for logging
	// starts from zero and incremented after logendrequest modifier
	int request;
	
	// how many request to save at once
	int max_requests;

};






template<typename char_type, size_t stack_size, size_t heap_block_size>
Log & Log::operator<<(const pt::TextStreamBase<char_type, stack_size, heap_block_size> & buf)
{
	pt::Log::operator<<(buf);
	return *this;
}



} // namespace Winix



#endif

