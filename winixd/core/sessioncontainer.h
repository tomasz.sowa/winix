/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_sessioncontainer
#define headerfile_winix_core_sessioncontainer

#include <list>
#include <map>
#include <ctime>

#include "session.h"
#include "cur.h"
#include "config.h"
#include "winixmodeldeprecated.h"



namespace Winix
{




class SessionContainer : public WinixModelDeprecated
{
public:

	typedef std::list<Session>       Table;
	typedef Table::iterator          Iterator;
	typedef std::map<long, Iterator> IndexId;
	typedef IndexId::iterator        IdIterator;


	SessionContainer();

	void SetCur(Cur * pcur);
	void SetConfig(Config * pconfig);
	void SetTmpSession(Session * psession);

	void Clear();

	size_t    Size();
	Iterator  Begin();
	Iterator  End();
	Session & Back();
	Iterator AddSession(long id);
	Iterator FindById(long);

	IdIterator IdBegin();
	IdIterator IdEnd();

	void EraseById(long id);

	bool ChangeSessionId(Iterator ses, long new_id);
	bool ChangeSessionId(long old_id, long new_id);

private:

	Table table;
	IndexId index_id;

	Cur     * cur;
	Config  * config;
	Session * tmp_session;

	Session empty_session;

	// in FreeBSD implementation (GCC) list::size() has linear complexity
	// so we use our own table_size with O(1)
	size_t table_size;

};



} // namespace Winix


#endif
