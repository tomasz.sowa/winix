/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "timezones.h"
#include "misc.h"
#include "log.h"


namespace Winix
{



TimeZones::TimeZones()
{
	Clear();
}


void TimeZones::Clear()
{
	zone_tab.clear();

	for(size_t i=0 ; i<zone_indices.size() ; ++i)
		zone_indices[i] = size_t(-1);
}


void TimeZones::SetTimeZoneMaxId(size_t max_id)
{
	if( max_id > 1000 )
	{
		max_id = 1000;
		log << log1 << "TZ: time_zone_max_id is too big (changed to 1000)" << logend;
	}

	size_t old_size = zone_indices.size();
	zone_indices.resize(max_id + 1);

	for(size_t i=old_size ; i<zone_indices.size() ; ++i)
		zone_indices[i] = size_t(-1);
}



bool TimeZones::HasZone(size_t zone_id)
{
	if( zone_id < zone_indices.size() )
		return zone_indices[zone_id] < zone_tab.size();

return false;
}


TimeZone * TimeZones::GetZone(size_t zone_id)
{
	if( zone_id < zone_indices.size() )
	{
		size_t index = zone_indices[zone_id];

		if( index < zone_tab.size() )
			return &zone_tab[index];
	}

return 0;
}


TimeZone * TimeZones::GetZoneByIndex(size_t zone_index)
{
	if( zone_index < zone_tab.size() )
		return &zone_tab[zone_index];

return 0;
}


size_t TimeZones::Size() const
{
	return zone_tab.size();
}


bool TimeZones::Empty() const
{
	return zone_tab.empty();
}





void TimeZones::ParseZones()
{
	pt::Space::TableType * timezones_table = temp_space.get_table(L"timezones");

	if( timezones_table )
	{
		for(pt::Space * zone : *timezones_table)
		{
			temp_zone.Clear();

			if( temp_zone.SetTz(*zone) )
			{
				if( !HasZone(temp_zone.id) )
				{
					if( temp_zone.id < zone_indices.size() )
					{
						zone_tab.push_back(temp_zone);
						zone_indices[temp_zone.id] = zone_tab.size() - 1;
					}
					else
					{
						log << log1 << "Tz: zone: " << temp_zone.name << " has too big id: "
							<< temp_zone.id << " (skipping)" << logend;
					}
				}
				else
				{
					log << log1 << "Tz: zone with id: " << temp_zone.id
						<< " already exists (skipping)" << logend;
				}
			}
			else
			{
				log << log1 << "System: problem with reading time zone info from time zone: "
					<< zone->to_wstr(L"name") << " (skipping) " << logend;
			}
		}
	}
}



// !! IMPROVE ME
// in the future we do not have to read the whole file
// just space by space (not implemented in Space at the moment)
bool TimeZones::ReadTimeZones(const wchar_t * path)
{
	zone_tab.clear();
	pt::SpaceParser::Status status = parser.parse_space_file(path, temp_space);

	if( status == pt::SpaceParser::ok )
	{
		ParseZones();
		log << log2 << "Tz: time zones loaded, there are " << zone_tab.size() << " zones" << logend;
	}
	else
	if( status == pt::SpaceParser::syntax_error )
	{
		log << log1 << "TZ: error in time zone file, line: " << parser.get_last_parsed_line() << logend;
	}
	else
	if( status == pt::SpaceParser::cant_open_file )
	{
		log << log1 << "TZ: I cannot open the time zone file: " << path << logend;
	}

	temp_space.clear();

return status == pt::SpaceParser::ok;
}



bool TimeZones::ReadTimeZones(const std::wstring & path)
{
	return ReadTimeZones(path.c_str());
}





} // namespace Winix

