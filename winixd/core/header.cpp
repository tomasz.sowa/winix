/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "header.h"


namespace Winix
{

const wchar_t * Header::find_status_string_value(int http_status)
{
	size_t table_len = sizeof(status_int_string_map) / sizeof(StatusIntStringMapHelper);

	for(size_t i=0 ; i < table_len ; ++i)
	{
		if( status_int_string_map[i].status_int == http_status )
		{
			return status_int_string_map[i].status_str;
		}
	}

	return nullptr;
}


void Header::prepare_status_value(int http_status, pt::WTextStream & value, bool clear_value)
{
	if( clear_value )
		value.clear();

	value << http_status;
	const wchar_t * value_str = find_status_string_value(http_status);

	if( value_str )
	{
		value << ' ' << value_str;
	}
}


bool Header::is_header_value_char_correct(wchar_t c)
{
	/*
	 * make sure to not allow at least \r or \r
	 */
	return c > 32 && c < 127;
}


bool Header::is_header_value_correct(const wchar_t * str)
{
	for( ; *str ; ++str)
	{
		if( !is_header_value_char_correct(*str) )
		{
			return false;
		}
	}

	return true;
}


bool Header::is_header_value_correct(const std::wstring & str)
{
	/*
	 * dont use is_header_value_correct(str.c_str()) as there can be a null character (0) inside the string
	 */
	for(size_t i=0 ; i < str.size() ; ++i)
	{
		if( !is_header_value_char_correct(str[i]) )
		{
			return false;
		}
	}

	return true;
}



}


