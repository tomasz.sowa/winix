/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2009-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 

#ifndef headerfile_winix_core_mounts
#define headerfile_winix_core_mounts


#include <map>
#include <string>
#include <vector>
#include "mount.h"
#include "mountparser.h"
#include "winixmodeldeprecated.h"


namespace Winix
{
class Dirs;
class Request;


class Mounts : public WinixModelDeprecated
{
public:

	
	void SkipStaticDirs(bool skip);

	/*
		mount point's types
	*/
	int AddMountType(const wchar_t * type);
	int AddMountType(const std::wstring & type);
	const std::wstring & GetMountType(int id);

	// id of a specific mount type (the id is always valid)
	int MountTypeCms()    { return mount_type_cms; }
	int MountTypeStatic() { return mount_type_static; }

	// return -1 if there is no such a mount type
	// or index otherwhise
	int FindMountType(const std::wstring & type);


	/*
		file systems
	*/
	int AddMountFs(const wchar_t * fs);
	int AddMountFs(const std::wstring & fs);
	const std::wstring & GetMountFs(int id);

	// id of a specific file system (the id is always valid)
	int MountFsSimplefs() { return mount_fs_simplefs; }
	int MountFsHashfs()   { return mount_fs_hashfs; }


	/*
		mount point's parameters
	*/
	int AddMountPar(const wchar_t * par);
	int AddMountPar(const std::wstring & par);
	const std::wstring & GetMountPar(int id);

	int MountParPage()					{ return mount_par_page; }
	int MountParThumbSize()				{ return mount_par_thumb_size; }
	int MountParThumbMode()				{ return mount_par_thumb_mode; }
	int MountParThumbQuality()			{ return mount_par_thumb_quality; }
	int MountParImageSize()				{ return mount_par_image_size; }
	int MountParImageMode()				{ return mount_par_image_mode; }
	int MountParImageQuality()			{ return mount_par_image_quality; }
	int MountParEmacsOn()				{ return mount_par_emacs_on; }
	int MountParMkdirOn()				{ return mount_par_mkdir_on; }
	int MountParApp()					{ return mount_par_app; }
	int MountParHtmlTemplate()			{ return mount_par_html_template; }
	int MountParChangeTemplate()		{ return mount_par_change_template; }
	int MountParStatic()				{ return mount_par_static; }
	int MountParCss()					{ return mount_par_css; }
	int MountParLang()					{ return mount_par_lang; }

	void SetDirs(Dirs * pdirs);

	// dir_id, mount_point
	typedef std::map<long, Mount> MountTab;

	Mounts();
	void CreateMounts();

	void ReadMounts(const std::wstring & mounts);
	void ReadMounts();

	Mount * CalcCurMount(Request * request);
	Mount * CalcMount(long dir_id);
	
	// current mount point
	// will not be null after calling CalcCurMount() or ReadMounts([...])
	// !! nie korzystac obecnie z niego
	// korzystac z cur->mount
	// a tez zostanie wycofany
	Mount * pmount;


	const MountTab * GetMountTab();

	// at the beginning used to initialize cur->mount
	Mount * GetEmptyMount();

private:

	Dirs * dirs;

	bool skip_static;

	Mount empty_mount;
	const std::wstring empty_str;

	MountParser mount_parser;

	std::vector<std::wstring> mount_type_tab;

	int mount_type_cms;
	int mount_type_static;

	// simplefs
	// hashfs
	std::vector<std::wstring> mount_fs_tab;
	
	int mount_fs_simplefs;
	int mount_fs_hashfs;

	std::vector<std::wstring> mount_par_tab;

	int mount_par_page;
	int mount_par_thumb_size;
	int mount_par_thumb_mode;
	int mount_par_thumb_quality;
	int mount_par_image_size;
	int mount_par_image_mode;
	int mount_par_image_quality;
	int mount_par_emacs_on;
	int mount_par_mkdir_on;
	int mount_par_app;
	int mount_par_html_template;
	int mount_par_change_template;
	int mount_par_static;
	int mount_par_css;
	int mount_par_lang;
	

	MountTab mount_tab;

	void CreateMountType();
	void CreateMountFs();
	void CreateMountPar();

	void MountCmsForRoot();
};


} // namespace Winix



#endif
