/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_core_plugin
#define headerfile_winix_core_plugin

#include <vector>
#include <string>
#include <map>
#include "pluginmsg.h"
#include "plugindata.h"
#include "winixmodeldeprecated.h"
#include "modelconnector.h"
#include "mount.h"




namespace Winix
{
class Db;
struct Cur;
class System;
class Functions;
class Templates;
class SessionManager;

class WinixModelDeprecated;
class WinixRequest;



/*
	all your plugin functions can have signature either:
	void my_function(PluginInfo & info); or
	void my_function();

	only the main Init should have:
	extern "C" void Init(PluginFunction & info);

	in the Init you can add your own functions by using plugin.Assign() method
	and you can set the name of the plugin by setting info.p1 pointer 
	to a string buffer (const wchar_t *)
	(this buffer will not be copied so it should not be destroyed after Init finishes)
	also in Init you can only use logger (log) info.config and info.db objects
	(the rest winix objects are not initialized yet)
*/


class Plugin;
class Session;
class Request;


// move me to a different file
// may it should be based on WinixModelDeprecated?
struct PluginInfo
{
	// these variables are used for some purposes
	// depending on a hook in which they are used
	void * p1;
	void * p2;
	long l1;
	long l2;

	// unique plugin identifier
	int plugin_id;
	

	// objects from winix which are accessible from a plugin
	Config    * config;
	Cur       * cur;
	System    * system;
	Functions * functions;
	Templates * templates;
	Synchro   * synchro;
	SessionManager * session_manager;

	Log log;

	Plugin * plugin;

	morm::ModelConnector * model_connector;

	// pointer to the plugin session (can be null if not set by the plugin or if session is null)
	// this is taken from session->plugin_data.Get()
	// you should use WINIX_SESSION_CREATED and WINIX_PLUGIN_SESSION_DATA_REMOVE
	// to create your plugin's session data
	PluginDataBase * plugin_data_base; // !! zmienic nazwe na plugin_session_base ? a moze session_base; a moze plugin_session?

	// whether or not there is a res (bool) value set
	// default: true
	// if set to false then the 'res' is not taken into account when calculating how many plugins
	// have returned true or false
	bool has_res;

	// function return status
	// default: false (if not set by the plugin)
	bool res;


	void Clear()
	{
		// pointers to winix objects are not cleared here

		p1 = 0;
		p2 = 0;
		l1 = 0;
		l2 = 0;

		plugin_id			= -1;
		plugin_data_base	= 0;

		has_res = true;
		res = false;
	}

};


/*
	this structure tells how many plugins returned true and false
*/
struct PluginRes
{
	int res_false;
	int res_true;

	PluginRes()
	{
		res_false = 0;
		res_true  = 0;
	}
};



class Plugin : public WinixModelDeprecated
{
public:	

	// index of a plugin which is called by CallAllRegisteredMessages() method
	// normally: -1
	int current_plugin;

	// Fun is a type of a function you should provide in your plugin
	typedef void (*Fun1)(PluginInfo &);
	typedef void (*Fun2)(void);


	struct Slot
	{
		Fun1 fun1;
		Fun2 fun2;
		int index; // plugin index (which plugin has inserted the slot)
		bool is_running;

		Slot()
		{
			fun1  = 0;
			fun2  = 0;
			index = -1;
			is_running = false;
		}
	};

	struct PluginsItem
	{
		void * handle;
		const wchar_t * plugin_name; // plugin name (can be null if was not set by the plugin)
	};

	typedef std::vector<PluginsItem> Plugins;


	Plugin();
	~Plugin();

	void SetSystem(System * psystem);
	void SetCur(Cur * cur);
	void SetFunctions(Functions * pfunctions);
	void SetTemplates(Templates * ptemplates);
	void SetSessionManager(SessionManager * psession_manager);

	void SetWinixRequest(WinixRequest * winix_request);

	void LoadPlugin(const wchar_t * filename);
	void LoadPlugin(const std::wstring & filename);

	void LoadPlugins(const std::wstring & plugins_dir, const std::vector<std::wstring> & plugins);
	void UnloadPlugins();
	
	bool HasPlugin(const wchar_t * name);
	bool HasPlugin(const std::wstring & name);

	bool HasMessage(int message);

	PluginRes Call(int message);
	PluginRes Call(int message, void * p1_);
	PluginRes Call(int message, void * p1_, void * p2_);
	PluginRes Call(int message, long l1_);
	PluginRes Call(int message, long l1_, long l2_);
	PluginRes Call(int message, void * p1_, long l1_);
	PluginRes Call(int message, void * p1_, long l1_, long l2_);
	PluginRes Call(int message, void * p1_, void * p2_, long l1_);
	
	PluginRes Call(morm::ModelConnector * model_connector, Log * plog, Cur * cur, int message, void * p1 = nullptr, void * p2 = nullptr, long l1 = 0, long l2 = 0);
	PluginRes Call(morm::ModelConnector * model_connector, Log * plog, Session * session, Request * request, Mount * mount, int message, void * p1 = nullptr, void * p2 = nullptr, long l1 = 0, long l2 = 0);

	// how many plugins there are
	size_t Size();

	// assign a function to a message
	// you can assign more than one function to a specific message
	void Assign(int message, Fun1);
	void Assign(int message, Fun2);

	// return a const pointer to the plugin tab
	const Plugins * GetPlugins();

private:	

	Cur       * cur;
	System    * system;
	Functions * functions;
	Templates * templates;
	SessionManager * session_manager;

	WinixRequest * winix_request; // is it needed anymore? !!!!!!!!!!!!!!!!

	std::wstring temp_path;	// used when loading plugins

	Plugins plugins;
	
	typedef std::multimap<int, Slot> Slots;
	Slots slots;

	void * LoadInitFun(const wchar_t * filename, Fun1 & fun_init);
	void Call(morm::ModelConnector * model_connector, Log * plog, Cur * cur, int message, Slots::iterator & slot, PluginInfo & info);


	bool SetDependencyForPluginInfo(morm::ModelConnector * pmodel_connector, Log * plog, Cur * pcur, PluginInfo & info);
	void Lock();
	void Unlock();

	friend PluginInfo;
};



} // namespace Winix


#endif

