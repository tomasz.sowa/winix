/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2008-2023, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef headerfile_winix_core_postmultiparser
#define headerfile_winix_core_postmultiparser

#include <string>
#include <fcgiapp.h>
#include <fstream>
#include "error.h"
#include "request.h"
#include "config.h"
#include "misc.h"
#include "winixbase.h"


namespace Winix
{



// 2 MB
#define WINIX_POSTMULTI_INPUT_BUFFER  2097152
#define WINIX_POSTMULTI_OUTPUT_BUFFER 2097152


class PostMultiParser : public WinixBase
{

public:

	PostMultiParser();
	PostMultiParser(const PostMultiParser &);
	PostMultiParser & operator=(const PostMultiParser &);
	~PostMultiParser();

	void SetConfig(Config * pconfig);
	Error Parse(FCGX_Stream * in_, Request & request, bool copy_raw_post);

private:

	Config * config;

	FCGX_Stream * in;
	unsigned char * in_buffer;
	std::ofstream tmp_file;
	std::wstring tmp_filename;
	std::string atmp_filename;
	int tmp_filename_postfix;
	size_t in_buffer_ind;
	size_t in_buffer_len;
	bool copy_raw_post;

	Request * request;

	int last; // last read character
	int var_index; // used as a postfix to the same name (is auto increment)

	bool line_end_dos;
	std::string boundary;
	std::string content;
	size_t content_len;

	std::string header_name, header_value;
	Error err;

	std::string name, filename;
	std::wstring namew, contentw;
	PostFile post_file_temp;

	void LogFirst(const std::string & to_log, size_t len);
	void ConvStr(const std::string & src, std::wstring & dst);

	bool IsWhite(int c);
	void SkipWhite();

	void AddNormalPostVar();
	void AddFilePostVar();
	void AddPostVar();

	void ReadBoundary();

	bool IsHeader();
	void ReadHeaderName();
	void ReadHeaderValue();
	void ReadPartHeader();

	void CreateTmpFile();
	bool HasBoundary();
	void ReadContentSkipBoundary(bool has_boundary);
	void ReadContentToFileLoop();
	void ReadContentToFile();
	void ReadContentLoop();
	void ReadContent();
	void CheckBoundaryEnd();
	void ReadPart();

	void ReadChar();


	template<class Container, class Value>
	bool InsertPostVar(Container & container, std::wstring & key, const Value & value);

};



template<class Container, class Value>
bool PostMultiParser::InsertPostVar(Container & container, std::wstring & key, const Value & value)
{
bool added;
std::pair<typename Container::iterator, bool> res;

	res = container.insert( std::make_pair(key, value) );
	added = res.second;

	if( !added )
	{
		key += L"_inc";
		pt::Toa(var_index, key, false, 10, 5);
		res = container.insert( std::make_pair(key, value) );
		added = res.second;
		var_index += 1;
	}

return added;
}



} // namespace Winix



#endif
