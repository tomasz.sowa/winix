/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_sessionmanager
#define headerfile_winix_core_sessionmanager

#include <set>
#include <ctime>

#include "sessioncontainer.h"
#include "ipbancontainer.h"
#include "config.h"
#include "request.h"
#include "lastcontainer.h"
#include "synchro.h"
#include "basethread.h"
#include "sessionidmanager.h"



namespace Winix
{



class SessionManager : public BaseThread
{
public:

	SessionManager();

	void SetCur(Cur * pcur);
	void SetSystem(System * psystem);
	void SetLastContainer(LastContainer * plast_container);

	void set_dependency(WinixModelDeprecated * winix_model);

	// can return a null pointer
	Session * FindSession(long id);

	Session * PrepareSession();

	// some functions require a session (need_session flag) so if there was
	// a temporary session and a function requires a session then we create a new session
	Session * CheckIfFunctionRequireSession();

	void DeleteSessions(); // deleting all sessions
	bool ChangeSessionId(long old_id);

	void IncrementBanLevel(IPBan * ip_ban);

	void InitTmpSession();
	void InitBanList();
	void InitCookieEncoding();

	void UninitTmpSession();

	Session * GetTmpSession();
	Session * GetCurSession();

	void LoadSessions();
	void SaveSessions();

	SessionContainer::Iterator SessionBegin();
	SessionContainer::Iterator SessionEnd();

	size_t Size();
	size_t MarkAllSessionsToRemove(long user_id);

	IPBan & AddIPToBanList(int ip);
	IPBan & AddIPToBanList(int ip, time_t last_used);
	size_t  BanListSize();
	IPBan & GetIPBan(size_t index);
	void    RemoveIPBan(int ip);
	void    ClearIPBanList();


	bool EncodeSessionId(long id, unsigned int index, std::wstring & str);


private:

	Cur     * cur;
	System  * system;
	LastContainer * last_container;

	Session * session;
	SessionContainer session_tab;
	IPBanContainer   ban_tab;
	IPBan * current_ip_ban;
	Session temporary_session;
	SessionIdManager session_id_manager;

	bool IsSession(long s);
	long CreateSessionId();
	void CreateSession();
	bool IsSessionCorrect(long id, bool has_index, unsigned int index, const SessionContainer::Iterator & s, unsigned int & difference);
	bool SetSessionFromCookie(long id, bool has_index, unsigned int index);
	bool SetSessionFromCookie(const std::wstring & cookie);
	void SetTemporarySession();
	unsigned int CalculateIndexDifference(Session & ses, unsigned int index);
	void SetSessionPutLogInfo(Session & ses, bool has_index, unsigned int difference);
	bool IsIPBanned();
	void SetFirstExpirationTime(IPBan * ip_ban);
	void BrokenCookieCheckBan();
	void IncorrectSessionCheckBan();
	void NoSessionCookieWasSent();


	/*
	 * second thread
	 */
	int deleted;
	virtual void Work();
	void CheckSession(SessionContainer::Iterator & i);
	bool IsSessionOutdated(const Session & s) const;
	void DeleteSession(Session * del_session);
	void CheckWheterIPListIsSorted();

};


} // namespace Winix


#endif
