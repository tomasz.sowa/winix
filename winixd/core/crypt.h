/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2011-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_crypt
#define headerfile_winix_core_crypt

#include <string>
#include "run.h"
#include "config.h"
#include "models/user.h"
#include "winixbase.h"



namespace Winix
{



/*
	the kind of hashes we are able to obtain in winix
*/
#define WINIX_CRYPT_HASH_NONE		0
#define WINIX_CRYPT_HASH_MD4		1
#define WINIX_CRYPT_HASH_MD5		2
#define WINIX_CRYPT_HASH_SHA1		10
#define WINIX_CRYPT_HASH_SHA224		11
#define WINIX_CRYPT_HASH_SHA256		12
#define WINIX_CRYPT_HASH_SHA384		13
#define WINIX_CRYPT_HASH_SHA512		14



/*
	calculating hashes, encrypting and decrypting with RSA
*/
class Crypt : public WinixBase
{
public:


	void set_dependency(WinixBase * winix_base);


	//void SetConfig(Config * pconfig);


	/*
		calculating a hash from a given input

		input:
			hash  - the kind of the hash - WINIX_CRYPT_HASH_*
			in    - input buffer
			inlen - the length of the buffer
		output:
			out   - the hash in binary form
	*/
	bool HashBin(int hash, const char * in, size_t inlen, std::string & out);
	bool HashBin(int hash, const char * in,               std::string & out);
	bool HashBin(int hash, const std::string & in,        std::string & out);


	/*
		calculating a hash from a given input
		the input string is first changed to UTF8 and then hash is calculated

		input:
			hash  - the kind of the hash - WINIX_CRYPT_HASH_*
			in    - input buffer
			inlen - the length of the buffer
		output:
			out   - the hash in binary form
	*/
	bool HashBin(int hash, const wchar_t * in, size_t inlen, std::string & out);
	bool HashBin(int hash, const wchar_t * in,               std::string & out);
	bool HashBin(int hash, const std::wstring & in,          std::string & out);


	/*
		calculating a hash from a given input

		input:
			hash  - the kind of the hash - WINIX_CRYPT_HASH_*
			in    - input buffer
			inlen - the length of the buffer
		output:
			out   - the hash in the hex form (one byte is saved as two hex digits)
	*/
	bool HashHex(int hash, const char * in, size_t inlen,    std::string & out);
	bool HashHex(int hash, const char * in,                  std::string & out);
	bool HashHex(int hash, const std::string & in,           std::string & out);


	/*
		calculating a hash from a given input
		the input string is first changed to UTF8 and then hash is calculated

		input:
			hash  - the kind of the hash - WINIX_CRYPT_HASH_*
			in    - input buffer
			inlen - the length of the buffer
		output:
			out   - the hash in the hex form (one byte is saved as two hex digits)
					the 'out' here is std::wstring (not std::string like beforehand)
	*/
	bool HashHex(int hash, const wchar_t * in, size_t inlen, std::wstring & out);
	bool HashHex(int hash, const wchar_t * in,               std::wstring & out);
	bool HashHex(int hash, const std::wstring & in,          std::wstring & out);


	/*
		encrypt/decrypt by using RSA algorithm

		input:
			encrypt  - true means encrypting, false means decrypting
			keypath  - path to a RSA private key (this is a private and public key in one file)
			in       - input buffer
			inlen    - the size of the buffer

		output:
			out      - encrypted or decrypted buffer (always binary)
	*/
	bool RSA(bool encrypt, const char * keypath,         const char * in, size_t inlen, std::string & out);
	bool RSA(bool encrypt, const char * keypath,         const std::string & in,        std::string & out);
	bool RSA(bool encrypt, const std::string & keypath,  const std::string & in,        std::string & out);
	bool RSA(bool encrypt, const wchar_t * keypath,      const char * in, size_t inlen, std::string & out);
	bool RSA(bool encrypt, const wchar_t * keypath,      const std::string & in,        std::string & out);
	bool RSA(bool encrypt, const std::wstring & keypath, const std::string & in,        std::string & out);


	/*
		this method creates a hash from the given plain text password

		input.
			salt         - salt for the hash
			up.pass_type - what kind of hash do you want - look at WINIX_CRYPT_HASH_* macros (in crypt.h)
			up.pass      - plain text password

			if salt is empty then the hash will not be salted

		output:
			up.pass_type - (can be changed to 0 when there is a problem with generating a hash)
			up.pass      - hash from the password (or plain text if up.pass_type was zero)
			up.pass_hash_salted (true if the hash is salted - when salt was not empty)

		if there is a problem with generating a hash the method stores a plain text password
		and changes up.pass_type to zero (plain text passwords are not salted)
	*/
	bool PassHash(const std::wstring & salt, User & user);


	/*
		this method encrypts the given password

		input:
			path_to_rsa_private_key - a path to rsa private key (this are a private and public keys both in one file)
			up.pass - given password (can be a plain text or a hash)

			if path_to_rsa_private_key is empty then the password will not be encrypted

		output:
			up.pass_encrypted

		if there is a problem (or the path to the key is empty) then up.pass_encrypted will be empty
		and the method returns false
	*/
	bool PassCrypt(const std::wstring & path_to_rsa_private_key, User & user);


	/*
		this method creates a hash from the given plain text password and then encrypts it

		input:
			salt         - salt for the hash
			path_to_rsa_private_key - a path to rsa private key (this are a private and public keys both in one file)
			up.pass_type - what kind of hash do you want - look at WINIX_CRYPT_HASH_* macros (in crypt.h)
			up.pass      - plain text password

			if salt is empty then the hash will not be salted
			if path_to_rsa_private_key is empty then the password will not be encrypted

		output:
			up.pass_type - (can be changed to 0 when there is a problem with generating a hash)
			up.pass      - hash from the password (or plain text if up.pass_type was zero)
			up.pass_hash_salted (true if the hash is salted - when salt was not empty)
			up.pass_encrypted - encrypted password (if not empty)
		
	*/
	void PassHashCrypt(const std::wstring & salt, const std::wstring & path_to_rsa_private_key, User & user);


	/*
		this method creates a hash from the given plain text password and then encrypts it

		input:
			up.pass - plain text password

		output:
			up.pass_type        - what kind of hash there is in up.pass
			up.pass             - hash from the password (or plain text if up.pass_type is zero)
			up.pass_hash_salted - true if the hash is salted (plain text are never salted)
			up.pass_encrypted   - encrypted password (if not empty)
	*/
	void PassHashCrypt(User & user);


	/*
		putting some characters into the string and then calling clear()
	*/
	template<class StringType>
	void ClearString(StringType & str);



private:

	//Config * config;
	Run run;
	std::string command, bufina, keypatha;
	//std::wstring pass_salted;//, pass_hashed;
	//std::string pass_hasheda, pass_encrypteda;

	std::wstring pass_salted, pass_org;
	std::string passa, out_temp;
	std::wstring empty;


	template<typename StringType>
	void ConvertToHexForm(const std::string & in, StringType & out);

	char ConvertToHexForm(int val);

};



template<typename StringType>
void Crypt::ClearString(StringType & str)
{
	for(size_t i=0 ; i<str.size() ; ++i)
		str[i] = 0x0c;

	str.clear();
}


template<typename StringType>
void Crypt::ConvertToHexForm(const std::string & in, StringType & out)
{
	out.clear();

	if( in.size() * 2 > out.capacity() )
		out.reserve(in.size() * 2);

	for(size_t i=0 ; i<in.size() ; ++i)
	{
		out += ConvertToHexForm(((unsigned char)in[i]) >> 4);
		out += ConvertToHexForm(((unsigned char)in[i]) & 0x0f);
	}
}



} // namespace Winix


#endif

