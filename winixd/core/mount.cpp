/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2009-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "mount.h"
#include "misc.h"


namespace Winix
{




Mount::Mount()
{
	Clear();
}


Mount::Mount(const Mount & m)
{
	operator=(m);
}


Mount & Mount::operator=(const Mount & m)
{
	dir_id = m.dir_id;
	type = m.type;
	fs = m.fs;
	param = m.param;

	return *this;
}



void Mount::Clear()
{
	dir_id = -1;
	type   = -1;
	fs     = -1;
	ClearParams();
}


void Mount::ClearParams()
{
size_t i;

	for(i=0 ; i<param.size() ; ++i)
		param[i].Clear();
}


bool Mount::IsPar(int code)
{
	if( code < 0 || code >= (int)param.size() )
		return false;

	if( !param[code].defined )
		return false;

return true;
}



bool Mount::IsArg(int code, const wchar_t * arg)
{
	ParamRow::ParamArg::iterator i;	

	if( code < 0 || code >= (int)param.size() )
		return false;

	if( !param[code].defined )
		return false;

	for(i=param[code].arg.begin() ; i!=param[code].arg.end() ; ++i)
	{
		if( *i == arg )
			return true;
	}

return false;
}


bool Mount::IsArg(int code, const std::wstring & arg)
{
	return IsArg(code, arg.c_str());
}


bool Mount::IsArg(int code, int arg)
{
	ParamRow::ParamArg::iterator i;	

	if( code < 0 || code >= (int)param.size() )
		return false;

	if( !param[code].defined )
		return false;

	for(i=param[code].arg.begin() ; i!=param[code].arg.end() ; ++i)
	{
		if( Toi(*i) == arg )
			return true;
	}

return false;
}


const std::wstring & Mount::Arg(int code, int arg) const
{
	if( code < 0 || code >= (int)param.size() )
		return empty_str;

	if( !param[code].defined )
		return empty_str;

	if( arg >= (int)param[code].arg.size() )
		return empty_str;

return param[code].arg[arg];
}


const std::wstring & Mount::FirstArg(int code) const
{
	return Arg(code, 0);
}



} // namespace Winix

