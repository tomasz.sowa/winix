/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_loadavg
#define headerfile_winix_core_loadavg

#include <ctime>
#include "winixbase.h"
#include "request.h"


namespace Winix
{




// in seconds
#define WINIX_LOADAVG_GRANULARITY1		2
#define WINIX_LOADAVG_GRANULARITY5		15
#define WINIX_LOADAVG_GRANULARITY15		45



class LoadAvg : public WinixBase
{
public:
	LoadAvg();
	~LoadAvg();
	LoadAvg & operator=(const LoadAvg & l);
	LoadAvg(const LoadAvg & l);

	void StartRequest(Request * req);
	void StopRequest(Request * req);

	double LoadAvgNow(); // load average withing last WINIX_LOADAVG_GRANULARITY1 seconds
	double LoadAvg1();
	double LoadAvg5();
	double LoadAvg15();

	double ReqPerSecNow();
	double ReqPerSec1();
	double ReqPerSec5();
	double ReqPerSec15();

private:

	struct Times
	{
		double dr; // time for the request (in seconds)
		double dp; // time for the pause between requestes(in seconds)
		long req;  // how many requests 

		void Clear()
		{
			dr  = 0.0;
			dp  = 0.0;
			req = 0;
		}

		Times & operator=(const Times & t)
		{
			dr  = t.dr;
			dp  = t.dp;
			req = t.req;

		return *this;
		}
	};

	timespec timespec_old_req_stop;

	void CheckTimers();
	void UpdateTimer1();
	void UpdateTimer5();
	void UpdateTimer15();

	Times current1;
	Times current5;
	Times current15;

	void CreateTable(size_t seconds, size_t granulatiry, Times* & tab, size_t & len);
	void CreateTable();

	void MoveTab(Times * tab, size_t len);
	void SumTab(Times * tab, size_t len, double expected, Times & t);

	void Calculate1();
	void Calculate5();
	void Calculate15();


	Times * tab1;
	size_t len1;

	Times * tab5;
	size_t len5;

	Times * tab15;
	size_t len15;

	double cache_load1;
	double cache_load5;
	double cache_load15;

	double cache_req_per_sec1;
	double cache_req_per_sec5;
	double cache_req_per_sec15;
};



} // namespace Winix



#endif
