/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_core_filelog
#define headerfile_winix_core_filelog

#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include "core/synchro.h"
#include "log/log.h"



namespace Winix
{
class TimeZones;
class Plugin;

class FileLog : public pt::FileLog
{
public:

	FileLog();
	virtual ~FileLog();

	void set_synchro(Synchro * synchro);
	void set_plugin(Plugin * plugin);

	// using pt::FileLog::init to suppress clang warning:
	// warning: 'Winix::FileLog::init' hides overloaded virtual function [-Woverloaded-virtual]
	using pt::FileLog::init;

	void init(const std::wstring & log_file, bool log_stdout, int log_level, bool save_each_line, size_t log_time_zone_id);

	void set_time_zones(TimeZones * time_zones);
	pt::Date get_local_date(const pt::Date & date);

	int get_log_level();
	bool should_save_each_line();

	void save_log(pt::WTextStream * buffer);



protected:

	size_t log_time_zone_id;

	TimeZones * time_zones;
	Synchro * synchro;
	Plugin * plugin; // can be null (only at the beginning when winix starts)


};


} // namespace Winix



#endif

