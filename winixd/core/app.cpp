/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <signal.h>
#include <utility>
#include <fastcgi.h>
#include <stddef.h>
#include "app.h"
#include "plugin.h"
#include "misc.h"
#include "functions/functions.h"
#include "utf8/utf8.h"
#include "convert/convert.h"
#include "models/migration.h"
#include "lock.h"



namespace Winix
{




App::App()
{
	stdout_is_closed = false;
	last_sessions_save = std::time(0);
	fcgi_socket = -1;
	request_id = 1;

	file_log.set_synchro(&synchro);
	file_log.set_time_zones(&system.time_zones);
	// 	file_log.set_plugin(...) is called later, when a plugin object is initialized

	log.set_log_buffer(&log_buffer);
	log.set_file_log(&file_log);

	// objects dependency for main thread
	winix_base.set_config(&config);
	winix_base.set_log_buffer(&log_buffer);
	winix_base.set_file_log(&file_log);
	winix_base.set_synchro(&synchro);

	winix_model.set_dependency(&winix_base);
	winix_model.set_plugin(&plugin);
	winix_model.set_model_connector(&model_connector);

	winix_system.set_dependency(&winix_model);
	winix_system.set_system(&system);

	winix_request.set_dependency(&winix_system);
	winix_request.set_cur(&cur);
	winix_request.set_session_manager(&session_manager);
	winix_request.set_locale(&TemplatesFunctions::locale);
	winix_request.set_model_connector(&model_connector);
	// //////////////////////////////////


	config.SetFileLog(&file_log);
	config.SetLogBuffer(&log_buffer);

	cur.request = nullptr; // they are first set in App::Init()
	cur.session = nullptr;
	cur.mount   = nullptr;

	plugin.set_dependency(&winix_model);
	plugin.SetCur(&cur);
	plugin.SetSystem(&system);
	plugin.SetFunctions(&functions);
	plugin.SetTemplates(&templates);
	plugin.SetSessionManager(&session_manager);
	plugin.SetWinixRequest(&winix_request);

	// now we can set the plugin in the file_log
	file_log.set_plugin(&plugin);

	functions.set_dependency(&winix_request);
//	functions.set_config(&config);
//	functions.set_file_log(&file_log);
//	functions.set_model_connector(&model_connector);
//	functions.set_synchro(&synchro);

	//functions.SetConfig(&config);
	functions.SetCur(&cur);
	functions.SetSystem(&system);
	functions.SetTemplates(&templates);
	//functions.SetSynchro(&synchro);
	functions.SetSessionManager(&session_manager);


	system.set_dependency(&winix_model);
	//system.SetConfig(&config);
	system.SetCur(&cur);
	//system.SetSynchro(&synchro);
	system.SetFunctions(&functions);
	system.SetSessionManager(&session_manager);

	templates.set_dependency(&winix_request);
	templates.SetConfig(&config);
	templates.SetCur(&cur);
	templates.SetSystem(&system);
	templates.SetFunctions(&functions);
	templates.SetSessionManager(&session_manager);

	session_manager.set_dependency(&winix_model); // zobaczyc czy wskoczy do przeciazonej metody w session manager
	session_manager.SetLastContainer(&system.users.last);
	session_manager.SetCur(&cur);
	session_manager.SetSystem(&system);

	post_multi_parser.set_dependency(&winix_base);
	post_multi_parser.SetConfig(&config);
}


void App::InitLoggers()
{
	file_log.init(config.log_file, config.log_stdout, config.log_level, config.log_save_each_line, config.log_time_zone_id);
	log.SetMaxRequests(config.log_request);
}


Log & App::GetMainLog()
{
	return log;
}


/*
 * chmod and chown of the socket are set before winix drops privileges
 */
bool App::InitFCGIChmodChownSocket(const char * sock)
{
	if( config.fcgi_set_socket_chmod )
	{
		if( chmod(sock, config.fcgi_socket_chmod) < 0 )
		{
			log << log1 << "App: I cannot chmod a FastCGI socket, check fcgi_socket_chmod in the config" << logend;
			return false;
		}
	}
	
	if( config.fcgi_set_socket_owner )
	{
		std::string sock_user, sock_group;
		pt::wide_to_utf8(config.fcgi_socket_user, sock_user);
		pt::wide_to_utf8(config.fcgi_socket_group, sock_group);

		passwd * pw = getpwnam(sock_user.c_str());

		if( !pw )
		{
			log << log1 << "App: there is no a user: " << config.fcgi_socket_user << logend;
			return false;
		}

		group * gr = getgrnam(sock_group.c_str());

		if( !gr )
		{
			log << log1 << "App: there is no a group: " << config.fcgi_socket_group << logend;
			return false;
		}
	
		if( chown(sock, pw->pw_uid, gr->gr_gid) < 0 )
		{
			log << log1 << "App: I cannot chown a FastCGI socket, check fcgi_socket_user "
						<< "and fcgi_socket_group in the config" << logend;
			return false;
		}
	}

return true;
}


bool App::InitFCGI()
{
	std::string sock, sock_user, sock_group;
	pt::wide_to_utf8(config.fcgi_socket, sock);

	unlink(sock.c_str());

	log << log3 << "App: trying to create a fcgi connection socket: " << sock << logend << logsave;
	fcgi_socket = FCGX_OpenSocket(sock.c_str(), config.fcgi_socket_listen);
	/*
	 * WARNING:
	 * when there is a problem with creating the socket then FCGX_OpenSocket will call exit()
	 * and we never reach here
	 */

	if( fcgi_socket < 0 )
	{
		log << log1 << "App: An error during creating a fcgi socket" << logend;
		return false;
	}

	log << log3 << "App: FastCGI socket number: " << fcgi_socket << logend;

	if( !InitFCGIChmodChownSocket(sock.c_str()) )
		return false;

	if( FCGX_Init() != 0 )
	{
		log << log1 << "App: FCGX_Init has failed" << logend;
		return false;
	}

	return true;
}


bool App::DoDatabaseMigration()
{
	bool ok = true;

	Migration migration;
	User user;
	ItemContent item_content;
	Item item;
	Group group;

	migration.set_connector(model_connector);
	migration.create_winix_schema();
	ok = ok && Migration::do_migration(&model_connector, migration);
	ok = ok && Migration::do_migration(&model_connector, user);

	/*
	 * do migration of ItemContent before Item
	 * Item::do_migration_to_3() requires that ItemContent should have new columns
	 *
	 */
	ok = ok && Migration::do_migration(&model_connector, item_content);
	ok = ok && Migration::do_migration(&model_connector, item);
	ok = ok && Migration::do_migration(&model_connector, group);

	return ok;
}


bool App::TryToMakeDatabaseMigration()
{
	if( config.db_make_migration_if_needed )
	{
		if( !DoDatabaseMigration() )
		{
			if( config.db_stop_if_migration_fails )
			{
				log << log1 << "App: database migration failed, stopping winix" << logend;
				return false;
			}
		}

		PluginRes res = plugin.Call(WINIX_MAKE_DATABASE_MIGRATION);

		if( config.db_stop_if_migration_fails && res.res_false > 0 )
		{
			log << log1 << "App: database migration of some plugins failed, stopping winix" << logend;
			return false;
		}
	}

	return true;
}


bool App::InitializePlugins()
{
	PluginRes plugin_res = plugin.Call(WINIX_PLUGIN_INIT);

	if( plugin_res.res_false > 0 )
	{
		log << log1 << "App: " << plugin_res.res_false << " plugin(s) cannot initialize itself, exiting" << logend;
		return false;
	}

	return true;
}


bool App::Init()
{
	cur.session = session_manager.GetTmpSession();
	cur.mount   = system.mounts.GetEmptyMount();;
	system.req_tab.resize(1);
	cur.request = &system.req_tab.back();
	InitializeNewRequest(*cur.request); // this will set cur.request->session (from cur.session) and cur.request->mount (from cur.mount)

	if( !config.db_conn_string.empty() )
		postgresql_connector.set_conn_param(config.db_conn_string);
	else
		postgresql_connector.set_conn_param(config.db_host, config.db_hostaddr, config.db_port, config.db_database, config.db_user, config.db_pass);

	postgresql_connector.set_logger(log);
	postgresql_connector.set_log_queries(config.log_db_query);

	if( !postgresql_connector.wait_for_connection(config.db_startup_connection_max_attempts, config.db_startup_connection_attempt_delay) )
	{
		return false;
	}

	model_connector.set_flat_connector(json_connector);
	model_connector.set_db_connector(postgresql_connector);
	model_connector.set_logger(log);
	model_connector.set_winix_config(&config);
	//model_connector.set_winix_request(&req);
	model_connector.set_winix_logger(&log);
	model_connector.set_winix_dirs(&system.dirs);
	model_connector.set_winix_mounts(&system.mounts);
	model_connector.set_winix_users(&system.users);
	model_connector.set_winix_groups(&system.groups);
	model_connector.set_winix_session(nullptr); // will be set for each request
	model_connector.set_winix_locale(&TemplatesFunctions::locale);
	model_connector.set_winix_session_manager(&session_manager);
	model_connector.set_winix_time_zones(&system.time_zones);
	model_connector.set_winix_pattern_cacher(&TemplatesFunctions::pattern_cacher);

	// load plugins before loading sessions - session_manager.LoadSessions()
	// because some of the plugins can init its own sessions datas
	plugin.LoadPlugins(config.plugins_dir, config.plugin_file);

	// CHECKME this will call WINIX_MAKE_DATABASE_MIGRATION, but WINIX_PLUGIN_INIT was not called yet, it is correct?
	if( !TryToMakeDatabaseMigration() )
		return false;

	compress.set_dependency(&winix_base);
	compress.Init();

	if( !system.Init() )
		return false;

	functions.Init();
	templates.Init(); // init templates after functions are created

	// init notify after templates (it uses locales from templates)
	system.notify.ReadTemplates();

	session_manager.InitTmpSession();
	session_manager.InitBanList();
	session_manager.InitCookieEncoding();
	session_manager.LoadSessions();

	CreateStaticTree();

	post_parser.set_dependency(&winix_model);
	post_parser.LogValueSize(config.log_post_value_size);
	// post_multi_parser has a pointer to the config

	cookie_parser.set_dependency(&winix_model);

	if( !AddSystemThreads() )
		return false;

	return InitializePlugins();
}


void App::Close()
{
	{
		Winix::Lock lock(synchro);

		plugin.Call(WINIX_PREPARE_TO_CLOSE);

		session_manager.SaveSessions();
		session_manager.DeleteSessions();
		session_manager.UninitTmpSession();
		functions.Finish();

		// now all sessions are cleared
	}

	WaitForThreads();
	// now all others threads are terminated

	/*
	 * at the moment plugins are removed when winix is finishing
	 * but in the future we can add plug-in removal before the end
	 */
	plugin.Call(WINIX_PLUGIN_QUIT);

	/*
	 * this is the last message for plugins
	 */
	plugin.Call(WINIX_QUIT);
}


void App::BaseUrlRedirect(int code, bool add_subdomain)
{
	system.PutUrlProto(cur.request->redirect_to);

	if( add_subdomain && !cur.request->subdomain.empty() )
	{
		cur.request->redirect_to += cur.request->subdomain;
		cur.request->redirect_to += '.';
	}

	cur.request->redirect_to += config.base_url;
	cur.request->redirect_to += cur.request->env_request_uri;
	// cur.request->env_request_uri should not be UrlEncoded because it contains slashes
	cur.request->redirect_type = code;
}


bool App::BaseUrlRedirect()
{
	plugin.Call(WINIX_BASE_URL_REDIRECT);

	if( !cur.request->redirect_to.empty() )
		return true;

	if( !config.base_url_redirect )
		return false;

	if( config.base_url.empty() )
		return false;

	if( cur.request->method == Request::post )
		return false;
	
	if( config.base_url == cur.request->env_http_host )
		return false;

	BaseUrlRedirect(config.base_url_redirect_code, false);
	log << log3 << "App: BaseUrlRedirect from: " << cur.request->env_http_host << logend;
	
return true;
}


void App::CheckIfNeedSSLredirect()
{
	if( cur.request->method == Request::post )
	{
		// something comes via POST, don't do the redirect because you lose the date
		return;
	}

	if( config.use_ssl )
	{
		if( config.use_ssl_only_for_logged_users )
		{
			bool function_need_ssl = (cur.request->function && cur.request->function->need_ssl);

			if( cur.request->using_ssl )
			{
				if( !cur.session->puser && !function_need_ssl )
				{
					log << log3 << "App: this operation should NOT be used through SSL" << logend;
					BaseUrlRedirect(config.use_ssl_redirect_code, true);
				}
			}
			else
			{
				if( cur.session->puser || function_need_ssl )
				{
					log << log3 << "App: this operation should be used through SSL" << logend;
					BaseUrlRedirect(config.use_ssl_redirect_code, true);
				}
			}
		}
		else
		{
			/*
			 * use ssl for everyone
			 */
			if( !cur.request->using_ssl )
			{
				log << log3 << "App: this operation should be used through SSL" << logend;
				BaseUrlRedirect(config.use_ssl_redirect_code, true);
			}
		}
	}
	else
	{
		if( cur.request->using_ssl )
		{
			log << log3 << "App: this operation should NOT be used through SSL" << logend;
			BaseUrlRedirect(config.use_ssl_redirect_code, true);
		}
	}
}


void App::SetLocale()
{
size_t locale_id;

	if( cur.session->puser )
	{
		locale_id = cur.session->puser->locale_id;

		if( !TemplatesFunctions::locale.HasLanguage(locale_id) )
			locale_id = config.locale_default_id;
	}
	else
	{
		locale_id = config.locale_default_id;
	}

	TemplatesFunctions::locale.SetCurLang(locale_id);
}


bool App::CheckAccessFromPlugins()
{
	PluginRes res = plugin.Call(WINIX_CHECK_PLUGIN_ACCESS);

	if( res.res_false > 0 )
	{
		cur.request->status = WINIX_ERR_PERMISSION_DENIED;
		cur.request->http_status = Header::status_403_forbidden;
		log << log2 << "App: access prevented by a plugin" << logend;
		return false;
	}

return true;
}


/*
 * REFACTOR ME
 */
void App::MakeRenameMeToABetterName()
{
	if( cur.request->function )
	{
		cur.request->function->fun.set_connector(model_connector); // IMPROVEME may would be better to add set_connector() method to functions?
		cur.request->function->fun.propagate_connector();
	}

	/*
	 * set global connector for now
	 * in the future each thread will have its own model_connector
	 *
	 * don't set connector for item_tab - it will be moved out from request
	 */
	cur.request->item.set_connector(model_connector);

	cur.session = session_manager.PrepareSession();
	cur.request->session = cur.session;
	model_connector.set_winix_session(cur.session);

	functions.CheckFunctionAndSymlink(); // here a function can be changed (and mount point but cur.mount and cur->request->mount will be set)

	if( cur.request->function )
	{
		cur.request->function->fun.set_connector(model_connector);
		cur.request->function->fun.propagate_connector();
	}

	cur.session = session_manager.CheckIfFunctionRequireSession();
	cur.request->session = cur.session;
	model_connector.set_winix_session(cur.session);

	SetLocale();

	if( cur.session->new_session )
	{
		cur.session->plugin_data.Resize(plugin.Size());
		plugin.Call(WINIX_SESSION_CREATED);
	}

	plugin.Call(WINIX_SESSION_CHANGED);


	////////////////////////
	cur.request->PrepareAnswerType();

	if( cur.session->ip_ban && cur.session->ip_ban->IsIPBanned() )
	{
		pt::Date date(cur.session->ip_ban->expires);

		// IMPROVE ME there is no slog now
		//slog << logerror << T("this_ip_is_banned_until") << ' ' << date << " UTC" << logend;

		cur.request->status = WINIX_ERR_PERMISSION_DENIED;
		cur.request->http_status = Header::status_403_forbidden;
	}

	if( cur.request->function )
	{
		cur.request->function->check_origin_header();
	}

	// cur.request->status can be changed by function_parser
	if( cur.request->status == WINIX_ERR_OK && cur.request->http_status == Header::status_200_ok )
		plugin.Call(WINIX_PREPARE_REQUEST);

//	if( cur.request->status == WINIX_ERR_OK )
//		functions.CheckFunctionAndSymlink();

	CheckAccessFromPlugins();

	// !! CHECK ME CheckFunctionAndSymlink can set redirect_to
	// may it should be tested before calling CheckIfNeedSSLredirect?
	CheckIfNeedSSLredirect();

	if( !cur.request->redirect_to.empty() )
		return;

	AddDefaultModels();

	// what about 204 No Content from preflight requests? should we make MakeOptions()?
	if( (cur.request->status == WINIX_ERR_OK && cur.request->http_status == Header::status_200_ok) ||
		(cur.request->method == Request::Method::options && cur.request->http_status == Header::status_204_no_content))
		functions.MakeFunction();

	if( cur.request->run_state == Request::RunState::normal_run )
	{
		if( cur.session->spam_score > 0 )
			log << log1 << "App: spam score: " << cur.session->spam_score << logend;

		if( cur.request->IsParam(L"noredirect") )
			cur.request->redirect_to.clear();

		if( cur.request->status == WINIX_ERR_OK && cur.request->http_status == Header::status_200_ok )
			plugin.Call(WINIX_PROCESS_REQUEST);

		CheckPostRedirect();

		if( !cur.request->redirect_to.empty() )
			return;

		if( cur.request->dir_tab.empty() )
		{
			log << log1 << "App: there is no a root dir (dir_tab is empty -- after calling a function)" << logend;
			return;
		}
	}
}


bool App::AddRootDir()
{
	Item * root_dir = system.dirs.GetRootDir();

	if( root_dir )
	{
		cur.request->dir_tab.push_back(root_dir);
		cur.request->last_item = cur.request->dir_tab.back();
	}
	else
	{
		log << log3 << "App: there is no a root directory" << logend;
 		cur.request->http_status = Header::status_404_not_found;
	}

	return root_dir != nullptr;
}


void App::ProcessRequestThrow()
{
	if( AddRootDir() )
	{
		ReadRequest();

		if( !BaseUrlRedirect() )
		{
			if( functions.Parse() )
			{
				ReadPostVars();
			}

			cur.mount = system.mounts.CalcCurMount(cur.request);
			cur.request->mount = cur.mount;

			if( cur.mount->type != system.mounts.MountTypeStatic() )
			{
				MakeRenameMeToABetterName();
			}
		}
	}
}


void App::ProcessRequest()
{
	system.load_avg.StartRequest(cur.request);
	log << log2 << config.log_delimiter << logend;

	try
	{
		ProcessRequestThrow();
	}
	catch(const std::exception & e)
	{
		log << log1 << "App: there was an exception: std::exception: " << e.what() << logend;
	}
	catch(const Error & e)
	{
		log << log1 << "App: there was an exception: Error: " << e << logend;
	}
	catch(...)
	{
		log << log1 << "App: there was an unknown exception" << logend;
	}

	try
	{
		if( cur.request->run_state == Request::RunState::normal_run )
		{
			cur.request->FinishRequest();
			system.load_avg.StopRequest(cur.request);
		}

		SaveSessionsIfNeeded();
	}
	catch(...)
	{
		log << log1 << "App: an exception when finishing a request" << logend;
	}

	try
	{
		if( cur.request->function )
			cur.request->function->clear();

		if( cur.request->run_state == Request::RunState::finished )
		{
			ClearAfterRequest();
		}
	}
	catch(...)
	{
		log << log1 << "App: an exception when clearing after a request" << logend;
	}
}


void App::ClearAfterRequest()
{
	// simple operations which should not throw an exception
	cur.request->Clear();
	cur.session->ClearAfterRequest();
	cur.session = session_manager.GetTmpSession();
	cur.mount = system.mounts.GetEmptyMount();
	system.mounts.pmount = cur.mount; // IMPROVE ME system.mounts.pmount will be removed
	// send_data_buf doesn't have to be cleared and it is better to not clear it (optimizing)

	model_connector.set_winix_request(nullptr);
	model_connector.set_winix_session(nullptr);

	log << logendrequest;
}


bool App::InitializeRequestForFastCGI(Request & request)
{
	bool request_initialized = false;

	while( !synchro.was_stop_signal && !request_initialized )
	{
		if( FCGX_InitRequest(&request.fcgi_request, fcgi_socket, FCGI_FAIL_ACCEPT_ON_INTR) == 0 )
		{
			request_initialized = true;
		}
		else
		{
			log << log1 << "App: FCGX_InitRequest fails, I cannot read a new request, I wait 3s and will try again..." << logend << logsave;
			sleep(config.fcgi_cannot_create_request_delay);
		}
	}

	return request_initialized;
}


void App::SetRequestDependency(Request & request)
{
	request.set_json_connector(&json_connector);
	request.set_xml_connector(&xml_connector);

	request.SetConfig(&config);
	request.SetTemplates(&templates);
	request.SetCompress(&compress);
	request.SetPlugin(&plugin);
	request.SetMounts(&system.mounts);

	request.session = cur.session;
	request.mount = cur.mount;

	request.set_connector(&model_connector);
	request.item.set_connector(&model_connector);
}


void App::InitializeNewRequest(Request & request)
{
	SetRequestDependency(request);
	request.Clear();
	InitializeRequestForFastCGI(request);
}



void App::PutRequestToJob()
{
	if( cur.request->session )
		cur.request->session->allow_to_delete = false;

	if( cur.request->dir_tab.size() > 1 )
	{
		// leave only the root dir, directories will be analyzed again
		cur.request->dir_tab.resize(1);
	}

	if( cur.request->is_item )
		cur.request->last_item = &cur.request->item;
	else
	if( !cur.request->dir_tab.empty() )
		cur.request->last_item = cur.request->dir_tab.back();
	else
		cur.request->last_item = nullptr;

	system.job.Add(cur.request->job_id, cur.request, cur.request->job, Job::PRIORITY_REQUEST_CONTINUATION);
	log << log3 << "App: this request (" << cur.request << ") has been moved to the job queue" << logend;
}


void App::Start()
{
	bool was_stop_signal = false;
	FCGX_Request * fcgi_request = nullptr;

	{
		Winix::Lock lock(synchro);
		was_stop_signal = synchro.was_stop_signal;
		fcgi_request = &cur.request->fcgi_request;
		plugin.Call(WINIX_STARTED);
	}

	while( !was_stop_signal && FCGX_Accept_r(fcgi_request) == 0 )
	{
		Winix::Lock lock(synchro);

		cur.request = &system.req_tab.back(); // cur.request could have been changed by the job (we have at least one object in req_tab)
		cur.session = session_manager.GetTmpSession();
		cur.mount   = system.mounts.GetEmptyMount();

		model_connector.set_flat_connector(json_connector);
		model_connector.set_winix_request(cur.request);
		xml_connector.set_putting_doctype(false);
		xml_connector.set_putting_root_element(false);

		cur.request->session = cur.session;
		cur.request->mount = cur.mount;
		cur.request->run_state = Request::RunState::normal_run;
		cur.request->id = request_id;
		cur.request->RequestStarts();
		bool do_process_request = true;

		if( synchro.was_stop_signal )
		{
			was_stop_signal = true;
			FCGX_Finish_r(&cur.request->fcgi_request);
			do_process_request = false;
		}
		else
		if( system.req_tab.size() > config.request_queue_job_limit )
		{
			log << log2 << "App: requests queue exceeded limit of " << config.request_queue_job_limit
				<< " requests (skipping this request)" << logend;

			cur.request->http_status = Header::status_503_service_unavailable;
			cur.request->FinishRequest();
			do_process_request = false;

			// although requests should be deleted at the end of the job
			// we want to make sure no old requests are left
			system.DeleteOldRequests(true); // true - leave at least one object
		}

		if( do_process_request )
		{
			ProcessRequest();
			model_connector.set_flat_connector(json_connector); // the flat connector could have been changed

			if( cur.request->run_state == Request::RunState::assigned_to_job )
			{
				PutRequestToJob();

				/*
				 * prepare a new request
				 */
				system.req_tab.resize(system.req_tab.size() + 1);
				cur.request = &system.req_tab.back();
				InitializeNewRequest(*cur.request); // cur.request->session, cur.request->mount, cur.request->id etc. are set when the request starts
			}
		}

		IncrementRequestId();
		fcgi_request = &cur.request->fcgi_request;
	}

	/*
	 * we do not wait for some job requests to finish (if there are such requests)
	 * the job loop will exit immediately when a synchro->is_stop_signal is defined
	 * and there is no need to process such requests (the www server will return a 50x error
	 * to the client)
	 */
}



void App::SaveSessionsIfNeeded()
{
	time_t t = std::time(0);

	if( last_sessions_save + 86400 > t )
		return;

	// saving once a day for safety
	last_sessions_save = t;
	session_manager.SaveSessions();
}


void App::CheckPostRedirect()
{
	if( cur.request->method == Request::post )
	{
		if( cur.request->IsParam(L"postredirect") )
		{
			cur.request->redirect_to   = cur.request->ParamValue(L"postredirect");
			cur.request->redirect_type = 303;
		}
		else
		if( cur.request->IsPostVar(L"postredirect") )
		{
			cur.request->redirect_to   = cur.request->PostVar(L"postredirect");
			cur.request->redirect_type = 303;
		}
	}
}


void App::AddDefaultModels()
{
	if( cur.request->function && cur.request->function->register_default_models )
	{
		// may it would be better do not return cur.request by default?
		cur.request->models.Add(L"request", cur.request);

		if( cur.session && cur.session->puser )
		{
			cur.request->models.Add(L"user", *cur.session->puser);
		}

		if( cur.request->is_item )
		{
			cur.request->models.Add(L"item", cur.request->item);
		}
	}
}


void App::LogEnvironmentVariables()
{
	for(char ** e = cur.request->fcgi_request.envp ; *e ; ++e )
		log << log1 << "Env: " << *e << logend;
}


void App::LogEnvironmentHTTPVariables()
{
	if( cur.request->headers_in.is_object() )
	{
		pt::Space::ObjectType::iterator i = cur.request->headers_in.value.value_object.begin();

		for( ; i != cur.request->headers_in.value.value_object.end() ; ++i)
		{
			log << log1 << "HTTP Env: " << i->first << "=";

			if( i->second->is_wstr() )
				log << *i->second->get_wstr() << logend;
			else
				log << "(incorrect value type, expected wstr)" << logend;
		}
	}
}


void App::ParseAcceptHeader(const wchar_t * header_name, const std::wstring & env, std::vector<HeaderValue> & container, size_t max_len)
{
	accept_base_parser.parse(env, container, max_len);

	if( !container.empty() )
	{
		log << log3 << "App: " << header_name << " header consists of: ";
		HeaderValue::log_values(container, log);
		log << logend;
	}
	else
	{
		log << log3 << "App: there is no " << header_name << " header" << logend;
	}
}


void App::ParseAcceptHeader()
{
	ParseAcceptHeader(
			Winix::Header::accept,
			cur.request->env_http_accept,
			cur.request->accept_mime_types,
			config.request_max_accept_fields);
}


void App::ParseAcceptLanguageHeader()
{
	ParseAcceptHeader(
			Winix::Header::accept_language,
			cur.request->env_http_accept_language,
			cur.request->accept_languages,
			config.request_max_accept_language_fields);
}



/*
 * reading the request (without GET parameters in the URL)
 */
void App::ReadRequest()
{
	cur.request->ReadEnvVariables();
	cur.request->ReadEnvRemoteIP();
	cur.request->CheckRequestMethod();
	cur.request->CheckSSL();
	cur.request->SetSubdomain();

	LogAccess();
	ReadEnvHTTPVariables();

	//ReadPostVars();

	if( config.log_env_variables )
		LogEnvironmentVariables();

	if( config.log_env_http_variables )
		LogEnvironmentHTTPVariables();

	ParseAcceptHeader();
	ParseAcceptLanguageHeader();

	accept_encoding_parser.ParseAndLog(cur.request->env_http_accept_encoding, log);
	cur.request->accept_deflate = accept_encoding_parser.AcceptDeflate();
	cur.request->accept_gzip = accept_encoding_parser.AcceptGzip();

	cookie_parser.Parse(cur.request->env_http_cookie, cur.request->cookie_tab);

	CheckIE();
	CheckKonqueror();
	CheckHtmx();

	if( cur.request->using_ssl )
		log << log3 << "App: connection secure through SSL" << logend;

}




// reading from fastcgi env
void App::ReadEnvHTTPVariables()
{
	const char http_prefix[] = "HTTP_";
	size_t http_prefix_len = sizeof(http_prefix) / sizeof(char) - 1; // 1 means terminating null character
	size_t http_headers_saved = 0;

	for(char ** e = cur.request->fcgi_request.envp ; *e && http_headers_saved < Request::MAX_INPUT_HEADERS ; ++e)
	{
		char * env = *e;

		if( pt::is_substr_nc("HTTP_", env) )
		{
			env += http_prefix_len;

			// cookies we have in a different table
			if( !pt::is_substr_nc("COOKIE=", env) )
			{
				if( SaveEnvHTTPVariable(env) )
				{
					http_headers_saved += 1;
				}
			}
		}
	}

	if( http_headers_saved == Request::MAX_INPUT_HEADERS )
	{
		log << log4 << "App: the maximum number of HTTP headers has been reached, skipping the rest" << logend;
	}
}


/*
 * headers in fcgi are in the form of name=value
 */
bool App::SaveEnvHTTPVariable(const char * env)
{
	// CHECK ME may move to a better place? Request::INPUT_HEADER_VALUE_MAX_LENGTH is a high value
	char header_name[Request::INPUT_HEADER_NAME_MAX_LENGTH + 1];
	char header_value[Request::INPUT_HEADER_VALUE_MAX_LENGTH + 1];

	size_t i = 0;

	for( ; env[i] != 0 && env[i] != '=' && i < Request::INPUT_HEADER_NAME_MAX_LENGTH ; ++i)
	{
		header_name[i] = pt::to_lower(env[i]);
	}

	header_name[i] = 0;

	if( env[i] != '=' )
	{
		// too long header name, skipping
		log << log4 << "App: skipped HTTP header \"" << env << "\" because the header name is too long" << logend;
		return false;
	}

	i += 1; // skipping '=' character
	size_t h = 0;

	for( ; env[i] != 0 && h < Request::INPUT_HEADER_VALUE_MAX_LENGTH ; ++i, ++h)
	{
		header_value[h] = env[i];
	}

	header_value[h] = 0;

	if( env[i] != 0 )
	{
		// too long header value, skipping
		log << log4 << "App: skipped HTTP header \"" << env << "\" because the header value is too long" << logend;
		return false;
	}

	pt::utf8_to_wide(header_name, http_header_name);
	pt::utf8_to_wide(header_value, http_header_value);

	cur.request->headers_in.add(http_header_name, http_header_value);
	http_header_name.clear();
	http_header_value.clear();

	return true;
}



void App::CheckHtmx()
{
	 // fastcgi will change the header to hx_request
	cur.request->is_htmx_request = (cur.request->headers_in.has_key(L"HX-Request") || cur.request->headers_in.has_key(L"hx_request"));

	if( cur.request->is_htmx_request && config.add_header_cache_no_store_in_htmx_request )
	{
		cur.request->out_headers.add(L"Cache-Control", L"no-store, max-age=0");
	}
}


void App::LogAccess()
{
	log << log1;
	log.PrintDate(cur.request->start_date);

	log << ' '
		<< cur.request->ip_str << ' '
		<< cur.request->env_request_method	<< ' '
		<< cur.request->env_http_host
		<< cur.request->env_request_uri		<< ' '
		<< cur.request->env_http_user_agent	<< logend;

	if( !cur.request->subdomain.empty() )
		log << log3 << "Subdomain: " << cur.request->subdomain << logend;
}


void App::ReadInputPostToBuffer()
{
	char buffer[1024];
	const int buffer_len = sizeof(buffer) / sizeof(char) - 1;
	int read_len;

	cur.request->raw_post.clear();
	cur.request->raw_post.reserve(1024 * 1024 * 5); // IMPROVEME add to config?

	do
	{
		read_len = FCGX_GetStr(buffer, buffer_len, cur.request->fcgi_request.in);

		if( read_len > 0 )
			cur.request->raw_post.write(buffer, read_len);
	}
	while( read_len == buffer_len );
}


void App::ParsePostJson()
{
	FunctionBase * fun = cur.request->function;

	space_parser.set_object_items_limit( (fun && fun->post_max_object_items != 0) ?   fun->post_max_object_items   : config.post_max_object_items);
	space_parser.set_table_items_limit(  (fun && fun->post_max_table_items != 0) ?    fun->post_max_table_items    : config.post_max_table_items);
	space_parser.set_all_items_limit(    (fun && fun->post_max_all_items != 0) ?      fun->post_max_all_items      : config.post_max_all_items);
	space_parser.set_nested_level_limit( (fun && fun->post_max_nested_objects != 0) ? fun->post_max_nested_objects : config.post_max_nested_objects);

	pt::SpaceParser::Status parse_status = space_parser.parse_json(cur.request->raw_post, cur.request->post_in);

	if( parse_status == pt::SpaceParser::ok )
	{
	}
	else
	if( parse_status == pt::SpaceParser::syntax_error )
	{
		log << log1 << "App: cannot parse the input stream as an JSON object"
			<< ", syntax error in line: " << space_parser.get_last_parsed_line() << ":" << space_parser.get_last_parsed_column() << logend;

		cur.request->post_in.clear();
		cur.request->http_status = Header::status_400_bad_request;
	}
	else
	if( parse_status == pt::SpaceParser::limit_object_items_exceeded )
	{
		log << log1 << "App: object items limit exceeded when parsing input JSON object" << logend;
		cur.request->post_in.clear();
		cur.request->http_status = Header::status_400_bad_request;
	}
	else
	if( parse_status == pt::SpaceParser::limit_table_items_exceeded )
	{
		log << log1 << "App: table items limit exceeded when parsing input JSON object" << logend;
		cur.request->post_in.clear();
		cur.request->http_status = Header::status_400_bad_request;
	}
	else
	if( parse_status == pt::SpaceParser::limit_all_items_exceeded )
	{
		log << log1 << "App: all items limit exceeded when parsing input JSON object" << logend;
		cur.request->post_in.clear();
		cur.request->http_status = Header::status_400_bad_request;
	}
	else
	if( parse_status == pt::SpaceParser::limit_nested_level_exceeded )
	{
		log << log1 << "App: nested objects/tables limit exceeded when parsing input JSON object" << logend;
		cur.request->post_in.clear();
		cur.request->http_status = Header::status_400_bad_request;
	}
}


void App::ReadPostJson(bool copy_raw_post)
{
	ReadInputPostToBuffer();

	if( !cur.request->raw_post.empty() )
	{
		if( config.post_json_max == 0 || cur.request->raw_post.size() <= config.post_json_max )
		{
			ParsePostJson();
		}
		else
		{
			log << log1 << "App: the input stream exceeded the limit of " << config.post_json_max << " bytes (skipping parsing)" << logend;
			cur.request->http_status = Header::status_400_bad_request;
		}

		if( !copy_raw_post )
			cur.request->raw_post.clear();
	}
	else
	{
		log << log3 << "App: input body empty (skipping parsing)" << logend;
	}
}


void App::ReadPostVars()
{
	if( cur.request->method == Request::post || cur.request->method == Request::put ||
		cur.request->method == Request::patch || cur.request->method == Request::delete_ )
	{
		bool copy_raw_post = (cur.request->function && cur.request->function->need_to_copy_raw_post());

		if( pt::is_substr_nc(Header::multipart_form_data, cur.request->env_content_type.c_str()) )
		{
			log << log3 << "App: content type: " << Header::multipart_form_data << logend;
			post_multi_parser.Parse(cur.request->fcgi_request.in, *cur.request, copy_raw_post); // IMPROVEME add checking for return status
		}
		else
		if( pt::is_substr_nc(Winix::Header::application_json, cur.request->env_content_type.c_str()) )
		{
			log << log3 << "App: content type: " << Winix::Header::application_json << ", using json parser" << logend;
			ReadPostJson(copy_raw_post);
		}
		else
		if( pt::is_substr_nc(Header::application_x_www_form_urlencoded, cur.request->env_content_type.c_str()) )
		{
			log << log3 << "App: content type: " << Winix::Header::application_x_www_form_urlencoded << logend;
			post_parser.Parse(cur.request->fcgi_request.in, *cur.request, copy_raw_post); // IMPROVEME add checking for return status
		}
		else
		if( cur.request->env_content_type.empty() )
		{
			log << log2 << "App: Content-Type header is not set or is empty, I try to parse as " << Header::application_x_www_form_urlencoded << logend;
			post_parser.Parse(cur.request->fcgi_request.in, *cur.request, copy_raw_post); // IMPROVEME add checking for return status
		}
		else
		{
			log << log2 << "App: I cannot parse the input body, an unknown Content-Type header: " << cur.request->env_content_type << logend;
		}

		if( config.log_whole_http_post )
		{
			cur.request->post_in.serialize_to_json_stream(post_log_tmp_buffer, true);
			log << log3 << "App: the whole http body after parsing:" << logend << post_log_tmp_buffer << logend;
			post_log_tmp_buffer.clear();
		}
	}
}


void App::CheckIE()
{
	size_t msie = cur.request->env_http_user_agent.find(L"MSIE");
	cur.request->browser_msie = (msie != std::wstring::npos);
}


void App::CheckKonqueror()
{
	size_t kon = cur.request->env_http_user_agent.find(L"Konqueror");
	cur.request->browser_konqueror = (kon != std::wstring::npos);
}


bool App::IsRequestedFrame()
{
	if( !config.request_frame_parameter.empty() )
	{
		return cur.request->ParamValuep(config.request_frame_parameter);
	}

	return false;
}


void App::LogUser(const char * msg, uid_t id)
{
	log << log3 << msg << " ";

	passwd * p = getpwuid(id);
	
	if( p )
		log << p->pw_name;
	else
		log << (int)id;

	log << logend;
}


void App::LogGroup(const char * msg, gid_t id, bool put_logend)
{
	log << log3 << msg << " ";

	group * g = getgrgid(id);

	if( g )
		log << g->gr_name;
	else
		log << (int)id;

	if( put_logend )
		log << logend;
}


void App::LogUsers()
{
uid_t eid, rid;

	eid = geteuid();
	rid = getuid();

	if( eid == rid )
	{
		LogUser("App: effective/real user:", eid);
	}
	else
	{
		LogUser("App: effective user:", eid);
		LogUser("App: real user:",      rid);
	}
}


void App::LogEffectiveGroups(std::vector<gid_t> & tab)
{
	log << log3 << "App: effective groups:";

	for(size_t i=0 ; i<tab.size() ; ++i)
	{
		bool was_printed = false;

		for(size_t x=0 ; x<i ; ++x)
		{
			if( tab[i] == tab[x] )
			{
				was_printed = true;
				break;
			}
		}

		if( !was_printed )
			LogGroup("", tab[i], false);
	}

	log << logend;
}


void App::LogGroups()
{
std::vector<gid_t> tab;
gid_t rgid;
int len;

	rgid = getgid(); 
	len  = getgroups(0, 0);

	if( len <= 0 )
	{
		log << log3 << "App: I can't read how many groups there are" << logend;
		return;
	}

	tab.resize(len);
	len = getgroups(len, &(tab[0]));

	if( len == -1 )
	{
		log << log3 << "App: I can't read groups" << logend;
		return;
	}

	if( len == 1 && rgid == tab[0] )
	{
		LogGroup("App: effective/real group:", rgid);
	}
	else
	{
		tab.resize(len);
		LogEffectiveGroups(tab);
		LogGroup("App: real group:", rgid);
	}
}


void App::LogUserGroups()
{
	LogUsers();
	LogGroups();

	log << log3 << "base_url: " << config.base_url << logend;
}


bool App::DropPrivileges(char * user, char * group)
{
	if( !pt::wide_to_utf8(config.user, (char*)user, WINIX_OS_USERNAME_SIZE) )
		return false;

	if( !pt::wide_to_utf8(config.group, (char*)group, WINIX_OS_USERNAME_SIZE) )
		return false;

return true;
}


bool App::DropPrivileges(const char * user, uid_t uid, gid_t gid, bool additional_groups)
{
	if( additional_groups )
	{
		if( initgroups(user, gid) < 0 )
		{
			log << log1 << "App: I can't init groups for a user: " << user << logend;
			return false;
		}
	}
	else
	{
		if( setgroups(1, &gid) < 0 )
		{
			log << log1 << "App: I can't init groups for user: " << user << logend;
			return false;
		}
	}

	// for setting real and saved gid too
	if( setgid(gid) < 0 )
	{
		log << log1 << "App: I can't change real and saved gid" << logend;
		return false;
	}

	if( setuid(uid) < 0 )
	{	
		log << log1 << "App: I can't drop privileges to user: " << user
			<< " (uid:" << (int)uid << ")" << logend;
		return false;
	}

	if( getuid()==0 || geteuid()==0 )
	{
		log << log1 << "App: sorry, for security reasons you should not run me as the root" << logend;
		return false;
	}

return true;
}


bool App::DropPrivileges()
{
char user_name[WINIX_OS_USERNAME_SIZE];
char group_name[WINIX_OS_USERNAME_SIZE];

	if( getuid()!=0 && geteuid()!=0 )
		return true;

	log << log2 << "App: dropping privileges" << logend;

	if( config.user.empty() )
	{
		log << log1 << "App: in the config file you should specify a user name and a group "
					<< "to which I have to drop privileges" << logend;
		return false;
	}

	if( config.group.empty() )
	{
		log << log1 << "App: you should specify a group name in the config file "
					<< "to which I have to drop privileges" << logend;
		return false;
	}

	if( !DropPrivileges(user_name, group_name) )
		return false;

	passwd * p = getpwnam(user_name);
	group * g  = getgrnam(group_name);

	if( !p )
	{	
		log << log1 << "App: there is no such a user as: \"" << config.user << "\"" << logend;
		return false;
	}

	if( !g )
	{	
		log << log1 << "App: there is no such a group as: \"" << config.group << "\"" << logend;
		return false;
	}

	if( !DropPrivileges(user_name, p->pw_uid, g->gr_gid, config.additional_groups) )
		return false;

return true;
}


bool App::Demonize()
{
	// in linux fork() should be used twice

	int pid = fork();
	
	if( pid == -1 )
	{
		log << log1 << "App: I can't demonize myself (fork problem)" << logend;
		return false;
	}
		
	if( pid != 0 )
	{
		// parent
		exit(0);
	}

	// child
	if( setsid() == -1 )
	{
		log << log1 << "App: I can't demonize myself (setsid problem)" << logend;
		return false;
	}
	
return true;
}


// this method is called from a signal routine
void App::SetStopSignal()
{
	synchro.was_stop_signal = true;
}


bool App::WasStopSignal()
{
	return synchro.was_stop_signal;
}


Synchro * App::GetSynchro()
{
	return &synchro;
}


void App::WaitForThreads()
{
	pthread_join(signal_thread, 0);
	system.thread_manager.StopAll();
}


/*
	we send a one FastCGI record at the end when winix closes (to wake up the main thread)
	this method is called from the special thread

        typedef struct {
            unsigned char version;
            unsigned char type;
            unsigned char requestIdB1;
            unsigned char requestIdB0;
            unsigned char contentLengthB1;
            unsigned char contentLengthB0;
            unsigned char paddingLength;
            unsigned char reserved;
            unsigned char contentData[contentLength];
            unsigned char paddingData[paddingLength];
        } FCGI_Record;
*/
void App::SendEmptyFastCGIPacket()
{
std::string msg;
sockaddr_un to;
int res;

	int s = socket(PF_LOCAL, SOCK_STREAM, 0);

	if( s < 0 )
		return;

	memset(&to, 0, sizeof(to));

	#ifdef __FreeBSD__
	to.sun_len = offsetof(sockaddr_un, sun_path)
				+ socket_to_send_on_exit.size()
				+ 1; // terminating zero
	#endif

	to.sun_family = AF_UNIX;
	snprintf(to.sun_path, sizeof(to.sun_path)/sizeof(char), "%s", socket_to_send_on_exit.c_str());

	// actually we can send one byte only
	msg += FCGI_VERSION_1;
	msg += FCGI_GET_VALUES;
	msg += (char)0; // requestid
	msg += (char)0;
	msg += (char)0; // contentlength
	msg += (char)0;
	msg += (char)0; // padding length
	msg += (char)0;
	msg += (char)0; // reserved

	res = connect(s, (sockaddr*)&to, sizeof(to));

	if( res == 0 )
	{
		send(s, msg.c_str(), msg.size(), 0);
	}

	close(s);
}


void * App::SpecialThreadForSignals(void * app_object)
{
sigset_t set;
int sig;

	App * app = reinterpret_cast<App*>(app_object);

	sigemptyset(&set);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGINT);

	// waiting for SIGTERM or SIGINT
	sigwait(&set, &sig);

	{
		Lock lock(app->GetSynchro());
		app->synchro.was_stop_signal = true;
		FCGX_ShutdownPending();

		pt::wide_to_utf8(app->config.fcgi_socket, app->socket_to_send_on_exit);
	}

	app->SendEmptyFastCGIPacket();

	pthread_exit(0);
	return 0;
}


bool App::AddSystemThreads()
{
	bool ok = true;

	ok = ok && system.thread_manager.Add(&session_manager, L"session_manager");

	return ok;
}


void App::StartThreads()
{
	// make sure system.thread_manager.Init() was called beforehand
	// it is called in Init() -> system.Init()

	// special thread only for signals
	pthread_create(&signal_thread, 0, SpecialThreadForSignals, this);

	// start all threads from thread manager
	system.thread_manager.StartAll();
}


void App::CreateStaticTree()
{
	if( config.upload_dir.empty() )
	{
		log << log1 << "App: config: upload_dir not set, you are not allowed to upload static content" << logend;
		return;
	}

	CreateDirs(L"/", config.upload_dir.c_str(), config.upload_dirs_chmod);

	CreateDirs(config.upload_dir.c_str(), L"simplefs/normal", config.upload_dirs_chmod);
	CreateDirs(config.upload_dir.c_str(), L"simplefs/thumb", config.upload_dirs_chmod);

	CreateDirs(config.upload_dir.c_str(), L"hashfs/normal", config.upload_dirs_chmod);
	CreateDirs(config.upload_dir.c_str(), L"hashfs/thumb", config.upload_dirs_chmod);

	CreateDirs(config.upload_dir.c_str(), L"tmp", config.upload_dirs_chmod);
}


void App::IncrementRequestId()
{
	if( ++request_id == 0 )
	{
		++request_id;
	}
}


} // namespace Winix

