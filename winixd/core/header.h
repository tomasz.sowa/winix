/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_winix_core_header
#define headerfile_winix_core_header

#include "log.h"
#include <textstream/textstream.h>


namespace Winix
{

class Header
{
public:

	/*
	 * headers' names
	 */
	static constexpr const wchar_t * content_type 				= L"Content-Type";
	static constexpr const wchar_t * accept						= L"Accept";
	static constexpr const wchar_t * accept_language				= L"Accept-Language";
	static constexpr const wchar_t * authorization 				= L"Authorization";
	static constexpr const wchar_t * allow 						= L"Allow";
	static constexpr const wchar_t * very 						= L"Very";
	static constexpr const wchar_t * origin						= L"Origin";
	static constexpr const wchar_t * access_control_allow_methods 		= L"Access-Control-Allow-Methods";
	static constexpr const wchar_t * access_control_allow_origin			= L"Access-Control-Allow-Origin";
	static constexpr const wchar_t * access_control_allow_headers		= L"Access-Control-Allow-Headers";
	static constexpr const wchar_t * access_control_allow_credentials	= L"Access-Control-Allow-Credentials";
	static constexpr const wchar_t * access_control_expose_headers		= L"Access-Control-Expose-Headers";
	static constexpr const wchar_t * access_control_max_age				= L"Access-Control-Max-Age";

	static constexpr const wchar_t * hx_location					= L"HX-Location";
	static constexpr const wchar_t * hx_push_url					= L"HX-Push-Url";
	static constexpr const wchar_t * hx_redirect					= L"HX-Redirect";
	static constexpr const wchar_t * hx_refresh					= L"HX-Refresh";
	static constexpr const wchar_t * hx_replace_url				= L"HX-Replace-Url";
	static constexpr const wchar_t * hx_reswap					= L"HX-Reswap";
	static constexpr const wchar_t * hx_retarget					= L"HX-Retarget";
	static constexpr const wchar_t * hx_reselect					= L"HX-Reselect";
	static constexpr const wchar_t * hx_trigger					= L"HX-Trigger";
	static constexpr const wchar_t * hx_trigger_after_settle		= L"HX-Trigger-After-Settle";
	static constexpr const wchar_t * hx_trigger_after_swap		= L"HX-Trigger-After-Swap";

	/*
	 * headers' names lower case
	 */
	static constexpr const wchar_t * content_type_lc 			= L"content-type";
	static constexpr const wchar_t * accept_lc					= L"accept";
	static constexpr const wchar_t * accept_language_lc			= L"accept-language";

	/*
	 * headers' values
	 */
	static constexpr const wchar_t * application_octet_stream 	= L"application/octet-stream";

	static constexpr const wchar_t * text_html					= L"text/html";
	static constexpr const wchar_t * application_json			= L"application/json";
	static constexpr const wchar_t * application_xml				= L"application/xml";
	static constexpr const wchar_t * application_xhtml_xml		= L"application/xhtml+xml";
	static constexpr const wchar_t * text_csv					= L"text/csv";
	static constexpr const wchar_t * text_javascript				= L"text/javascript";
	static constexpr const wchar_t * text_xml					= L"text/xml";

	static constexpr const wchar_t * all_all						= L"*/*";
	static constexpr const wchar_t * text_all					= L"text/*";
	static constexpr const wchar_t * application_all				= L"application/*";

	static constexpr const wchar_t * text_html_utf8				= L"text/html; charset=UTF-8";
	static constexpr const wchar_t * application_json_utf8		= L"application/json; charset=UTF-8";
	static constexpr const wchar_t * application_xml_utf8		= L"application/xml; charset=UTF-8";
	static constexpr const wchar_t * application_xhtml_xml_utf8	= L"application/xhtml+xml; charset=UTF-8";
	static constexpr const wchar_t * text_csv_utf8				= L"text/csv; charset=UTF-8";
	static constexpr const wchar_t * text_javascript_utf8		= L"text/javascript; charset=UTF-8";
	static constexpr const wchar_t * text_xml_utf8				= L"text/xml; charset=UTF-8";

	static constexpr const wchar_t * application_x_www_form_urlencoded 	= L"application/x-www-form-urlencoded";
	static constexpr const wchar_t * multipart_form_data 				= L"multipart/form-data";

	static constexpr const wchar_t * bearer						= L"Bearer";
	static constexpr const wchar_t * winix 						= L"Winix";


	static const int status_200_ok = 200;
	static const int status_201_created = 201;
	static const int status_204_no_content = 204;
	static const int status_300_multiple_choices = 300;
	static const int status_301_moved_permanently = 301;
	static const int status_302_found = 302;
	static const int status_303_see_other = 303;
	static const int status_307_temporary_redirect = 307;
	static const int status_400_bad_request = 400;
	static const int status_403_forbidden = 403;
	static const int status_404_not_found = 404;
	static const int status_414_uri_too_long = 414;
	static const int status_422_unprocessable_entity = 422;
	static const int status_500_internal_server_error = 500;
	static const int status_503_service_unavailable = 503;


	static constexpr const wchar_t * str_status_200		= L"OK";
	static constexpr const wchar_t * str_status_201		= L"Created";
	static constexpr const wchar_t * str_status_204		= L"No Content";
	static constexpr const wchar_t * str_status_300		= L"Multiple Choices";
	static constexpr const wchar_t * str_status_301		= L"Moved Permanently";
	static constexpr const wchar_t * str_status_302		= L"Found";
	static constexpr const wchar_t * str_status_303		= L"See Other";
	static constexpr const wchar_t * str_status_307		= L"Temporary Redirect";
	static constexpr const wchar_t * str_status_400		= L"Bad Request";
	static constexpr const wchar_t * str_status_403		= L"Forbidden";
	static constexpr const wchar_t * str_status_404		= L"Not Found";
	static constexpr const wchar_t * str_status_414		= L"URI Too Long";
	static constexpr const wchar_t * str_status_422		= L"Unprocessable Entity";
	static constexpr const wchar_t * str_status_500		= L"Internal Server Error";
	static constexpr const wchar_t * str_status_503		= L"Service Unavailable";


	static const wchar_t * find_status_string_value(int http_status);
	static void prepare_status_value(int http_status, pt::WTextStream & value, bool clear_value = true);

	static bool is_header_value_char_correct(wchar_t c);
	static bool is_header_value_correct(const wchar_t * str);
	static bool is_header_value_correct(const std::wstring & str);

protected:


	struct StatusIntStringMapHelper
	{
		int status_int;
		const wchar_t * status_str;
	};

	static constexpr StatusIntStringMapHelper status_int_string_map[] = {
		{status_200_ok, str_status_200},
		{status_201_created, str_status_201},
		{status_204_no_content, str_status_204},
		{status_300_multiple_choices, str_status_300},
		{status_301_moved_permanently, str_status_301},
		{status_302_found, str_status_302},
		{status_303_see_other, str_status_303},
		{status_307_temporary_redirect, str_status_307},
		{status_400_bad_request, str_status_400},
		{status_403_forbidden, str_status_403},
		{status_404_not_found, str_status_404},
		{status_414_uri_too_long, str_status_414},
		{status_422_unprocessable_entity, str_status_422},
		{status_500_internal_server_error, str_status_500},
		{status_503_service_unavailable, str_status_503},
	};

};



class HeaderValue
{
public:

	std::wstring value;
	double weight; // q parameter in some headers


	HeaderValue()
	{
		weight = 0.0;
	}


	static void log_values(const std::vector<HeaderValue> & header_values, Log & log)
	{
		char buf[64];
		bool is_first = true;

		for(const HeaderValue & h: header_values)
		{
			snprintf(buf, sizeof(buf)/sizeof(char), "%.3f", h.weight);

			if( !is_first )
				log << ", ";

			log << h.value << ";q=" << buf;
			is_first = false;
		}
	}

};

}

#endif

