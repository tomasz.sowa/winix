/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2023, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include <fcgiapp.h>
#include <string>
#include "postparser.h"
#include "httpsimpleparser.h"
#include "convert/text.h"


namespace Winix
{


PostParser::PostParser()
{
	log_value_size = 0;
	HttpSimpleParser::getchar_returns_utf8_chars = true;
}


void PostParser::LogValueSize(size_t s)
{
	log_value_size = s;
}


void PostParser::Parse(FCGX_Stream * in, Request & request, bool copy_raw_post)
{
	this->in = in;
	this->request = &request;
	this->copy_raw_post = copy_raw_post;
	var_index = 1;
	this->request->raw_post.clear();

	HttpSimpleParser::Parse();
}


int PostParser::GetChar()
{
	int c = FCGX_GetChar(in);

	if( c != -1 && copy_raw_post )
		request->raw_post << static_cast<unsigned char>(c);

	return c;
}


void PostParser::CreateLog(bool param_added, const std::wstring & name, const std::wstring & value)
{
	log << log2 << "Method POST, name: \"" << name << "\"";
	
	if( log_value_size > 0 && !pt::is_substr_nc(L"pass", name.c_str()) )
	{
		log << ", value: ";

		if( value.size() > log_value_size )
			log << "(first " << log_value_size << " characters) ";
		
		log << "\"";
		log.put_string(value, log_value_size);
		log << "\" (size: " << value.size() << ")";
	}

	if( param_added == false )
		log << log2 << " (skipped)";

	log << log2 << logend;
}


void PostParser::Parameter(std::wstring & name, std::wstring & value)
{
	bool added = request->AddPostVar(name, value);
	CreateLog(added, name, value);
}



} // namespace Winix

