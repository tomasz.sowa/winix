/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_users
#define headerfile_winix_core_users

#include <map>
#include "models/user.h"
#include "ugcontainer.h"
#include "lastcontainer.h"
#include "cur.h"
#include "models/winixmodel.h"



namespace Winix
{



class SessionManager;


class Users : public WinixModel
{
	typedef UGContainer<User> Table;

public:

	//void set_dependency(WinixModelDeprecated * winix_model);


	typedef Table::Iterator Iterator;
	typedef Table::SizeType SizeType;

	LastContainer last;

	Users();

//	void SetCur(Cur * pcur);
//	void SetSessionManager(SessionManager * sm);

	void Clear();
	void ReadUsers();
	bool AddUser(const User & user);
	bool IsUser(const std::wstring & name);
	User * GetUser(long user_id);
	User * GetUser(const std::wstring & name);
	long GetUserId(const std::wstring & name);
	Iterator Begin();
	Iterator End();
	SizeType Size();
	bool Remove(long user_id);

	bool   LoginUser(long user_id, bool remember_me);
	size_t LogoutUser(long user_id);
	void   LogoutCurrentUser();

	void IncrementLoggedUsers();
	long HowManyLogged();

protected:

	void fields();



private:

	Table table;
	//Cur * cur;
	//SessionManager * session_manager;
	long how_many_logged;

	bool LoginUserCheckSession();
	User * LoginUserCheckStatus(long user_id);

};




} // namespace Winix



#endif
