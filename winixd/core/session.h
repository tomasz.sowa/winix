/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_session
#define headerfile_winix_core_session

#include <vector>
#include <string>
#include <ctime>
#include <map>
#include "error.h"
#include "models/user.h"
#include "plugindata.h"
#include "rebus.h"
#include "date/date.h"
#include "ipban.h"


namespace Winix
{



class Session
{
public:

	Session();
	Session(const Session & ses);
	Session & operator=(const Session & ses);

	void SetTimesTo(time_t time);
	void Clear(bool clear_plugin_data = true);
	void ClearAfterRequest();


	// 0 - means that this is a temporary session
	long id;
	
	// a session index
	// incremented each time a request to this session is made
	unsigned int id_index;

	// the last time when id_index was incremented
	time_t id_index_changed;

	// true if the session was created now
	bool new_session;

	// when this session was created
	// (the same values)
	time_t   start_time;
	pt::Date start_date;
	
	// when this session was last used
	// (the same values)
	time_t   last_time;
	pt::Date last_date;

	// when there was a last get request
	// (used to calculate spam or invalid login attempts etc.)
	time_t last_time_get;
	

	// 0 - means that nobody is logged	
	User * puser;

	// if false the session will end when the user browser is shutdown
	bool remember_me;


	// if remove_me is true and if allow_to_delete is true then this session will be
	// removed by SessionManager without checking the time expiration
	bool remove_me;

	// if the session can be removed then this value is true (default)
	// if we continue a request from a controller to a job
	// then we set this value to false in order for the SessionManager
	// to not delete this
	bool allow_to_delete;


	PluginData plugin_data;


	// !! IMPROVE ME it is still needed?
	// css cannot be taken directly from the mountpoint?
	// table with css files
	// used by some javascript wysiwyg editors (ckeditor, tinymce)
	std::vector<std::wstring> last_css;

	// pointer to IPBan struct if exists for this IP
	// many sessions can pointer to the same IPBan struct
	// (it can be null)
	IPBan * ip_ban;




	// rebus - set by rebus_question(Info & i) from templates
	Rebus::Item * rebus_item;
	bool rebus_checked;
	int spam_score;
	std::map<long, long> antispan;



	bool is_temporary_session() const;

};



} // namespace Winix



#endif

