/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2009-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */


#include "lastcontainer.h"
#include "log.h"
#include "misc.h"


namespace Winix
{




LastItem::LastItem()
{
	user_id    = 0;
	ip         = 0;
	session_id = 0;
}


bool LastItem::IsLoggedOut()
{
	return end.year > 1970;
}





LastContainer::Iterator LastContainer::Begin()
{
	return last_tab.begin();	
}


LastContainer::Iterator LastContainer::End()
{
	return last_tab.end();
}


LastContainer::Iterator LastContainer::FindNotLoggedOut(long user_id, long session_id)
{
LastTab::iterator i;

	for(i=last_tab.begin() ; i!=last_tab.end() ; ++i)
	{
		if( i->user_id == user_id && i->session_id == session_id && !i->IsLoggedOut() )
			return i;
	}

return last_tab.end();
}



void LastContainer::UserLogin(long user_id, const std::wstring & name, unsigned int ip, long session_id)
{
LastTab::iterator i = FindNotLoggedOut(user_id, session_id);

	if( i != last_tab.end() )
	{
		log << log1 << "LC: such a user and session_id exist, not added as a new one" << logend;
		return;
	}

	if( last_tab.size() >= WINIX_LASTCONTAINER_TABLE_SIZE ) // last_tab has O(n) complexity
		last_tab.erase(last_tab.begin());

	LastItem li;
	
	li.user_id    = user_id;
	li.name       = name;
	li.ip         = ip;
	li.session_id = session_id;
	li.start      = std::time(0);

	last_tab.insert(last_tab.end(), li);
	
	log << log2 << "LC: added user: " << name << " into the last table" << logend;
}




void LastContainer::UserLogout(long user_id, long session_id)
{
LastTab::iterator i = FindNotLoggedOut(user_id, session_id);
	
	if( i != last_tab.end() )
	{
		i->end = std::time(0);
	}
	else
	{
		/*
			!! IMPROVE ME
			users read from the session file (at boot time)
			are not added to LastContainer
		*/

		log << log4 << "LC: there is no such a user to log out: user_id: "
			<< user_id << " ses_id: " << session_id << logend;
	}
}



} // namespace Winix

