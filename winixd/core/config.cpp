/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "config.h"
#include "log.h"
#include "misc.h"
#include "crypt.h"


namespace Winix
{



Config::Config()
{
}


void Config::SetFileLog(FileLog * file_log)
{
	log.set_file_log(file_log);
}


void Config::SetLogBuffer(pt::WTextStream * log_buffer)
{
	log.set_log_buffer(log_buffer);
}


void Config::ShowError(const std::wstring & config_file)
{
	switch( parser.status )
	{
		case pt::SpaceParser::ok:
			break;

		case pt::SpaceParser::cant_open_file:
			log	<< log1 << "Config: I cannot open a config file: " << config_file << logend;
			break;

		case pt::SpaceParser::syntax_error:
			log << log1 << "Config: syntax error in file: " << config_file << ", line: "
				<< parser.get_last_parsed_line() << ":" << parser.get_last_parsed_column() << logend;
			break;

		default:
			// there are no limits sets when parsing the config file
			break;
	}
}






bool Config::ReadConfig(const std::wstring & config_file)
{
	if( config_file.empty() )
	{
		log << log2 << "Config: name of the config file is empty" << logend;
		return false;	
	}

	log << log2 << "Config: reading a config file: " << config_file << logend;
	pt::SpaceParser::Status status = parser.parse_space_file(config_file, space, false);

	if( status == pt::SpaceParser::ok )
	{
		return true;
	}
	else
	{
		ShowError(config_file);
		return false;
	}
}


void Config::AssignValuesFromSpace()
{
	AssignValues();
	SetAdditionalVariables();
}



void Config::AssignValues()
{
	server_mode				= Text(L"server_mode");
	demonize 				= Bool(L"demonize", true);

	user  					= Text(L"user");
	group 					= Text(L"group");
	additional_groups		= Bool(L"additional_groups", true);

	log_file				= Text(L"log_file");
	log_delimiter			= Text(L"log_delimiter", L"---------------------------------------------------------------------------------");
	fcgi_socket				= Text(L"fcgi_socket");
	fcgi_set_socket_chmod	= Bool(L"fcgi_set_socket_chmod", true);
	fcgi_set_socket_owner	= Bool(L"fcgi_set_socket_owner", true);
	fcgi_socket_chmod 		= Int(L"fcgi_socket_chmod", 0770);
	fcgi_socket_user  		= Text(L"fcgi_socket_user");
	fcgi_socket_group 		= Text(L"fcgi_socket_group");
	fcgi_socket_listen		= Int(L"fcgi_socket_listen", 1024);
	fcgi_cannot_create_request_delay = Size(L"fcgi_cannot_create_request_delay", 3);
	log_level				= Int(L"log_level", 1);
	log_request				= Int(L"log_request", 1);
	log_save_each_line 		= Bool(L"log_save_each_line", false);
	log_time_zone_id		= Size(L"log_time_zone_id", 34);
	log_server_answer		= Bool(L"log_server_answer", false);
	log_stdout				= Bool(L"log_stdout", false);
	log_db_query			= Bool(L"log_db_query", false);
	log_plugin_call			= Bool(L"log_plugin_call", false);
	log_post_value_size 	= Size(L"log_post_value_size", 80);
	log_env_variables		= Bool(L"log_env_variables", false);
	log_env_http_variables  = Bool(L"log_env_http_variables", false);
	log_whole_http_post		= Bool(L"log_whole_http_post", false);
	log_http_answer_headers = Bool(L"log_http_answer_headers", false);

	post_file_max			= Size(L"post_file_max", 8388608); // 8 MB
	post_json_max			= Size(L"post_json_max", 8388608); // 8 MB
	post_max_object_items	= Size(L"post_max_object_items", 1024);
	post_max_table_items	= Size(L"post_max_table_items", 4096);
	post_max_all_items		= Size(L"post_max_all_items", 8192);
	post_max_nested_objects = Size(L"post_max_nested_objects", 32);

	upload_dir				= Text(L"upload_dir");
	common_dir				= Text(L"common_dir");
	NoLastSlash(upload_dir);
	NoLastSlash(common_dir);

	upload_group			= Text(L"upload_group");
	upload_dirs_chmod		= Int(L"upload_dirs_chmod", 0750);
	upload_files_chmod		= Int(L"upload_files_chmod", 0640);
	ListText(L"static_dirs", static_dirs);
	dont_use_static_dirs 	= Bool(L"dont_use_static_dirs", false);
	create_thumb			= Bool(L"create_thumb", true);
	thumb_mode				= Int(L"thumb_mode", 2);
	thumb_cx				= Size(L"thumb_cx", 150);
	thumb_cy				= Size(L"thumb_cy", 150);
	thumb_quality			= Int(L"thumb_quality", 92);
	image_resize			= Bool(L"image_resize", true);
	image_mode				= Int(L"image_mode", 6);
	image_cx				= Size(L"image_cx", 1000);
	image_cy				= Size(L"image_cy", 800);
	image_quality			= Int(L"image_quality", 92);
	convert_cmd				= Text(L"convert_cmd", L"/usr/local/bin/convert");

	templates_dir			= Text(L"templates_dir");
	templates_dir_default	= Text(L"templates_dir_default");
	txt_templates_dir		= Text(L"txt_templates_dir");
	txt_templates_dir_default = Text(L"txt_templates_dir_default");
	templates_fun_prefix	= Text(L"templates_fun_prefix", L"fun_");
	templates_fun_postfix	= Text(L"templates_fun_postfix", L".html");
	templates_index			= Text(L"templates_index", L"index.html");
	templates_index_generic = Text(L"templates_index_generic", L"index_generic.html");
	templates_index_raw     = Text(L"templates_index_raw", L"index_raw.html");
	templates_request_status = Text(L"templates_request_status", L"request_status.html");
	template_only_root_use_template_fun = Bool(L"template_only_root_use_template_fun", false);

	use_internal_session_mechanism = Bool(L"use_internal_session_mechanism", true);
	use_internal_loggin_mechanism  = Bool(L"use_internal_loggin_mechanism", true);

	session_cookie_name			= Text(L"session_cookie_name", L"session_id");
	session_cookie_path			= Text(L"session_cookie_path", L"/");
	session_cookie_domain 		= Text(L"session_cookie_domain", L"");
	session_cookie_same_site 	= Int(L"session_cookie_same_site", 0);
	session_cookie_http_only 	= Bool(L"session_cookie_http_only", false);
	session_cookie_secure		= Bool(L"session_cookie_secure", false);

	db_conn_string			= Text(L"db_conn_string");
	db_host					= Text(L"db_host");
	db_hostaddr				= Text(L"db_hostaddr");
	db_port					= Text(L"db_port");
	db_database				= Text(L"db_database");
	db_user					= Text(L"db_user");
	db_pass					= Text(L"db_pass");
	db_startup_connection_max_attempts  = Size(L"db_startup_connection_max_attempts", 0);
	db_startup_connection_attempt_delay = Size(L"db_startup_connection_attempt_delay", 5);
	db_make_migration_if_needed = Bool(L"db_make_migration_if_needed", true);
	db_stop_if_migration_fails  = Bool(L"db_stop_if_migration_fails", true);


	item_url_empty			= Text(L"item_url_empty");

	url_proto				= Text(L"url_proto", L"http://");
	url_ssl_proto			= Text(L"url_ssl_proto", L"https://");

	use_ssl					= Bool(L"use_ssl", false);
	use_ssl_static			= Bool(L"use_ssl_static", false);
	use_ssl_common			= Bool(L"use_ssl_common", false);
	use_ssl_only_for_logged_users = Bool(L"use_ssl_only_for_logged_users", false);
	use_ssl_redirect_code	= Int(L"use_ssl_redirect_code", 303);
	assume_connection_is_through_ssl = Bool(L"assume_connection_is_through_ssl", false);

	base_url				= Text(L"base_url");
	base_url_static			= Text(L"base_url_static");
	base_url_common			= Text(L"base_url_common");

	base_url_redirect		= Bool(L"base_url_redirect", false);
	base_url_redirect_code	= Int(L"base_url_redirect_code", 301);

	NoLastSlash(base_url);
	NoLastSlash(base_url_static);
	NoLastSlash(base_url_common);

	priv_no_user			= Text(L"priv_no_user", L"-- no user --");
	priv_no_group			= Text(L"priv_no_group", L"-- no group --");
	
	session_max_idle		= Int(L"session_max_idle", 10800); // 3h
	session_remember_max_idle = Int(L"session_remember_max_idle", 16070400); // 3 months
	session_file			= Text(L"session_file");
	session_max				= Size(L"session_max", 1000000);
	session_cookie_encode	= Bool(L"session_cookie_encode", false);
	session_keys_file		= Text(L"session_keys_file");
	session_allow_index_difference 	= Size(L"session_allow_index_difference", 8);
	session_index_time_increment 	= Long(L"session_index_time_increment", 30);
	session_key_renew_time			= Size(L"session_key_renew_time", 172800); // 2 days
	broken_encoded_cookie_treshold	= Size(L"broken_encoded_cookie_treshold", 2);
	session_hijacking_treshold		= Size(L"session_hijacking_treshold", 128);
	no_session_cookie_treshold		= Size(L"no_session_cookie_treshold", 128);
	no_session_cookie_ban_mode		= Int(L"no_session_cookie_ban_mode", 0);

	compression				= Bool(L"compression", true);
	compression_page_min_size = Size(L"compression_page_min_size", 512);
	compression_encoding	= Int(L"compression_encoding", 20);

	html_filter				= Bool(L"html_filter", false);
	html_filter_white_char_mode = Int(L"html_filter_white_char_mode", 2);
	html_filter_wrap_line	= Int(L"html_filter_wrap_line", 110);
	html_filter_tabs		= Size(L"html_filter_tabs", 2);
	html_filter_orphans		= Bool(L"html_filter_orphans", true);
	html_filter_orphans_mode_str = Text(L"html_filter_orphans_mode_str", L"nbsp");
	html_filter_nofilter_tag     = Text(L"html_filter_nofilter_tag", L"nofilter");

	locale_dir				= Text(L"locale_dir");
	locale_dir_default		= Text(L"locale_dir_default");
	locale_max_id			= Size(L"locale_max_id", 100);
	locale_default_id		= Size(L"locale_default_id", 0);
	ListText(L"locale_files", locale_files);

	title_separator			= Text(L"title_separator", L" / ");

	send_file_mode			= Int(L"send_file_mode", 0);
	send_file_header 		= Text(L"send_file_header", L"X-SENDFILE");
	send_file_relative_prefix = Text(L"send_file_relative_prefix", L"upload-files-internal");

	editors_html_safe_mode 	= Bool(L"editors_html_safe_mode", true);
	editors_html_safe_mode_skip_root = Bool(L"editors_html_safe_mode_skip_root", true);

	plugins_dir 			= Text(L"plugins_dir", L"/usr/local/winix/plugins");
	NoLastSlash(plugins_dir);
	ListText(L"plugins", plugin_file);

	symlinks_follow_max 	= Size(L"symlinks_follow_max", 20);

	ticket_form_prefix 		= Text(L"ticket_form_prefix", L"ticketparam");

	pass_min_size			= Size(L"pass_min_size", 5);
	pass_type				= Int(L"pass_type", 12);
	pass_hash_use_salt		= Bool(L"pass_hash_use_salt", false);
	pass_hash_salt			= Text(L"pass_hash_salt");
	pass_use_rsa			= Bool(L"pass_use_rsa", false);
	pass_rsa_private_key	= Text(L"pass_rsa_private_key");

	opensll_path 			= Text(L"opensll_path", L"/usr/bin/openssl");

	pattern_cacher_when_delete     = Size(L"pattern_cacher_when_delete", 130);
	pattern_cacher_how_many_delete = Size(L"pattern_cacher_how_many_delete", 30);

	content_type_header 	= Int(L"content_type_header", 0);
	umask 					= Int(L"umask", 0222);

	ezc_max_elements		= Size(L"ezc_max_elements", 50000);
	ezc_max_loop_elements 	= Size(L"ezc_max_loop_elements", 5000);
	ezc_out_streams_size	= Size(L"ezc_out_streams_size", 128);
	request_frame_parameter 		= Text(L"request_frame_parameter", L"frame");
	request_all_frames_parameter 	= Text(L"request_all_frames_parameter", L"all_frames");
	request_main_stream_parameter	= Text(L"request_main_stream_parameter", L"main_stream");

	request_frame_parameter_max_length = Size(L"request_frame_parameter_max_length", 128);
	request_frame_parameter_max_frames = Size(L"request_frame_parameter_max_frames", 16);

	xml_root 				= Text(L"xml_root", L"winix");
	bin_stream_field		= Text(L"bin_stream_field", L"bin_stream");
	main_stream_field 		= Text(L"main_stream_field", L"main_stream");
	ezc_frames_field		= Text(L"ezc_frames_field", L"ezc_frames");
	request_max_accept_fields = Size(L"request_max_accept_fields", 8);
	request_max_accept_language_fields = Size(L"request_max_accept_language_fields", 8);


	account_need_email_verification     = Bool(L"account_need_email_verification", true);
	reset_password_code_expiration_time = Long(L"reset_password_code_expiration_time", 86400);

	time_zone_default_id	= Size(L"time_zone_default_id", 34);
	time_zone_max_id		= Size(L"time_zone_max_id", 130);

	etc_dir					= Text(L"etc_dir", L"");
	time_zones_file 		= Text(L"time_zones_file", L"time_zones.conf");

	use_ban_list			= Bool(L"use_ban_list", true);
	ban_list_soft_max_size	= Size(L"ban_list_soft_max_size", 50000);
	ban_list_max_size		= Size(L"ban_list_max_size", 51000);
	ban_level_1_delay		= Size(L"ban_level_1_delay", 1800);
	ban_level_2_delay		= Size(L"ban_level_2_delay", 86400);
	ban_level_3_delay		= Size(L"ban_level_3_delay", 604800);

	incorrect_login_min_time_between_get_post	= Size(L"incorrect_login_min_time_between_get_post", 2);
	incorrect_login_captcha_treshold			= Size(L"incorrect_login_captcha_treshold", 3);
	incorrect_login_cannot_login_mode			= Int(L"incorrect_login_cannot_login_mode", 0);
	incorrect_login_cannot_login_treshold		= Size(L"incorrect_login_cannot_login_treshold", 20);
	incorrect_login_cannot_login_delay			= Size(L"incorrect_login_cannot_login_delay", 1800);

	pid_file				= Text(L"pid_file", L"");
	allow_ezc_frames_in_executable_items = Bool(L"allow_ezc_frames_in_executable_items", false);

	check_proxy_ip_header 	= Bool(L"check_proxy_ip_header", false);
	proxy_ip_header			= Text(L"proxy_ip_header", L"X_Real_IP");

	use_antispam_mechanism_for_not_logged_users = Bool(L"use_antispam_mechanism_for_not_logged_users", true);
	antispam_list_max_size  = Size(L"antispam_list_max_size", 10);
	add_header_cache_no_store_in_htmx_request = Bool(L"add_header_cache_no_store_in_htmx_request", true);

	request_queue_job_limit = Size(L"request_queue_job_limit", 1024);

	ListText(L"allowed_origins", allowed_origins);
	allow_all_origins = Bool(L"allow_all_origins", true);
	are_cors_preflight_requests_available = Bool(L"are_cors_preflight_requests_available", false);
	ListText(L"access_control_expose_headers", access_control_expose_headers);
	access_control_allow_credentials = Bool(L"access_control_allow_credentials", false);
}





void Config::SetAdditionalVariables()
{
	for(size_t i=0 ; i<static_dirs.size() ; ++i)
		NoLastSlash(static_dirs[i]);

	CheckPasswd();

	if( content_type_header < 0 || content_type_header > 2 )
		content_type_header = 0;

	if( locale_default_id > locale_max_id )
		locale_default_id = locale_max_id;

	if( locale_files.empty() )
		locale_files.push_back(L"en");

	if( !upload_group.empty() )
		upload_group_int = GetGroupId(upload_group);
	else
		upload_group_int = -1;

	if( session_cookie_encode && session_keys_file.empty() )
		session_cookie_encode = false;

	if( session_index_time_increment < 0 )
		session_index_time_increment = 0;
}





void Config::CheckPasswd()
{
	switch(pass_type)
	{
		case WINIX_CRYPT_HASH_NONE:
		case WINIX_CRYPT_HASH_MD4:
		case WINIX_CRYPT_HASH_MD5:
		case WINIX_CRYPT_HASH_SHA1:
		case WINIX_CRYPT_HASH_SHA224:
		case WINIX_CRYPT_HASH_SHA256:
		case WINIX_CRYPT_HASH_SHA384:
		case WINIX_CRYPT_HASH_SHA512:
		break;

		default:
		pass_type = WINIX_CRYPT_HASH_SHA256;
	}
}




std::wstring Config::Text(const wchar_t * name)
{
	return space.to_wstr(name);
}


std::wstring Config::Text(const wchar_t * name, const wchar_t * def)
{
	return space.to_wstr(name, def);
}


std::wstring Config::Text(const std::wstring & name, const wchar_t * def)
{
	return space.to_wstr(name, def);
}


int Config::Int(const wchar_t * name)
{
	return space.to_int(name);
}


int Config::Int(const wchar_t * name, int def)
{
	return space.to_int(name, def);
}


int Config::Int(const std::wstring & name, int def)
{
	return space.to_int(name, def);
}


long Config::Long(const wchar_t * name)
{
	return space.to_long(name);
}

long Config::Long(const wchar_t * name, long def)
{
	return space.to_long(name, def);
}

long Config::Long(const std::wstring & name, long def)
{
	return space.to_long(name, def);
}



size_t Config::Size(const wchar_t * name)
{
	return space.to_ulong(name);
}


size_t Config::Size(const wchar_t * name, size_t def)
{
	return space.to_ulong(name, def);
}


size_t Config::Size(const std::wstring & name, size_t def)
{
	return space.to_ulong(name, def);
}


bool Config::Bool(const wchar_t * name)
{
	return space.to_bool(name);
}


bool Config::Bool(const wchar_t * name, bool def)
{
	return space.to_bool(name, def);
}


bool Config::Bool(const std::wstring & name, bool def)
{
	return space.to_bool(name, def);
}



bool Config::ListText(const wchar_t * name, std::vector<std::wstring> & list)
{
	return space.to_list(name, list);
}


bool Config::ListText(const std::wstring & name, std::vector<std::wstring> & list)
{
	return space.to_list(name, list);
}

bool Config::HasValue(const wchar_t * name, const wchar_t * value)
{
	return space.has_value(name, value);
}


bool Config::HasValue(const std::wstring & name, const std::wstring & value)
{
	return space.has_value(name.c_str(), value.c_str());
}




//void Config::Print(std::wostream & out)
//{
//	space.serialize_to_space_stream(out);
//}





} // namespace Winix




