/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "log.h"
#include <ctime>
#include <string.h>
#include "utf8/utf8.h"



namespace Winix
{



Log::Log()
{
	request       = 0;
	max_requests  = 1;
}


Log::~Log()
{
}



void Log::SetDependency(Log * log)
{
	buffer = log->buffer;
	file_log = log->file_log;
}



void Log::SetMaxRequests(int max_requests)
{
	this->max_requests = max_requests;
}



void Log::PrintDate(const pt::Date & date)
{
	FileLog * winix_file_log = dynamic_cast<FileLog*>(file_log);

	if( winix_file_log )
		(*this) << winix_file_log->get_local_date(date); // synchronization is made in get_local_date
	else
		(*this) << date;
}


	
Log & Log::operator<<(const void * s)
{
	pt::Log::operator<<(s);
    return *this;
}



Log & Log::operator<<(const char * s)
{
	pt::Log::operator<<(s);
    return *this;
}



Log & Log::operator<<(const std::string & s)
{
	pt::Log::operator<<(s);
    return *this;
}



Log & Log::operator<<(const std::string * s)
{
	pt::Log::operator<<(s);
    return *this;
}





Log & Log::operator<<(const wchar_t * s)
{
	pt::Log::operator<<(s);
    return *this;
}


Log & Log::operator<<(const std::wstring & s)
{
	pt::Log::operator<<(s);
    return *this;
}


Log & Log::operator<<(const std::wstring * s)
{
	pt::Log::operator<<(s);
    return *this;
}



Log & Log::operator<<(bool s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(short s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(int s)
{
	pt::Log::operator<<(s);
    return *this;
}


Log & Log::operator<<(long s)
{
	pt::Log::operator<<(s);
    return *this;
}


Log & Log::operator<<(long long s)
{
	pt::Log::operator<<(s);
	return *this;
}



Log & Log::operator<<(char s)
{
	pt::Log::operator<<(s);
    return *this;
}


Log & Log::operator<<(unsigned char s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(wchar_t s)
{
	pt::Log::operator<<(s);
    return *this;
}


Log & Log::operator<<(char32_t s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(unsigned short s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(unsigned int s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(unsigned long s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(unsigned long long s)
{
	pt::Log::operator<<(s);
	return *this;
}



Log & Log::operator<<(float s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(double s)
{
	pt::Log::operator<<(s);
    return *this;
}


Log & Log::operator<<(long double s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(const pt::Stream & s)
{
	pt::Log::operator<<(s);
	return *this;
}


Log & Log::operator<<(const pt::Space & s)
{
	pt::Log::operator<<(s);
    return *this;
}



Log & Log::operator<<(const pt::Date & date)
{
	pt::Log::operator<<(date);
    return *this;
}


Log & Log::operator<<(morm::Model & model)
{
	pt::Log::operator<<(model);
    return *this;
}


Log & Log::operator<<(LogManipulators m)
{
	switch(m)
	{
	case LogManipulators::log1:
		pt::Log::operator<<(pt::Log::log1);
		break;

	case LogManipulators::log2:
		pt::Log::operator<<(pt::Log::log2);
		break;

	case LogManipulators::log3:
		pt::Log::operator<<(pt::Log::log3);
		break;

	case LogManipulators::log4:
		pt::Log::operator<<(pt::Log::log4);
		break;

	case LogManipulators::logend:
		pt::Log::operator<<(pt::Log::logend);
		break;

	case LogManipulators::logsave:
		pt::Log::operator<<(pt::Log::logsave);
		break;

	case LogManipulators::logendrequest:
		if( ++request >= max_requests )
		{
			save_log_and_clear();
			request = 0;
		}
		break;

	default:
		break;
	}

	return *this;
}


Log & Log::operator<<(pt::Log::Manipulators m)
{
	pt::Log::operator<<(m);
	return *this;
}


//Log & Log::SystemErr(int err)
//{
//	(*this) << "errno: " << err;
//
//  // strerror is not thread safe and we now use Log in each thread
//	const char * err_msg = strerror(err);
//
//	if( err_msg )
//		(*this) << " (" << err_msg << ")";
//
//	return *this;
//}



/*
Log & Log::put_string(const std::string & value, size_t max_size)
{
	pt::Log::put_string(value, max_size);
	return *this;
}


Log & Log::put_string(const std::wstring & value, size_t max_size)
{
	pt::Log::put_string(value, max_size);
	return *this;
}


Log & Log::put_binary_blob(const char * blob, size_t blob_len)
{
	pt::Log::put_binary_blob(blob, blob_len);
	return *this;
}

Log & Log::put_binary_blob(const std::string & blob)
{
	pt::Log::put_binary_blob(blob);
	return *this;
}
*/




} // namespace Winix

