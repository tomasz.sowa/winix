/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_system
#define headerfile_winix_core_system

#include <ctime>
#include "job.h"
#include "dirs.h"
#include "mounts.h"
#include "request.h"
#include "config.h"
#include "crypt.h"
#include "users.h"
#include "groups.h"
#include "rebus.h"
#include "loadavg.h"
#include "synchro.h"
#include "image.h"
#include "threadmanager.h"
#include "notify/notify.h"
#include "timezones.h"


namespace Winix
{



class Functions;
class SessionManager;


// file system
class System : public WinixModelDeprecated
{
public:

	// contains current directories tree
	Dirs dirs;

	// mount points
	Mounts mounts;

	// users
	Users users;
	
	// groups
	Groups groups;

	// rebus (captcha)
	Rebus rebus;

	// load averages
	LoadAvg load_avg;

	// notifications (by emails)
	Notify notify;

	// images (resizing, generating thumbnails)
	Image image;

	// the time when the winix starts
	time_t system_start;

	// cryptography and hashes
	Crypt crypt;

	// thread management
	ThreadManager thread_manager;

	// jobs
	Job job;

	// time zones read from etc/time_zones.conf
	// when winix starts
	TimeZones time_zones;

	// list of requests, if a request is to be continued in a job then it is preserved in the req_tab
	// and a new Request is inserted for the next incoming request
	std::list<Request> req_tab;

	using WinixModelDeprecated::get_model_connector;

	void SetCur(Cur * pcur);
	//void SetConfig(Config * pconfig);
	//void SetSynchro(Synchro * psynchro);
	void SetFunctions(Functions * pfunctions);
	void SetSessionManager(SessionManager * sm);

	void set_dependency(WinixModelDeprecated * winix_model);

	bool Init();

	void AddParams(const ParamTab & param_tab, std::wstring & str, bool clear_str = true);

	bool IsSSLRequired(bool try_to_use_ssl);
	bool IsSSLRequired();
	void PutUrlProto(bool can_use_ssl, std::wstring & str, bool clear_str = true);
	void PutUrlProto(bool can_use_ssl, pt::Stream & str, bool clear_stream = true);
	void PutUrlProto(std::wstring & str, bool clear_str = true);
	void PutUrlProto(pt::Stream & str, bool clear_stream = true);

	void RedirectTo(const Item & item, const wchar_t * postfix = 0, bool use_reqtype = true);
	void RedirectTo(long item_id, const wchar_t * postfix = 0, bool use_reqtype = true);
	void RedirectTo(const wchar_t * url, bool use_reqtype = true);
	void RedirectTo(const std::wstring & url, bool use_reqtype = true);
	void RedirectWithFunctionAndParamsTo(const wchar_t * url);
	void RedirectWithFunctionAndParamsTo(const std::wstring & url);
	void RedirectToLastDir(const wchar_t * postfix = 0, bool use_reqtype = true);
	void RedirectToLastItem(const wchar_t * postfix = 0, bool use_reqtype = true); // redirect to an item if exists or to the last directory
	void RedirectToLastFunction(const wchar_t * postfix = 0, bool use_reqtype = true);


	bool CanChangeUser(const Item & item, long new_user_id);
	bool CanChangeGroup(const Item & item, long new_group_id);
	bool CanChangePrivileges(const Item & item, int new_priv);


	bool HasReadAccess(const Item & item);
	bool HasWriteAccess(const Item & item);
	bool HasReadWriteAccess(const Item & item);
	bool HasReadExecAccess(const Item & item);
	bool HasReadExecAccessToPath(long dir_id);
	bool HasReadExecAccessToPath(const std::vector<Item*> & dir_tab);
	bool DirsHaveReadExecPerm();
	void CheckAccessToItems(std::vector<Item> & item_tab);
	void CheckWriteAccessToItems(std::vector<Item> & item_tab);

	/*
		this method checks the sticky bit and write permissions
		it returns true if we can remove/rename an item for the given child_item_user_id user id
	*/
	bool CanRemoveRenameChild(const Item & dir, long child_item_user_id);

	int NewFilePrivileges();
	int NewDirPrivileges();


	bool CanUseHtml(long user_id);
	bool CanUseBBCode(long user_id);
	bool CanUseOther(long user_id);

	bool IsSuperUser(long user_id);
	bool IsMemberOfGroup(long user_id, const wchar_t * group_name);

	// creating item.file_path and item.file_fs (the mountpoint where the item is located)
	bool CreateNewFile(Item & item);
	bool MakeFilePath(const Item & item, std::wstring & path, bool thumb = false, bool create_dir = false, int chmod = 0755, int group = -1);

	bool MakeRelativeFilePath(const Item & item, const std::wstring & path_prefix, std::wstring & path, bool thumb = false);

	bool MakePath(const Item & item, std::wstring & path, bool clear_path = true);

	bool AddFile(Item & item, int notify_code = 0, bool call_plugins = true);
	bool EditFile(Item & item, bool with_url = true, int notify_code = 0, bool call_plugins = true);


	time_t   ToLocal(time_t utc_time);
	pt::Date ToLocal(const pt::Date & utc_date);

	time_t   ToUTC(time_t local_time);
	pt::Date ToUTC(const pt::Date & local_date);


	int FollowLink(const std::vector<Item*> & current_dir_tab, const std::wstring & link_to,
				   std::vector<Item*> & out_dir_tab, Item & out_item);

	int FollowAllLinks(const std::vector<Item*> & current_dir_tab, const std::wstring & link_to,
					   std::vector<Item*> & out_dir_tab, Item & out_item,
					   bool follow_dir_default = false, bool stop_on_link_redirect = false, bool check_access = true);

	// starting from root dir
	int FollowAllLinks(const std::wstring & link_to,
					   std::vector<Item*> & out_dir_tab, Item & out_item,
					   bool follow_dir_default = false, bool stop_on_link_redirect = false, bool check_access = true);

	// using cur->request->dir_tab and cur->request->item
	bool FollowAllLinks(const std::wstring & link_to,
						bool follow_dir_default = false, bool stop_on_link_redirect = false, bool check_access = true);


	bool AddCommonFileToVar(const wchar_t * file_path, const wchar_t * url, const wchar_t * mime_type = nullptr, bool overwrite_existing = true);

	// reloading time zones
	void ReadTimeZones();


	void CreateItemLink(long parent_id, const std::wstring & url, const std::wstring & subdomain,
						std::wstring & link, bool clear_str = true);

	void CreateItemLink(const Item & item, std::wstring & link, bool clear_str = true);

	void DeleteOldRequests(bool leave_one_object = false);

private:
	
	Cur     * cur;
	//Config  * config;
	//Synchro * synchro;
	Functions * functions;
	SessionManager * session_manager;

	Item item_temp;
	std::wstring link_to_temp, name_temp;
	std::wstring file_content, file_name;
	Item file_content_item;

	// for FollowAllLinks
	std::vector<Item*> temp_follow_dir_tab;
	std::vector<Item*> root_follow_dir_tab;
	Item temp_follow_item;

	int  NewPrivileges(int creation_mask);

	bool CreateNewFileSimpleFs(Item & item);
	bool CreateNewFileHashFs(Item & item);

	bool FollowAllLinksDirFound(std::vector<Item*> & out_dir_tab,
								bool follow_dir_default, bool stop_on_link_redirect, bool check_access);

	bool FollowAllLinksFileOrSymlinkFound(std::vector<Item*> & out_dir_tab, Item & out_item,
										  bool stop_on_link_redirect, bool check_access);
};



} // namespace Winix


#endif

