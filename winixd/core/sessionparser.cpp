/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "sessionparser.h"
#include "log.h"
#include "misc.h"


namespace Winix
{



bool SessionParser::Parse(const std::wstring & path, SessionContainer & container)
{
	return Parse(path.c_str(), container);
}


void SessionParser::SetUsers(Users * pusers)
{
	users = pusers;
}


bool SessionParser::Parse(const wchar_t * path, SessionContainer & container)
{
char file_path[WINIX_OS_PATH_SIZE];

	container.Clear();

	if( !pt::wide_to_utf8(path, (char*)file_path, WINIX_OS_PATH_SIZE) )
		return false;

	file.open(file_path, std::ios_base::in | std::ios_base::binary);

	if( !file )
	{
		log << log1 << "SP: cannot open the session file for reading (file: '" << path << "')" << logend;
		return false;
	}

	bool res = Parse(container);

	file.close();

return res;
}



bool SessionParser::Parse(SessionContainer & container)
{
	/*
		file format, each rows:
		session_id(long) user_id(long) remember_me(0|1) time(long) last_time(long)
	*/
	last = file.get();

	log << log2 << "SP: reading sessions from the session file" << logend;

	while( true )
	{
		long id = ReadLong();

		if( file.eof() )
			break;
		
		long user_id = ReadLong();
		MakeSession(id, user_id, container);
		SkipLine();
	}

	log << log2 << "SP: read " << container.Size() << " session(s)" << logend;

return true;
}


void SessionParser::MakeSession(long id, long user_id, SessionContainer & container)
{
	User * puser = users->GetUser(user_id);

	if( !puser )
	{
		log << log1 << "SP: there is no a user with id: " << user_id << " (skipped)" << logend;
		return;
	}

	SessionContainer::Iterator i = container.AddSession(id);

	if( i != container.End() )
	{
		i->Clear();
		i->id			= id;
		i->puser		= puser;
		i->new_session	= true;
		i->remember_me	= ReadLong();
		i->start_time	= ReadLong();
		i->last_time	= ReadLong();
		i->id_index		= ReadLong();
		i->start_date	= i->start_time;
		i->last_date	= i->last_time;
		// !! IMPROVE ME we do not save last_time_get

		users->IncrementLoggedUsers();
		log << log2 << "SP: read session id: " << id << " for user: " << puser->login << logend;
	}
	else
	{
		log << log1 << "SP: session with id: " << id << " already exists (skipping)" << logend;
	}
}


bool SessionParser::IsWhite(int c)
{
	if( c==' ' || c=='\t' || c==13 )
		return true;

return false;
}

bool SessionParser::IsDigit(int c)
{
	if( c>='0' && c<='9' )
		return true;

return false;
}

void SessionParser::SkipWhite()
{
	while( IsWhite(last) )
		last = file.get();
}


void SessionParser::SkipLine()
{
	while( last != 10 )
		last = file.get();

	last = file.get(); // first character from the new line
}



long SessionParser::ReadLong()
{
long res = 0;
bool is_sign = false;

	SkipWhite();

	if( last == '-' )
	{
		is_sign = true;
		last = file.get();
	}

	while( IsDigit(last) )
	{
		res  = res*10 + (last-'0');
		last = file.get();
	}

	if( is_sign )
		res = -res;

return res;
}



} // namespace Winix

