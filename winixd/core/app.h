/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_core_app
#define headerfile_winix_core_app

#include <iostream>
#include <ctime>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <fcgiapp.h>

#include "sessionmanager.h"
#include "functions/functions.h"
#include "templates/templates.h"
#include "compress.h"
#include "postparser.h"
#include "cookieparser.h"
#include "postmultiparser.h"
#include "utils/acceptencodingparser.h"
#include "winixrequest.h"
#include "log/log.h"
#include "filelog.h"
#include "models/winixmodelconnector.h"


namespace Winix
{


class App
{
public:

	App();

	bool InitFCGI();
	bool DropPrivileges();
	void InitLoggers();
	Log & GetMainLog();
	bool InitializePlugins();
	bool Init();
	bool InitializeRequestForFastCGI(Request & request);
	void SetRequestDependency(Request & request);
	void InitializeNewRequest(Request & request);
	void PutRequestToJob();
	void Start();
	void Close();
	void LogUserGroups();
	bool Demonize();
	void SetStopSignal();
	bool WasStopSignal();
	Synchro * GetSynchro();
	void StartThreads();
	void WaitForThreads();

	// configuration read from a config file
	Config config;

	// pointers to the current request and a session
	Cur cur;

	// users sessions
	SessionManager session_manager;

	// an unique id for each request
	size_t request_id;

	// ...
	System system;

	// functions (ls, cat, emacs, ...)
	Functions functions;

	// false at the beginning
	// !! IMPROVE ME moze to do loggera dac?
	bool stdout_is_closed;



	Templates templates;




private:

	PostParser post_parser;
	PostMultiParser post_multi_parser;
	pt::SpaceParser space_parser;

	CookieParser cookie_parser;
	AcceptBaseParser accept_base_parser;
	AcceptEncodingParser accept_encoding_parser;
	Compress compress;
	int fcgi_socket;
	Synchro synchro;
	pthread_t signal_thread;
	std::string socket_to_send_on_exit;
	pt::WTextStream post_log_tmp_buffer;

	std::wstring http_header_name;
	std::wstring http_header_value;
	pt::WTextStream empty_response;

	WinixModelConnector model_connector; // main thread model connector, each thread has its own connector
	morm::JSONConnector json_connector;
	morm::XMLConnector xml_connector;
	morm::PostgreSQLConnector postgresql_connector;

	// objects for main thread
	WinixBase winix_base;
	WinixModelDeprecated winix_model;
	WinixSystem winix_system;
	WinixRequest winix_request;
	// ///////////////////////

	Plugin plugin;

	//////////////////////////

	// log_buffer for the main thread
	pt::WTextStream log_buffer;

	// logger only for the main thread
	Log log;

	// file logger, one object for all Log objects
	FileLog file_log;

	bool AddSystemThreads();

	bool InitFCGIChmodChownSocket(const char * sock);
	bool DropPrivileges(char * user, char * group);
	bool DropPrivileges(const char * user, uid_t uid, gid_t gid, bool additional_groups);
	bool CheckAccessFromPlugins();
	void MakeRenameMeToABetterName();
	bool AddRootDir();
	void ProcessRequestThrow();
	void ProcessRequest();
	void BaseUrlRedirect(int code, bool add_subdomain);
	bool BaseUrlRedirect();
	void CheckIfNeedSSLredirect();
	void SetLocale();
	void CheckPostRedirect();
	void AddDefaultModels();
	void SaveSessionsIfNeeded(); // !! IMPROVE ME wywalic do menagera sesji??
	void LogAccess();
	void ReadRequest();
	void LogEnvironmentVariables();
	void LogEnvironmentHTTPVariables();
	void ParseAcceptHeader(const wchar_t * header_name, const std::wstring & env, std::vector<HeaderValue> & container, size_t max_len);
	void ParseAcceptHeader();
	void ParseAcceptLanguageHeader();
	void SetEnv(const char * name, std::wstring & env);
	void ReadEnvVariables();
	void ReadEnvHTTPVariables();
	bool SaveEnvHTTPVariable(const char * env);
	void ReadInputPostToBuffer();
	void ParsePostJson();
	void ReadPostJson(bool copy_raw_post);
	void ReadPostVars();
	void CheckIE();
	void CheckKonqueror();
	void CheckHtmx();
	bool IsRequestedFrame();
	void SendHeaders();
	void SendCookies();
	void ClearAfterRequest();
	void IncrementRequestId();

	void LogUser(const char * msg, uid_t id);
	void LogGroup(const char * msg, gid_t id, bool put_logend = true);
	void LogUsers();
	void LogEffectiveGroups(std::vector<gid_t> & tab);
	void LogGroups();

	static void * SpecialThreadForSignals(void*);
	void SendEmptyFastCGIPacket();

	void CreateStaticTree();

	bool DoDatabaseMigration();
	bool TryToMakeDatabaseMigration();


	// !! IMPROVE ME
	// !! move to the session manager?
	time_t last_sessions_save;
};


} // namespace Winix


#endif


