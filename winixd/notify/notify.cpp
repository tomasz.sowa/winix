/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "notify.h"
#include "core/misc.h"
#include "templates/templates.h"
#include "core/request.h"
#include "core/config.h"
#include "core/users.h"
#include "core/dirs.h"
#include "core/plugin.h"

namespace Winix
{


Notify::Notify()
{
}


void Notify::SetCur(Cur * pcur)
{
	cur = pcur;
}


//void Notify::SetConfig(Config * pconfig)
//{
//	config = pconfig;
//}


void Notify::SetUsers(Users * pusers)
{
	users = pusers;
}

void Notify::SetDirs(Dirs * pdirs)
{
	dirs = pdirs;
}

void Notify::SetThreadManager(ThreadManager * pmanager)
{
	thread_manager = pmanager;
}



//void Notify::SetFileLog(FileLog * file_log)
//{
//	this->file_log = file_log;
//}



bool Notify::Init()
{
	//notify_thread.SetConfig(config);
	notify_thread.SetUsers(users);
	notify_thread.SetNotifyPool(&notify_pool);
	notify_thread.SetPatterns(&patterns);
	//notify_thread.SetFileLog(file_log);

	if( !thread_manager->Add(&notify_thread, L"notifications") )
		return false;

	patterns.SetDirectories(config->txt_templates_dir, config->txt_templates_dir_default);
	patterns.SetLocale(&TemplatesFunctions::locale);
	patterns.SetLocaleFilter(&TemplatesFunctions::locale_filter);

	notify_template_cms              = AddTemplate(L"notify_email_cms.txt");
	notify_template_activate_account = AddTemplate(L"notify_confirm_account.txt");
	notify_template_reset_password   = AddTemplate(L"notify_reset_password.txt");

	plugin->Call(WINIX_NOTIFY_ADD_TEMPLATE);

	return true;
}



void Notify::ReadTemplates()
{
	patterns.Reload();
	notify_thread.PatternsChanged();
}




void Notify::ItemChanged(int notify_code, const Item & item)
{
	if( notify_code == 0 )
		return;

	msg.Clear();
	msg.code = notify_code;
	msg.template_index = notify_template_cms;
	CreateItemLink(item, msg.item_link, msg.dir_link);

	ItemChanged(msg);
}



// raw form
void Notify::ItemChanged(const NotifyMsg & msg)
{
	notify_pool.Add(msg);
	notify_thread.WakeUpThread(); // we are in the first locked thread
}


void Notify::ActivateAccount(const std::wstring & name, const std::wstring & email, long code)
{
	msg.Clear();
	msg.code  = WINIX_NOTIFY_CODE_CONFIRM_ACCOUNT;
	msg.name  = name;
	msg.email = email;
	msg.lang  = config->locale_default_id;
	msg.activate_code  = code;
	msg.template_index = notify_template_activate_account;
	CreateActivateLink(name, code, msg.item_link);

	notify_pool.Add(msg);
	notify_thread.WakeUpThread(); // we are in the first locked thread
}


void Notify::ResetPassword(const std::wstring & name, const std::wstring & email, long code)
{
	msg.Clear();
	msg.code  = WINIX_NOTIFY_CODE_RESET_PASSWORD;
	msg.name  = name;
	msg.email = email;
	msg.lang  = config->locale_default_id;	// !! IMPROVE ME a better language can be chose (the same as the user has)
	msg.activate_code  = code;
	msg.template_index = notify_template_reset_password;
	CreateResetPasswordLink(name, code, msg.item_link);

	notify_pool.Add(msg);
	notify_thread.WakeUpThread(); // we are in the first locked thread
}


size_t Notify::AddTemplate(const std::wstring & file_name)
{
	return patterns.Add(file_name, false);
}





void Notify::CreateItemLink(const Item & item, std::wstring & item_link, std::wstring & dir_link)
{
	tmp_path.clear();

	if( item.type == Item::dir )
	{
		dirs->MakePath(item.id, tmp_path);
		item_link  = config->url_proto;
		item_link += config->base_url;	// !! IMPROVE ME what about subdomains?
		item_link += tmp_path;
		dir_link   = item_link;
	}
	else
	{
		dirs->MakePath(item.parent_id, tmp_path);
		item_link  = config->url_proto;
		item_link += config->base_url; // !! IMPROVE ME what about subdomains?
		item_link += tmp_path;
		dir_link   = item_link;
		item_link += item.url;
	}
}



void Notify::CreateActivateLink(const std::wstring & name, long code, std::wstring & link)
{
wchar_t buff[50];

	link  = config->url_proto;
	link += config->base_url;// !! IMPROVE ME what about subdomains?
	link += L"/account/activate/login:";
	UrlEncode(name, link, false);
	link += L"/code:";
	Toa(code, buff, sizeof(buff)/sizeof(wchar_t));
	link += buff;
}

void Notify::CreateResetPasswordLink(const std::wstring & name, long code, std::wstring & link)
{
wchar_t buff[50];

	link  = config->url_proto;
	link += config->base_url;// !! IMPROVE ME what about subdomains?
	link += L"/passwd/resetpassword/login:";
	UrlEncode(name, link, false);
	link += L"/code:";
	Toa(code, buff, sizeof(buff)/sizeof(wchar_t));
	link += buff;
}

} // namespace Winix

