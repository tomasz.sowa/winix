/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#include "templatesnotify.h"
#include "core/misc.h"

namespace Winix
{


namespace TemplatesNotifyFunctions
{
Ezc::Functions<NotifyStream> ezc_functions;




// you can use this pointer in template functions (will be always valid)
NotifyUserMsg notify_user_msg;
NotifyMsg notify_msg;

static pt::WTextStream qencode_tmp;


void fil_qencode(Info & i)
{
	// QEncode can be used in other threads
	q_encode(i.in, qencode_tmp);
	i.out << qencode_tmp;
}



void notify_add(Info & i)
{
	i.res = (notify_msg.code & WINIX_NOTIFY_CODE_ADD) != 0;
}



void notify_edit(Info & i)
{
	i.res = (notify_msg.code & WINIX_NOTIFY_CODE_EDIT) != 0;
}



void notify_delete(Info & i)
{
	i.res = (notify_msg.code & WINIX_NOTIFY_CODE_DELETE) != 0;
}


void notify_reply(Info & i)
{
	i.res = (notify_msg.code & WINIX_NOTIFY_CODE_REPLY) != 0;
}


void notify_to_email(Info & i)
{
	i.out << notify_user_msg.email;	
}


void notify_to_name(Info & i)
{
	i.out << notify_user_msg.name;	
}


void notify_item_link(Info & i)
{
	i.out << notify_msg.item_link;
}


void notify_dir_link(Info & i)
{
	i.out << notify_msg.dir_link;
}




void CreateFunctions()
{
	ezc_functions.Clear();

	ezc_functions.Insert("fil_qencode",        fil_qencode);

	/*
	 * IMPROVE ME
	 * now we are able to read from a Space struct and these functions can be removed
	 * and we can use directly a Space struct
	 * (copy those functions here from normal templates)
	 */
	ezc_functions.Insert("notify_add",		   notify_add);
	ezc_functions.Insert("notify_edit",        notify_edit);
	ezc_functions.Insert("notify_delete",      notify_delete);
	ezc_functions.Insert("notify_reply",       notify_reply);

	ezc_functions.Insert("notify_to_email",    notify_to_email);
	ezc_functions.Insert("notify_to_name",     notify_to_name);
	ezc_functions.Insert("notify_item_link",   notify_item_link);
	ezc_functions.Insert("notify_dir_link",    notify_dir_link);
}





} // namespace TemplatesNotifyFunctions

} // namespace Winix

