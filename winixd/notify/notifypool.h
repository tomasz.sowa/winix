/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2014, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_notify_notifypool
#define headerfile_winix_notify_notifypool

#include <list>
#include <string>
#include "templates/locale.h"

namespace Winix
{


#define WINIX_NOTIFY_CODE_ADD				1
#define WINIX_NOTIFY_CODE_EDIT				2
#define WINIX_NOTIFY_CODE_DELETE			4
#define WINIX_NOTIFY_CODE_REPLY				8
#define WINIX_NOTIFY_CODE_CONFIRM_ACCOUNT	16
#define WINIX_NOTIFY_CODE_RESET_PASSWORD	32



// !! IMPROVE ME
// may now we can use PT::Space instead of NotifyMsg?
struct NotifyMsg
{
	int code;
	std::wstring item_link;		// link to a file or a dir (can be the same as dir_link if the item is a directory)
	std::wstring dir_link;		// link to a dir
	size_t template_index;

	// used in account activations
	// we send one email directly to one user
	std::wstring email;
	std::wstring name;
	size_t lang;
	long activate_code;


	NotifyMsg()
	{
		Clear();
	}

	void Clear()
	{
		code = -1;
		item_link.clear();
		dir_link.clear();
		template_index = 0;
		email.clear();
		name.clear();
		lang = 0;
		activate_code = 0;
	}
};


// used by the second thread (and its templates)
struct NotifyUserMsg
{
	std::wstring name;
	std::wstring email;
	size_t lang; // locale id (not an index)
};




class NotifyPool
{
public:

	bool Empty() const;
	size_t Size() const;

	NotifyMsg & GetFirst();
	void DeleteFirst();
	void Add(const NotifyMsg & msg);

private:

	std::list<NotifyMsg> notify_pool;
};




} // namespace Winix

#endif
