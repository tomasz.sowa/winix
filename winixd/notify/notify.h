/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2010-2021, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_notify_notify
#define headerfile_winix_notify_notify

#include <vector>
#include <string>

#include "core/winixmodeldeprecated.h"
#include "notifypool.h"
#include "templates/patterns.h"
#include "notifythread.h"



namespace Winix
{


class Request;
class Config;
class Users;
class Dirs;
class ThreadManager;
class FileLog;


class Notify : public WinixModelDeprecated
{
public:

	Notify();

	void SetCur(Cur * pcur);
	//void SetConfig(Config * pconfig);
	void SetUsers(Users * pusers);
	void SetDirs(Dirs * pdirs);
	void SetThreadManager(ThreadManager * pmanager);
	//void SetFileLog(FileLog * file_log);

	bool Init();

	size_t AddTemplate(const std::wstring & file_name);
	void ReadTemplates();

	void ItemChanged(int notify_code, const Item & item);
	void ItemChanged(const NotifyMsg & msg);
	void ActivateAccount(const std::wstring & name, const std::wstring & email, long code);
	void ResetPassword(const std::wstring & name, const std::wstring & email, long code);

private:

	Cur     * cur;
	//Config  * config;
	Dirs    * dirs;
	Users   * users;
	//FileLog * file_log;
	ThreadManager * thread_manager;

	NotifyThread notify_thread;
	NotifyMsg    msg;
	NotifyPool   notify_pool;
	std::wstring tmp_path;

	size_t notify_template_cms;
	size_t notify_template_activate_account;
	size_t notify_template_reset_password;

	Patterns patterns;

	void CreateItemLink(const Item & item, std::wstring & link, std::wstring & dir_link);
	void CreateActivateLink(const std::wstring & name, long code, std::wstring & link);
	void CreateResetPasswordLink(const std::wstring & name, long code, std::wstring & link);
};




} // namespace Winix

#endif
