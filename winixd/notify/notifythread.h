/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2008-2024, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef headerfile_winix_notify_notifythread
#define headerfile_winix_notify_notifythread

#include "core/basethread.h"
#include "templatesnotify.h"
#include "ezc.h"
#include "core/config.h"
#include "core/users.h"
#include "templates/patterns.h"
#include "core/filelog.h"


namespace Winix
{


// we have set following pointers: request, config, system (from BaseThread)

class NotifyThread : public BaseThread
{
public:

	NotifyThread();

	//void SetConfig(Config * pconfig);
	void SetUsers(Users * pusers);
	void SetNotifyPool(NotifyPool * pool);
	void SetPatterns(Patterns * pat);
	//void SetFileLog(FileLog * file_log);
	void PatternsChanged();


private:

	//Log log;
	//Config * config;
	Users * users;
	NotifyPool * notify_pool;
	Patterns * pat_global;

	NotifyUserMsg msg;
	typedef std::list<NotifyUserMsg> NotifyUser;
	NotifyUser notify_user;
	std::string sendmail_command;
	bool patterns_changed;
	Patterns patterns;
	TemplatesNotifyFunctions::NotifyStream notify_stream;
	Ezc::Generator<TemplatesNotifyFunctions::NotifyStream, true> generator;
	std::wstring msg_str;

	virtual bool Init();
	virtual bool SignalReceived();
	bool AddNextNotify();
	virtual void Do();
	void SendMail();
	void SendMail(const std::wstring & email, const std::wstring & message);
	void SendMail(FILE * sendmail, const std::wstring & message);

};



} // namespace Winix

#endif
