/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2023, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <iostream>
#include "convert/strtoint.h"
#include "mainoptions/mainoptionsparser.h"
#include "pgmodeler.h"
#include "misc.h"
#include "ezc.h"


namespace Winix
{


// make a static method in MainOptionsParser?
std::wstring get_param(const pt::Space::TableType * input_mode)
{
	if( input_mode && input_mode->size() > 0 )
	{
		pt::Space * first_par = (*input_mode)[0];

		if( first_par->is_table() && first_par->table_size() > 0 )
		{
			return first_par->value.value_table.front()->to_wstr();
		}
	}

	return std::wstring();
}


bool use_pgmodeler(const std::wstring & input_file, const pt::Space & input_options, pt::Space & schema, bool sort_tables)
{
	PGModeler pgmodeler;
	return pgmodeler.parse(input_file, schema, sort_tables);
}



typedef pt::WTextStream MyStream;




// move me to a better place
void esc_tex(wchar_t c, pt::WTextStream & str)
{
	if( c == '$' )
		str << L"\\$";
	else
	if( c == '#' )
		str << L"\\#";
	else
	if( c == '%' )
		str << L"\\%";
	else
	if( c == '&' )
		str << L"\\&";
	else
	if( c == '\\' )
		str << L"$\\backslash$";
	else
	if( c == '{' )
		str << L"$\\{$";
	else
	if( c == '}' )
		str << L"$\\}$";
	else
	if( c == '^' )
		str << L""; // !! IMPROVE ME add \char with specific code
	else
	if( c == '_' )
		str << L"\\_";
	else
	if( c == '~' )
		str << L""; // !! IMPROVE ME add \char with specific code
	else
	if( c == '-' )
		str << L"{-}";
	else
	if( c != 0 )
		str << c;

}


void fil_tex(Ezc::FunInfo<MyStream> & env)
{
	auto i = env.in.begin();

	for( ; i != env.in.end() ; ++i)
	{
		esc_tex(*i, env.out);
	}
}


void fil_max_size(Ezc::FunInfo<MyStream> & env)
{
	auto i = env.in.begin();
	size_t max_len = pt::to_ul(env.par);
	size_t len = 0;

	for( ; i != env.in.end() ; ++i)
	{
		if( len >= max_len )
		{
			env.out << ' ';
			len = 0;
		}

		if( pt::is_white(*i) )
		{
			len = 0;
		}
		else
		{
			len += 1;
		}

		env.out << *i;
	}
}


void cmp(Ezc::FunInfo<MyStream> & env)
{
	if( env.params.size() >= 2 )
	{
		env.res = true;

		for(size_t a=0 ; a < env.params.size() - 1 ; ++a)
		{
			if( env.params[a].str != env.params[a+1].str )
			{
				env.res = false;
				break;
			}
		}
	}
}


bool generate(const pt::Space & input_options, pt::Space & schema)
{
	pt::WTextStream log_buffer;
	pt::FileLog file_log;
	pt::Log log;

	file_log.init(L"log.txt", true, 4, true);
	log.set_file_log(&file_log);
	log.set_log_buffer(&log_buffer);

	Ezc::Pattern pattern;
	Ezc::PatternParser parser;
	Ezc::Functions<MyStream> functions;
	Ezc::Blocks blocks;
	Ezc::Models models;

	std::wstring dir = get_param(input_options.get_table(L"templates-dir"));
	std::wstring template_file = get_param(input_options.get_table(L"template"));

	if( dir.empty() )
	{
		std::cerr << "you have to set the templates directory with --templates-dir parameter" << std::endl;
		return false;
	}

	if( template_file.empty() )
	{
		std::cerr << "you have to set the template file name with --template_file parameter" << std::endl;
		return false;
	}

	functions.Insert("fil_tex", fil_tex);
	functions.Insert("cmp", cmp);
	functions.Insert("fil_max_size", fil_max_size);

	parser.Directory(dir);
	parser.SetBlocks(blocks);
	parser.SetLogger(&log);
	parser.ParseFile(template_file, pattern);

	models.Add(L"schema", schema);

	Ezc::Generator<MyStream, true, false> generator;

	generator.SetPattern(pattern);
	generator.SetFunctions(functions);
	generator.SetModels(models);

	MyStream str;
	generator.SetLogger(log);
	generator.Generate(str);

	std::string tmp;
	str.to_str(tmp);
	std::cout << tmp << std::endl;

	log << pt::Log::logsave;
	return true;
}





} // namespace Winix




int main(int argc, const char ** argv)
{
	using namespace Winix;

	pt::MainOptionsParser parser;
	pt::Space input_options, args, schema;

	args.add(L"input-mode", 1);
	args.add(L"input-file", 1);
	args.add(L"templates-dir", 1);
	args.add(L"template", 1);

	pt::MainOptionsParser::Status status = parser.parse(argc, argv, input_options, args);

	std::wstring input_mode = get_param(input_options.get_table(L"input-mode"));
	std::wstring input_file = get_param(input_options.get_table(L"input-file"));

	if( input_mode.empty() )
	{
		std::cout << "you have to use --input-mode parameter" << std::endl;
		return 1;
	}

	print("input-mode", input_mode);
	print("input-file", input_file);

	bool sort_tables = input_options.has_key(L"sort-tables");

	if( input_mode == L"pgmodeler" )
	{
		if( !use_pgmodeler(input_file, input_options, schema, sort_tables) )
			return 3;
	}
	else
	{
		std::cerr << "unknown --input-mode parameter, available are: pgmodeler" << std::endl;
		return 2;
	}

	generate(input_options, schema);


	// for debug purposes
//	std::string tmp;
//	tmp = input_options.serialize_to_json_str(true);
//	std::cout << tmp << std::endl;

}



