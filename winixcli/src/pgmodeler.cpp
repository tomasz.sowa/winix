/*
 * This file is a part of Winix
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2023, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "pgmodeler.h"
#include "html/htmlparser.h"
#include "misc.h"



namespace Winix
{


PGModeler::PGModeler()
{
}

PGModeler::~PGModeler()
{
}




bool PGModeler::parse(const std::wstring & input_file, pt::Space & schema, bool sort_tables)
{
	schema.clear();
	pt::HTMLParser parser;

	pt::HTMLParser::Status status = parser.parse_xml_file(input_file, schema, true, true);

	if( status == pt::HTMLParser::Status::cant_open_file )
	{
		print("cannot open the input file", input_file);
		return false;
	}
	else
	if( status == pt::HTMLParser::Status::syntax_error )
	{
		print("syntax error", input_file, parser.get_last_parsed_line());
		schema.clear();
		return false;
	}
	else
	{
		if( sort_tables )
			make_sort_tables(schema);
	}

	return true;
}



std::pair<const std::wstring*, const std::wstring*> PGModeler::get_schema_table_names(const pt::Space & table)
{
	const pt::Space * schema = table.get_space(L"schema");
	const std::wstring * schema_name = nullptr;
	const std::wstring * table_name = nullptr;

	if( schema )
	{
		const pt::Space * attr = schema->get_space(L"attr");

		if( attr )
		{
			schema_name = attr->get_wstr(L"name");
		}
	}

	const pt::Space * attr = table.get_space(L"attr");

	if( attr )
	{
		table_name = attr->get_wstr(L"name");
	}

	return std::make_pair(schema_name, table_name);
}


void PGModeler::make_sort_tables(pt::Space & schema)
{
	pt::Space * dbmodel = schema.get_space(L"dbmodel");

	if( dbmodel )
	{
		pt::Space::TableType * table = dbmodel->get_table(L"table");

		if( table )
		{
			std::sort(table->begin(), table->end(), [](const pt::Space * t1, const pt::Space * t2) -> bool {
				auto t1_names = get_schema_table_names(*t1);
				auto t2_names = get_schema_table_names(*t2);

				if( t1_names.first && t2_names.second && t2_names.first && t2_names.second )
				{
					if( *t1_names.first != *t2_names.first )
					{
						return *t1_names.first < *t2_names.first;
					}
					else
					{
						return *t1_names.second < *t2_names.second;
					}
				}
				else
				{
					return false;
				}
			});
		}
	}
}



}

