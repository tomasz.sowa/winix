

export CXX
export CXXFLAGS
export LDFLAGS


all: winixd winixcli


winixd: FORCE
	$(MAKE) -C winixd


winixcli: FORCE
	$(MAKE) -C winixcli


clean: FORCE
	$(MAKE) -C winixd clean
	$(MAKE) -C winixcli clean


cleanall: FORCE
	$(MAKE) -C winixd cleanall
	$(MAKE) -C winixcli clean


depend: FORCE
	$(MAKE) -C winixd depend
	$(MAKE) -C winixcli depend


install: FORCE
	$(MAKE) -C winixd install
	#$(MAKE) -C winixcli depend


clangd: FORCE
	@.templates/install_clangd.sh


clangdall: clangd
	@$(MAKE) -C ../pikotools clangd
	@$(MAKE) -C ../morm clangd
	@$(MAKE) -C ../ezc clangd
	@$(MAKE) -C ../tito clangd


qtcreator: clangd
	@.templates/install_qtcreator.sh


qtcreatorall: qtcreator
	@$(MAKE) -C ../pikotools qtcreator
	@$(MAKE) -C ../morm qtcreator
	@$(MAKE) -C ../ezc qtcreator
	@$(MAKE) -C ../tito qtcreator


FORCE:

